package com.example.repository;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.Grouping;
import com.example.serializable.GroupingSerializable;
public interface GroupingRepository  extends JpaRepository<Grouping, GroupingSerializable>{

	@Query(nativeQuery = true, value = "select * from list_subgroup(:company)")
	public List<Object[]> listSubgroup(@Param("company") int company);
	
}
