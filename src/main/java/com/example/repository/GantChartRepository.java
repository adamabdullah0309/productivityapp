package com.example.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.entity.ListDocument;

import net.sf.json.JSONArray;

public interface GantChartRepository extends JpaRepository<ListDocument, Integer> {
	@Query(nativeQuery = true, value = "select * from list_gant_chart(:company, :user_id)")
	public List<Object[]> listgantChart(@Param("company") int int1, @Param("user_id") UUID user_uuid);


//	@Query(nativeQuery = true, 
//			value = "select * from contoh_gantchart(:company, :projectDetailId, :taskId, :contactIdArray, :mProjectId, :user_uuid ) ")
//	public List<Object[]> detailGantChart(
//			@Param("company") int company_id, 
//			@Param("mProjectId") int mProjectId,
//			@Param("projectDetailId") JSONArray projectDetailId, 
//			@Param("taskId")JSONArray taskId,
//			@Param("contactIdArray") JSONArray contactIdArray, 
//			@Param("user_uuid") UUID user_uuid);
}
