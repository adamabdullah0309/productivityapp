package com.example.repository;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.ProjectDetail;
import com.example.serializable.ProjectDetailSerializable;

import net.sf.json.JSONObject;
public interface ProjectDetailRepository  extends JpaRepository<ProjectDetail, ProjectDetailSerializable>{

	@Query(nativeQuery = true, value = "select * from list_project_detail(:company,:project)")
	public List<Object[]> getListProjectDetail(int project, int company);

	@Query(nativeQuery = true, value = "select * from coba_arrayv4(:dataProject2222) ")
	public void saveCoba(@Param("dataProject2222") List<ProjectDetail> dataProject	);

	@Query(nativeQuery = true, value = "select * from coba_arrayv4(:dataProject) ")
	public void saveCobaV3(@Param("dataProject") List<Object> dataProject);
	
	
}
