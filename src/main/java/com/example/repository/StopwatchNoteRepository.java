package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entity.StopwatchNote;
import com.example.serializable.StopwatchNoteSerializable;

public interface StopwatchNoteRepository  extends JpaRepository<StopwatchNote, StopwatchNoteSerializable>{
	
}
