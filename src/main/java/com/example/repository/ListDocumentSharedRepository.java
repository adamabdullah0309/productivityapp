package com.example.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.entity.ListDocumentShared;
import com.example.serializable.ListDocumentSharedSerializable;


public interface ListDocumentSharedRepository extends JpaRepository<ListDocumentShared, ListDocumentSharedSerializable> {

}
