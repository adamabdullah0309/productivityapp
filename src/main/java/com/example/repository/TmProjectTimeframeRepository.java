package com.example.repository;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.TmProjectTimeframe;
import com.example.serializable.TmProjectTimeframeSerializable;
public interface TmProjectTimeframeRepository  extends JpaRepository<TmProjectTimeframe, TmProjectTimeframeSerializable>{

//	@Query(nativeQuery = true, value = "select * from fn_thirdmenumaster(:menuId, :menuParentId) WHERE menu_parentid=:menuOrder")
//	public List<MMenuIndexThirdMaster> callProcMenuIndexThirdMaster(
//			@Param("menuId") int menuId, 
//			@Param("menuParentId") int menuParentId,
//			@Param("menuOrder") int menuOrder);
	
	@Query(nativeQuery = true, value = "select * from list_timeframe(:company, :project)")
	public List<Object[]> getTimeframe(@Param("project") int project,@Param("company") int company);
	
}
