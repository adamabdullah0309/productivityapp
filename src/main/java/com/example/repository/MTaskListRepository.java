package com.example.repository;



import com.example.entity.TaskList;
import com.example.serializable.MTaskListSerializable;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
	
public interface MTaskListRepository extends JpaRepository<TaskList, MTaskListSerializable> 
{
	public List<TaskList> findByStatusAndCompanyIdAndCreatedBy(final int status, final int companyId, final UUID user_uuid);
	public List<TaskList> findByCompanyIdAndCreatedBy(final int companyId, UUID user_uuid);
}
