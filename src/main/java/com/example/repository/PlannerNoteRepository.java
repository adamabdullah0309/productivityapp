package com.example.repository;

import com.example.entity.PlannerNote;
import com.example.entity.TaskList;
import com.example.serializable.MTaskListSerializable;
import com.example.serializable.PlannerNoteSerializable;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
	
public interface PlannerNoteRepository extends JpaRepository<PlannerNote, PlannerNoteSerializable> 
{

}
