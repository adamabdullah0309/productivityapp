package com.example.repository;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.TmBoardProjectDetailNote;
import com.example.serializable.TmBoardProjectDetailNoteSerializable;
public interface TmBoardProjectDetailNoteRepository  extends JpaRepository<TmBoardProjectDetailNote, TmBoardProjectDetailNoteSerializable>{
	
}
