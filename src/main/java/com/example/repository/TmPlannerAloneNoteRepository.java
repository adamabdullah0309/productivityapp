package com.example.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.TmPlannerAlone;
import com.example.entity.TmPlannerAloneNote;
import com.example.serializable.PlannerAloneNoteSerializable;
public interface TmPlannerAloneNoteRepository  extends JpaRepository<TmPlannerAloneNote, PlannerAloneNoteSerializable>{
	
}
