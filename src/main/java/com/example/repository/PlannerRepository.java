package com.example.repository;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.Planner;
import com.example.serializable.PlannerSerializable;
public interface PlannerRepository  extends JpaRepository<Planner, PlannerSerializable>{

	@Query(nativeQuery = true, value = "select * from list_planner(:user_uuid, :company, :status)")
	public List<Object[]> listPlanner(@Param("user_uuid") UUID user_uuid,@Param("company") int int1, @Param("status") int status);
//	List<Object[]> listPlanner(UUID user_uuid, int int1);
	
}
