package com.example.repository;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.TmBoardProjectDetail;
import com.example.serializable.TmBoardProjectDetailSerializable;
public interface TmBoardProjectDetailRepository  extends JpaRepository<TmBoardProjectDetail, TmBoardProjectDetailSerializable>{
	
	@Query(nativeQuery = true, value = "select * from detail_grouping_template(:user_uuid, :company, :project,:boardId, :projectDetailId)")
	public List<Object[]> detailGroupingAndTemplate(@Param("company") int company_id, @Param("project") int projectId, @Param("user_uuid") UUID user_uuid, @Param("boardId") int boardId, @Param("projectDetailId") int projectDetailId);

	@Query(nativeQuery = true, value = "select * from detail_task(:user_uuid, :company, :project,:boardId,  :boardProjectDetail)")
	public List<Object[]> detailTask(@Param("company") int company_id, @Param("project") int projectId, @Param("user_uuid") UUID user_uuid, @Param("boardId") int boardId, 
			 @Param("boardProjectDetail") int boardProjectDetail);

	@Query(nativeQuery = true, value = "select * from detail_board_member(:user_uuid, :company, :project, :projectDetailId, :boardId)")
	public List<Object[]> detailMember(@Param("company") int company_id, @Param("project") int projectId, @Param("projectDetailId") int projectDetailId, @Param("user_uuid") UUID user_uuid, @Param("boardId") int boardId);

	@Query(nativeQuery = true, value = "select * from detail_board_member_template(:user_uuid, :company, :project, :boardId)")
	public List<Object[]> detailMemberTemplate(@Param("company") int company_id, @Param("project") int projectId, @Param("user_uuid") UUID user_uuid, @Param("boardId") int boardId);

	@Query(nativeQuery = true, value = "select * from detail_board_attachement_template(:user_uuid, :company, :project, :boardId)")
	public List<Object[]> detailAttachementTemplate(@Param("company") int company, 
			@Param("project")int project, @Param("user_uuid") UUID user_uuid,@Param("boardId") int boardId);

	@Query(nativeQuery = true, value = "select * from detail_board_attachement_grouping(:user_uuid, :company, :project, :projectDetailId , :boardId)")
	public List<Object[]> detailBoardAttachementGrouping(@Param("company") int company_id,@Param("project") int projectId,@Param("user_uuid") UUID user_uuid,@Param("boardId") int boardId,
			@Param("projectDetailId") int projectDetailId);

	@Query(nativeQuery = true, value = "select * from detail_board_discussion_template(:user_uuid, :company, :project, :boardId)")
	public List<Object[]> detailBoardDiscussionTemplate(@Param("company") int company_id, @Param("project") int projectId, @Param("user_uuid") UUID user_uuid, @Param("boardId") int boardId);

	@Query(nativeQuery = true, value = "select * from detail_board_attachement_discussion_template(:user_uuid, :company, :project, :boardId)")
	public List<Object[]> detailBoardAttachementDiscussionTemplate(@Param("company") int company_id, @Param("project") int projectId, @Param("user_uuid") UUID user_uuid, @Param("boardId") int boardId);

	@Query(nativeQuery = true, value = "select * from detail_board_discussion_subgroup(:user_uuid, :company, :project, :projectDetailId , :boardId)")
	public List<Object[]> detailBoardDiscussionGrouping(@Param("company") int company_id,@Param("project") int projectId,@Param("projectDetailId") int projectDetailId, @Param("user_uuid") UUID user_uuid,@Param("boardId") int boardId
			);

	@Query(nativeQuery = true, value = "select * from detail_board_attachement_discussion_subgroup(:user_uuid, :company, :project, :projectDetailId , :boardId)")
	public List<Object[]> detailBoardAttachementDiscussionGrouping(@Param("company") int company_id,@Param("project") int projectId,@Param("projectDetailId") int projectDetailId, @Param("user_uuid") UUID user_uuid,@Param("boardId") int boardId
			);

	@Query(nativeQuery = true, value = "select * from list_monitor(:user_uuid, :company)")
	public List<Object[]> listMonitor(@Param("user_uuid") UUID user_uuid,@Param("company") int int1);
	
	
}
