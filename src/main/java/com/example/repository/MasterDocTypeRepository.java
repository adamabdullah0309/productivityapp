package com.example.repository;



import com.example.entity.MasterDocType;
import com.example.serializable.MasterDocTypeSerializable;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
	
public interface MasterDocTypeRepository extends JpaRepository<MasterDocType, MasterDocTypeSerializable> 
{
}
