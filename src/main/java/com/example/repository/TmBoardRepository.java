package com.example.repository;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.TmBoard;
import com.example.entity.TmBoardStage;
import com.example.serializable.TmBoardSerializable;
public interface TmBoardRepository  extends JpaRepository<TmBoard, TmBoardSerializable>{

	@Query(nativeQuery = true, value = "select * from list_board_template(:user_uuid, :company, :project,:boardId)")
	public List<Object[]> listBoardTemplate(@Param("company") int company, @Param("project") int project, @Param("user_uuid") UUID user_uuid, @Param("boardId") int boardId);

	@Query(nativeQuery = true, value = "select * from list_board_grouping(:user_uuid, :company, :project,:boardId)")
	public List<Object[]> listBoardGrouping(@Param("company") int company, @Param("project") int project, @Param("user_uuid") UUID user_uuid, @Param("boardId") int boardId);

	@Query(nativeQuery = true, value = "select * from list_board_task(:user_uuid, :company, :project,:boardId)")
	public List<Object[]> listBoardTask(@Param("company") int company, @Param("project") int projectId, @Param("user_uuid") UUID user_uuid, @Param("boardId") int boardId);

	@Query(nativeQuery = true, value = "select * from list_discussion_task(:user_uuid, :company_id, :boardId, :boardProjectDetailId)")
	public List<Object[]> listDiscussionTask(@Param("user_uuid") UUID user_uuid,@Param("company_id") int company_id,@Param("boardId") int boardId,
			@Param("boardProjectDetailId") int boardProjectDetailId);

	
	
}
