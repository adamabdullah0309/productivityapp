package com.example.repository;

import com.example.entity.TimesheetNote;
import com.example.entity.TaskList;
import com.example.serializable.TimesheetNoteSerializable;
import com.example.serializable.PlannerNoteSerializable;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
	
public interface TimesheetNoteRepository extends JpaRepository<TimesheetNote, TimesheetNoteSerializable> 
{

}
