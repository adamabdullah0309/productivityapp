package com.example.repository;

import com.example.entity.TmBoardProjectDetailAttchementDiscussion;
import com.example.serializable.TmBoardProjectDetailAttchementDiscussionSerializable;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
	
public interface TmBoardProjectDetailAttachementDiscussionRepository extends JpaRepository<TmBoardProjectDetailAttchementDiscussion, TmBoardProjectDetailAttchementDiscussionSerializable> 
{

	@Query(nativeQuery = true, value = "select count(*) from delete_attchement_discussion(:board,  :boardProjectDetail, :boardProjectDetailAttId, :boardProjectDetailAttDiscId,:company) ")
	void deleteFunction(@Param("board") int board, @Param("boardProjectDetail") int boardProjectDetail,@Param("boardProjectDetailAttId") int boardProjectDetailAttId,
			@Param("boardProjectDetailAttDiscId") int boardProjectDetailAttDiscId,@Param("company") int company);

}
