package com.example.demo;

import java.io.File;
import java.util.Arrays;

import org.hibernate.annotations.Loader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import java.lang.Object;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import com.example.controllers.*;


@SpringBootApplication
//@EnableEurekaClient
//@EnableCircuitBreaker

@ComponentScan(basePackages = {
"com.example.controllers,"
+ "com.example.entity,"
+ "com.example.repository,"
+ "com.example.service,"
+ "com.example.serviceimpl,"
+ "com.example.source,"
+ "com.example.config" })
@EnableJpaRepositories("com.example.repository")
@EntityScan("com.example.entity")
@EnableCaching

public class DemoApplication {

	public static void main(String[] args) {
		//below line will create uploads folder at startup if not created
//		new File(System.getProperty("user.dir") + "/images").mkdir();
		SpringApplication.run(DemoApplication.class, args);
	}
	
	
//	 @Bean
//	    public WebMvcConfigurer corsConfigurer() {
//	        return new WebMvcConfigurerAdapter() {
//	            @Override
//	            public void addCorsMappings(CorsRegistry registry) {
//	                registry.addMapping("/**").allowedOrigins("*");
//	            }
//	        };
//	    }
	 

}
