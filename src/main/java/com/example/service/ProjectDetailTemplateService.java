package com.example.service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ProjectDetailTemplate;

@Component
public interface ProjectDetailTemplateService {

	void save(ProjectDetailTemplate data);
}
