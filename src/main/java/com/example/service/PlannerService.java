package com.example.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Planner;
import com.example.entity.Timesheet;
import com.example.entity.TmPlannerAlone;

@Component
public interface PlannerService {

	void save(Planner data);

	List<Object[]> nextval();

//	Optional<Planner> findIdAndIdCompany(int int1, int int2);

	List<Object[]> listContent(String paramAktif, int int1, UUID user_uuid);

	

	List<Object[]> listPlannerProject(int company, UUID user_uuid);

	List<Object[]> listPlannerProjectDetail(int company, UUID user_uuid);

	List<Object[]> listPlanner(int int1, UUID user_uuid, int status);

	List<Object[]> listPlannerAlone(int int1, UUID user_uuid, String status);

	List<Object[]> listDetailPlanner(int plannerId, int company_id, int mProjectId, int projectDetailId, int timeframeId, UUID user_uuid);

	List<Object[]> detailPlannerAlone(int plannerAloneId, int companyId, UUID user_uuid);

	List<Object[]> detailTimeframe(int company_id, int mProjectId, int projectDetailId, int timeframeId, Date date);

	List<Object[]> listTaskProject(int projectId, int company_id, String paramAktif, UUID user_uuid);
	
	Optional<Planner> UpdatePlanner(int plannerId, int projectId, int projectDetailId, int timeframeId, int company_id);

	List<Object[]> listProjectCombobox(UUID user_uuid, int int1, int int2);

	List<Object[]> listParty(UUID user_uuid, int int1);

	List<Object[]> listTimeframeFromPlanner(int projectId, int projectDetailId, int timeframe, int company);

	List<Object[]> listTimeframeFromPlannerId(int int1, int int2, int int3, int int4, int int5);

	
	
}	
