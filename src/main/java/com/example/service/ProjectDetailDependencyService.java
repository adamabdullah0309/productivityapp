package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Planner;
import com.example.entity.ProjectDetailDependency;
import com.example.entity.Stopwatch;
import com.example.entity.Timesheet;

@Component
public interface ProjectDetailDependencyService {

	void save(ArrayList<ProjectDetailDependency> dataProjectDetailDependency);

	void delete(int project, int company);

	List<ProjectDetailDependency> findDetail(int project, int projectdetail, int company);
	
	
}	
