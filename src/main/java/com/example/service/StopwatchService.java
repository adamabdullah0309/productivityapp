package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Planner;
import com.example.entity.Stopwatch;
import com.example.entity.StopwatchNote;
import com.example.entity.Timesheet;

@Component
public interface StopwatchService {

	void save(Stopwatch data, List<StopwatchNote> note);

	List<Object[]> nextval();

	Optional<Stopwatch> findIdAndIdCompany(int int1, int int2);

	List<Object[]> listContent(String paramAktif, int int1, UUID user_uuid);

	List<Object[]> listDetailStopwatch(int int1, int int2, UUID user_uuid);

	List<Object[]> listStopwatch(String paramAktif, int int1, UUID user_uuid);

	List<Object[]> detail(int stopwatch, int company);
	
	
}	
