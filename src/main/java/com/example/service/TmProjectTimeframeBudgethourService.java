package com.example.service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.TmProjectTimeframeBudgethour;


@Component
public interface TmProjectTimeframeBudgethourService {

	void save(ArrayList<TmProjectTimeframeBudgethour> dataTimeframeBudgethour);

	List<Object[]> getTimeframeBudgetDetail(int project, int detail, int timeframe, int company);

	

}
