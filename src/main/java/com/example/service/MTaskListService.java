package com.example.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.TaskList;

@Component
public interface MTaskListService {
	
	
	List<TaskList> findByStatusAndCompanyId(int status, int companyId, UUID user_uuid);
	List<TaskList> findByCompanyId(int companyId, UUID user_uuid);
	Optional<TaskList> FindByTaskListIdAndCompanyId (Integer tasklistId, int companyId);
	void save(List<TaskList> paramList);
	void updateSave(TaskList param);
}	
