package com.example.service;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.entity.MasterInitial;
 
@Component
public interface ListDocumentSummarySubcategoryService {

	void save(List<ListDocumentSummarySubcategory> newData,ListDocument id);

	void delete(int doc, int company);

	List<Object[]> listDetailHierarchy(int company, int doc, int idcategory);

}
