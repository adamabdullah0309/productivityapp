package com.example.service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.TmProjectTimeframe;


@Component
public interface MStageGroupDetailService {

	List<Object[]> getStageDetail(int stageId, int companyid);

	List<Object[]> getStageDetailAll(int stageId, int companyid);



	

}
