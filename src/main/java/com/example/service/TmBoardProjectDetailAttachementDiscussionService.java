package com.example.service;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.TmBoardProjectDetailAttchementDiscussion;

@Component
public interface TmBoardProjectDetailAttachementDiscussionService {

	void save(TmBoardProjectDetailAttchementDiscussion data);

	List<Object[]> detailBoardAttachementDiscussion(int company, int board,  int boardProjectDetail, int boardProjectDetailAttId, UUID user_uuid);

	List<Object[]> nextval(int int1,int int3, int int4, int int5);

	void delete(int board, int boardProjectDetail, int boardProjectDetailAttId, int boardProjectDetailAttDiscId, int company);


}
