package com.example.service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.Grouping;
import com.example.entity.GroupingDetail;
import com.example.entity.ListDocument;
import com.example.entity.ListDocumentShared;
import com.example.entity.ListDocumentSummary;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.entity.MasterInitial;
import com.example.entity.Planner;
import com.example.entity.PlannerNote;
import com.example.entity.ProjectDetail;
import com.example.entity.ProjectDetailDependency;
import com.example.entity.ProjectDetailKaryawan;
import com.example.entity.Template;
import com.example.entity.TemplateDetail;
import com.example.entity.Timesheet;
import com.example.entity.TimesheetNote;
import com.example.entity.TmBoard;
import com.example.entity.TmBoardProjectDetail;
import com.example.entity.TmBoardProjectDetailAtt;
import com.example.entity.TmBoardStage;
import com.example.entity.TmPlannerAlone;
import com.example.entity.TmPlannerAloneNote;
import com.example.entity.TmProjectTimeframe;
import com.example.entity.TmProjectTimeframeBudgethour;

@Component
public interface GabunganService {
	public void SaveShared(ListDocument data1, List<ListDocumentShared> data2);

	public void saveSummary(ListDocument lastId, List<ListDocumentSummary> dataCategory,
			List<ListDocumentSummarySubcategory> newData);

	public void updateSummary(ListDocument listDocument, ArrayList<ListDocumentSummary> dataCategory,
			ArrayList<ListDocumentSummarySubcategory> newData);

	public void saveGrouping(Grouping dataHeader, List<GroupingDetail> detail);
	

	public void updateGrouping(Grouping grouping, List<GroupingDetail> detail);

	public void saveTemplate(Template dataHeader, List<TemplateDetail> detail);

	public void updateTemplate(Template template, List<TemplateDetail> detail);
	

	public void saveProjectDetail(List<ProjectDetail> dataProject, int MprojectId, int company_id, int template, List<ProjectDetailKaryawan> dataKaryawan, ArrayList<ProjectDetailDependency> dependency);

	public void saveTimeframeDetail(ArrayList<TmProjectTimeframe> dataTimeframe,
			ArrayList<TmProjectTimeframeBudgethour> dataTimeframeBudgethour);

	public void saveBoardFirst(TmBoard data, List<TmBoardStage> dataStage,
			List<TmBoardProjectDetail> dataProjectDetail);

	public void saveKaryawan(ArrayList<ProjectDetailKaryawan> dataProjectDetailDependency);
	
	public void saveDependency(ArrayList<ProjectDetailDependency> dataProjectDetailDependency);

	public void saveBoardAttchement(ArrayList<TmBoardProjectDetailAtt> dataProjectDetail);

	public void savePlanner(Planner dataPlanner, ArrayList<PlannerNote> isiPlannerNote);
	
	public void savePlannerAlone(TmPlannerAlone data, ArrayList<TmPlannerAloneNote> isiPlannerNote);

	public void saveTimesheet(Timesheet data, ArrayList<TimesheetNote> isiPlannerNote);

	
}
