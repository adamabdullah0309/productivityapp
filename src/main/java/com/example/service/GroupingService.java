package com.example.service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.Grouping;
import com.example.entity.ListDocument;
import com.example.entity.ListDocumentShared;
import com.example.entity.ListDocumentSummary;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.entity.MasterInitial;

@Component
public interface GroupingService {

	Optional<Grouping>  detailGroupHeader(int int1, int int2);

	

	public Grouping save(Grouping dataHeader);

	List<Object[]> nextval();

	List<Object[]> getHeaderSubgroup(int int1, UUID user_uuid, String paramAktif);

	List<Object[]> getSubgroupCoba(int company);



	List<Object[]> getDetailSubgroup(int company, int group);



	List<Object[]> checkDuplicate(int company, int group);


	
}
