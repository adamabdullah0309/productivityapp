package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Planner;
import com.example.entity.ProjectDetail;
import com.example.entity.Stopwatch;
import com.example.entity.Timesheet;

@Component
public interface ProjectDetailService {

	List<Object[]> getProjectDetail(int int1, int int2);

	void save(List<ProjectDetail> dataProject);

	void deleteAll(int mprojectId, int company_id);

	void saveTemplate(int template, int company_id, int mprojectId);

	List<Object[]> getProjectDetailAktif(int project, int company);

	void saveCoba(List<ProjectDetail> dataProject, int template);

	List<Object[]> listTask(int project, int companyId, String projectType, int projectDetail, String param);

	List<Object[]> listGroup(int projectId, int companyId, String projectType, String param);

	
	
}	
