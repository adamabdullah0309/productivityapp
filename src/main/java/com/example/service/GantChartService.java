package com.example.service;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import net.sf.json.JSONArray;

@Component
public interface GantChartService {

	List<Object[]> listGantChart(int int1, UUID user_uuid, JSONArray projectId, JSONArray projectPartyId);

	List<Object[]> detailGantChart(int company_id, int mProjectId, JSONArray projectDetailId, JSONArray taskId, JSONArray contactIdArray,
			UUID user_uuid);

	List<Object[]> ListGantChartSubgroup(int company, int project);

	List<Object[]> ListGantChartTask(int company, int project, JSONArray projectDetail);

	List<Object[]> dataDependency(int project, int company);


	

}
