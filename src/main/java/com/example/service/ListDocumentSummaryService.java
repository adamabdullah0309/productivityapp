package com.example.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.ListDocumentSummary;

@Component
public interface ListDocumentSummaryService {

	void save(List<ListDocumentSummary> data, ListDocument id);

	void delete(int doc, int companyid);

	List<Object[]> listSubcategory(String paramAktif, int company);

	List<Object[]> listSubSubcategory(int company, int subcategoryId);

	List<Object[]> listCategory(String paramAktif, int company);

	List<Object[]> listDocumentType(String paramAktif, int company);

	

	List<Object[]> getSubgroupCari(int childData);

	

	List<Object[]> getDetailGroupingHeader(int int1, int int2);

}
