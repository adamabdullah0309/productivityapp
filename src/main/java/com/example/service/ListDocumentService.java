package com.example.service;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;

@Component
public interface ListDocumentService {

	List<Object[]> listDocument();

	List<Object[]> listDocumentCategory(int id);

	List<Object[]> listDocumentDocumentType(int id);

	List<Object[]> listDocumentDetail(int id, int user);

	List<Object[]> listSummaryDocumentNo(int id, int summaryId,int user);

	List<Object[]> listSummaryDocumentDate(int parseInt, int parseInt2,int user);

	List<Object[]> listSummaryValueType(int id, int summary,int user);

	List<Object[]> listBasedOnCategory(String param, int user_id);

	List<Object[]> listSearchedOnCategory(int id,String paramAktif);

	List<Object[]> listSummaryValueUnit(int id, int summary,int user);

	List<Object[]> listIndex(int int1, String paramAktif, UUID user_uuid);

	List<Object[]> listDocumentCategoryDetail(int id, int summary, int user);
 
	List<Object[]> listContent(String paramAktif, int companyid, UUID createdby);

	List<Object[]> listDetail(int company, int id);

	List<Object[]> listDetailHierarchy(int company, int id, int category);

	List<Object[]> listDetailCategory(int company, int id);

	public ListDocument saveDoc(ListDocument listDoc);

	List<Object[]> listMyDocument(int int1, String paramAktif, UUID user_uuid);

	List<Object[]> listMyDocumentDetail(int company,  Integer doc_id);

	Optional<ListDocument> findIdAndIdCompany(int id, int company);



//	java.util.Optional<ListDocument> findOne(int id));
	
	
//	List<MasterInitial> findByStatusAndCompanyId(int status, int companyId);
//	List<MasterInitial> findByCompanyId(int companyId);
//	Optional<MasterInitial> FindByTaskListIdAndCompanyId (String tasklistId, int companyId);
//	void save(List<MasterInitial> paramList);
//	void updateSave(List<MasterInitial> isiArray);
}	
