package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Component;

import com.example.entity.MasterInitial;

@Component
public interface MasterDocTypeService {

	List<Object[]> listDataType(String paramAktif, int int1);
	
	
//	List<MasterInitial> findByStatusAndCompanyId(int status, int companyId);
//	List<MasterInitial> findByCompanyId(int companyId);
//	Optional<MasterInitial> FindByTaskListIdAndCompanyId (String tasklistId, int companyId);
//	void save(List<MasterInitial> paramList);
//	void updateSave(List<MasterInitial> isiArray);
}	
