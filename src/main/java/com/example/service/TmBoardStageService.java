package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Timesheet;
import com.example.entity.TmBoard;
import com.example.entity.TmBoardStage;

@Component
public interface TmBoardStageService {

	void save(List<TmBoardStage> dataStage);



}
