package com.example.service;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.example.entity.TmPlannerAloneNote;

@Component
public interface TmPlannerAloneNoteService {

	void delete(ArrayList<TmPlannerAloneNote> isiPlannerNote);

	void save(ArrayList<TmPlannerAloneNote> isiPlannerNote);

}
