package com.example.service;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.example.entity.TimesheetNote;

@Component
public interface TimesheetNoteService {

	void delete(ArrayList<TimesheetNote> isiPlannerNote);

	void save(ArrayList<TimesheetNote> isiPlannerNote);

}
