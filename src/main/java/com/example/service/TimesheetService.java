package com.example.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Timesheet;

@Component
public interface TimesheetService {

	void save(Timesheet data);

	List<Object[]> nextval();

	Optional<Timesheet> findIdAndIdCompany(int int1, int int2);

	List<Object[]> listContent(String paramAktif, int int1, UUID user_uuid);

	List<Object[]> listDetailTimesheet(int int1, int int2, UUID user_uuid);

	List<Object[]> listTimeframe(String paramAktif, int int1, UUID user_uuid, String startDate, String endDate);

	List<Object[]> listTimesheet(String paramAktif, int int1, UUID user_uuid, String string, String string2);

	List<Object[]> listTimesheet(int project, int projectDetail, UUID user_uuid, int company);

	List<Object[]> listTimesheetId(int project, int projectDetail, UUID user_uuid, int company, int timesheetId);

	
	
}	
