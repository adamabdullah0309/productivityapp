package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.TmBoardProjectDetailNote;

@Component
public interface TmBoardProjectDetailNoteService {

	void save(TmBoardProjectDetailNote data);

	void delete(TmBoardProjectDetailNote data);

	List<Object[]> nextval();

	List<Object[]> listNote(int board,  int boardProjectDetailId, int company_id);

	List<Object[]> detailNote(int boardId, int boardProjectDetailId, int boardProjectDetailNoteId, int company_id);


}
