package com.example.service;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.example.entity.PlannerNote;

@Component
public interface PlannerNoteService {

	void delete(ArrayList<PlannerNote> isiPlannerNote);

	void save(ArrayList<PlannerNote> isiPlannerNote);

}
