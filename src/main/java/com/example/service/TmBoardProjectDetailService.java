package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Timesheet;
import com.example.entity.TmBoard;
import com.example.entity.TmBoardProjectDetail;

@Component
public interface TmBoardProjectDetailService {

	void save(List<TmBoardProjectDetail> dataProjectDetail);

	void update(TmBoardProjectDetail data);

	List<Object[]> detailTask(int boardId, int boardProjectDetail, int company_id, int projectId, UUID user_uuid);

	List<Object[]> detailGroupingAndTemplate(int boardId, int company_id, int projectId,int projectDetailId, UUID user_uuid);

	List<Object[]> detailMember(int company_id, int projectId, int projectDetailId, UUID user_uuid, int boardId);

	List<Object[]> detailMemberTemplate(int company_id, int project, UUID user_uuid, int boardId);

	List<Object[]> detailMemberTask(int company, int board, UUID user_uuid, int boardProjectDetailId);

	List<Object[]> detailAttachementTemplate(int company, int project, UUID user_uuid, int boardId);

	List<Object[]> detailBoardAttachementGrouping(int company_id, int projectId, int projectDetailId, UUID user_uuid, int boardId);

	List<Object[]> detailBoardAttachementTask(int company, int board, UUID user_uuid, int boardProjectDetailId);

	List<Object[]> detailBoardDiscussionTemplate(int company, int project, UUID user_uuid, int boardId);

	List<Object[]> detailBoardAttachementDiscussionTemplate(int company, int project, UUID user_uuid, int boardId);

	List<Object[]> detailBoardDiscussionGrouping(int company, int project, int projectDetailId, UUID user_uuid, int board);

	List<Object[]> detailBoardAttachementDiscussionGrouping(int company, int project, int projectDetailId, UUID user_uuid, int board);

	List<Object[]> detailBoardDiscussionTask(int company, int boardid, int boardprojectdetailid, UUID user_uuid);

	List<Object[]> detailBoardAttachementDiscussionTask(int company, int boardid, int boardprojectdetailid, UUID user_uuid);

	void updateMuch(List<TmBoardProjectDetail> dataFinal);

	Optional<TmBoardProjectDetail> getDetail(int board, int int1, int int2);

	void updateBoardStage(TmBoardProjectDetail tmBoardProjectDetail);

	List<Object[]> listLog(int int1);

	List<Object[]> listMonitor(UUID user_uuid, int int1);

	
}
