package com.example.service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.TmProjectTimeframe;


@Component
public interface TmProjectTimeframeService {

	void save(ArrayList<TmProjectTimeframe> dataTimeframe);

	void delete(Integer tmProjectId, Integer companyId);

	List<Object[]> getTimeframeDetail(int project, int detail, int company);

	List<Object[]> listPlanner();

	List<Object[]> getTimeframe(int project, int company);

	List<Object[]> getTimeframeBudgetHour(int project, int company);

	List<Object[]> cobaProcedure();

	Optional<TmProjectTimeframe> getDetail(int project, int detail, int timeframeId, int company);


	

}
