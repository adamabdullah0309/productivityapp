package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Timesheet;
import com.example.entity.TmBoard;

@Component
public interface TmBoardService {

	List<Object[]> dataTaskAllFromBoard(int company, int project, UUID user_uuid, int boardId);

	List<Object[]> dataBoardGrouping(int company, int project, UUID user_uuid, int boardId); //final get grouping board coba1

	List<Object[]> listStageBoard(int projectId, int company); // final get pertaskboard

	List<Object[]> listdataTask(int projectId, int companyn, UUID user_uuid, int boardId);

	List<Object[]> listDrawerProject(int company);

	List<Object[]> listProjectBoard(int company, UUID user_uuid);

	List<Object[]> listStageDrawer(int company);

	List<Object[]> nextval();

	void save(TmBoard data);

	List<Object[]> listBoardDetailAtt(int company, int board, int boardProjectDetailId);

	List<Object[]> listDiscussionTask(int int1,int int3, int int4, UUID user_uuid);


}
