package com.example.service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.Grouping;
import com.example.entity.GroupingDetail;
import com.example.entity.ListDocument;
import com.example.entity.ListDocumentShared;
import com.example.entity.ListDocumentSummary;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.entity.MasterInitial;
import com.example.entity.Template;
import com.example.entity.TemplateDetail;

@Component
public interface TemplateDetailService {

	void save(List<TemplateDetail> detail, Template lastId);

	void delete(Integer groupingId, int companyId);

}
