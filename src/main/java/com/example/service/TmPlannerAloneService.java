package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Timesheet;
import com.example.entity.TmPlannerAlone;

@Component
public interface TmPlannerAloneService {

	List<Object[]> nextval();

	void save(TmPlannerAlone data);

	Optional<TmPlannerAlone> listDetail(int int1, int int2);

	Optional<TmPlannerAlone> UpdatePlannerAlone(int id, int companyId);

	

	
	
}	
