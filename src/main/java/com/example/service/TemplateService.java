package com.example.service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.Template;
import com.example.entity.ListDocument;
import com.example.entity.ListDocumentShared;
import com.example.entity.ListDocumentSummary;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.entity.MasterInitial;

@Component
public interface TemplateService {

	Optional<Template>  detailTemplateHeader(int int1, int int2);

	List<Object[]> detailTemplateDetail(int int1, int int2);

	public Template save(Template dataHeader);

	List<Object[]> nextval();
	
	List<Object[]> getHeaderTemplate(int int1, UUID user_uuid, String paramAktif);
	
	List<Object[]> getSubgroup(Integer integer, int company);

	List<Object[]> getDetailTemplate(Integer templateDetailId, int companyId);

	List<Object[]> callDetailTemplate(Integer id, Integer company);

	
}
