package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Planner;
import com.example.entity.ProjectDetailKaryawan;
import com.example.entity.Stopwatch;
import com.example.entity.Timesheet;

@Component
public interface ProjectDetailKaryawanService {

	List<Object[]> getProjectDetailKaryawa(int project, int detail, int company);

	void save(List<ProjectDetailKaryawan> dataKaryawan);

	void delete(int project, int company);
	
	
}	
