package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class MasterDocTypeSerializable implements Serializable  {
	
	@Column(name ="doc_type_id")
	public int docTypeId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public MasterDocTypeSerializable() {
		
	}
	
	public MasterDocTypeSerializable(int docTypeId,int companyId)
	{
		this.companyId = companyId;
		this.docTypeId = docTypeId;
	}
	
	

	public int getDocTypeId() {
		return docTypeId;
	}

	public void setDocTypeId(int docTypeId) {
		this.docTypeId = docTypeId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	

	
	
	

}
