package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;
@Embeddable
public class PlannerAloneNoteSerializable implements Serializable  {
	
	@Column(name ="planner_alone_id")
	public int plannerAloneId;
	
	@Column(name ="planner_alone_note_id")
	public int plannerAloneNoteId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public PlannerAloneNoteSerializable() {
		
	}
	
	public PlannerAloneNoteSerializable(int plannerAloneId,int companyId, int plannerAloneNoteId)
	{
		this.plannerAloneId = plannerAloneId;
		this.companyId = companyId;
		this.plannerAloneNoteId = plannerAloneNoteId;
	}
	
	

	public int getPlannerAloneNoteId() {
		return plannerAloneNoteId;
	}

	public void setPlannerAloneNoteId(int plannerAloneNoteId) {
		this.plannerAloneNoteId = plannerAloneNoteId;
	}

	public int getPlannerAloneId() {
		return plannerAloneId;
	}

	public void setPlannerAloneId(int plannerAloneId) {
		this.plannerAloneId = plannerAloneId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	

	

	

	
	
	

}
