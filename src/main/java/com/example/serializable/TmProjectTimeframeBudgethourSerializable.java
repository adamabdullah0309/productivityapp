package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

@Embeddable
public class TmProjectTimeframeBudgethourSerializable implements Serializable  {
	
	@Column(name = "project_detail_id")
	public Integer projectDetailId;
	
	@Column(name = "tm_project_id")
	public Integer TmProjectId;
	  
	
	  
	
	@Column(name = "company_id")
	public Integer companyId;
	
	@Column(name = "timeframe_id")
	public Integer timeframeId;
		
	@Column(name = "date_budgethour")
	public Date date;
	
	
	public TmProjectTimeframeBudgethourSerializable() {
		
	}
	
	public TmProjectTimeframeBudgethourSerializable(Integer TmProjectId, Integer timeframeId,int companyId, Date date, Integer projectDetailId)
	{
		this.companyId = companyId;
		this.TmProjectId = TmProjectId;
		this.timeframeId = timeframeId;
		this.date = date;
		this.projectDetailId = projectDetailId;
		
	}
	
	

	public Integer getProjectDetailId() {
		return projectDetailId;
	}

	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}

	public Integer getTmProjectId() {
		return TmProjectId;
	}

	public void setTmProjectId(Integer tmProjectId) {
		TmProjectId = tmProjectId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getTimeframeId() {
		return timeframeId;
	}

	public void setTimeframeId(Integer timeframeId) {
		this.timeframeId = timeframeId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
	

}
