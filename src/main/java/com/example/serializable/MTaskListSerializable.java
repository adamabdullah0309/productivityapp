package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class MTaskListSerializable implements Serializable {
	@Column(name = "task_id")
	public Integer tasklistId;
	
	@Column(name = "company_id")
	public Integer companyId;
	
	public MTaskListSerializable() {}
	
	public MTaskListSerializable(Integer tasklistId, Integer companyId) 
	{
		this.tasklistId = tasklistId;
		this.companyId = companyId;
	}

	public Integer getTasklistId() {
		return tasklistId;
	}

	public void setTasklistId(Integer tasklistId) {
		this.tasklistId = tasklistId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
}
