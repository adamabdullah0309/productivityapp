package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class TmBoardSerializable implements Serializable  {
	
	@Column(name ="board_id")
	public int boardId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public TmBoardSerializable() {
		
	}
	
	public TmBoardSerializable(int boardId,int companyId)
	{
		this.companyId = companyId;
		this.boardId = boardId;
	}
	
	

	public int getBoardId() {
		return boardId;
	}

	public void setBoardId(int boardId) {
		this.boardId = boardId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	

	
	
	

}
