package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class MStageSerializable implements Serializable  {
	
	@Column(name ="stage_id")
	public int stageId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public MStageSerializable() {
		
	}
	
	public MStageSerializable(int stageId,int companyId)
	{
		this.companyId = companyId;
		this.stageId = stageId;
	}
	
	public int getStageId() {
		return stageId;
	}

	public void setStageId(int stageId) {
		this.stageId = stageId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	

	
	
	

}
