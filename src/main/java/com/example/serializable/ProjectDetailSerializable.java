package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class ProjectDetailSerializable implements Serializable {
	@Column(name = "project_detail_id")
	public Integer projectDetailId;
	
	@Column(name = "m_project_id")
	public Integer mProjectId;
	
	@Column(name = "company_id")
	public Integer companyId;
	
	public ProjectDetailSerializable() {}
	
	public ProjectDetailSerializable(Integer projectDetailId, Integer mProjectId, Integer companyId) 
	{
		this.projectDetailId = projectDetailId;
		this.mProjectId = mProjectId;
		this.companyId = companyId;
	}

	

	public Integer getProjectDetailId() {
		return projectDetailId;
	}

	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}

	public Integer getmProjectId() {
		return mProjectId;
	}

	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	
	
}
