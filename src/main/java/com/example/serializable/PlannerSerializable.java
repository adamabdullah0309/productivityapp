package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;
@Embeddable
public class PlannerSerializable implements Serializable  {
	
	@Column(name ="planner_id")
	public int plannerId;
	
	@Column(name ="project_id")
	public int projectId;
	
	@Column(name ="project_detail_id")
	public int projectDetailId;
	
	@Column(name ="timeframe_id")
	public int timeframeId;
	
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public PlannerSerializable() {
		
	}
	
	public PlannerSerializable(int plannerId, int projectId, int projectDetailId, int timeframeId, int companyId)
	{
		this.projectId = projectId;
		this.projectDetailId = projectDetailId;
		this.timeframeId = timeframeId;
		this.companyId = companyId;
		this.plannerId = plannerId;
	}

	public int getPlannerId() {
		return plannerId;
	}

	public void setPlannerId(int plannerId) {
		this.plannerId = plannerId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getProjectDetailId() {
		return projectDetailId;
	}

	public void setProjectDetailId(int projectDetailId) {
		this.projectDetailId = projectDetailId;
	}

	public int getTimeframeId() {
		return timeframeId;
	}

	public void setTimeframeId(int timeframeId) {
		this.timeframeId = timeframeId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	

	

	
	
	

}
