package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;
@Embeddable
public class StopwatchNoteSerializable implements Serializable  {
	
	@Column(name ="stopwatch_id")
	public int stopwatchId;
	
	@Column(name ="stopwatch_note_id")
	public int stopwatchNoteId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public StopwatchNoteSerializable() {
		
	}
	
	public StopwatchNoteSerializable(int stopwatchId,int companyId, int stopwatchNoteId)
	{
		this.stopwatchId = stopwatchId;
		this.companyId = companyId;
		this.stopwatchNoteId = stopwatchNoteId;
	}

	public int getStopwatchId() {
		return stopwatchId;
	}

	public void setStopwatchId(int stopwatchId) {
		this.stopwatchId = stopwatchId;
	}

	public int getStopwatchNoteId() {
		return stopwatchNoteId;
	}

	public void setStopwatchNoteId(int stopwatchNoteId) {
		this.stopwatchNoteId = stopwatchNoteId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	

	
	
	

	

	

	
	
	

}
