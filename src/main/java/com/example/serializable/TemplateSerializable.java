package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class TemplateSerializable implements Serializable  {
	
	@Column(name ="template_id")
	public int templateId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public TemplateSerializable() {
		
	}
	
	public TemplateSerializable(int templateId,int companyId)
	{
		this.companyId = companyId;
		this.templateId = templateId;
	}

	
	
	

	public int getTemplateId() {
		return templateId;
	}

	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	

	
	
	

}
