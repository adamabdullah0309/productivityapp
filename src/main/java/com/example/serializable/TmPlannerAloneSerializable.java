package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;
@Embeddable
public class TmPlannerAloneSerializable implements Serializable  {
	
	@Column(name ="planner_alone_id")
	public int plannerAloneId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public TmPlannerAloneSerializable() {
		
	}
	
	public TmPlannerAloneSerializable(int plannerAloneId,int companyId)
	{
		this.plannerAloneId = plannerAloneId;
		this.companyId = companyId;
	}

	public int getPlannerAloneId() {
		return plannerAloneId;
	}

	public void setPlannerAloneId(int plannerAloneId) {
		this.plannerAloneId = plannerAloneId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	

	

	

	
	
	

}
