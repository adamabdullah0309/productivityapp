package com.example.serializable;
import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class GroupingDetailSerializable implements Serializable  {
	
	@Column(name = "nourut")
	public Integer nourut;
	
	@Column(name = "m_grouping_id")
	public int mGroupingId;
	
	@Column(name = "company_id")
	public int companyId;
	
	
	public GroupingDetailSerializable() {
		
	}
	
	public GroupingDetailSerializable(Integer nourut,int mGroupingId,int companyId)
	{
		this.nourut = nourut;
		this.mGroupingId = mGroupingId;
		this.companyId = companyId;
	}

	public Integer getNourut() {
		return nourut;
	}

	public void setNourut(Integer nourut) {
		this.nourut = nourut;
	}

	public int getmGroupingId() {
		return mGroupingId;
	}

	public void setmGroupingId(int mGroupingId) {
		this.mGroupingId = mGroupingId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	
	

	
	
	

}
