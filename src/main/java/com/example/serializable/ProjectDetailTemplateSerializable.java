package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class ProjectDetailTemplateSerializable implements Serializable {
	
	@Column(name = "m_project_id")
	public Integer mProjectId;
	
	@Column(name = "company_id")
	public Integer companyId;
	
	public ProjectDetailTemplateSerializable() {}
	
	public ProjectDetailTemplateSerializable(Integer projectDetailId, Integer companyId) 
	{
		this.mProjectId = mProjectId;
		this.companyId = companyId;
	}

	public Integer getmProjectId() {
		return mProjectId;
	}

	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	
	
}
