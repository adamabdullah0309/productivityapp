package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class TimesheetSerializable implements Serializable  {
	
	@Column(name ="timesheet_id")
	public int timesheetId;
	
	 @Column(name = "timesheet_company_id")
	 public int timesheetCompanyId;
	
	
	public TimesheetSerializable() {
		
	}
	
	public TimesheetSerializable(int timesheetId,int timesheetCompanyId)
	{
		this.timesheetCompanyId = timesheetCompanyId;
		this.timesheetId = timesheetId;
	}

	public int getTimesheetId() {
		return timesheetId;
	}

	public void setTimesheetId(int timesheetId) {
		this.timesheetId = timesheetId;
	}

	public int getTimesheetCompanyId() {
		return timesheetCompanyId;
	}

	public void setTimesheetCompanyId(int timesheetCompanyId) {
		this.timesheetCompanyId = timesheetCompanyId;
	}

	

	
	
	

}
