package com.example.serializable;
import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class TemplateDetailSerializable implements Serializable  {
	
	@Column(name = "nourut")
	public Integer nourut;
	
	@Column(name = "m_template_id")
	public int mTemplateId;
	
	@Column(name = "company_id")
	public int companyId;
	
	
	public TemplateDetailSerializable() {
		
	}
	
	public TemplateDetailSerializable(Integer nourut,int mTemplateId,int companyId)
	{
		this.nourut = nourut;
		this.mTemplateId = mTemplateId;
		this.companyId = companyId;
	}

	public Integer getNourut() {
		return nourut;
	}

	public void setNourut(Integer nourut) {
		this.nourut = nourut;
	}

	

	public int getmTemplateId() {
		return mTemplateId;
	}

	public void setmTemplateId(int mTemplateId) {
		this.mTemplateId = mTemplateId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	
	

	
	
	

}
