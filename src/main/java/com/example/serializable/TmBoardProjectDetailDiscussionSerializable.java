package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class TmBoardProjectDetailDiscussionSerializable implements Serializable  {
	
	@Column(name ="tm_board_id")
	public int TmBoardId;
	
	@Column(name ="tm_board_project_detail_id")
	public int TmBoardProjectDetailId;
	
	@Column(name ="tm_board_project_detail_dis_id")
	public int TmBoardProjectDetailDisId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public TmBoardProjectDetailDiscussionSerializable() {
		
	}
	
	public TmBoardProjectDetailDiscussionSerializable(int TmBoardId,int TmBoardProjectDetailId, int TmBoardProjectDetailDisId, int companyId)
	{
		this.companyId = companyId;
		this.TmBoardId = TmBoardId;
		this.TmBoardProjectDetailId = TmBoardProjectDetailId;
		this.TmBoardProjectDetailDisId = TmBoardProjectDetailDisId;
	}

	public int getTmBoardId() {
		return TmBoardId;
	}

	public void setTmBoardId(int tmBoardId) {
		TmBoardId = tmBoardId;
	}

	public int getTmBoardProjectDetailId() {
		return TmBoardProjectDetailId;
	}

	public void setTmBoardProjectDetailId(int tmBoardProjectDetailId) {
		TmBoardProjectDetailId = tmBoardProjectDetailId;
	}

	public int getTmBoardProjectDetailDisId() {
		return TmBoardProjectDetailDisId;
	}

	public void setTmBoardProjectDetailDisId(int tmBoardProjectDetailDisId) {
		TmBoardProjectDetailDisId = tmBoardProjectDetailDisId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	
	
	
	
}
