package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class TmProjectTimeframeSerializable implements Serializable  {
	
	
	@Column(name = "tm_project_id")
	public Integer TmProjectId;
	  
	
	@Column(name = "company_id")
	public Integer companyId;
	
	@Column(name = "timeframe_id")
	public Integer timeframeId;
		
	@Column(name = "project_detail_id")
	public Integer projectDetailId;
	
	
	public TmProjectTimeframeSerializable() {
		
	}
	
	public TmProjectTimeframeSerializable(Integer TmProjectId,Integer timeframeId,int companyId, int projectDetailId)
	{
		this.companyId = companyId;
		this.TmProjectId = TmProjectId;
		this.timeframeId = timeframeId;
		this.projectDetailId = projectDetailId;
	}
	
	

	public Integer getProjectDetailId() {
		return projectDetailId;
	}

	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}

	public Integer getTmProjectId() {
		return TmProjectId;
	}

	public void setTmProjectId(Integer tmProjectId) {
		TmProjectId = tmProjectId;
	}
	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getTimeframeId() {
		return timeframeId;
	}

	public void setTimeframeId(Integer timeframeId) {
		this.timeframeId = timeframeId;
	}

	
	
	

}
