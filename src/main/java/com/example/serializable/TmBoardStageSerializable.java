package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class TmBoardStageSerializable implements Serializable  {
	
	@Column(name ="tm_board_id")
	public int TmBoardId;
	
	@Column(name ="tm_board_stage_id")
	public int TmBoardStageId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public TmBoardStageSerializable() {
		
	}
	
	public TmBoardStageSerializable(int TmBoardId, int TmBoardStageId, int companyId)
	{
		this.companyId = companyId;
		this.TmBoardId = TmBoardId;
		this.TmBoardStageId = TmBoardStageId;
	}

	public int getTmBoardId() {
		return TmBoardId;
	}

	public void setTmBoardId(int tmBoardId) {
		TmBoardId = tmBoardId;
	}

	public int getTmBoardStageId() {
		return TmBoardStageId;
	}

	public void setTmBoardStageId(int tmBoardStageId) {
		TmBoardStageId = tmBoardStageId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	
}
