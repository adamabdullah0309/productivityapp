package com.example.serializable;
import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class ListDocumentSummarySubcategorySerializable implements Serializable  {
	
	@Column(name = "document_no_id")
	public Integer documentNoId;
	
	@Column(name = "id")
	public int id;
	
	
	public ListDocumentSummarySubcategorySerializable() {
		
	}
	
	public ListDocumentSummarySubcategorySerializable(int id,Integer documentNoId)
	{
		this.id = id;
		this.documentNoId = documentNoId;
	}

	public Integer getDocumentNoId() {
		return documentNoId;
	}

	public void setDocumentNoId(Integer documentNoId) {
		this.documentNoId = documentNoId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	
	
	

}
