package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class StopwatchSerializable implements Serializable  {
	
	@Column(name ="stopwatch_id")
	public int stopwatchId;
	
	 @Column(name = "stopwatch_company_id")
	 public int stopwatchCompanyId;
	
	
	public StopwatchSerializable() {
		
	}
	
	public StopwatchSerializable(int stopwatchId,int stopwatchCompanyId)
	{
		this.stopwatchCompanyId = stopwatchCompanyId;
		this.stopwatchId = stopwatchId;
	}

	public int getStopwatchId() {
		return stopwatchId;
	}

	public void setStopwatchId(int stopwatchId) {
		this.stopwatchId = stopwatchId;
	}

	public int getStopwatchCompanyId() {
		return stopwatchCompanyId;
	}

	public void setStopwatchCompanyId(int stopwatchCompanyId) {
		this.stopwatchCompanyId = stopwatchCompanyId;
	}

	
	
	

	
	
	

}
