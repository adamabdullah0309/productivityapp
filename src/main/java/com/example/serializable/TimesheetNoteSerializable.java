package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;
@Embeddable
public class TimesheetNoteSerializable implements Serializable  {
	
	@Column(name ="timesheet_id")
	public int timesheetId;
	
	@Column(name ="timesheet_note_id")
	public int timesheetNoteId;
	
	 @Column(name = "timesheet_company_id")
	 public int timesheetCompanyId;
	
	
	public TimesheetNoteSerializable() {
		
	}
	
	public TimesheetNoteSerializable(int timesheetId,int timesheetCompanyId, int timesheetNoteId)
	{
		this.timesheetId = timesheetId;
		this.timesheetCompanyId = timesheetCompanyId;
		this.timesheetNoteId = timesheetNoteId;
	}

	public int getTimesheetId() {
		return timesheetId;
	}

	public void setTimesheetId(int timesheetId) {
		this.timesheetId = timesheetId;
	}

	public int getTimesheetNoteId() {
		return timesheetNoteId;
	}

	public void setTimesheetNoteId(int timesheetNoteId) {
		this.timesheetNoteId = timesheetNoteId;
	}

	public int getTimesheetCompanyId() {
		return timesheetCompanyId;
	}

	public void setTimesheetCompanyId(int timesheetCompanyId) {
		this.timesheetCompanyId = timesheetCompanyId;
	}
	
	
	
}
