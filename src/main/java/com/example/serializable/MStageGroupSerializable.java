package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class MStageGroupSerializable implements Serializable  {
	
	@Column(name ="stage_group_id")
	public int stageGroupId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public MStageGroupSerializable() {
		
	}
	
	public MStageGroupSerializable(int stageGroupId,int companyId)
	{
		this.companyId = companyId;
		this.stageGroupId = stageGroupId;
	}
	
	

	public int getStageGroupId() {
		return stageGroupId;
	}

	public void setStageGroupId(int stageGroupId) {
		this.stageGroupId = stageGroupId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	

	
	
	

}
