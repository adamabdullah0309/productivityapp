package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class MStageGroupDetailSerializable implements Serializable  {
	
	@Column(name = "stage_detail_id")
	public int stageDetailId;
	
	@Column(name ="m_stage_id")
	public int stageId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public MStageGroupDetailSerializable() {
		
	}
	
	public MStageGroupDetailSerializable(int stageId, int stageDetailId, int companyId)
	{
		this.stageDetailId = stageDetailId;
		this.companyId = companyId;
		this.stageId = stageId;
	}
	
	
	
	public int getStageDetailId() {
		return stageDetailId;
	}

	public void setStageDetailId(int stageDetailId) {
		this.stageDetailId = stageDetailId;
	}

	public int getStageId() {
		return stageId;
	}

	public void setStageId(int stageId) {
		this.stageId = stageId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	

	
	
	

}
