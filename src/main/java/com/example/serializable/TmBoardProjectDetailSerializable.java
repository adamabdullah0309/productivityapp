package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class TmBoardProjectDetailSerializable implements Serializable  {
	
	@Column(name ="tm_board_id")
	public int TmBoardId;
	
	@Column(name ="tm_board_project_detail_id")
	public int TmBoardProjectDetailId;
	
	
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public TmBoardProjectDetailSerializable() {
		
	}
	
	public TmBoardProjectDetailSerializable(int TmBoardId,int TmBoardProjectDetailId, int companyId)
	{
		this.companyId = companyId;
		this.TmBoardId = TmBoardId;
		this.TmBoardProjectDetailId = TmBoardProjectDetailId;
	}
	
	public int getTmBoardProjectDetailId() {
		return TmBoardProjectDetailId;
	}

	public void setTmBoardProjectDetailId(int tmBoardProjectDetailId) {
		TmBoardProjectDetailId = tmBoardProjectDetailId;
	}

	public int getTmBoardId() {
		return TmBoardId;
	}

	public void setTmBoardId(int tmBoardId) {
		TmBoardId = tmBoardId;
	}


	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	
}
