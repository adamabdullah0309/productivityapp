package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class TmBoardProjectDetailAttchementDiscussionSerializable implements Serializable  {
	
	@Column(name ="tm_board_id")
	public int TmBoardId;
	
	@Column(name ="tm_board_project_detail_id")
	public int TmBoardProjectDetailId;
	
	@Column(name ="tm_board_project_detail_att_id")
	public int TmBoardProjectDetailAttId;
	
	@Column(name ="tm_board_project_detail_att_disc_id")
	public int TmBoardProjectDetailAttDiscId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public TmBoardProjectDetailAttchementDiscussionSerializable() {
		
	}
	
	public TmBoardProjectDetailAttchementDiscussionSerializable(int TmBoardId,int TmBoardProjectDetailId, int TmBoardProjectDetailAttId, int TmBoardProjectDetailAttDiscId, int companyId)
	{
		this.companyId = companyId;
		this.TmBoardId = TmBoardId;
		this.TmBoardProjectDetailId = TmBoardProjectDetailId;
		this.TmBoardProjectDetailAttDiscId = TmBoardProjectDetailAttDiscId;
		this.TmBoardProjectDetailAttId = TmBoardProjectDetailAttId;
	}
	
	

	public int getTmBoardProjectDetailAttDiscId() {
		return TmBoardProjectDetailAttDiscId;
	}

	public void setTmBoardProjectDetailAttDiscId(int tmBoardProjectDetailAttDiscId) {
		TmBoardProjectDetailAttDiscId = tmBoardProjectDetailAttDiscId;
	}

	public int getTmBoardProjectDetailAttId() {
		return TmBoardProjectDetailAttId;
	}

	public void setTmBoardProjectDetailAttId(int tmBoardProjectDetailAttId) {
		TmBoardProjectDetailAttId = tmBoardProjectDetailAttId;
	}

	public int getTmBoardProjectDetailId() {
		return TmBoardProjectDetailId;
	}

	public void setTmBoardProjectDetailId(int tmBoardProjectDetailId) {
		TmBoardProjectDetailId = tmBoardProjectDetailId;
	}

	public int getTmBoardId() {
		return TmBoardId;
	}

	public void setTmBoardId(int tmBoardId) {
		TmBoardId = tmBoardId;
	}
	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	
}
