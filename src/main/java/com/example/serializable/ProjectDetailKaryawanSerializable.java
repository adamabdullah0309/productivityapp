package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class ProjectDetailKaryawanSerializable implements Serializable {
	@Column(name = "project_detail_id")
	public Integer projectDetailId;
	
	@Column(name = "project_detail_karyawan_id")
	public Integer projectDetailKaryawanId;
	
	@Column(name = "project_id")
	public Integer mProjectId;
	
	@Column(name = "company_id")
	public Integer companyId;
	
	public ProjectDetailKaryawanSerializable() {}
	
	public ProjectDetailKaryawanSerializable(Integer projectDetailId, Integer mProjectId, Integer projectDetailKaryawanId, Integer companyId) 
	{
		this.projectDetailId = projectDetailId;
		this.mProjectId = mProjectId;
		this.projectDetailKaryawanId = projectDetailKaryawanId;
		this.companyId = companyId;
	}

	public Integer getProjectDetailKaryawanId() {
		return projectDetailKaryawanId;
	}

	public void setProjectDetailKaryawanId(Integer projectDetailKaryawanId) {
		this.projectDetailKaryawanId = projectDetailKaryawanId;
	}

	public Integer getProjectDetailId() {
		return projectDetailId;
	}

	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}

	public Integer getmProjectId() {
		return mProjectId;
	}

	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	
	
}
