package com.example.serializable;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class GroupingSerializable implements Serializable  {
	
	@Column(name ="grouping_id")
	public int groupingId;
	
	 @Column(name = "company_id")
	 public int companyId;
	
	
	public GroupingSerializable() {
		
	}
	
	public GroupingSerializable(int groupingId,int companyId)
	{
		this.companyId = companyId;
		this.groupingId = groupingId;
	}

	
	
	public int getGroupingId() {
		return groupingId;
	}

	public void setGroupingId(int groupingId) {
		this.groupingId = groupingId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	

	
	
	

}
