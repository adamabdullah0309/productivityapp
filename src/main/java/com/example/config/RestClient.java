package com.example.config;
import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RestClient {
	private String baseUrl = "http://54.169.109.123:3003/";
	private String apiUrl;
	private HttpHeaders headers;
	
	public RestClient() {
		this.headers = new HttpHeaders();
	}
	
	public String getApiUrl() {
		return apiUrl;
	}
	
	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}
	
	public String getFullUrl() {
		return this.baseUrl + this.apiUrl;
	}
	
	public void setHeaders(String key, String value) {
		this.headers.set(key, value); ;
	}
	
	public HttpHeaders getHeaders() {
		return headers;
	}
	
	// this is for method GET
	public ResponseEntity<String> get() throws URISyntaxException{
		String fullUrl = this.getFullUrl();
		URI uri = new URI(fullUrl);
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>("parameters", this.getHeaders());
		
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
		return result;
	}
	
	// this is for method POST
	public <T> ResponseEntity<String> post(T body) throws URISyntaxException{
		String fullUrl = this.getFullUrl();
		URI uri = new URI(fullUrl);
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<T> entity = new HttpEntity<> (body, this.getHeaders());
		
		ResponseEntity<String> result = restTemplate.postForEntity(uri, entity, String.class);
		return result;
	}
}
