package com.example.controllers;

import java.util.Comparator;



import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public class SortBasedOnMessageId implements Comparator {

	@Override
	public int compare(Object lhs, Object rhs) {
		// TODO Auto-generated method stub
		try {
            return ((JSONObject) lhs).getInt("id") > ((JSONObject) rhs).getInt("id") ? 1 : (((JSONObject) lhs)
                .getInt("id") < ((JSONObject) rhs).getInt("id") ? -1 : 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
	}

}
