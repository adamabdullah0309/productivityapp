package com.example.controllers;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;

public class ChildComment {
    private Integer childId;
    private Integer boardProjectDetailId;
    private Integer boardProjectDetailDisId;
    private Integer parentId;
    private Integer boardId;
    private Integer MProjectId;
    private Integer projectDetailId;
    private String namaTask;
    
    private Integer company_id;
	private String commentDiscussion;
	private String dateComment;
	private String contactId;
	private String role;
	
	
	
	
	public Integer getProjectDetailId() {
		return projectDetailId;
	}
	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}
	public Integer getMProjectId() {
		return MProjectId;
	}
	public void setMProjectId(Integer mProjectId) {
		MProjectId = mProjectId;
	}
	public String getNamaTask() {
		return namaTask;
	}
	public void setNamaTask(String namaTask) {
		this.namaTask = namaTask;
	}
	public Integer getBoardId() {
		return boardId;
	}
	public void setBoardId(Integer boardId) {
		this.boardId = boardId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getDateComment() {
		return dateComment;
	}
	public void setDateComment(String dateComment) {
		this.dateComment = dateComment;
	}
	public String getContactId() {
		return contactId;
	}
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	public Integer getChildId() {
		return childId;
	}
	public void setChildId(Integer childId) {
		this.childId = childId;
	}
	public Integer getBoardProjectDetailId() {
		return boardProjectDetailId;
	}
	public void setBoardProjectDetailId(Integer boardProjectDetailId) {
		this.boardProjectDetailId = boardProjectDetailId;
	}
	public Integer getBoardProjectDetailDisId() {
		return boardProjectDetailDisId;
	}
	public void setBoardProjectDetailDisId(Integer boardProjectDetailDisId) {
		this.boardProjectDetailDisId = boardProjectDetailDisId;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public Integer getCompany_id() {
		return company_id;
	}
	public void setCompany_id(Integer company_id) {
		this.company_id = company_id;
	}
	public String getCommentDiscussion() {
		return commentDiscussion;
	}
	public void setCommentDiscussion(String commentDiscussion) {
		this.commentDiscussion = commentDiscussion;
	}
	
	
        
	
}