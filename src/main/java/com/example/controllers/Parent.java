package com.example.controllers;
import java.util.ArrayList;
import java.util.List;

public class Parent {

	private Integer idDatatype;
	private String namaDatatype;
	private Integer subcategory;
	private String namaSubcategory;
    private String value;
    private Integer subsubcategory;
    private String namaSubsubcategory;
    private Integer parentId;
    private Integer Id;
    private Integer nourut;
    private List<Parent> childrenItems; 

    public Parent() {
        this.subcategory = null;
        this.namaSubcategory = "";
        this.value = "";     
        this.subsubcategory = null;
        this.namaSubsubcategory = "";
        this.idDatatype = null;
        this.namaDatatype = "";
        this.parentId = null;
        this.childrenItems = new ArrayList<Parent>();
        this.Id = null;
        this.nourut = null;
    }
    
        
    



	public Integer getIdDatatype() {
		return idDatatype;
	}






	public void setIdDatatype(Integer idDatatype) {
		this.idDatatype = idDatatype;
	}






	public String getNamaDatatype() {
		return namaDatatype;
	}






	public void setNamaDatatype(String namaDatatype) {
		this.namaDatatype = namaDatatype;
	}






	public String getNamaSubcategory() {
		return namaSubcategory;
	}






	public void setNamaSubcategory(String namaSubcategory) {
		this.namaSubcategory = namaSubcategory;
	}






	public String getNamaSubsubcategory() {
		return namaSubsubcategory;
	}






	public void setNamaSubsubcategory(String namaSubsubcategory) {
		this.namaSubsubcategory = namaSubsubcategory;
	}






	public Integer getSubcategory() {
		return subcategory;
	}






	public void setSubcategory(Integer subcategory) {
		this.subcategory = subcategory;
	}






	public String getValue() {
		return value;
	}






	public void setValue(String value) {
		this.value = value;
	}






	public Integer getSubsubcategory() {
		return subsubcategory;
	}






	public void setSubsubcategory(Integer subsubcategory) {
		this.subsubcategory = subsubcategory;
	}






	public Integer getParentId() {
		return parentId;
	}






	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}






	public Integer getId() {
		return Id;
	}






	public void setId(Integer id) {
		Id = id;
	}






	public Integer getNourut() {
		return nourut;
	}






	public void setNourut(Integer nourut) {
		this.nourut = nourut;
	}






	public List<Parent> getChildrenItems() {
        return childrenItems;
    }
    public void setChildrenItems(List<Parent> childrenItems) {
        this.childrenItems = childrenItems;
    }
    public void addChildrenItem(Parent childrenItem){
        if(!this.childrenItems.contains(childrenItem))
            this.childrenItems.add(childrenItem);
    }

}