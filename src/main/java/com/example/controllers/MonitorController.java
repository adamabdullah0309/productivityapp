package com.example.controllers;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;import static java.util.Objects.isNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.client.RestTemplate;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.service.GantChartService;
import com.example.service.TmBoardProjectDetailService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static java.util.stream.Collectors.toCollection;

import java.math.BigDecimal;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.collectingAndThen;


@RestController
@RequestMapping("/api")
public class MonitorController {
	private static final Logger logger = LoggerFactory.getLogger(MonitorController.class);
	
	@Autowired
	private TmBoardProjectDetailService TmBoardProjectDetailService;
	
	int a = 0;
	String date = "";
	String dateDateline = "";
	@PostMapping("/ListMonitor")
	public String ListMonitor(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> cont1 = new ArrayList<>();
	    JSONObject dataRequestLevel = new JSONObject();
	    String level = "";
	    List<JSONObject> final11 = new ArrayList<>();
	    List<JSONObject> dataHeader = new ArrayList<>();
	    List<JSONObject> dataDetail = new ArrayList<>();
	    List<TmBoardProjectDetail> dataFinal = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("call ListMonitor()");
	    logger.info("input >>>> " + dataRequest);
	    
	    try { 
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> dataAll = TmBoardProjectDetailService.listMonitor(user_uuid, dataRequest.getInt("company_id"));
	        if(dataAll.size()>0)
	        {
	        	dataAll.stream().forEach(col->{
	        		JSONObject data = new JSONObject();
	        		data.put("priority", col[0] == null ? "" : col[0]);
	        		data.put("mProjectId", col[1] == null ? "" : col[1]);
	        		data.put("projectName", col[2] == null ? "" : col[2]);
	        		data.put("projectDetailId", col[3] == null ? "" : col[3]);
	        		data.put("group", col[4] == null ? "" : col[4]);
	        		data.put("task", col[5] == null ? "" : col[5]);
	        		data.put("dateline", col[6] == null ? "" : col[6]);
	        		data.put("donedate", col[7] == null ? "" : col[7]);
	        		data.put("description", col[8] == null ? "" : col[8]);
	        		data.put("contactId", col[9] == null ? "" : col[9]);
	        		data.put("progress", col[10] == null ? "" : col[10]);
	        		data.put("jumlahDocument", col[11] == null ? "" : col[11]);
	        		data.put("jumlahDiscussion", col[12] == null ? "" : col[12]);
	        		data.put("stageName", col[13] == null ? "" : col[13]);
	        		data.put("boardId", col[14] == null ? "" : col[14]);
	        		data.put("boardProjectDetailId", col[15] == null ? "" : col[15]);
	        		data.put("company_id", col[16] == null ? "" : col[16]);
	        		data.put("role", col[17] == null ? "" : col[17]);
	        		cont.add(data);
	        	});
	        }
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "List Monitor Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "List Monitor Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}

	public String getUserId(String header)
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}
}
