package com.example.controllers;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.entity.ListDocument;
import com.example.entity.ListDocumentShared;
import com.example.entity.MasterInitial;
import com.example.repository.ListDocumentRepository;
import com.example.service.GabunganService;
import com.example.service.ListDocumentService;
import com.example.service.ListDocumentSharedService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class ListDocumentController {
	
	
	@Autowired
	private ListDocumentService ListDocumentService;
	
	@Autowired
	private ListDocumentSharedService ListDocumentSharedService;
	
	@Autowired
	private ListDocumentRepository ListDocumentRepository;
	
	@Autowired
	private GabunganService GabunganService;
	
	
	private static final Logger logger = LoggerFactory.getLogger(ListDocumentController.class);
	
	//content
	@PostMapping({"/listDocument/{paramAktif}"})
	public String listDocument(@PathVariable("paramAktif") String paramAktif,@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	        
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	        List<Object[]> listContent = ListDocumentService.listContent(paramAktif, dataRequest.getInt("company_id"),user_uuid);
	        listContent.stream().forEach(col2->{
	        	JSONObject data = new JSONObject();
	    		data.put("category_name",col2[3] == null ? "Uncategory" : col2[3].toString());
	    		data.put("listDocumentId",col2[0]);
		    		List<JSONObject> detail = new ArrayList<>();
		    		JSONObject data1 = new JSONObject();
	    			data1.put("DocumentType",col2[5]);
	    			data1.put("title",col2[1]);
	    			detail.add(data1);
	    		data.put("detail",detail);
	    		cont.add(data);
	        });
	        response.put("responseCode", "00");
		      response.put("responseDesc", "Show List Document Success");
		      response.put("responseData", cont);
	    }
	    catch (Exception e) 
	    {
	      response.put("responseCode", "99");
	      response.put("responseDesc", "Show List Document Failed");
	      response.put("responseError", e.getMessage());
	      logger.info("ERROR #### " + e.getMessage());
	    } 
	    return response.toString();
	}
	
	//detail content
	@PostMapping({"/listDocumentDetail"})
	public String listDocumentDetail(@RequestBody String request) {
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	
	    	List<Object[]> listDetail = ListDocumentService.listDetail(dataRequest.getInt("company_id"), dataRequest.getInt("listDocumentId"));
	    	listDetail.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("subcategory",col[0]);
	    		data.put("value",col[1]);
	    		data.put("subsubcategory",col[2]);
	    		data.put("title",col[4]);
	    		data.put("category_name",col[6]);
	    		data.put("nourut",col[7]);
	    		data.put("parent_id",col[8]);
	    		data.put("id",col[9]);
	    		cont.add(data);
	    	});
	    	response.put("responseCode", "00");
		      response.put("responseDesc", "Show Detail Document Success");
		      response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		      response.put("responseDesc", "Show Detail Document Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	//index
	@PostMapping({"/listIndex/{paramAktif}"})
	public String listIndex(@RequestBody String request,@PathVariable("paramAktif") String paramAktif,  @RequestHeader(value = "User-Access") String header) {
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> listIndex = ListDocumentService.listIndex(dataRequest.getInt("company_id"),paramAktif, user_uuid);
	    	listIndex.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("listDocumentId", col[0]);
	    		data.put("title", col[1]);
	    		data.put("listDocumentShared" , col[2] == null ? "" : col[2]);
	    		cont.add(data);
	    	});
	    	response.put("responseCode", "00");
	    	response.put("responseDesc", "Show List Index Success");
	    	response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		      response.put("responseDesc", "Show List Index Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	@PostMapping({"/listDocumentDetailSO"})
	public String listDocumentDetailSO(@RequestBody String request) {
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	
	    	List<Object[]> listDetailCategory = ListDocumentService.listDetailCategory(dataRequest.getInt("company_id"), dataRequest.getInt("listDocumentId"));
	    	
	    	listDetailCategory.stream().forEach(col1->{
	    		ArrayList<Pair> pairs = new ArrayList<Pair>();
	    		String level = "";
	    		JSONObject data2 = new JSONObject();
	    		data2.put("category",col1[3]);
	    		List<Object[]> listDetailHierarchy = ListDocumentService.listDetailHierarchy(dataRequest.getInt("company_id"), dataRequest.getInt("listDocumentId"), Integer.parseInt(String.valueOf(col1[0])));
//		    	Integer childId adalah id sesungguhnya, Integer parentId, String subcategory, String value, String subsubcategory , nourut
		    	listDetailHierarchy.stream().forEach(col->{
		    		 pairs.add(new Pair( 
		    				 Integer.parseInt(col[2].toString()),
		    				 col[0] != null ? Integer.parseInt(String.valueOf(col[0])) : 0,
			    	    		col[6].toString(), 
			    	    		col[7].toString() , 	
			    	    		col[8].toString() ,
			    	    		Integer.parseInt(col[10].toString())
			    	    		));
		    	});

		    	Map<Integer, MegaMenuDTO> hm = new HashMap<>();


		        // you are using MegaMenuDTO as Linked list with next and before link 

		        // populate a Map
		        for(Pair p:pairs){

		        	
		            //  ----- Child -----
		            MegaMenuDTO mmdChild ;
		            if(hm.containsKey(p.getChildId())){
		                mmdChild = hm.get(p.getChildId());
		            }
		            else{
		                mmdChild = new MegaMenuDTO();
		                hm.put(p.getChildId(), mmdChild);
		            }  
		            mmdChild.setSubcategory(p.getSubcategory());
		            mmdChild.setValue(p.getValue());
		            mmdChild.setSubsubcategory(p.getSubsubcategory());
		            mmdChild.setId(p.getChildId());
		            mmdChild.setParentId(p.getParentId());
		            mmdChild.setNourut(p.getNourut());
		            // no need to set ChildrenItems list because the constructor created a new empty list



		            // ------ Parent ----
		            MegaMenuDTO mmdParent ;
		            if(hm.containsKey(p.getParentId())){
		                mmdParent = hm.get(p.getParentId());
		            }
		            else{
		                mmdParent = new MegaMenuDTO();
		                hm.put(p.getParentId(),mmdParent);
		            }
		            mmdParent.setId(p.getParentId());
		            mmdParent.setParentId(null);
		            mmdParent.addChildrenItem(mmdChild);
		            mmdParent.setSubcategory(p.getSubcategory());
		            mmdParent.setValue(p.getValue());
		            mmdParent.setSubsubcategory(p.getSubsubcategory());
		            mmdParent.setNourut(p.getNourut());

		        }
		        
		     // Get the root
		        List<MegaMenuDTO> DX = new ArrayList<MegaMenuDTO>(); 
		        for(MegaMenuDTO mmd : hm.values()){
		            if(mmd.getParentId() == null)
		                DX.add(mmd);
		        }

		        // Print 
		        for(MegaMenuDTO mmd: DX){
		            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
		            JSONObject data = new JSONObject();
		            
		            data.put("data",mmd);
		            cont.add(data);
		        }
		        for(int a= 0 ; a< cont.size() ; a++)
		        {
		        	level = cont.get(a).getString("data");
		        	logger.info("cont = " + cont.get(a).getString("data"));
		        }
		        JSONObject dataRequestLevel = new JSONObject();
		        if(level.isEmpty() == false) {
		        	dataRequestLevel = (JSONObject)JSONSerializer.toJSON(level);
		        	data2.put("detail",dataRequestLevel.get("childrenItems"));
		        }
		        else
		        {
		        	data2.put("detail","");
		        }
	    		final11.add(data2);
	    	});
	    	response.put("responseCode", "00");
		      response.put("responseDesc", "Show Detail Document Success");
		      response.put("responseData", final11);
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		      response.put("responseDesc", "Show Detail Document Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	
	@PostMapping({"/saveDocumentShared"})
	public String saveDocumentShared(@RequestBody String request,@RequestHeader(value = "User-Access") String header) {
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<ListDocumentShared> masterSharedDocument = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	JSONArray SharedDocument = dataRequest.getJSONArray("SharedDocument");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	logger.info("ada isinya = " + SharedDocument.size());
	    	Optional<ListDocument> listDoc = ListDocumentRepository.findById(dataRequest.getInt("listDocumentId"));
	    	if(SharedDocument.size() >= 1)
		      {
	    		
	    			for(int a = 0; a < SharedDocument.size(); a++)
					{
//	    				Integer listDocumentShareId, UUID listDocumentShareUser, Date listDocumentShareStartDate,Date listDocumentShareEndDate,
//	    				int listDocumentShareRole, Integer listDocumentShareUserGroup, Integer listDocumentShareSo, int listDocumentId,
//	    				UUID userId, int companyId
	    				ListDocumentShared masterSharedDocumentDetail = new ListDocumentShared(
	    						SharedDocument.getJSONObject(a).getInt("listDocumentShareId"),
	    						SharedDocument.getJSONObject(a).getString("listDocumentShareUser").isEmpty() ? null : UUID.fromString(SharedDocument.getJSONObject(a).getString("listDocumentShareUser")),
	    						formatter11.parse(SharedDocument.getJSONObject(a).getString("listDocumentShareStartDate")),
	    						SharedDocument.getJSONObject(a).getString("listDocumentShareEndDate").isEmpty() ? null : formatter11.parse(SharedDocument.getJSONObject(a).getString("listDocumentShareEndDate")),
	    								
	    						SharedDocument.getJSONObject(a).getInt("listDocumentShareRole"),
	    						SharedDocument.getJSONObject(a).getInt("listDocumentShareUserGroup") == 0 ? null : SharedDocument.getJSONObject(a).getInt("listDocumentShareUserGroup"),
	    						SharedDocument.getJSONObject(a).getInt("listDocumentShareSo") == 0 ? null : SharedDocument.getJSONObject(a).getInt("listDocumentShareSo"),
	    								dataRequest.getInt("listDocumentId"),
	    						user_uuid,
	    						SharedDocument.getJSONObject(a).getInt("companyId")
	    						);
	    				masterSharedDocument.add(masterSharedDocumentDetail);
					}
	    			
	    			((ListDocument)listDoc.get()).setListDocumentShared(1);
//	    			ListDocumentService.saveDoc(listDoc.get());
		      }
	    	else
	    	{
    			((ListDocument)listDoc.get()).setListDocumentShared(null);
//    			ListDocumentService.saveDoc(listDoc.get());
	    	}
	    	
	    	if(masterSharedDocument.size() > 0)
	    	{
//	    		ListDocumentSharedService.save(masterSharedDocument);
	    	}
	    	
	    	GabunganService.SaveShared(listDoc.get(), masterSharedDocument);
	    	
//	    	UUID user_uuid = UUID.fromString(getUserId(header));
//	    	ListDocument listDoc = new ListDocument();
//	    	listDoc.setCreatedBy(user_uuid);
//	    	ListDocumentService.saveDoc(listDoc);
	    	response.put("responseCode", "00");
		      response.put("responseDesc", "Save Document Shared Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		      response.put("responseDesc", "Save Document Shared Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping({"/detailDocumentShared"})
	public String detailDocumentShared(@RequestBody String request,@RequestHeader(value = "User-Access") String header) {
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<ListDocumentShared> masterSharedDocument = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> detailDocumentShared = ListDocumentSharedService.detailDocumentShared(user_uuid, dataRequest.getInt("listDocumentId"));
	    	detailDocumentShared.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("listDocumentShareId",col[0]);
	    		data.put("listDocumentShareUser",col[1] == null ? "" : col[1].toString());
	    		data.put("listDocumentShareStartDate",col[2].toString());
	    		data.put("listDocumentShareEndDate",col[3] == null ? "" : col[3].toString());
	    		data.put("listDocumentShareRole",col[4]);
	    		data.put("listDocumentShareUserGroup",col[5] == null ? "" : col[5].toString());
	    		data.put("listDocumentShareSo",col[6] == null ? "" : col[6].toString());
	    		data.put("listDocumentId",col[7]);
	    		data.put("userId",col[8].toString());
	    		data.put("companyId",col[9]);
	    		cont.add(data);
	    	});
	    	
	    	response.put("responseCode", "00");
		      response.put("responseDesc", "Detail Document Shared Success");
		      response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		      response.put("responseDesc", "Detail Document Shared Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	@PostMapping({"/myDocument/{paramAktif}"})
	public String myDocument(@PathVariable("paramAktif") String paramAktif,@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> listIndex = ListDocumentService.listMyDocument(dataRequest.getInt("company_id"),paramAktif,user_uuid);
	    	listIndex.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("listDocumentId", col[0]);
	    		data.put("title", col[1]);
	    		data.put("documentType", col[2] == null ? "" : col[2].toString());
	    		data.put("category", col[3] == null ? "" : col[3].toString());
	    		data.put("LogoShared", col[4] == null ? "" : col[4]);
	    		List<Object[]> listMyDocumentDetail = ListDocumentService.listMyDocumentDetail(dataRequest.getInt("company_id"), Integer.valueOf(col[0].toString()));
	    		
	    		cont.add(data);
	    	});
	    	
	    	response.put("responseCode", "00");
		      response.put("responseDesc", "List My Document Success");
		      response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		      response.put("responseDesc", "List My Document Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping({"/listSharedWithMe/{paramAktif}"})
	public String listSharedWithMe(@PathVariable("paramAktif") String paramAktif,@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	logger.info("formatter11 = " + formatter11.parse(dataRequest.getString("startDate")));
	    	List<Object[]> listSharedWithMe = ListDocumentSharedService.listSharedWithMe(
	    			user_uuid, 
	    			dataRequest.getInt("company_id"), 
	    			dataRequest.getInt("group_id"), 
	    			dataRequest.getInt("so_id"), 
	    			formatter11.parse(dataRequest.getString("startDate")), 
	    			formatter11.parse(dataRequest.getString("endDate")),
	    			paramAktif  );
	    	logger.info("listSharedWithMe = " + listSharedWithMe.size());
	    	listSharedWithMe.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("listDocumentId", col[0]);
	    		data.put("title", col[1] == null ? "" : col[1].toString());
	    		data.put("documentType", col[2] == null ? "" : col[2].toString());
	    		data.put("category", col[3] == null ? "" : col[3].toString());
	    		data.put("role", col[4] == null ? "" : col[4].toString());
	    		data.put("sharedUserId", col[5] == null ? "" : col[5].toString());
	    		data.put("sharedUserGroupId", col[6] == null ? "" : col[6]);
	    		data.put("sharedSOId", col[7] == null ? "" : col[7]);
	    		data.put("startDate", col[8] == null ? "" : col[8].toString());
	    		data.put("endDate", col[9] == null ? "" : col[9].toString());
	    		
	    		cont.add(data);
	    	});
	    	response.put("responseCode", "00");
		      response.put("responseDesc", "List Shared With Me Success");
		      response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		      response.put("responseDesc", "List Shared With Me Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	
	@PostMapping({"/saveDocumentSummary"})
	public String saveDocumentSummary(@RequestBody String request,@RequestHeader(value = "User-Access") String header) {
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	
//	    	UUID user_uuid = UUID.fromString(getUserId(header));
//	    	ListDocument listDoc = new ListDocument();
//	    	listDoc.setCreatedBy(user_uuid);
//	    	ListDocumentService.saveDoc(listDoc);
	    	response.put("responseCode", "00");
		      response.put("responseDesc", "Save Document Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		      response.put("responseDesc", "Save Document Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	@PostMapping({"/saveDocument2"})
	public String saveDocument2(@RequestBody String request,@RequestHeader(value = "User-Access") String header) {
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	ListDocument listDoc = new ListDocument();
	    	listDoc.setCreatedBy(user_uuid);
	    	ListDocumentService.saveDoc(listDoc);
	    	response.put("responseCode", "00");
		      response.put("responseDesc", "Save Document Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		      response.put("responseDesc", "Save Document Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	
	@PostMapping({"/listDocument2/{paramAktif}"})
	public String listDocument2(@PathVariable("paramAktif") String paramAktif,@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	        
	        String user_uuid = getUserId(header);
	        System.out.println(user_uuid);

	    	List<Object[]> listBasedOnCategory = ListDocumentService.listBasedOnCategory(paramAktif, dataRequest.getInt("user_id"));
	    	listBasedOnCategory.stream().forEach(col2->{
	    		JSONObject data = new JSONObject();
	    		data.put("category_name",col2[0]);
	    		data.put("listDocumentId",col2[1]);
	    		List<JSONObject> detail = new ArrayList<>();
	    		logger.info("Integer.parseInt(col2[1].toString()) = " + Integer.parseInt(col2[1].toString()));
	    		List<Object[]> listSearchedOnCategory = ListDocumentService.listSearchedOnCategory(Integer.parseInt(col2[1].toString()), paramAktif);
	    		listSearchedOnCategory.stream().forEach(col3->{
	    			JSONObject data1 = new JSONObject();
	    			data1.put("DocumentType",col3[0]);
	    			data1.put("title",col3[1]);
	    			detail.add(data1);
	    		});
	    		data.put("detail",detail);
	    		cont.add(data);
	    	});
	      response.put("responseCode", "00");
	      response.put("responseDesc", "Show List Document Success");
	      response.put("responseData", cont);
	    }
	    catch (Exception e) 
	    {
	      response.put("responseCode", "99");
	      response.put("responseDesc", "Show List Document Failed");
	      response.put("responseError", e.getMessage());
	      logger.info("ERROR #### " + e.getMessage());
	    } 
	    return response.toString();
	}
	
	
	
	
	@PostMapping({"/listDocumentDetail2"})
	public String listDocumentDetail2(@RequestBody String request) {
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	List<Object[]> listDocumentDetail = ListDocumentService.listDocumentDetail(dataRequest.getInt("listDocumentId"),dataRequest.getInt("user_id"));
	    	listDocumentDetail.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("listDocumentSummaryId", col[0]);
	    		data.put("listDocumentId", col[1]);
	    		List<Object[]> DocumentNoDetail = ListDocumentService.listSummaryDocumentNo(Integer.parseInt(col[1].toString()), Integer.parseInt(col[0].toString()), dataRequest.getInt("user_id"));
	    		data.put("summaryDocumentNoType",DocumentNoDetail.get(0) == null ? "" : DocumentNoDetail.get(0));
	    		data.put("summaryDocumentNoValue", col[2] == null ? "" : col[2]);
	    		
	    		List<Object[]> DocumentDateDetail = ListDocumentService.listSummaryDocumentDate(Integer.parseInt(col[1].toString()), Integer.parseInt(col[0].toString()), dataRequest.getInt("user_id"));
	    		data.put("summaryDocumentDateType",DocumentDateDetail.get(0) == null ? "" : DocumentDateDetail.get(0));
	    		data.put("summaryDocumentDateValue", col[3] == null ? "" : col[3].toString());
	    		
	    		List<Object[]> DocumentValueType = ListDocumentService.listSummaryValueType(Integer.parseInt(col[1].toString()), Integer.parseInt(col[0].toString()), dataRequest.getInt("user_id"));
	    		List<Object[]> DocumentValueUnit = ListDocumentService.listSummaryValueUnit(Integer.parseInt(col[1].toString()), Integer.parseInt(col[0].toString()),dataRequest.getInt("user_id") );
	    		data.put("summaryDocumentValueType",DocumentValueType.get(0) == null ? "" : DocumentValueType.get(0));
	    		data.put("summaryDocumentValueUnit",DocumentValueUnit.get(0) == null ? "" : DocumentValueUnit.get(0));
	    		data.put("summaryDocumentValueValue", col[4] == null ? "" : col[4]);
	    		
	    		List<Object[]> DocumentCategory = ListDocumentService.listDocumentCategoryDetail(Integer.parseInt(col[1].toString()), Integer.parseInt(col[0].toString()), dataRequest.getInt("user_id"));
	    		data.put("summaryDocumentCategory", DocumentCategory.get(0) == null ? "" : DocumentCategory.get(0));
	    		
	    		data.put("summaryNote", col[5] == null ? "" : col[5]);
	    		
	    		data.put("listDocumentParentId", col[6]);
	    		cont.add(data);
	    	});
	    	response.put("responseCode", "00");
		      response.put("responseDesc", "Show Detail Document Success");
		      response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		      response.put("responseDesc", "Show Detail Document Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
//	@PostMapping({"/listIndex2/{paramAktif}"})
//	public String listIndex2(@RequestBody String request,@PathVariable("paramAktif") String paramAktif) {
//		JSONObject response = new JSONObject();
//	    List<JSONObject> cont = new ArrayList<>();
//	    
//	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
//	    logger.info("input >>>> " + dataRequest);
//	    try {
//	    	List<Object[]> listIndex = ListDocumentService.listIndex(dataRequest.getInt("user_id"),paramAktif);
//	    	listIndex.stream().forEach(col->{
//	    		JSONObject data = new JSONObject();
//	    		data.put("listDocumentId", col[0]);
//	    		data.put("title", col[1]);
//	    		cont.add(data);
//	    	});
//	    	response.put("responseCode", "00");
//	    	response.put("responseDesc", "Show List Index Success");
//	    	response.put("responseData", cont);
//	    }
//	    catch(Exception e)
//	    {
//	    	response.put("responseCode", "99");
//		      response.put("responseDesc", "Show List Index Failed");
//		      response.put("responseError", e.getMessage());
//		      logger.info("ERROR #### " + e.getMessage());
//	    }
//	    return response.toString();
//	}
	
	public String getUserId(String header )
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}
}
