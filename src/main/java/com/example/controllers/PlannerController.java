package com.example.controllers;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.helper.Helper;
import com.example.service.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/api")
public class PlannerController {
	private static final Logger logger = LoggerFactory.getLogger(PlannerController.class);
	Object data;
	
	@Autowired
	private PlannerService PlannerService;
	int a11 = 0;
	Float planned;
		Float overtimeHour;
		Float requestBudgetHour;
		
	@Autowired
	private TmPlannerAloneService TmPlannerAloneService;
	
	@Autowired
	private GabunganService GabunganService;
	
	@Autowired
	private TmProjectTimeframeService TmProjectTimeframeService;
	
	int jumlah = 0;
	//yang dipakek ini
	@PostMapping({"/listPlanner/{paramAktif}"})
	public String listPlanner(@PathVariable("paramAktif") String paramAktif,@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> HeaderProject = new ArrayList<>();
	    List<JSONObject> HeaderProjectDetail = new ArrayList<>();
	    List<JSONObject> Detail = new ArrayList<>();
	    List<JSONObject> finalFull = new ArrayList<>();
	    List<JSONObject> dataFinal = new ArrayList<>();
	    
	    List<JSONObject> HeaderProjectAlone = new ArrayList<>();
	    List<JSONObject> HeaderProjectDetailAlone = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	JSONObject master = new JSONObject();
	    	
	    	int status = 0;
	    	if(paramAktif.toLowerCase().equals("y"))
	    	{
	    		status = 1;
	    	}
	    	else if(paramAktif.toLowerCase().equals("n"))
	    	{
	    		status = 0;
	    	}
	    	logger.info("status = " + status);
	    	
	    	List<Object[]> listContent = PlannerService.listPlanner(dataRequest.getInt("company_id"),user_uuid, status);
	    	listContent.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("projectName", col[0]);
	    		data.put("projectDetailId", col[1]);
	    		data.put("mProjectId", col[2]);
	    		data.put("company_id", col[3]);
	    		data.put("contactId", col[4] == null ? "" : col[4].toString());
	    		data.put("role", col[5]);
	    		data.put("startdate", col[6] == null ? "" : col[6].toString());
	    		data.put("dateline", col[7] == null ? "" : col[7].toString());
	    		data.put("timeframeId", col[8] == null ? "" : col[8]);
	    		data.put("plannerId", col[9] == null ? "" : col[9]);
	    		data.put("dateBudgetHour", col[10] == null ? "" : col[10].toString());
	    		data.put("budgetHourHeaderTimeframe", col[11] == null ? "" : col[11].toString());
	    		data.put("budgetHourDetailTimeframe", col[12] == null ? "" : col[12].toString());
	    		data.put("plannedHour", col[13] == null ? "" : col[13].toString());
	    		data.put("overtimeHour", col[14] == null ? "" : col[14].toString());
	    		data.put("requestBudgetHour", col[15] == null ? "" : col[15].toString());
	    		data.put("buildingPlanner", col[16] == null ? "" : col[16].toString());
	    		data.put("addressPlanner", col[17] == null ? "" : col[17].toString());
	    		data.put("projectParty", col[18] == null ? "" : col[18]);
	    		data.put("mTaskId", col[19] == null ? "" : col[19]);
	    		data.put("mGroupingId", col[20] == null ? "" : col[20]);
	    		data.put("task", col[21] == null ? "" : col[21].toString());
	    		data.put("fromPlanner", col[22] == null ? "" : col[22].toString());
	    		data.put("toPlanner", col[23] == null ? "" : col[23].toString());
	    		data.put("date_budgethour_timeframe_re", col[24] == null ? "" : col[24].toString());
	    		data.put("status", col[25] == null ? "" : col[25]);
	    		HeaderProjectDetail.add(data);
	    		
	    		JSONObject data1 = new JSONObject();
	    		data1.put("projectName", col[0]);
	    		data1.put("projectDetailId", col[1]);
	    		data1.put("mProjectId", col[2]);
	    		data1.put("company_id", col[3]);
	    		data1.put("contactId", col[4] == null ? "" : col[4].toString());
	    		data1.put("role", col[5]);
	    		data1.put("startdate", col[6] == null ? "" : col[6].toString());
	    		data1.put("dateline", col[7] == null ? "" : col[7].toString());
	    		data1.put("timeframeId", col[8] == null ? "" : col[8]);
	    		data1.put("plannerId", col[9] == null ? "" : col[9]);
	    		data1.put("dateBudgetHour", col[10] == null ? "" : col[10].toString());
	    		data1.put("budgetHourHeaderTimeframe", col[11] == null ? "" : col[11].toString());
	    		data1.put("budgetHourDetailTimeframe", col[12] == null ? "" : col[12].toString());
	    		data1.put("plannedHour", col[13] == null ? "" : col[13].toString());
	    		data1.put("overtimeHour", col[14] == null ? "" : col[14].toString());
	    		data1.put("requestBudgetHour", col[15] == null ? "" : col[15].toString());
	    		data1.put("buildingPlanner", col[16] == null ? "" : col[16].toString());
	    		data1.put("addressPlanner", col[17] == null ? "" : col[17].toString());
	    		data1.put("projectParty", col[18] == null ? "" : col[18]);
	    		data1.put("mTaskId", col[19] == null ? "" : col[19]);
	    		data1.put("mGroupingId", col[20] == null ? "" : col[20]);
	    		data1.put("task", col[21] == null ? "" : col[21].toString());
	    		data1.put("fromPlanner", col[22] == null ? "" : col[22].toString());
	    		data1.put("toPlanner", col[23] == null ? "" : col[23].toString());
	    		data1.put("date_budgethour_timeframe_re", col[24] == null ? "" : col[24].toString());
	    		data1.put("status", col[25] == null ? "" : col[25]);
	    		HeaderProject.add(data1);
	    		
	    		JSONObject data2 = new JSONObject();
	    		data2.put("projectName", col[0]);
	    		data2.put("projectDetailId", col[1]);
	    		data2.put("mProjectId", col[2]);
	    		data2.put("company_id", col[3]);
	    		data2.put("contactId", col[4] == null ? "" : col[4].toString());
	    		data2.put("role", col[5]);
	    		data2.put("startdate", col[6] == null ? "" : col[6].toString());
	    		data2.put("dateline", col[7] == null ? "" : col[7].toString());
	    		data2.put("timeframeId", col[8] == null ? "" : col[8]);
	    		data2.put("plannerId", col[9] == null ? "" : col[9]);
	    		data2.put("dateBudgetHour", col[10] == null ? "" : col[10].toString());
	    		data2.put("budgetHourHeaderTimeframe", col[11] == null ? "" : col[11].toString());
	    		data2.put("budgetHourDetailTimeframe", col[12] == null ? "" : col[12].toString());
	    		data2.put("plannedHour", col[13] == null ? "" : col[13].toString());
	    		data2.put("overtimeHour", col[14] == null ? "" : col[14].toString());
	    		data2.put("requestBudgetHour", col[15] == null ? "" : col[15].toString());
	    		data2.put("buildingPlanner", col[16] == null ? "" : col[16].toString());
	    		data2.put("addressPlanner", col[17] == null ? "" : col[17].toString());
	    		data2.put("projectParty", col[18] == null ? "" : col[18]);
	    		data2.put("mTaskId", col[19] == null ? "" : col[19]);
	    		data2.put("mGroupingId", col[20] == null ? "" : col[20]);
	    		data2.put("task", col[21] == null ? "" : col[21].toString());
	    		data2.put("fromPlanner", col[22] == null ? "" : col[22].toString());
	    		data2.put("toPlanner", col[23] == null ? "" : col[23].toString());
	    		data2.put("date_budgethour_timeframe_re", col[24] == null ? "" : col[24].toString());
	    		data2.put("status", col[25] == null ? "" : col[25]);
	    		
	    		Detail.add(data2);
	    	});
	    	
	    	//untuk menghapus duplikat
	   		HashSet<Object> seenAtt= new HashSet<>();
	   		HeaderProjectDetail.removeIf(e -> !seenAtt.add(Arrays.asList(e.getInt("company_id"), e.getInt("mProjectId"), e.getInt("projectDetailId") )));
	   		
	   		HashSet<Object> seenHeader= new HashSet<>();
	   		HeaderProject.removeIf(e -> !seenHeader.add(Arrays.asList(e.getInt("company_id"), e.getInt("mProjectId") )));
	   		
	   		a11 = 0;
	   		HeaderProjectDetail.stream().forEach(col->{
	   			planned = (float) 0;
	   			overtimeHour = (float) 0;
	   			requestBudgetHour = (float) 0;
	   			JSONObject dataHeaderFull = new JSONObject();
	   			
	   			
	   			List<JSONObject> DetailData = new ArrayList<>();
	   			
	   			Detail.stream().forEach(col2->{
	   				
	   				if(col.getInt("company_id") == col2.getInt("company_id") && col.getInt("mProjectId") == col2.getInt("mProjectId") && col.getInt("projectDetailId") == col2.getInt("projectDetailId") )
	   				{
	   					JSONObject data = new JSONObject();
	   					data.put("taskId", col.get("mTaskId"));
	   					data.put("groupingId", col.get("mGroupingId"));
	   					data.put("task", col2.getString("task"));
	   					data.put("dateBudgetHour", col2.getString("dateBudgetHour"));
	   					data.put("fromPlanner", col2.getString("fromPlanner"));
	   					data.put("toPlanner", col2.getString("toPlanner"));
	   					data.put("buildingPlanner", col2.getString("buildingPlanner"));
	   		    		data.put("addressPlanner", col2.getString("addressPlanner"));
	   		    		data.put("timeframeId", col2.get("timeframeId"));
	   					data.put("budgetHourDetailTimeframe", col2.getString("budgetHourDetailTimeframe"));
	   					data.put("plannedHour", col2.getString("plannedHour"));
	   					data.put("overtimeHour", col2.getString("overtimeHour"));
	   					data.put("requestBudgetHour", col2.getString("requestBudgetHour"));
	   					data.put("plannerId", col2.get("plannerId") );
	   					data.put("projectDetailId", col2.get("projectDetailId"));
	   					data.put("date_budgethour_timeframe_re", col2.get("date_budgethour_timeframe_re"));
	   					data.put("status", col2.get("status"));
	   					data.put("contactId", col2.get("contactId"));
//	   					data.put("plannerId", col2.)
	   					
	   					if(col2.getString("plannedHour") != "")
	   					{
	   						planned = planned + Float.valueOf(col2.getString("plannedHour"));
	   					}
	   					else
	   					{
	   						planned = planned + (float) 0; 
	   					}
	   					
	   					if(col2.getString("overtimeHour") != "")
	   					{
	   						overtimeHour = overtimeHour + Float.valueOf(col2.getString("overtimeHour"));
	   					}
	   					else
	   					{
	   						overtimeHour = overtimeHour + (float) 0; 
	   					}
	   					
	   					if(col2.getString("requestBudgetHour") != "")
	   					{
	   						requestBudgetHour = requestBudgetHour + Float.valueOf(col2.getString("requestBudgetHour"));
	   					}
	   					else
	   					{
	   						requestBudgetHour = requestBudgetHour + (float) 0; 
	   					}
	   					
	   					DetailData.add(data);
	   				}
	   				
	   				
	   			});
	   			dataHeaderFull.put("taskId", col.get("mTaskId"));
	   			dataHeaderFull.put("groupingId", col.get("mGroupingId"));
	   			dataHeaderFull.put("task", col.getString("task"));
	   			dataHeaderFull.put("budgetHourHeaderTimeframe", col.getString("budgetHourHeaderTimeframe"));
	   			dataHeaderFull.put("company_id", col.getInt("company_id"));
	   			dataHeaderFull.put("mProjectId", col.getInt("mProjectId"));
	   			dataHeaderFull.put("plannedHour", planned);
	   			dataHeaderFull.put("overtimeHour", overtimeHour);
	   			dataHeaderFull.put("requestBudgetHour", requestBudgetHour);
	   			dataHeaderFull.put("sizeDetail", DetailData.size());
	   			dataHeaderFull.put("role", col.getString("role"));
	   			dataHeaderFull.put("contactId", col.get("contactId"));
	   			dataHeaderFull.put("detail", DetailData);
	   			
	   			finalFull.add(dataHeaderFull);
	   		});
	   		
	   		HeaderProject.stream().forEach(col->{
	   			List<JSONObject> DetailData = new ArrayList<>();
	   			finalFull.stream().forEach(col2->{
	   				if(col.getInt("company_id") == col2.getInt("company_id") && col.getInt("mProjectId") == col2.getInt("mProjectId"))
	   				{
	   					DetailData.add(col2);
	   					
	   					HeaderProject.get(a11).put("detail", DetailData);
	   				}
	   			});
	   			HeaderProject.get(a11).remove("projectDetailId");
//				HeaderProject.get(a11).remove("contactId");
				HeaderProject.get(a11).remove("role");
				HeaderProject.get(a11).remove("startdate");
				HeaderProject.get(a11).remove("dateline");
				HeaderProject.get(a11).remove("timeframeId");
				HeaderProject.get(a11).remove("plannerId");
				HeaderProject.get(a11).remove("dateBudgetHour");
				HeaderProject.get(a11).remove("budgetHourHeaderTimeframe");
				HeaderProject.get(a11).remove("budgetHourDetailTimeframe");
				HeaderProject.get(a11).remove("plannedHour");
				HeaderProject.get(a11).remove("overtimeHour");
				HeaderProject.get(a11).remove("requestBudgetHour");
				HeaderProject.get(a11).remove("fromPlanner");
				HeaderProject.get(a11).remove("toPlanner");
				HeaderProject.get(a11).remove("task");
//				HeaderProject.get(a11).remove("mTaskId");
//				HeaderProject.get(a11).remove("mGroupingId");
				HeaderProject.get(a11).remove("buildingPlanner");
				HeaderProject.get(a11).remove("addressPlanner");
	   			a11++;
	   		});
	   		
	   		List<Object[]> listPlanner = PlannerService.listPlannerAlone(dataRequest.getInt("company_id"),user_uuid, paramAktif.toLowerCase());
	   		listPlanner.stream().forEach(col->{
	   			JSONObject data = new JSONObject();
	   			data.put("plannerAloneId", col[0]);
	   			data.put("partyId", col[1]);
	   			data.put("taskId", col[2]);
	   			data.put("datePlannerInternal", col[3] == null ? "" : col[3].toString());
	   			data.put("startHour", col[4] == null ? "" : col[4].toString());
	   			data.put("endHour", col[5] == null ? "" : col[5].toString() );
//	   			data.put("note", col[6]);
	   			data.put("location", col[7] == null ? "" : col[7]);
	   			data.put("plannedHour", col[8] == null ? 0 : col[8] );
	   			data.put("overtimeHour", col[9] == null ? 0 : col[9]);
	   			data.put("contactId", col[11].toString());
	   			data.put("companyId", col[12]);
	   			data.put("building", col[13] == null ? "" : col[13]);
	   			data.put("address", col[14] == null ? "" : col[14]);
	   			data.put("status", col[15]);
	   			data.put("taskNama", col[16]);
	   			HeaderProjectAlone.add(data);
	   			HeaderProjectDetailAlone.add(data);
	   		});
	   		
	   		//untuk menghapus duplikat
	   		HashSet<Object> seenAttProjectAlone= new HashSet<>();
	   		HeaderProjectAlone.removeIf(e -> !seenAttProjectAlone.add(Arrays.asList(e.get("partyId"), e.get("contactId"), e.get("companyId") )));
	   		
	   		List<JSONObject> HeaderProjectDetailAlone2 = new ArrayList<>();
	   		HeaderProjectAlone.stream().forEach(col->{
	   			planned = (float) 0;
	   			overtimeHour = (float) 0;
	   			List<JSONObject> DetailData = new ArrayList<>();
	   			a11=0;
	   			HeaderProjectDetailAlone.stream().forEach(col2->{
	   				if(col.get("companyId") == col2.get("companyId") && col.get("partyId") == col2.get("partyId") && col.get("contactId").equals(col2.get("contactId")) )
	   				{
	   					DetailData.add(col2);
	   				}
	   				
	   			});	
	   			col.put("detail", DetailData);
	   			HeaderProjectDetailAlone2.add(col);
	   			a11++;
	   		});
	   		
	   		master.put("type", "NONPROJECT");
	   		master.put("detail",HeaderProjectDetailAlone2);
	    	cont.add(master);
	    	
	    	master = new JSONObject();
	    	master.put("type", "PROJECT");
	    	master.put("detail",HeaderProject);
	    	cont.add(master);
	    	 response.put("responseCode", "00");
		      response.put("responseDesc", "Show List Planner Success");
		      response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		      response.put("responseDesc", "Show List Planner Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping({"/detailPlanner"})
	public String detailPlanner(@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> HeaderProjectAlone = new ArrayList<>();
	    List<JSONObject> HeaderProjectDetailAlone = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("detailPlanner function");
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date(); 
	    	List<Object[]> listContent = PlannerService.listDetailPlanner(dataRequest.getInt("plannerId"),dataRequest.getInt("company_id"),dataRequest.getInt("mProjectId"), dataRequest.getInt("projectDetailId"), dataRequest.getInt("timeframeId"), user_uuid);
	    	listContent.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("plannerId", col[0]);
	    		data.put("projectId", col[2]);
	    		data.put("projectDetailId", col[1]);
	    		data.put("datePlanner", col[3] == null ? "" : col[3].toString());
	    		data.put("toPlanner", col[4] == null ? "" : col[4].toString());
	    		data.put("plannedHour", col[5] == null ? 0 : col[5]);
	    		data.put("overtimeHour", col[6] == null ? 0 : col[6]);
	    		data.put("buildingPlanner", col[9] == null ? "" : col[9].toString());
	    		data.put("fromPlanner", col[10] == null ? "" : col[10].toString());
	    		data.put("contactId", col[11]);
	    		data.put("timeframeId", col[12]);
	    		data.put("companyId", col[13]);
	    		data.put("date_budgethour_timeframe_re", col[14]== null ? "" : col[14].toString());
	    		data.put("addressPlanner", col[15]== null ? "" : col[15].toString());
	    		data.put("plannerNoteId", col[17]);
	    		data.put("note", col[18]== null ? "" : col[18].toString());
	    		data.put("dateNote", col[19]== null ? "" : col[19].toString());
	    		data.put("mTaskId", col[20]);
	    		HeaderProjectAlone.add(data);
	    		
	    		JSONObject data2 = new JSONObject();
	    		data2.put("plannerId", col[0]);
	    		data2.put("projectId", col[2]);
	    		data2.put("projectDetailId", col[1]);
	    		data2.put("datePlanner", col[3] == null ? "" : col[3].toString());
	    		data2.put("toPlanner", col[4] == null ? "" : col[4].toString());
	    		data2.put("plannedHour", col[5] == null ? 0 : col[5]);
	    		data2.put("overtimeHour", col[6] == null ? 0 : col[6]);
	    		data2.put("buildingPlanner", col[9] == null ? "" : col[9].toString());
	    		data2.put("fromPlanner", col[10] == null ? "" : col[10].toString());
	    		data2.put("contactId", col[11]);
	    		data2.put("timeframeId", col[12]);
	    		data2.put("companyId", col[13]);
	    		data2.put("date_budgethour_timeframe_re", col[14] == null ? "" : col[14].toString());
	    		data2.put("addressPlanner", col[15]== null ? "" : col[15].toString());
	    		data2.put("plannerNoteId", col[17]);
	    		data2.put("note", col[18]== null ? "" : col[18].toString());
	    		data2.put("dateNote", col[19]== null ? "" : col[19].toString());
	    		data2.put("mTaskId", col[20]);
	    		HeaderProjectDetailAlone.add(data2);
	    	});
	    	
	    	//untuk menghapus duplikat
	   		HashSet<Object> seenAtt= new HashSet<>();
	   		HeaderProjectAlone.removeIf(e -> !seenAtt.add(Arrays.asList(e.getString("date_budgethour_timeframe_re"), e.getInt("timeframeId"), e.getInt("plannerId"), e.getInt("companyId"), e.getInt("projectId"), e.getInt("projectDetailId") )));
	   		
	   		HeaderProjectAlone.stream().forEach(col->{
	   			List<JSONObject> detail = new ArrayList<>();
	   			HeaderProjectDetailAlone.stream().forEach(col2->{
	   				logger.info("detail = " + col2);
	   				if(    col.get("plannerId").equals(col2.get("plannerId")) 
	   					&& col.get("projectId").equals(col2.get("projectId"))
	   					&& col.get("projectDetailId").equals(col2.get("projectDetailId"))
	   					&& col.get("timeframeId").equals(col2.get("timeframeId"))
	   					&& col.get("companyId").equals(col2.get("companyId"))
//	   					&& col.get("date_budgethour_timeframe_re").equals(col2.get("date_budgethour_timeframe_re")
	   							
	   						)
	   				{
	   					col2.remove("datePlanner");
		   				col2.remove("toPlanner");
		   				col2.remove("plannedHour");
		   				col2.remove("overtimeHour");
		   				col2.remove("buildingPlanner");
		   				col2.remove("fromPlanner");
		   				col2.remove("contactId");
		   				col2.remove("addressPlanner");
		   				col2.remove("mTaskId");
		   				detail.add(col2);
	   				}
	   				
	   			});
	   			
	   			col.remove("plannerNoteId");
	   			col.remove("note");
	   			col.remove("dateNote");
	   			col.put("detail", detail);
	   		});
	   		
	    	
	    	
	    	 response.put("responseCode", "00");
		     response.put("responseDesc", "Show Detail Planner Success");
		     response.put("responseData", HeaderProjectAlone);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		      response.put("responseDesc", "Show Detail Planner Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping({"/detailPlannerAlone"})
	public String detailPlannerAlone(@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> HeaderProjectAlone = new ArrayList<>();
	    List<JSONObject> HeaderProjectDetailAlone = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("detailPlannerAlone function");
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	
	    	List<Object[]> listContent = PlannerService.detailPlannerAlone(dataRequest.getInt("plannerAloneId"), dataRequest.getInt("company_id"), user_uuid);
	    	listContent.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("plannerAloneId", col[0]);
	    		data.put("partyId", col[1]);
	    		data.put("taskId", col[2]);
	    		data.put("datePlannerInternal", col[3] == null ? "" : col[3].toString());
	    		data.put("startHour", col[4] == null ? "" : col[4].toString());
	    		data.put("endHour", col[5] == null ? "" : col[5].toString());
	    		data.put("location", col[6] == null ? "" : col[6].toString());
	    		data.put("plannedHour", col[7] == null ? 0 : col[7]);
	    		data.put("overtimeHour", col[8] == null ? 0 : col[8]);
	    		data.put("contactId", col[9] == null ? "" : col[9].toString());
	    		data.put("companyId", col[10] == null ? "" : col[10]);
	    		data.put("building", col[11] == null ? "" : col[11].toString());
	    		data.put("plannerAloneNoteId", col[12] == null ? "" : col[12]);
	    		data.put("note", col[13] == null ? "": col[13]);
	    		data.put("dateNote", col[14] == null ? "": col[14].toString());
	    		data.put("address", col[15] == null ? "" : col[15]);
	    		data.put("status", col[16] == null ? "" : col[16]);
	    		HeaderProjectAlone.add(data);
	    		
	    		JSONObject data2 = new JSONObject();
	    		data2.put("plannerAloneId", col[0]);
	    		data2.put("partyId", col[1]);
	    		data2.put("taskId", col[2]);
	    		data2.put("datePlannerInternal", col[3] == null ? "" : col[3].toString());
	    		data2.put("startHour", col[4] == null ? "" : col[4].toString());
	    		data2.put("endHour", col[5] == null ? "" : col[5].toString());
	    		data2.put("location", col[6] == null ? "" : col[6].toString());
	    		data2.put("plannedHour", col[7] == null ? 0 : col[7]);
	    		data2.put("overtimeHour", col[8] == null ? 0 : col[8]);
	    		data2.put("contactId", col[9] == null ? "" : col[9].toString());
	    		data2.put("companyId", col[10] == null ? "" : col[10]);
	    		data2.put("building", col[11] == null ? "" : col[11].toString());
	    		data2.put("plannerAloneNoteId", col[12] == null ? "" : col[12]);
	    		data2.put("note", col[13] == null ? "": col[13]);
	    		data2.put("dateNote", col[14] == null ? "": col[14].toString());
	    		data2.put("address", col[15] == null ? "" : col[15]);
	    		data2.put("status", col[16] == null ? "" : col[16]);
	    		HeaderProjectDetailAlone.add(data2);
	    	});
	    	
	    	HashSet<Object> seenAtt= new HashSet<>();
	   		HeaderProjectAlone.removeIf(e -> !seenAtt.add(Arrays.asList(e.getInt("plannerAloneId"), e.getInt("companyId"))));
	   		
	   		HeaderProjectAlone.stream().forEach(col->{
	   			List<JSONObject> detail = new ArrayList<>();
	   			HeaderProjectDetailAlone.stream().forEach(col2->{
	   				if(col.get("plannerAloneId").equals(col2.get("plannerAloneId")) 
	   					&& col.get("companyId").equals(col2.get("companyId"))
	   					&& col2.get("plannerAloneNoteId") != ""
	   						)
	   				{
	   					
	   					col2.remove("partyId");
		   				col2.remove("taskId");
		   				col2.remove("datePlannerInternal");
		   				col2.remove("startHour");
		   				col2.remove("endHour");
		   				col2.remove("location");
		   				col2.remove("plannedHour");
		   				col2.remove("overtimeHour");
		   				col2.remove("contactId");
		   				col2.remove("building");
		   				detail.add(col2);
	   				}
	   				
	   			});
	   			col.remove("plannerAloneNoteId");
	   			col.remove("note");
	   			col.remove("dateNote");
	   			col.put("detail", detail);
	   		});
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show Detail Planner Alone Success");
		    response.put("responseData", HeaderProjectAlone);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show Detail Planner Alone Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping({"/detailTimeframe"})
	public String detailTimeframe(@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> HeaderProjectAlone = new ArrayList<>();
	    List<JSONObject> HeaderProjectDetailAlone = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("detailTimeframe function");
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));

			SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date(); 
			date = formatter11.parse(dataRequest.getString("date_budgethour_timeframe_re"));
	    	List<Object[]> listContent = PlannerService.detailTimeframe(
	    			dataRequest.getInt("company_id"),
	    			dataRequest.getInt("mProjectId"), 
	    			dataRequest.getInt("projectDetailId"), 
	    			dataRequest.getInt("timeframeId"), 
	    			date);
	    	listContent.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("projectDetailId", col[0]);
	    		data.put("projectId", col[1]);
	    		data.put("timeframeId", col[2]);
	    		data.put("companyId", col[3]);
	    		data.put("date_budgethour_timeframe_re", col[4].toString());
	    		data.put("projectId", col[6]);
	    		data.put("mTaskId", col[7]);
	    		cont.add(data);
	    	});
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show Detail Timeframe Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show Detail Timeframe Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping({"/listTaskProject/{paramAktif}"})
	public String listTaskProject(@PathVariable("paramAktif") String paramAktif, @RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> HeaderProjectAlone = new ArrayList<>();
	    List<JSONObject> HeaderProjectDetailAlone = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("listTaskProject function");
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));

	    	List<Object[]> listContent = PlannerService.listTaskProject(dataRequest.getInt("projectId"), dataRequest.getInt("company_id"), paramAktif, user_uuid);
	    	listContent.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("taskId", col[0]);
	    		data.put("taskNama", col[1]);
	    		data.put("company_id", col[2]);
	    		data.put("taskStatus", col[3]);
	    		data.put("projectDetailId", col[4] == null ? "" : col[4]);
	    		data.put("projectId", col[5] == null ? "" : col[5]);
	    		data.put("timeframeId", col[6] == null ? "" : col[6]);
	    		cont.add(data);
	    	});
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Task Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Task Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping({"/listProjectCombobox"})
	public String listProjectCombobox(@RequestBody String request,  @RequestHeader(value = "User-Access") String header2) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> header = new ArrayList<>();
	    List<JSONObject> headerAlone = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header2));
	    	List<Object[]> listProject =  PlannerService.listProjectCombobox(user_uuid, dataRequest.getInt("company_id"), dataRequest.getInt("projectPartyId"));
	    	if(listProject.size()>0)
	    	{
	    		listProject.stream().forEach(col->{
	    			JSONObject data = new JSONObject();
	    			data.put("projectId", col[0]);
	    			data.put("projectName", col[1]);
	    			data.put("projectPartyId", col[2]);
	    			data.put("contactId", col[3]);
	    			cont.add(data);
	    		});
	    	}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Project Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Project Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping({"/listParty"})
	public String listParty(@RequestBody String request,  @RequestHeader(value = "User-Access") String header2) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> header = new ArrayList<>();
	    List<JSONObject> headerAlone = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	        
	    	UUID user_uuid = UUID.fromString(getUserId(header2));
	    	List<Object[]> listParty = PlannerService.listParty(user_uuid, dataRequest.getInt("company_id"));
	    	if(listParty.size()>0)
	    	{
	    		listParty.stream().forEach(col1->{
	    			JSONObject data = new JSONObject();
	    			data.put("projectPartyId", col1[0]);
	    			data.put("contactId", col1[1].toString());
	    			cont.add(data);
	    		});
	    	}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Party Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Party Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	Float actual = null;
	Float actualSemua = null;
	Float overtime = null;
	Float requestbudget = null;
	Float timeframe = null;
	
	@PostMapping("/savePlanner")
	public String savePlanner(@RequestBody String request,@RequestHeader(value = "User-Access") String header)
	{
		 JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    ArrayList<Planner> isiPlanner = new ArrayList<>();
		    ArrayList<PlannerNote> isiPlannerNote = new ArrayList<>();
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    UUID user_uuid = UUID.fromString(getUserId(header));
		    actual = 0.0f;
	    	overtime = 0.0f;
	    	requestbudget = 0.0f;
	    	timeframe = 0.0f;
	    	actualSemua = 0.0f;
		    LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        // berarti harus cek semua yang ada di planner dan di timeframe
	        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
	        DateTimeFormatter jam = DateTimeFormatter.ofPattern("HH:mm:ss");
		    logger.info("input >>>> " + dataRequest);
		    logger.info("dataHeader >>> " + header);
		    try {
		    	Planner dataPlanner = new Planner();
		    	
    	    	int id = 0;
    	    	
    	    	if(dataRequest.has("plannerId"))
    	    	{
    	    		List<Object[]> dataJam = PlannerService.listTimeframeFromPlannerId(dataRequest.getInt("projectId"), dataRequest.getInt("projectDetailId"), dataRequest.getInt("timeframeId"),  dataRequest.getInt("company_id"), dataRequest.getInt("plannerId"));
    	    		dataJam.stream().forEach(col->{
    	    			logger.info("col0 = " + col[0]);
    	    			actual = actual + Float.parseFloat(String.valueOf(col[0] == null ? 0 : col[0]));
    	    			overtime = overtime + Float.parseFloat(String.valueOf(col[1] == null ? 0 : col[1]));
    	    			requestbudget = requestbudget + Float.parseFloat(String.valueOf(col[2] == null ? 0 : col[2]));
    	    		});
//    	    		logger.info("actual = " + actual);
    	    		// get detail kecuali yang mau dirubah
    	    		timeframe = actual + overtime;
    	    		id = dataRequest.getInt("plannerId");
    	    	}
    	    	else
    	    	{
    	    		// get detail all
    	    		List<Object[]> dataJam = PlannerService.listTimeframeFromPlanner(dataRequest.getInt("projectId"), dataRequest.getInt("projectDetailId"), dataRequest.getInt("timeframeId"),  dataRequest.getInt("company_id"));
    	    		dataJam.stream().forEach(col->{
    	    			actual = actual + Float.parseFloat(String.valueOf(col[0] == null ? 0 : col[0]));
    	    			overtime = overtime + Float.parseFloat(String.valueOf(col[1] == null ? 0 : col[1]));
    	    			requestbudget = requestbudget + Float.parseFloat(String.valueOf(col[2] == null ? 0 : col[2]));
    	    		});
//    	    		logger.info("actual = " + actual);
    	    		timeframe = actual + overtime;
    	    		List<Object[]> dataNext = PlannerService.nextval();
    	    		id = Integer.parseInt(String.valueOf(dataNext.get(0)));
    	    	}
    	    	
    	    	Date date = new Date();
    	    	
    	    	//get timeframe
    	    	Optional<TmProjectTimeframe> dataTimeframe = TmProjectTimeframeService.getDetail(dataRequest.getInt("projectId"), dataRequest.getInt("projectDetailId"), dataRequest.getInt("timeframeId"),  dataRequest.getInt("company_id"));
    			JSONObject resultDiff = Helper.getdiff(dataRequest.getString("fromPlanner") , dataRequest.getString("toPlanner"), timeframe, dataTimeframe.get().getBudgetHour(), dataRequest.getString("plannedHour"));
    			logger.info("requestTime = " + resultDiff.get("requestTime"));
    			logger.info("actualTime = " + resultDiff.get("actualTime"));
    			logger.info("overtimeTime = " + resultDiff.get("overtimeTime"));
		    	dataPlanner.setPlannerId(id);
		    	dataPlanner.setProjectId(dataRequest.getInt("projectId"));
		    	dataPlanner.setProjectDetailId(dataRequest.getInt("projectDetailId"));
		    	dataPlanner.setDatePlanner(dataRequest.getString("datePlanner") != "" ? date=formatter1.parse(dataRequest.getString("datePlanner")) : null);
		    	dataPlanner.setTo_planner(dataRequest.getString("toPlanner") != "" ? Time.valueOf(dataRequest.getString("toPlanner")) : null );
		    	dataPlanner.setPlannedHour(Float.valueOf(resultDiff.getString("actualTime")));
		    	dataPlanner.setOvertime_hour(Float.valueOf(resultDiff.getString("overtimeTime")));
		    	dataPlanner.setRequest_budget_hour(Float.valueOf(resultDiff.getString("requestTime")));
		    	dataPlanner.setBuildingPlanner(dataRequest.getString("buildingPlanner"));
		    	dataPlanner.setFromplanner(dataRequest.getString("fromPlanner") != "" ? Time.valueOf(dataRequest.getString("fromPlanner")) : null);
		    	dataPlanner.setContactId(user_uuid);
		    	dataPlanner.setTimeframeId(dataRequest.getInt("timeframeId"));
		    	dataPlanner.setCompanyId(dataRequest.getInt("company_id"));
		    	
		    	if(!dataRequest.get("date_budgethour_timeframe_re").equals(null))
    	    	{
		    		date=formatter1.parse(dataRequest.getString("date_budgethour_timeframe_re"));
			    	dataPlanner.setDateBudgethourTimeframe(date);
    	    	}
		    	dataPlanner.setAddressPlanner(dataRequest.getString("addressPlanner") != "" ? dataRequest.getString("addressPlanner") : "" );
		    	dataPlanner.setStatus(dataRequest.getInt("status"));
		    	if(dataRequest.getJSONArray("detailNote").size() > 0) {
		    		for(int a = 0 ; a < dataRequest.getJSONArray("detailNote").size(); a++)
			    	{
		    			PlannerNote dataNote = new PlannerNote();
		    			dataNote.setPlannerId(id);
		    			dataNote.setProjectId(dataRequest.getInt("projectId"));
		    			dataNote.setProjectDetailId(dataRequest.getInt("projectDetailId"));
		    			dataNote.setTimeframeId(dataRequest.getInt("timeframeId"));
		    			dataNote.setCompanyId(dataRequest.getInt("company_id"));
//		    			date = formatter1.parse(dataRequest.getString("date_budgethour_timeframe_re"));
//		    			dataNote.setDateBudgethourTimeframe(date);
		    			if(!dataRequest.get("date_budgethour_timeframe_re").equals(null))
		    	    	{
				    		date=formatter1.parse(dataRequest.getString("date_budgethour_timeframe_re"));
				    		dataNote.setDateBudgethourTimeframe(date);
		    	    	}else
				    	{
		    	    		dataNote.setDateBudgethourTimeframe(null);
				    	}
		    			dataNote.setTmPlannerNoteId(dataRequest.getJSONArray("detailNote").getJSONObject(a).getInt("plannerNoteId"));
		    			dataNote.setNote(dataRequest.getJSONArray("detailNote").getJSONObject(a).getString("note"));
		    			dataNote.setDateNote(localDateTime2);
		    			isiPlannerNote.add(dataNote);
			    	}
		    	}
		    	
		    	GabunganService.savePlanner(dataPlanner, isiPlannerNote);
//		    	List<Object[]> dataNext = PlannerService.nextval();
		    	
//		    	Time.valueOf(dataRequest.getString("fromTime"))
//		    	if(dataRequest.getJSONArray("detailData").size() > 0)
//			    {
//		    		for(int a = 0 ; a < dataRequest.getJSONArray("detailData").size(); a++)
//			    	{
//		    			Planner dataPlanner = new Planner();
//		    			List<Object[]> dataNext = PlannerService.nextval();
//		    	    	int id = 0;
//		    	    	if(dataNext.size()>0)
//		    	    	{
//		    	    		id = Integer.parseInt(String.valueOf(dataNext.get(0)));
//		    	    	}
//		    	    	if(dataRequest.getJSONArray("detailData").getJSONObject(a).has("plannerId")) {
//		    	    		dataPlanner.setPlannerId(dataRequest.getJSONArray("detailData").getJSONObject(a).getInt("plannerId"));
//		    	    	}
//		    	    	else
//		    	    	{
//		    	    		dataPlanner.setPlannerId(id);
//		    	    	}
//		    			
//		    			dataPlanner.setProjectId(dataRequest.getJSONArray("detailData").getJSONObject(a).getInt("projectId"));
//		    			dataPlanner.setProjectDetailId(dataRequest.getJSONArray("detailData").getJSONObject(a).getInt("projectDetailId"));
//		    			dataPlanner.setDatePlanner(formatter1.parse(dataRequest.getJSONArray("detailData").getJSONObject(a).getString("datePlanner")));
//		    			if(dataRequest.getJSONArray("detailData").getJSONObject(a).getString("toPlanner").isEmpty() == false)
//		    			{
//		    				dataPlanner.setTo_planner(Time.valueOf(dataRequest.getJSONArray("detailData").getJSONObject(a).getString("toPlanner")));
//		    			}
//		    			
//		    			
//		    			if (!dataRequest.getJSONArray("detailData").getJSONObject(a).get("plannedHour").equals(null))
//		    			{
//		    				dataPlanner.setPlannedHour(Float.valueOf((float) dataRequest.getJSONArray("detailData").getJSONObject(a).getDouble("plannedHour")));
//		    			}
//			
//		    			if(!dataRequest.getJSONArray("detailData").getJSONObject(a).get("overtimeHour").equals(null))
//		    			{
//		    				dataPlanner.setOvertime_hour(Float.valueOf((float) dataRequest.getJSONArray("detailData").getJSONObject(a).getDouble("overtimeHour")));
//		    			}
//		    			
//		    			if(!dataRequest.getJSONArray("detailData").getJSONObject(a).get("requestBudgetHour").equals(null))
//		    			{
//		    				dataPlanner.setRequest_budget_hour(Float.valueOf((float) dataRequest.getJSONArray("detailData").getJSONObject(a).getDouble("requestBudgetHour")));
//		    			}
//		    			
//		    			if(dataRequest.getJSONArray("detailData").getJSONObject(a).getString("notePlanner").isEmpty() == false)
//		    			{
//		    				dataPlanner.setNotePlanner(dataRequest.getJSONArray("detailData").getJSONObject(a).getString("notePlanner"));
//		    			}
//		    			
//		    			if(dataRequest.getJSONArray("detailData").getJSONObject(a).getString("locationPlanner").isEmpty() == false)
//		    			{
//		    				dataPlanner.setLocationPlanner(dataRequest.getJSONArray("detailData").getJSONObject(a).getString("locationPlanner"));
//		    			}
//		    			
//		    			if(dataRequest.getJSONArray("detailData").getJSONObject(a).getString("fromPlanner").isEmpty() == false)
//		    			{
//		    				dataPlanner.setFromplanner(Time.valueOf(dataRequest.getJSONArray("detailData").getJSONObject(a).getString("fromPlanner")));
//		    			}
//		    			dataPlanner.setCompanyId(dataRequest.getInt("company_id"));
//		    			dataPlanner.setContactId(UUID.fromString(dataRequest.getJSONArray("detailData").getJSONObject(a).getString("contactId")));
//		    			
//		    			logger.info("dataPlanner = " + dataPlanner.getTo_planner());
//		    			isiPlanner.add(dataPlanner);
//			    	}
//			    }
//		    	PlannerService.save(isiPlanner);
		     	response.put("responseCode", "00");
			      response.put("responseDesc", "Save Planner Success");
		    }
		    catch(Exception e)
		    {
		    	 response.put("responseCode", "99");
			      response.put("responseDesc", "Save Planner Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		   return response.toString();
	}
	
	@PostMapping("/savePlannerAlone")
	public String savePlannerAlone(@RequestBody String request,@RequestHeader(value = "User-Access") String header)
	{
		 JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    ArrayList<TmPlannerAloneNote> isiPlannerNote = new ArrayList<>();
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    UUID user_uuid = UUID.fromString(getUserId(header));
		    
		    LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
	        DateTimeFormatter jam = DateTimeFormatter.ofPattern("HH:mm:ss");
		    logger.info("input >>>> " + dataRequest);
		    logger.info("dataHeader >>> " + header);
		    try {
		    	TmPlannerAlone data = new TmPlannerAlone();
    	    	int id = 0;
    	    	if(dataRequest.has("plannerAloneId")) {
    	    		id = dataRequest.getInt("plannerAloneId");
    	    	}
    	    	else
    	    	{
    	    		List<Object[]> dataNext = TmPlannerAloneService.nextval();
    	    		id = Integer.parseInt(String.valueOf(dataNext.get(0)));
    	    		
    	    	}
    	    	data.setPlannerAloneId(id);
    	    	data.setPartyId(dataRequest.getInt("partyId"));
    	    	data.setTaskId(dataRequest.getInt("taskId"));
    	    	data.setDatePlannerInternal(formatter1.parse(dataRequest.getString("datePlannerInternal")));
    	    	data.setStartHour(Time.valueOf(dataRequest.getString("startHour")));
    	    	data.setEndHour(Time.valueOf(dataRequest.getString("endHour")));
    	    	data.setContactId(UUID.fromString(dataRequest.getString("contactId")));
    	    	data.setCompanyId(dataRequest.getInt("company_id"));
    	    	data.setBuilding(dataRequest.getString("building"));
    	    	data.setAddress(dataRequest.getString("address"));
    	    	data.setStatus(dataRequest.getInt("status"));
    	    	if(!dataRequest.get("plannedHour").equals(null))
    	    	{
    	    		data.setPlannedHour(Float.valueOf(dataRequest.getString("plannedHour")));
    	    	}
    	    	
    	    	if(!dataRequest.get("overtimeHour").equals(null))
    	    	{
    	    		data.setOvertimeHour(Float.valueOf(dataRequest.getString("overtimeHour")));
    	    	}
    	    	
    	    	if(dataRequest.getJSONArray("detailNote").size()>0)
    	    	{
    	    		for(int a = 0 ; a < dataRequest.getJSONArray("detailNote").size(); a++)
			    	{
    	    			TmPlannerAloneNote dataNote = new TmPlannerAloneNote();
    	    			dataNote.setPlannerAloneId(id);
    	    			dataNote.setPlannerAloneNoteId(dataRequest.getJSONArray("detailNote").getJSONObject(a).getInt("plannerNoteId"));
    	    			dataNote.setCompanyId(dataRequest.getInt("company_id"));
    	    			dataNote.setNote(dataRequest.getJSONArray("detailNote").getJSONObject(a).getString("note"));
    	    			dataNote.setDateNote(localDateTime2);
    	    			isiPlannerNote.add(dataNote);
			    	}
    	    	}
    	    	
    	    	GabunganService.savePlannerAlone(data, isiPlannerNote);
    	    	
    	    	
    	    	
		    	response.put("responseCode", "00");
			    response.put("responseDesc", "Save Planner Alone Success");
		    }
		    catch(Exception e)
		    {
		    	response.put("responseCode", "99");
			      response.put("responseDesc", "Save Planner Alone Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
	}
	
	@PostMapping("/updateStatusPlanner")
	public String updateStatusPlanner(@RequestBody String request,@RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    ArrayList<Planner> isiPlanner = new ArrayList<>();
	    ArrayList<PlannerNote> isiPlannerNote = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    UUID user_uuid = UUID.fromString(getUserId(header));
	    
	    LocalDateTime localDateTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
        String formatDateTime = localDateTime.format(formatter);
        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
        logger.info("INPUT #### " + dataRequest);
        logger.info("FUNCTION #### updateStatusPlanner()");
        try {
        	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        	Date date = new Date();
        	date=formatter1.parse(dataRequest.getString("date_budgethour_timeframe_re"));
        	Optional <Planner> dataUpdate2 = PlannerService.UpdatePlanner(dataRequest.getInt("plannerId"), dataRequest.getInt("projectId"),dataRequest.getInt("projectDetailId"),  dataRequest.getInt("timeframeId"),  dataRequest.getInt("company_id"));
        	if(dataUpdate2.isPresent())
        	{
        		((Planner)dataUpdate2.get()).setStatus(dataRequest.getInt("status"));
        	}
        	PlannerService.save(dataUpdate2.get());
        	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Status Planner Success");
        }
        catch(Exception e)
        {
        	 response.put("responseCode", "99");
		      response.put("responseDesc", "Update Status Planner Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
        }
        return response.toString();
	}
	
	@PostMapping("/updateStatusPlannerAlone")
	public String updateStatusPlannerAlone(@RequestBody String request,@RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    ArrayList<Planner> isiPlanner = new ArrayList<>();
	    ArrayList<PlannerNote> isiPlannerNote = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    UUID user_uuid = UUID.fromString(getUserId(header));
	    
	    LocalDateTime localDateTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
        String formatDateTime = localDateTime.format(formatter);
        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
        logger.info("INPUT #### " + dataRequest);
        logger.info("FUNCTION #### updateStatusPlannerAlone()");
        try {
        	SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        	Date date = new Date();
//        	date=formatter1.parse(dataRequest.getString("date_budgethour_timeframe_re"));
        	Optional <TmPlannerAlone> dataUpdate2 = TmPlannerAloneService.UpdatePlannerAlone(dataRequest.getInt("plannerAloneId"), dataRequest.getInt("company_id"));
        	if(dataUpdate2.isPresent())
        	{
        		((TmPlannerAlone)dataUpdate2.get()).setStatus(dataRequest.getInt("status"));
        	}
        	TmPlannerAloneService.save(dataUpdate2.get());
        	
        	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Status Planner Success");
        }
        catch(Exception e)
        {
        	 response.put("responseCode", "99");
		      response.put("responseDesc", "Update Status Planner Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
        }
        return response.toString();
	}
	
	public String getUserId(String header )
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}
}
