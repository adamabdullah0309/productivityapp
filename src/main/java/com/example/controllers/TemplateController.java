package com.example.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.client.RestTemplate;

import com.example.service.ListDocumentSummaryService;
import com.example.service.ListDocumentSummarySubcategoryService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.service.*;
import com.example.service.ListDocumentSummaryService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/api")
public class TemplateController {
	private static final Logger logger = LoggerFactory.getLogger(CobaController.class);
	
	@Autowired
	private ListDocumentSummarySubcategoryService ListDocumentSummarySubcategoryService;
	
	@Autowired
	private ListDocumentSummaryService ListDocumentSummaryService;
	
	@Autowired
	private GabunganService GabunganService;
	
	@Autowired
	private GroupingService GroupingService;
	
	@Autowired
	private TemplateService TemplateService;
	List<JSONObject> template = new ArrayList<>();
	@PostMapping("/listTemplate2/{paramAktif}")
	public String v2listTemplate(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		template = new ArrayList<>();
		a11 = 1000000;
		JSONObject response = new JSONObject();
	   
	    List<JSONObject> contaa = new ArrayList<>();
	    List<JSONObject> Akhir = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try
	    {
	    	final11 = new ArrayList<>();
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> data1 = TemplateService.getSubgroup(1, dataRequest.getInt("company_id") );
	    	List<Object[]> data22 = TemplateService.getHeaderTemplate(dataRequest.getInt("company_id"), user_uuid, paramAktif);
	    	data22.stream().forEach(col9->{
	    		
	    		JSONObject dataNew = new JSONObject();
	    		dataNew.put("templateId", col9[0]);
	    		dataNew.put("templateNama", col9[1]);
	    		dataNew.put("templateDescription", col9[2] != null ? col9[2].toString() : "");
	    		dataNew.put("templateStatus", col9[3]);
	    		dataNew.put("company_id", col9[4]);
	    		
	    		
	    		
	    		List<JSONObject> detail1 = new ArrayList<>();
	    		detail1 = callDetailTemplate(Integer.valueOf(col9[0].toString()), dataRequest.getInt("company_id"));
//	    		List<Object[]> data1 = TemplateService.getDetailTemplate(Integer.valueOf(col9[0].toString()), dataRequest.getInt("company_id") );
	    		dataNew.put("detail", detail1);
	    		template.add(dataNew);
	    	});
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Template Success");
		    response.put("responseData", template);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Template Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	private List<JSONObject> callDetailTemplate(Integer id, Integer company) {
		// TODO Auto-generated method stub

		List<JSONObject> contaa = new ArrayList<>();
		List<Object[]> call = TemplateService.callDetailTemplate(id, company);
		call.stream().forEach(col->{
			List<JSONObject> cioba = new ArrayList<>();
			JSONObject data = new JSONObject();
			data.put("nourut", col[0]);
			data.put("groupingId", col[2] == null ? "" : col[2]);
			data.put("taskId", col[3] == null ? "" : col[3]);
			data.put("company_id", col[4]);
			
			JSONObject nama = new JSONObject();
			nama.put("name", col[5]);
			data.put("data", nama);
			if(col[2] != null)
			{
				cioba = callChild(data);
			}
			data.put("children", cioba);
			contaa.add(data);
		});
		return contaa;
	}

	private List<JSONObject> callChild(JSONObject dataNew) {
		// TODO Auto-generated method stub
		JSONObject dataNew1 = new JSONObject();
		List<JSONObject> contaa = new ArrayList<>();
		if(dataNew.get("groupingId") != null)
		{
			List<Object[]> data22 = GroupingService.getDetailSubgroup(dataNew.getInt("company_id"),dataNew.getInt("groupingId"));
			if(data22.size()>0)
			{
				data22.stream().forEach(col->{
					List<JSONObject> cioba = new ArrayList<>();
					JSONObject data = new JSONObject();
					data.put("nourut", col[0]);
					data.put("groupingId", col[2] == null ? "" : col[2]);
					data.put("taskId", col[3] == null ? "" : col[3]);
					data.put("company_id", col[4]);
					
					JSONObject nama = new JSONObject();
					nama.put("name", col[5]);
					data.put("data", nama);
					if(col[2] != null)
					{
						cioba = callChild(data);
					}
					data.put("children", cioba);
					contaa.add(data);
//					dataNew = data2;
				});
			}
			
		}
		return contaa;
	}

	int a11 = 1000000;
	@PostMapping("/listTemplate/{paramAktif}")
	public String listTemplate(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		a11 = 1000000;
		JSONObject response = new JSONObject();
	   
	    List<JSONObject> contaa = new ArrayList<>();
	    List<JSONObject> Akhir = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try
	    {
	    	final11 = new ArrayList<>();
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> data1 = TemplateService.getSubgroup(1, dataRequest.getInt("company_id") );
	    	List<Object[]> data22 = TemplateService.getHeaderTemplate(dataRequest.getInt("company_id"), user_uuid, paramAktif);
	    	data22.stream().forEach(col9->{
	    		
	    		JSONObject dataNew = new JSONObject();
	    		dataNew.put("templateId", col9[0]);
	    		dataNew.put("templateNama", col9[1]);
	    		dataNew.put("templateDescription", col9[2] != null ? col9[2].toString() : "");
	    		dataNew.put("templateStatus", col9[3]);
	    		dataNew.put("company_id", col9[4]);
	    		
	    		//---
	    		
	    		List<JSONObject> cont = new ArrayList<>();
		    	data1.stream().forEach(col1->{
		    		if(col1[3].equals(col9[4]) && col1[5].equals(col9[0]) )
		    		{
		    			JSONObject data = new JSONObject();
		    			data.put("id",col1[1]);
			    		data.put("childGroupId",col1[0] != null ? col1[0] : a11);
			    		data.put("childtaskId",col1[2] != null ? col1[2] : a11);
		    			data.put("company_id", col1[3]);
		    			JSONObject data2 = new JSONObject();
			            data2.put("name", col1[4]);
		    			data.put("data", data2);
		    			cont.add(data);
		    		}
		    			a11 = a11 + 1;
		    	});
		    	logger.info(" col94 == " + col9[4]);
		    	logger.info(" col90 == " + col9[0]);
		    	JSONObject dataRequestLevel = new JSONObject ();
		    	if(cont.size()>0)
		    	{
		    		String level = "";
	    			JSONObject data2 = new JSONObject();

	    			ArrayList<ChildGrouping> pairs = new ArrayList<ChildGrouping>();
		    		for(int a = 0 ; a<cont.size();a++) {
		    			pairs.add(new ChildGrouping(cont.get(a).getInt("childGroupId") , cont.get(a).getInt("id"), cont.get(a).getInt("childtaskId"),
		    					cont.get(a).getInt("company_id"), cont.get(a).getJSONObject("data")
		    					)
		    					
		    					);
		    		}
		    		
		    		Map<Integer, ParentGrouping> hm = new HashMap<>();
		    			
	    			for(ChildGrouping p:pairs){
			            //  ----- Child -----
	    				ParentGrouping mmdChild ;
			            if(hm.containsKey(p.getChildId())){
			                mmdChild = hm.get(p.getChildId());
			            }
			            else{
			                mmdChild = new ParentGrouping();
			                hm.put(p.getChildId(), mmdChild);
			            }
			            
			            
			            
			            mmdChild.setId(p.getChildId());
			            mmdChild.setParentId(p.getParentId());
			            mmdChild.setTaskId(p.getTaskId());
			            mmdChild.setCompany_id(p.getCompany_id());
			            mmdChild.setData(p.getData());
			            // no need to set ChildrenItems list because the constructor created a new empty list

			            if(mmdChild.getId() >= 1000000)
			            {
			            	mmdChild.setId(0);
			            }
			            
			            if(mmdChild.getTaskId() >= 1000000)
			            {
			            	mmdChild.setTaskId(0);
			            }

			            // ------ Parent ----
			            ParentGrouping mmdParent ;
			            if(hm.containsKey(p.getParentId())){
			                mmdParent = hm.get(p.getParentId());
			            }
			            else{
			                mmdParent = new ParentGrouping();
			                hm.put(p.getParentId(),mmdParent);
			            }
			            mmdParent.setId(p.getParentId());
			            mmdParent.setParentId(null);
			            mmdParent.setTaskId(p.getTaskId());
			            mmdParent.setCompany_id(p.getCompany_id());
			            mmdParent.setData(p.getData());
			            mmdParent.addChildren(mmdChild);
			            
			            if(mmdParent.getId() >= 1000000)
			            {
			            	mmdParent.setId(0);
			            }
			            
			            if(mmdParent.getTaskId() >= 1000000)
			            {
			            	mmdParent.setTaskId(0);
			            }
			        }
		    			
	    			List<ParentGrouping> DX = new ArrayList<ParentGrouping>(); 
			        for(ParentGrouping mmd : hm.values()){
			            if(mmd.getParentId() == null)
			                DX.add(mmd);
			        }

			        // Print 
			        for(ParentGrouping mmd: DX){
			            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
			            JSONObject data = new JSONObject();
			            
			            data.put("data",mmd);
			            contaa.add(data);
			        }
			        
			        for(int z= 0 ; z< contaa.size() ; z++)
			        {
			        	level = contaa.get(z).getString("data");
			        	logger.info("cont = " + contaa.get(z).getString("data"));
			        }
			        dataRequestLevel = (JSONObject)JSONSerializer.toJSON(level);
		    	}
	    		//---
	    		dataNew.put("detail",dataRequestLevel.get("children"));
	    		Akhir.add(dataNew);
	    	});
	    	
//	    	Akhir.stream().forEach(col->{
//	    		if(col.getJSONArray("detail").size() >0)
//	    		{
//	    			hasChild(col.getJSONArray("detail"));
//	    		}
//	    	});
	    	
	    	
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Template Success");
		    response.put("responseData",Akhir);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Template Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
		return response.toString();
	}
	

	
	@PostMapping("/saveTemplate")
	public String saveTemplate(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TemplateDetail> detail = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	
	    	Template dataHeader = new Template();
	    	List<Object[]> dataNext = TemplateService.nextval();
	    	int id = 0;
	    	if(dataNext.size()>0)
	    	{
	    		id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	}
	    	dataHeader.setTemplateId(id);;
	    	dataHeader.setTemplateNama(dataRequest.getString("templateNama"));
	    	dataHeader.setTemplateDescription(dataRequest.getString("templateDescription"));
	    	dataHeader.setTemplateStatus(dataRequest.getInt("templateStatus"));
	    	dataHeader.setCompanyId(dataRequest.getInt("company_id"));
	    	dataHeader.setCreatedBy(user_uuid);
	    	dataHeader.setCreatedAt(localDateTime2);
	    	
	    	if(dataRequest.getJSONArray("detail").size() > 0)
		    {
	    		for(int a = 0 ; a < dataRequest.getJSONArray("detail").size(); a++)
		    	{
//	    			Integer nourut, int mGroupingId, int companyId, int groupingChildId, int taskChildId
	    			TemplateDetail dataDetail = new TemplateDetail(
	    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("nourut"),
	    					id,
	    					dataRequest.getInt("company_id"),
	    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MGroupingId") != 0 ? dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MGroupingId") : null,
	    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MTaskId") != 0 ? dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MTaskId") : null
	    					);
	    			detail.add(dataDetail);
		    	}
		    }
	    	GabunganService.saveTemplate(dataHeader, detail);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Template Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Template Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	@PostMapping("/updateTemplate")
	public String updateTemplate(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TemplateDetail> detail = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        Optional<Template>  dataDetailGroupHeader = TemplateService.detailTemplateHeader(dataRequest.getInt("templateId"), dataRequest.getInt("company_id"));
	    	if(dataDetailGroupHeader.isPresent())
	    	{
	    		((Template)dataDetailGroupHeader.get()).setTemplateNama(dataRequest.getString("templateNama"));
	    		((Template)dataDetailGroupHeader.get()).setTemplateDescription(dataRequest.getString("templateDescription"));
	    		((Template)dataDetailGroupHeader.get()).setTemplateStatus(dataRequest.getInt("templateStatus"));
	    		((Template)dataDetailGroupHeader.get()).setUpdatedBy(user_uuid);
	    		((Template)dataDetailGroupHeader.get()).setUpdatedAt(localDateTime2);
	    		
	    		if(dataRequest.getJSONArray("detail").size() > 0)
			    {
		    		for(int a = 0 ; a < dataRequest.getJSONArray("detail").size(); a++)
			    	{
//		    			Integer nourut, int mGroupingId, int companyId, int groupingChildId, int taskChildId
		    			TemplateDetail dataDetail = new TemplateDetail(
		    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("nourut"),
		    					dataRequest.getInt("templateId"),
		    					dataRequest.getInt("company_id"),
		    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MGroupingId") != 0 ? dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MGroupingId") : null,
		    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MTaskId") != 0 ? dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MTaskId") : null
		    					);
		    			detail.add(dataDetail);
			    	}
			    }
	    		GabunganService.updateTemplate(dataDetailGroupHeader.get(), detail);
	    	}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Grouping Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Update Grouping Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/detailTemplate")
	public String detailTemplate(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	Optional<Template>  dataDetailGroupHeader = TemplateService.detailTemplateHeader(dataRequest.getInt("templateId"), dataRequest.getInt("company_id"));
	    	if(dataDetailGroupHeader.isPresent())
	    	{
	    		JSONObject data = new JSONObject();
	    		data.put("templateId", dataDetailGroupHeader.get().getTemplateId());
	    		data.put("templateNama", dataDetailGroupHeader.get().getTemplateNama());
	    		data.put("templateStatus", dataDetailGroupHeader.get().getTemplateStatus());
	    		data.put("templateDescription", dataDetailGroupHeader.get().getTemplateDescription() != null ? dataDetailGroupHeader.get().getTemplateDescription() : "");
	    		List<JSONObject> detail = new ArrayList<>();
	    		List<Object[]> dataDetail = TemplateService.detailTemplateDetail(dataRequest.getInt("templateId"), dataRequest.getInt("company_id"));
	    		if(dataDetail.size() > 0) {
	    			dataDetail.stream().forEach(col->{
	    				JSONObject data2 = new JSONObject();
	    				data2.put("nourut", col[0]);
	    				data2.put("templateId", col[1]);
	    				data2.put("company_id", col[2]);
	    				data2.put("MGroupingId", col[3] != null ? col[3] : "");
	    				data2.put("MGroupingNama", col[5] != null ? col[5].toString() : "");
	    				data2.put("MTaskId", col[4] != null ? col[4] : "");
	    				data2.put("MTaskNama", col[6] != null ? col[6] : "");
	    				detail.add(data2);
	    			});
	    			
	    		}
	    		data.put("detail",detail);
	    		cont.add(data);
	    	}
	    	 response.put("responseCode", "00");
		      response.put("responseDesc", "Show Detail Template Success");
		      response.put("responseData",cont);
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		      response.put("responseDesc", "Show Detail Template Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	 }
	
	public String getUserId(String header )
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}
}
