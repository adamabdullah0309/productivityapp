package com.example.controllers;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.helper.Helper;
import com.example.service.*;
import com.example.service.TimesheetService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/api")
public class TimesheetController {
	private static final Logger logger = LoggerFactory.getLogger(TimesheetController.class);
	Object data;
	
	@Autowired
	private TimesheetService TimesheetService;
	
	@Autowired
	private GabunganService GabunganService;
	
	@Autowired
	private TmProjectTimeframeService TmProjectTimeframeService;
	
	@PostMapping({"/listDetailTimesheet"})
	public String listDetailTimesheet(@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();

	    List<JSONObject> DetailNote = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	        
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	        List<Object[]> listContent = TimesheetService.listDetailTimesheet(dataRequest.getInt("timesheetId"),dataRequest.getInt("company_id"),user_uuid);
	        listContent.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	    		data.put("timesheetId",col[0] == null ? "" : col[0]);
	    		data.put("timesheetDate",col[1] == null ? "" : col[1].toString());
	    		data.put("timesheetFrom",col[2] == null ? "" : col[2].toString());
	    		data.put("timesheetTo",col[3] == null ? "" : col[3].toString());
	    		data.put("timesheetParty",col[4] == null ? "" : col[4]);
	    		data.put("timesheetProjectDetailId",col[5] == null ? "" : col[5]);
	    		data.put("timesheetProjectId",col[6] == null ? "" : col[6]);
	    		data.put("timesheetCompanyId",col[7] == null ? "" : col[7]);
	    		data.put("timesheetBuilding",col[8] == null ? "" : col[8]);
	    		data.put("timesheetAddress",col[9] == null ? "" : col[9]);
	    		data.put("timesheetStatus",col[10] == null ? "" : col[10]);
	    		data.put("timesheetOvertime",col[11] == null ? "" : col[11]);
	    		data.put("timesheetActual",col[12] == null ? "" : col[12]);
	    		data.put("timesheetContactId",col[13] == null ? "" : col[13]);
	    		data.put("taskId",col[14] == null ? "" : col[14]);
	    		data.put("taskNama",col[15] == null ? "" : col[15]);
	    		data.put("groupingId",col[16] == null ? "" : col[16]);
	    		data.put("groupingNama",col[17] == null ? "" : col[17]);
	    		data.put("timesheetNoteId",col[18] == null ? "" : col[18]);
	    		data.put("note",col[19] == null ? "" : col[19]);
	    		data.put("dateNote",col[20] == null ? "" : col[20].toString());
	    		data.put("timesheetRequestBudget",col[21] == null ? "" : col[21]);
	    		cont.add(data);
	    		
	    		JSONObject data2 = new JSONObject();
	    		data2.put("timesheetId",col[0] == null ? "" : col[0]);
	    		data2.put("timesheetDate",col[1] == null ? "" : col[1].toString());
	    		data2.put("timesheetFrom",col[2] == null ? "" : col[2].toString());
	    		data2.put("timesheetTo",col[3] == null ? "" : col[3].toString());
	    		data2.put("timesheetParty",col[4] == null ? "" : col[4]);
	    		data2.put("timesheetProjectDetailId",col[5] == null ? "" : col[5]);
	    		data2.put("timesheetProjectId",col[6] == null ? "" : col[6]);
	    		data2.put("timesheetCompanyId",col[7] == null ? "" : col[7]);
	    		data2.put("timesheetBuilding",col[8] == null ? "" : col[8]);
	    		data2.put("timesheetAddress",col[9] == null ? "" : col[9]);
	    		data2.put("timesheetStatus",col[10] == null ? "" : col[10]);
	    		data2.put("timesheetOvertime",col[11] == null ? "" : col[11]);
	    		data2.put("timesheetActual",col[12] == null ? "" : col[12]);
	    		data2.put("timesheetContactId",col[13] == null ? "" : col[13]);
	    		data2.put("taskId",col[14] == null ? "" : col[14]);
	    		data2.put("taskNama",col[15] == null ? "" : col[15]);
	    		data2.put("groupingId",col[16] == null ? "" : col[16]);
	    		data2.put("groupingNama",col[17] == null ? "" : col[17]);
	    		data2.put("timesheetNoteId",col[18] == null ? "" : col[18]);
	    		data2.put("note",col[19] == null ? "" : col[19]);
	    		data2.put("dateNote",col[20] == null ? "" : col[20].toString());
	    		data2.put("timesheetRequestBudget",col[21] == null ? "" : col[21]);
	    		DetailNote.add(data2);
	        });
	        
	      //untuk menghapus duplikat
	   		HashSet<Object> seenAtt= new HashSet<>();
	   		cont.removeIf(e -> !seenAtt.add(Arrays.asList(e.getInt("timesheetCompanyId"), e.getInt("timesheetId") )));
	   		
	   		cont.stream().forEach(col->{
	   			List<JSONObject> detail = new ArrayList<>();
	   			DetailNote.stream().forEach(col2->{
	   				if(col.getInt("timesheetCompanyId") == col2.getInt("timesheetCompanyId") && col.getInt("timesheetId") == col2.getInt("timesheetId") )
	   				{
	   					JSONObject data = new JSONObject();
	   					data.put("timesheetNoteId", col2.get("timesheetNoteId"));
	   					data.put("note", col2.get("note"));
	   					data.put("dateNote", col2.get("dateNote"));
	   					detail.add(data);
	   				}
	   			});
	   			col.put("detail", detail);
	   		});
	   		
	        
	        response.put("responseCode", "00");
		      response.put("responseDesc", "Show Detail Timesheet Success");
		      response.put("responseData", cont);
	    }
	    catch (Exception e) 
	    {
	      response.put("responseCode", "99");
	      response.put("responseDesc", "Show Detail Timesheet Failed");
	      response.put("responseError", e.getMessage());
	      logger.info("ERROR #### " + e.getMessage());
	    } 
	    return response.toString();
	}
	
	
	@PostMapping("/listTimeframe/{paramAktif}")
	public String listTimeframe(@PathVariable("paramAktif") String paramAktif, @RequestBody String request,@RequestHeader(value = "User-Access") String header)
	{
		 JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    ArrayList<TimesheetNote> isiPlannerNote = new ArrayList<>();
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    UUID user_uuid = UUID.fromString(getUserId(header));
		    
		    LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
	        DateTimeFormatter jam = DateTimeFormatter.ofPattern("HH:mm:ss");
	        
	        
		    logger.info("input >>>> " + dataRequest);
		    logger.info("dataHeader >>> " + header);
		    logger.info("call listTimeframe() >>> ");
		    try {
		    	List<Object[]> listTimeframe = TimesheetService.listTimeframe(paramAktif, dataRequest.getInt("company_id"),user_uuid, dataRequest.getString("startDate"), dataRequest.getString("endDate"));
		    	listTimeframe.stream().forEach(col->{
		    		JSONObject data = new JSONObject();
		    		data.put("dateBudgetHour", col[0] == null ? "" : col[0].toString());
		    		data.put("projectId", col[1] == null ? "" : col[1]);
		    		data.put("projectNama", col[2] == null ? "" : col[2]);
		    		data.put("projectDetailId", col[3] == null ? "" : col[3]);
		    		data.put("taskId", col[4] == null ? "" : col[4]);
		    		data.put("groupingId", col[5] == null ? "" : col[5]);
		    		data.put("task", col[6] == null ? "" : col[6]);
		    		data.put("budgetHour", col[7] == null ? "" : col[7]);
		    		data.put("contactId", col[8] == null ? "" : col[8]);
		    		data.put("timeframeId", col[9] == null ? "" : col[9]);
		    		data.put("companyId", col[10] == null ? "" : col[10]);
		    		data.put("projectParty", col[11] == null ? "" : col[11]);
		    		cont.add(data);
		    	});
		    	  response.put("responseCode", "00");
			      response.put("responseDesc", "Show List Timeframe Success");
			      response.put("responseData", cont);
		    }
		    catch (Exception e)
		    {
		      response.put("responseCode", "99");
		      response.put("responseDesc", "Show List Timeframe Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
	}
	
	@PostMapping("/listTimesheet/{paramAktif}")
	public String listTimesheet(@PathVariable("paramAktif") String paramAktif, @RequestBody String request,@RequestHeader(value = "User-Access") String header)
	{
		 JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    ArrayList<TimesheetNote> isiPlannerNote = new ArrayList<>();
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    UUID user_uuid = UUID.fromString(getUserId(header));
		    
		    LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
	        DateTimeFormatter jam = DateTimeFormatter.ofPattern("HH:mm:ss");
	        
	        
		    logger.info("input >>>> " + dataRequest);
		    logger.info("dataHeader >>> " + header);
		    logger.info("call listTimesheet() >>> ");
		    try {
		    	
		    	List<Object[]> listTimesheet = TimesheetService.listTimesheet(paramAktif, dataRequest.getInt("company_id"),user_uuid, dataRequest.getString("startDate"), dataRequest.getString("endDate"));
		    	listTimesheet.stream().forEach(col->{
		    		JSONObject data = new JSONObject();
		    		data.put("timesheetId",col[0] == null ? "" : col[0]);
		    		data.put("timesheetDate",col[1] == null ? "" : col[1].toString());
		    		data.put("timesheetFrom",col[2] == null ? "" : col[2].toString());
		    		data.put("timesheetTo",col[3] == null ? "" : col[3].toString());
		    		data.put("timesheetParty",col[4] == null ? "" : col[4]);
		    		data.put("timesheetProjectDetailId",col[5] == null ? "" : col[5]);
		    		data.put("timesheetProjectId",col[6] == null ? "" : col[6]);
		    		data.put("timesheetCompanyId",col[7] == null ? "" : col[7]);
		    		data.put("timesheetBuilding",col[8] == null ? "" : col[8]);
		    		data.put("timesheetAddress",col[9] == null ? "" : col[9]);
		    		data.put("timesheetStatus",col[10] == null ? "" : col[10]);
		    		data.put("timesheetOvertime",col[11] == null ? "" : col[11]);
		    		data.put("timesheetActual",col[12] == null ? "" : col[12]);
		    		data.put("timesheetContactId",col[13] == null ? "" : col[13]);
		    		data.put("taskId",col[14] == null ? "" : col[14]);
		    		data.put("taskNama",col[15] == null ? "" : col[15]);
		    		data.put("groupingId",col[16] == null ? "" : col[16]);
		    		data.put("groupingNama",col[17] == null ? "" : col[17]);
		    		data.put("timesheetRequestBudget",col[18] == null ? "" : col[18]);
		    		data.put("projectName", col[19] == null ? "" : col[19]);
		    		cont.add(data);
		    	});
		    	response.put("responseCode", "00");
			    response.put("responseDesc", "Show List Timesheet Success");
			    response.put("responseData", cont);
		    }
		    catch(Exception e)
		    {
		    	response.put("responseCode", "99");
			      response.put("responseDesc", "Show List Timesheet Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
	}
	Float actual = null;
	Float actualSemua = null;
	Float overtime = null;
	Float requestbudget = null;
	Float timeframe = null;
	@PostMapping("/saveTimesheet")
	public String saveTimesheet(@RequestBody String request,@RequestHeader(value = "User-Access") String header)
	{
		 JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    ArrayList<TimesheetNote> isiPlannerNote = new ArrayList<>();
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    UUID user_uuid = UUID.fromString(getUserId(header));
		    
		    LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        actual = 0.0f;
	    	overtime = 0.0f;
	    	requestbudget = 0.0f;
	    	timeframe = 0.0f;
	    	actualSemua = 0.0f;
	        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
	        DateTimeFormatter jam = DateTimeFormatter.ofPattern("HH:mm:ss");
		    logger.info("input >>>> " + dataRequest);
		    logger.info("dataHeader >>> " + header);
		    try {
		    	Timesheet data = new Timesheet();
    	    	int id = 0;
    	    	if(dataRequest.has("timesheetId")) {
    	    		if(dataRequest.has("timesheetProjectDetailId"))
        	    	{
        	    		List<Object[]> dataJam = TimesheetService.listTimesheetId(dataRequest.getInt("timesheetProjectId"), dataRequest.getInt("timesheetProjectDetailId"), user_uuid,  dataRequest.getInt("timesheetCompanyId"), dataRequest.getInt("timesheetId"));
        	    		logger.info("dataJam = " + dataJam.size());
        	    		dataJam.stream().forEach(col->{
        	    			actual = actual + Float.parseFloat(String.valueOf(col[0] == null ? 0 : col[0]));
        	    			overtime = overtime + Float.parseFloat(String.valueOf(col[1] == null ? 0 : col[1]));
        	    			requestbudget = requestbudget + Float.parseFloat(String.valueOf(col[2] == null ? 0 : col[2]));
        	    		});
        	    		timeframe = actual + overtime;
        	    	}
    	    		id = dataRequest.getInt("timesheetId");
    	    	}
    	    	else
    	    	{
    	    		if(dataRequest.has("timesheetProjectDetailId"))
        	    	{
        	    		List<Object[]> dataJam = TimesheetService.listTimesheet(dataRequest.getInt("timesheetProjectId"), dataRequest.getInt("timesheetProjectDetailId"), user_uuid,  dataRequest.getInt("timesheetCompanyId"));
        	    		logger.info("dataJam = " + dataJam.size());
        	    		dataJam.stream().forEach(col->{
        	    			actual = actual + Float.parseFloat(String.valueOf(col[0] == null ? 0 : col[0]));
        	    			overtime = overtime + Float.parseFloat(String.valueOf(col[1] == null ? 0 : col[1]));
        	    			requestbudget = requestbudget + Float.parseFloat(String.valueOf(col[2] == null ? 0 : col[2]));
        	    		});
        	    		timeframe = actual + overtime;
        	    	}
    	    		List<Object[]> dataNext = TimesheetService.nextval();
    	    		id = Integer.parseInt(String.valueOf(dataNext.get(0)));
    	    		
    	    	}
    	    	data.setTimesheetId(id);
    	    	data.setTimesheetDate(formatter1.parse(dataRequest.getString("timesheetDate")));
    	    	data.setTimesheetFrom(Time.valueOf(dataRequest.getString("timesheetFrom")));
    	    	data.setTimesheetTo(Time.valueOf(dataRequest.getString("timesheetTo")));
    	    	data.setTimesheetParty(dataRequest.getInt("timesheetParty"));
    	    	
    	    	if(dataRequest.has("timesheetProjectDetailId"))
    	    	{
    	    		actualSemua = 0.0f;
    	    		List<Object[]> dataa = TmProjectTimeframeService.getTimeframeDetail(dataRequest.getInt("timesheetProjectId"), dataRequest.getInt("timesheetProjectDetailId"), dataRequest.getInt("timesheetCompanyId"));
    	    		dataa.stream().forEach(col->{
    	    			actualSemua = actualSemua + Float.parseFloat(String.valueOf(col[4]));
    	    			
    	    		});
    	    		
    	    		JSONObject resultDiff = Helper.getdiff(dataRequest.getString("timesheetFrom") , dataRequest.getString("timesheetTo"), timeframe, actualSemua, dataRequest.getString("timesheetActual"));
    	    		logger.info("requestTime = " + resultDiff.get("requestTime"));
        			logger.info("actualTime = " + resultDiff.get("actualTime"));
        			logger.info("overtimeTime = " + resultDiff.get("overtimeTime"));
        			
    	    		data.setTimesheetOvertime(Float.valueOf(resultDiff.getString("overtimeTime")));
    	    		data.setTimesheetActual(Float.valueOf(resultDiff.getString("actualTime")));
    	    		data.setTimesheetRequestBudget(Float.valueOf(resultDiff.getString("requestTime")));

        	    	
    	    		data.setTimesheetProjectDetailId(dataRequest.getInt("timesheetProjectDetailId"));
    	    		data.setTimesheetProjectId(dataRequest.getInt("timesheetProjectId"));
    	    	}
    	    	else if (dataRequest.has("timesheetTaskId"))
    	    	{
    	    		data.setTimesheetTaskId(dataRequest.getInt("timesheetTaskId"));
    	    		
    	    		if(!dataRequest.getString("timesheetActual").equals(""))
	    			{
    	    			data.setTimesheetActual(Float.valueOf(dataRequest.getString("timesheetActual")));
	    			}
    	    	}
    	    	
    	    	data.setTimesheetCompanyId(dataRequest.getInt("timesheetCompanyId"));
    	    	data.setTimesheetBuilding(dataRequest.getString("timesheetBuilding"));
    	    	data.setTimesheetAddress(dataRequest.getString("timesheetAddress"));
    	    	data.setTimesheetStatus(dataRequest.getInt("timesheetStatus"));
    	    	
    	    	data.setTimesheetContactId(user_uuid);
    	    	
    	    	if(dataRequest.getJSONArray("detailNote").size()>0)
    	    	{
    	    		for(int a = 0 ; a < dataRequest.getJSONArray("detailNote").size(); a++)
			    	{
    	    			TimesheetNote dataNote = new TimesheetNote();
    	    			dataNote.setTimesheetId(id);
    	    			dataNote.setTimesheetNoteId(dataRequest.getJSONArray("detailNote").getJSONObject(a).getInt("timesheetNoteId"));
    	    			dataNote.setTimesheetCompanyId(dataRequest.getInt("timesheetCompanyId"));
    	    			dataNote.setNote(dataRequest.getJSONArray("detailNote").getJSONObject(a).getString("note"));
    	    			dataNote.setDateNote(localDateTime2);
    	    			isiPlannerNote.add(dataNote);
			    	}
    	    	}
    	    	
    	    	GabunganService.saveTimesheet(data, isiPlannerNote);
    	    	
    	    	
    	    	
		    	response.put("responseCode", "00");
			    response.put("responseDesc", "Save Timesheet Success");
		    }
		    catch(Exception e)
		    {
		    	response.put("responseCode", "99");
			      response.put("responseDesc", "Save Timesheet Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
	}
	
	@PostMapping("/updateTimesheetStage")
	public String updateTimesheetStage(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    logger.info("updateProcedureStage() >>> ");
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	
	    	Optional <Timesheet> dataUpdate2 = TimesheetService.findIdAndIdCompany(dataRequest.getInt("timesheetId"), dataRequest.getInt("company_id"));
        	if(dataUpdate2.isPresent())
        	{
        		((Timesheet)dataUpdate2.get()).setTimesheetStatus(dataRequest.getInt("timesheetStatus"));
        	}
        	TimesheetService.save(dataUpdate2.get()); 
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Timesheet Stage Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Update Timesheet Stage Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	public String getUserId(String header )
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}
}
