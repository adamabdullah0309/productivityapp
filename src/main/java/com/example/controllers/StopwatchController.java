package com.example.controllers;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.service.*;
import com.example.service.StopwatchService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/api")
public class StopwatchController {
	private static final Logger logger = LoggerFactory.getLogger(StopwatchController.class);
	Object data;
	
	@Autowired
	private StopwatchService StopwatchService;
	
	@PostMapping("/listStopwatch/{paramAktif}")
	public String listStopwatch(@PathVariable("paramAktif") String paramAktif,@RequestBody String request,@RequestHeader(value = "User-Access") String header)
	{
		 JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    UUID user_uuid = UUID.fromString(getUserId(header));
		    
		    LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
	        DateTimeFormatter jam = DateTimeFormatter.ofPattern("HH:mm:ss");
	        
	        
		    logger.info("input >>>> " + dataRequest);
		    logger.info("dataHeader >>> " + header);
		    logger.info("call listStopwatch() >>> ");
		    try {
		    	
		    	List<Object[]> listStopwatch = StopwatchService.listStopwatch(paramAktif, dataRequest.getInt("company_id"),user_uuid);
		    	listStopwatch.stream().forEach(col->{
		    		JSONObject data = new JSONObject();
		    		data.put("stopwatchId",col[0] == null ? "" : col[0]);
		    		data.put("stopwatchDate",col[1] == null ? "" : col[1].toString());
		    		data.put("stopwatchFrom",col[2] == null ? "" : col[2].toString());
		    		data.put("stopwatchTo",col[3] == null ? "" : col[3].toString());
		    		data.put("stopwatchParty",col[4] == null ? "" : col[4]);
		    		data.put("stopwatchProjectDetailId",col[5] == null ? "" : col[5]);
		    		data.put("stopwatchProjectId",col[6] == null ? "" : col[6]);
		    		data.put("stopwatchCompanyId",col[7] == null ? "" : col[7]);
		    		data.put("stopwatchBuilding",col[8] == null ? "" : col[8]);
		    		data.put("stopwatchAddress",col[9] == null ? "" : col[9]);
		    		data.put("stopwatchStatus",col[10] == null ? "" : col[10]);
		    		data.put("stopwatchContactId",col[11] == null ? "" : col[11]);
		    		data.put("taskId",col[12] == null ? "" : col[12]);
		    		data.put("taskNama",col[13] == null ? "" : col[13]);
		    		data.put("groupingId",col[14] == null ? "" : col[14]);
		    		data.put("groupingNama",col[15] == null ? "" : col[15]);
		    		data.put("stopwatchHour",col[16] == null ? "" : col[16]);
		    		data.put("projectName",col[17] == null ? "" : col[17]);
		    		data.put("jumlahNote", col[18] == null ? "" :col[18]);
		    		cont.add(data);
		    	});
		    	response.put("responseCode", "00");
			    response.put("responseDesc", "Show List Stopwatch Success");
			    response.put("responseData", cont);
		    }
		    catch(Exception e)
		    {
		    	response.put("responseCode", "99");
			      response.put("responseDesc", "Show List Stopwatch Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
	}
	
	@PostMapping("/detailStopwatch")
	public String detailStopwatch(@RequestBody String request,@RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();

	    List<JSONObject> note = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    UUID user_uuid = UUID.fromString(getUserId(header));
	    
	    LocalDateTime localDateTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
        String formatDateTime = localDateTime.format(formatter);
        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
        
        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        DateTimeFormatter jam = DateTimeFormatter.ofPattern("HH:mm:ss");
        
        
	    logger.info("input >>>> " + dataRequest);
	    logger.info("dataHeader >>> " + header);
	    logger.info("call listStopwatch() >>> ");
	    try {
	    	List<Object[]> detailStopwatch = StopwatchService.detail(dataRequest.getInt("stopwatchId"),dataRequest.getInt("companyId"));
	    	detailStopwatch.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("stopwatchId", col[0] == null ? "" : col[0]);
	    		data.put("stopwatchDate", col[1] == null ? "" : col[1].toString());
	    		data.put("stopwatchFrom", col[2] == null ? "" : col[2].toString());
	    		data.put("stopwatchTo", col[3] == null ? "" : col[3].toString());
	    		data.put("stopwatchParty", col[4] == null ? "" : col[4]);
	    		data.put("stopwatchProjectDetailId", col[5] == null ? "" : col[5]);
	    		data.put("stopwatchProjectId", col[6] == null ? "" : col[6]);
	    		data.put("stopwatchTaskId", col[7] == null ? "" : col[7]);
	    		data.put("stopwatchCompanyId", col[8] == null ? "" : col[8]);
	    		data.put("stopwatchBuilding", col[9] == null ? "" : col[9]);
	    		data.put("stopwatchAddress", col[10] == null ? "" : col[10]);
	    		data.put("stopwatchStatus", col[11] == null ? "" : col[11]);
	    		data.put("stopwatchHour", col[12] == null ? "" : col[12].toString());
	    		data.put("stopwatchContactId", col[13] == null ? "" : col[13].toString());
	    		data.put("taskId", col[14] == null ? "" : col[14]);
	    		data.put("taskNama", col[15] == null ? "" : col[15]);
	    		data.put("projectName", col[16] == null ? "" : col[16]);	
	    		data.put("stopwatchNoteId", col[18] == null ? "" : col[18]);
	    		data.put("note", col[19] == null ? "" : col[19]);
	    		data.put("dateNote", col[20] == null ? "" : col[02].toString());
	    		cont.add(data);
	    		
	    		JSONObject data1 = new JSONObject();
	    		data1.put("stopwatchId", col[0] == null ? "" : col[0]);
	    		data1.put("stopwatchDate", col[1] == null ? "" : col[1].toString());
	    		data1.put("stopwatchFrom", col[2] == null ? "" : col[2].toString());
	    		data1.put("stopwatchTo", col[3] == null ? "" : col[3].toString());
	    		data1.put("stopwatchParty", col[4] == null ? "" : col[4]);
	    		data1.put("stopwatchProjectDetailId", col[5] == null ? "" : col[5]);
	    		data1.put("stopwatchProjectId", col[6] == null ? "" : col[6]);
	    		data1.put("stopwatchTaskId", col[7] == null ? "" : col[7]);
	    		data1.put("stopwatchCompanyId", col[8] == null ? "" : col[8]);
	    		data1.put("stopwatchBuilding", col[9] == null ? "" : col[9]);
	    		data1.put("stopwatchAddress", col[10] == null ? "" : col[10]);
	    		data1.put("stopwatchStatus", col[11] == null ? "" : col[11]);
	    		data1.put("stopwatchHour", col[12] == null ? "" : col[12].toString());
	    		data1.put("stopwatchContactId", col[13] == null ? "" : col[13].toString());
	    		data1.put("taskId", col[14] == null ? "" : col[14]);
	    		data1.put("taskNama", col[15] == null ? "" : col[15]);
	    		data1.put("projectName", col[16] == null ? "" : col[16]);	
	    		data1.put("stopwatchNoteId", col[18] == null ? "" : col[18]);
	    		data1.put("note", col[19] == null ? "" : col[19]);
	    		data1.put("dateNote", col[20] == null ? "" : col[20].toString());
	    		note.add(data1);
	    	});
	    	
	    	//untuk menghapus duplikat
	   		HashSet<Object> seenAtt= new HashSet<>();
	   		cont.removeIf(e -> !seenAtt.add(Arrays.asList(e.getInt("stopwatchId"), e.getInt("stopwatchCompanyId") )));
	   		
	   		cont.stream().forEach(col->{
	   			List<JSONObject> nama = new ArrayList<>();
	   			note.stream().forEach(col2->{
	   				if(col.getInt("stopwatchId") == col2.getInt("stopwatchId") && col.getInt("stopwatchCompanyId") == col2.getInt("stopwatchCompanyId") && !col2.get("stopwatchNoteId").equals(""))
	   				{
	   					nama.add(col2);
	   				}
	   			});
	   			col.remove("dateNote");
	   			col.remove("stopwatchNoteId");
	   			col.remove("note");
	   			col.put("detailNote", nama);
	   		});
	   		
	   		response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Stopwatch Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		      response.put("responseDesc", "Detail Stopwatch Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/saveStopwatch")
	public String saveStopwatch(@RequestBody String request,@RequestHeader(value = "User-Access") String header)
	{
		 JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    List<StopwatchNote> note = new ArrayList<>();
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    UUID user_uuid = UUID.fromString(getUserId(header));
		    
		    LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
	        DateTimeFormatter jam = DateTimeFormatter.ofPattern("HH:mm:ss");
	        
	        
		    logger.info("input >>>> " + dataRequest);
		    logger.info("dataHeader >>> " + header);
		    logger.info("call saveStopwatch() >>> ");
		    try {
		    	Stopwatch data = new Stopwatch();
    	    	int id = 0;
    	    	if(dataRequest.has("stopwatchId")) {
    	    		id = dataRequest.getInt("stopwatchId");
    	    	}
    	    	else
    	    	{
    	    		List<Object[]> dataNext = StopwatchService.nextval();
    	    		id = Integer.parseInt(String.valueOf(dataNext.get(0)));
    	    		
    	    	}
    	    	data.setStopwatchId(id);
    	    	data.setStopwatchDate(formatter1.parse(dataRequest.getString("stopwatchDate")));
    	    	data.setStopwatchFrom(Time.valueOf(dataRequest.getString("stopwatchFrom")));
    	    	data.setStopwatchTo(Time.valueOf(dataRequest.getString("stopwatchTo")));
    	    	data.setStopwatchParty(dataRequest.getInt("stopwatchParty"));
    	    	
    	    	if(dataRequest.has("stopwatchProjectDetailId"))
    	    	{
    	    		data.setStopwatchProjectDetailId(dataRequest.getInt("stopwatchProjectDetailId"));
    	    		data.setStopwatchProjectId(dataRequest.getInt("stopwatchProjectId"));
    	    	}
    	    	else if (dataRequest.has("stopwatchTaskId"))
    	    	{
    	    		data.setStopwatchTaskId(dataRequest.getInt("stopwatchTaskId"));
    	    	}
    	    	
    	    	data.setStopwatchCompanyId(dataRequest.getInt("stopwatchCompanyId"));
    	    	data.setStopwatchBuilding(dataRequest.getString("stopwatchBuilding"));
    	    	data.setStopwatchAddress(dataRequest.getString("stopwatchAddress"));
    	    	data.setStopwatchStatus(dataRequest.getInt("stopwatchStatus"));
    	    	if(!dataRequest.get("stopwatchHour").equals(null))
    	    	{
    	    		data.setStopwatchHour(Float.valueOf(dataRequest.getString("stopwatchHour")));
    	    	}
    	    	data.setStopwatchContactId(user_uuid);
    	    	
    	    	if(dataRequest.getJSONArray("noteDetail").size() > 0)
				{
					JSONArray DetailProject = dataRequest.getJSONArray("noteDetail");
					
					
					for (int b = 0; b < DetailProject.size(); b++) {
						DateTimeFormatter formatter11 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); 
						LocalDateTime dateTime = LocalDateTime.parse(DetailProject.getJSONObject(b).getString("dateNote"), formatter11);

						StopwatchNote data2 = new StopwatchNote();
						data2.setCompanyId(dataRequest.getInt("stopwatchCompanyId"));
						data2.setDateNote(dateTime);
						data2.setNote(DetailProject.getJSONObject(b).getString("note"));
						data2.setStopwatchId(id);
						data2.setStopwatchNoteId(DetailProject.getJSONObject(b).getInt("stopwatchNoteId"));
						note.add(data2);
					}
					
				}
    	    	
    	    	StopwatchService.save(data, note);
    	    	response.put("responseCode", "00");
			    response.put("responseDesc", "Save Stopwatch Success");
		    }
		    catch(Exception e)
		    {
		    	response.put("responseCode", "99");
			      response.put("responseDesc", "Save List Stopwatch Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
	}
	public String getUserId(String header )
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}
}
