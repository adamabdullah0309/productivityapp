package com.example.controllers;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.client.RestTemplate;

import com.example.service.ListDocumentSummaryService;
import com.example.service.ListDocumentSummarySubcategoryService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.service.*;
import com.example.service.ListDocumentSummaryService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/api")
public class GroupingController {
	private static final Logger logger = LoggerFactory.getLogger(CobaController.class);
	
	@Autowired
	private ListDocumentSummarySubcategoryService ListDocumentSummarySubcategoryService;
	
	@Autowired
	private ListDocumentSummaryService ListDocumentSummaryService;
	
	@Autowired
	private GabunganService GabunganService;
	
	@Autowired
	private GroupingService GroupingService;
	
	@Autowired 
	private GroupingDetailService GroupingDetailService;
	
	int a11 = 1000;
	@PostMapping("/listGrouping/{paramAktif}")
	public String listGrouping(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		a11 = 1000;
		JSONObject response = new JSONObject();
	   
	    List<JSONObject> contaa = new ArrayList<>();
	    List<JSONObject> Akhir = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try
	    {
	    	final11 = new ArrayList<>();
	    	UUID user_uuid = UUID.fromString(getUserId(header));
//	    	List<Object[]> data1 = GroupingService.getSubgroup(1, dataRequest.getInt("company_id") );
	    	List<Object[]> data1 = GroupingService.getSubgroupCoba(dataRequest.getInt("company_id") );
	    	List<Object[]> data22 = GroupingService.getHeaderSubgroup(dataRequest.getInt("company_id"), user_uuid, paramAktif);
	    	data22.stream().forEach(col9->{
	    		
	    		JSONObject dataNew = new JSONObject();
	    		dataNew.put("groupingId", col9[0]);
	    		dataNew.put("groupingNama", col9[1]);
	    		dataNew.put("groupingDescription", col9[2] != null ? col9[2].toString() : "");
	    		dataNew.put("groupingStatus", col9[3]);
	    		dataNew.put("company_id", col9[4]);
	    		
	    		//---
	    		//coba git
//	    		List<Object[]> data1 = GroupingService.getSubgroup(Integer.valueOf(col9[0].toString()), dataRequest.getInt("company_id") );
	    		List<JSONObject> cont = new ArrayList<>();
		    	data1.stream().forEach(col1->{
		  
	    			
	    			if(col1[3].equals(col9[4]) && col1[5].equals(col9[0])) {
	    				JSONObject data = new JSONObject();
	    				data.put("id",col1[1]);
			    		data.put("childGroupId",col1[0] != null ? col1[0] : a11);
			    		data.put("childtaskId",col1[2] != null ? col1[2] : a11);
		    			data.put("company_id", col1[3]);
		    			JSONObject data2 = new JSONObject();
			            data2.put("name", col1[4]);
		    			data.put("data", data2);
	    				cont.add(data);
	    			}
	    			
	    			
	    			a11 = a11 + 1;
		    	});
//		    	if(col9[0].equals(3))
//		    	{
//		    		logger.info(" data = " + cont.get(0).getInt("id"));
//		    		logger.info(" data child group id = " + cont.get(0).getInt("childGroupId"));
//		    		logger.info(" data child nama group id = " + cont.get(0).getJSONObject("data"));
//		    	}
		    	JSONObject dataRequestLevel = new JSONObject ();
		    	if(cont.size()>0)
		    	{
		    		String level = "";
	    			JSONObject data2 = new JSONObject();

	    			ArrayList<ChildGrouping> pairs = new ArrayList<ChildGrouping>();
		    		for(int a = 0 ; a<cont.size();a++) {
		    			if(col9[0].equals(3))
				    	{
		    				logger.info(" data = " + cont.get(a).getInt("id"));
				    		logger.info(" data child group id = " + cont.get(a).getInt("childGroupId"));
				    		logger.info(" data child nama group id = " + cont.get(a).getJSONObject("data"));
				    	}
		    			pairs.add(new ChildGrouping(cont.get(a).getInt("childGroupId") , cont.get(a).getInt("id"), cont.get(a).getInt("childtaskId"),
		    					cont.get(a).getInt("company_id"), cont.get(a).getJSONObject("data")
		    					)
		    					
		    					);
		    		}
		    		
		    		Map<Integer, ParentGrouping> hm = new HashMap<>();
		    			
	    			for(ChildGrouping p:pairs){
			            //  ----- Child -----
	    				ParentGrouping mmdChild ;
			            if(hm.containsKey(p.getChildId())){
			                mmdChild = hm.get(p.getChildId());
			            }
			            else{
			                mmdChild = new ParentGrouping();
			                hm.put(p.getChildId(), mmdChild);
			            }  
			            mmdChild.setId(p.getChildId());
			            mmdChild.setParentId(p.getParentId());
			            mmdChild.setTaskId(p.getTaskId());
			            mmdChild.setCompany_id(p.getCompany_id());
			            mmdChild.setData(p.getData());
			            
			            // no need to set ChildrenItems list because the constructor created a new empty list



			            // ------ Parent ----
			            ParentGrouping mmdParent ;
			            if(hm.containsKey(p.getParentId())){
			                mmdParent = hm.get(p.getParentId());
			            }
			            else{
			                mmdParent = new ParentGrouping();
			                hm.put(p.getParentId(),mmdParent);
			            }
			            mmdParent.setId(p.getParentId());
			            mmdParent.setParentId(null);
			            mmdParent.setTaskId(p.getTaskId());
			            mmdParent.setCompany_id(p.getCompany_id());
			            mmdParent.setData(p.getData());
			            mmdParent.addChildren(mmdChild);
			        }
		    			
	    			List<ParentGrouping> DX = new ArrayList<ParentGrouping>(); 
			        for(ParentGrouping mmd : hm.values()){
			            if(mmd.getParentId() == null)
			                DX.add(mmd);
			        }

			        // Print 
			        for(ParentGrouping mmd: DX){
			            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
			            JSONObject data = new JSONObject();
			            
			            data.put("data",mmd);
			            contaa.add(data);
			        }
			        
			        for(int z= 0 ; z< contaa.size() ; z++)
			        {
			        	level = contaa.get(z).getString("data");
//			        	logger.info("cont = " + contaa.get(z).getString("data"));
			        }
			        dataRequestLevel = (JSONObject)JSONSerializer.toJSON(level);
		    	}
	    		//---
	    		dataNew.put("detail",dataRequestLevel.get("children"));
	    		Akhir.add(dataNew);
	    	});
	    	
	    	
	    	
	    	
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Grouping Success");
		    response.put("responseData",Akhir);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Grouping Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
		return response.toString();
	}
	int y = 0;
	
	List<JSONObject> grouping = new ArrayList<>();
	@PostMapping("/listGroupingCoba2/{paramAktif}")
	public String listGroupingCoba2(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		a11 = 1000;
		JSONObject response = new JSONObject();
	   
	    List<JSONObject> contaa = new ArrayList<>();
	    List<JSONObject> Akhir = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try
	    {
	    	final11 = new ArrayList<>();
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> data1 = GroupingService.getSubgroupCoba(dataRequest.getInt("company_id"));
	    	
	    	List<Object[]> data22 = GroupingService.getHeaderSubgroup(dataRequest.getInt("company_id"), user_uuid, paramAktif);
	    	grouping = new ArrayList<>();
	    	data22.stream().forEach(col9->{
	    		List<JSONObject> cioba = new ArrayList<>();
	    		JSONObject dataNew = new JSONObject();
	    		dataNew.put("groupingId", Integer.valueOf(col9[0].toString()));
	    		dataNew.put("groupingNama", col9[1]);
	    		dataNew.put("groupingDescription", col9[2] != null ? col9[2].toString() : "");
	    		dataNew.put("groupingStatus", col9[3]);
	    		dataNew.put("company_id", Integer.valueOf(col9[4].toString()));
	    		
	    		cioba = callChild(dataNew);
	    		dataNew.put("detail", cioba);
	    		grouping.add(dataNew);
	    	});
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Grouping Success");
		    response.put("responseData",grouping);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Grouping Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	public List<JSONObject> callChild(JSONObject dataNew)
	{
		List<JSONObject> contaa = new ArrayList<>();
		if(dataNew.get("groupingId") != null)
		{
			List<Object[]> data22 = GroupingService.getDetailSubgroup(dataNew.getInt("company_id"),dataNew.getInt("groupingId"));
			if(data22.size()>0)
			{
				data22.stream().forEach(col->{
					List<JSONObject> cioba = new ArrayList<>();
					JSONObject data = new JSONObject();
					data.put("nourut", col[0]);
					data.put("groupingId", col[2] == null ? "" : col[2]);
					data.put("taskId", col[3] == null ? "" : col[3]);
					data.put("company_id", col[4]);
					
					JSONObject nama = new JSONObject();
					nama.put("name", col[5]);
					data.put("data", nama);
					if(col[2] != null)
					{
						cioba = callChild(data);
					}
					data.put("children", cioba);
					contaa.add(data);
				});
			}
			
		}
		return contaa;
	}
	
	@PostMapping("/listGroupingCoba/{paramAktif}")
	public String listGroupingCoba(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		a11 = 1000;
		JSONObject response = new JSONObject();
	   
	    List<JSONObject> contaa = new ArrayList<>();
	    List<JSONObject> Akhir = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try
	    {
	    	final11 = new ArrayList<>();
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> data1 = GroupingService.getSubgroupCoba(dataRequest.getInt("company_id"));
	    	
	    	List<Object[]> data22 = GroupingService.getHeaderSubgroup(dataRequest.getInt("company_id"), user_uuid, paramAktif);
	    	data22.stream().forEach(col9->{
	    		
	    		JSONObject dataNew = new JSONObject();
	    		dataNew.put("groupingId", col9[0]);
	    		dataNew.put("groupingNama", col9[1]);
	    		dataNew.put("groupingDescription", col9[2] != null ? col9[2].toString() : "");
	    		dataNew.put("groupingStatus", col9[3]);
	    		dataNew.put("company_id", col9[4]);
	    		
	    		//---
	    		//coba git
//	    		List<Object[]> data1 = GroupingService.getSubgroup(Integer.valueOf(col9[0].toString()), dataRequest.getInt("company_id") );
	    		List<JSONObject> cont = new ArrayList<>();
		    	data1.stream().forEach(col1->{
		    		
		    		
		    		
	    			
	    			
	    			if(col1[3].equals(col9[4]) && col1[5].equals(col9[0])) {
	    				JSONObject data = new JSONObject();
	    				data.put("id",col1[1]);
			    		data.put("childGroupId",col1[0] != null ? col1[0] : a11);
			    		data.put("childtaskId",col1[2] != null ? col1[2] : 999);
		    			data.put("company_id", col1[3]);
		    			JSONObject data2 = new JSONObject();
			            data2.put("name", col1[4]);
		    			data.put("data", data2);
	    				cont.add(data);
	    			}
	    			
	    			
	    			a11 = a11 + 1;
		    	});
//		    	if(col9[0].equals(3))
//		    	{
//		    		logger.info(" data = " + cont.get(0).getInt("id"));
//		    		logger.info(" data child group id = " + cont.get(0).getInt("childGroupId"));
//		    		logger.info(" data child nama group id = " + cont.get(0).getJSONObject("data"));
//		    	}
		    	JSONObject dataRequestLevel = new JSONObject ();
		    	if(cont.size()>0)
		    	{
		    		String level = "";
	    			JSONObject data2 = new JSONObject();

	    			ArrayList<ChildGrouping> pairs = new ArrayList<ChildGrouping>();
		    		for(int a = 0 ; a<cont.size();a++) {
		    			if(col9[0].equals(3))
				    	{
		    				logger.info(" data = " + cont.get(a).getInt("id"));
				    		logger.info(" data child group id = " + cont.get(a).getInt("childGroupId"));
				    		logger.info(" data child nama group id = " + cont.get(a).getJSONObject("data"));
				    	}
		    			pairs.add(new ChildGrouping(cont.get(a).getInt("childGroupId") , cont.get(a).getInt("id"), cont.get(a).getInt("childtaskId"),
		    					cont.get(a).getInt("company_id"), cont.get(a).getJSONObject("data")
		    					)
		    					
		    					);
		    		}
		    		
		    		Map<Integer, ParentGrouping> hm = new HashMap<>();
		    		y = 0;
	    			for(ChildGrouping p:pairs){
			            //  ----- Child -----
	    				ParentGrouping mmdChild ;
			            if(hm.containsKey(p.getChildId())){
			                mmdChild = hm.get(p.getChildId());
			            }
			            else{
			                mmdChild = new ParentGrouping();
			                hm.put(p.getChildId(), mmdChild);
			            }  
			            mmdChild.setId(p.getChildId());
			            mmdChild.setParentId(p.getParentId());
			            mmdChild.setTaskId(p.getTaskId());
			            mmdChild.setCompany_id(p.getCompany_id());
			            mmdChild.setData(p.getData());
			            
			            // no need to set ChildrenItems list because the constructor created a new empty list



			            // ------ Parent ----
			            ParentGrouping mmdParent ;
			            if(hm.containsKey(p.getParentId())){
			                mmdParent = hm.get(p.getParentId());
			            }
			            else{
			                mmdParent = new ParentGrouping();
			                hm.put(p.getParentId(),mmdParent);
			            }
			            mmdParent.setId(p.getParentId());
			            mmdParent.setParentId(null);
			            mmdParent.setTaskId(p.getTaskId());
			            mmdParent.setCompany_id(p.getCompany_id());
			            mmdParent.setData(p.getData());
			            mmdParent.addChildren(mmdChild);
			            y++;
			        }
		    			
	    			List<ParentGrouping> DX = new ArrayList<ParentGrouping>(); 
			        for(ParentGrouping mmd : hm.values()){
			            if(mmd.getParentId() == null)
			                DX.add(mmd);
			        }

			        // Print 
			        for(ParentGrouping mmd: DX){
			            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
			            JSONObject data = new JSONObject();
			            
			            data.put("data",mmd);
			            contaa.add(data);
			        }
			        
			        for(int z= 0 ; z< contaa.size() ; z++)
			        {
			        	level = contaa.get(z).getString("data");
//			        	logger.info("cont = " + contaa.get(z).getString("data"));
			        }
			        dataRequestLevel = (JSONObject)JSONSerializer.toJSON(level);
		    	}
	    		//---
	    		dataNew.put("detail",dataRequestLevel.get("children"));
	    		Akhir.add(dataNew);
	    	});
	    	
	    	
	    	
	    	
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Grouping Success");
		    response.put("responseData",Akhir);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Grouping Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
		return response.toString();
	}
	
	
	
	@PostMapping("/saveGrouping")
	public String saveGrouping(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<GroupingDetail> detail = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	
	    	Grouping dataHeader = new Grouping();
	    	List<Object[]> dataNext = GroupingService.nextval();
	    	int id = 0;
	    	if(dataNext.size()>0)
	    	{
	    		id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	}
	    	dataHeader.setGroupingId(id);;
	    	dataHeader.setGroupingNama(dataRequest.getString("groupingNama"));
	    	dataHeader.setGroupingDescription(dataRequest.getString("groupingDescription"));
	    	dataHeader.setGroupingStatus(dataRequest.getInt("groupingStatus"));
	    	dataHeader.setCompanyId(dataRequest.getInt("company_id"));
	    	dataHeader.setCreatedBy(user_uuid);
	    	dataHeader.setCreatedAt(localDateTime2);
	    	
	    	if(dataRequest.getJSONArray("detail").size() > 0)
		    {
	    		for(int a = 0 ; a < dataRequest.getJSONArray("detail").size(); a++)
		    	{
	    			
//	    			Integer nourut, int mGroupingId, int companyId, int groupingChildId, int taskChildId
	    			GroupingDetail dataDetail = new GroupingDetail(
	    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("nourut"),
	    					id,
	    					dataRequest.getInt("company_id"),
	    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MGroupingId") != 0 ? dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MGroupingId") : null,
	    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MTaskId") != 0 ? dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MTaskId") : null
	    					);
	    			detail.add(dataDetail);
		    	}
		    }
	    	GabunganService.saveGrouping(dataHeader, detail);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Grouping Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Grouping Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	 
	 
	@PostMapping("/updateGrouping")
	public String updateGrouping(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<GroupingDetail> detail = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        Optional<Grouping>  dataDetailGroupHeader = GroupingService.detailGroupHeader(dataRequest.getInt("groupingId"), dataRequest.getInt("company_id"));
	    	if(dataDetailGroupHeader.isPresent())
	    	{
	    		((Grouping)dataDetailGroupHeader.get()).setGroupingNama(dataRequest.getString("groupingNama"));
	    		((Grouping)dataDetailGroupHeader.get()).setGroupingDescription(dataRequest.getString("groupingDescription"));
	    		((Grouping)dataDetailGroupHeader.get()).setGroupingStatus(dataRequest.getInt("groupingStatus"));
	    		((Grouping)dataDetailGroupHeader.get()).setUpdatedBy(user_uuid);
	    		((Grouping)dataDetailGroupHeader.get()).setUpdatedAt(localDateTime2);
	    		
	    		if(dataRequest.getJSONArray("detail").size() > 0)
			    {
		    		for(int a = 0 ; a < dataRequest.getJSONArray("detail").size(); a++)
			    	{
//		    			if(dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MGroupingId") != 0)
//		    			{
//		    				List<Object[]> checkDuplicate = GroupingService.checkDuplicate(dataRequest.getInt("company_id"), dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MGroupingId") );
//		    				checkDuplicate.stream().forEach(col->{
//		    					if(Integer.valueOf(col[0].toString()) == dataRequest.getInt("groupingId") && dataRequest.getInt("company_id") == Integer.valueOf(col[3].toString()) )
//		    					{
//		    						return;
//		    					}
//		    				});
//		    			}
//		    			Integer nourut, int mGroupingId, int companyId, int groupingChildId, int taskChildId
		    			GroupingDetail dataDetail = new GroupingDetail(
		    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("nourut"),
		    					dataRequest.getInt("groupingId"),
		    					dataRequest.getInt("company_id"),
		    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MGroupingId") != 0 ? dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MGroupingId") : null,
		    					dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MTaskId") != 0 ? dataRequest.getJSONArray("detail").getJSONObject(a).getInt("MTaskId") : null
		    					);
		    			detail.add(dataDetail);
			    	}
			    }
	    		GabunganService.updateGrouping(dataDetailGroupHeader.get(), detail);
	    	}
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Grouping Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Update Grouping Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
 

	@PostMapping("/detailGrouping")
	public String detailGrouping(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	Optional<Grouping>  dataDetailGroupHeader = GroupingService.detailGroupHeader(dataRequest.getInt("groupingId"), dataRequest.getInt("company_id"));
	    	if(dataDetailGroupHeader.isPresent())
	    	{
	    		JSONObject data = new JSONObject();
	    		data.put("groupingId", dataDetailGroupHeader.get().getGroupingId());
	    		data.put("groupingNama", dataDetailGroupHeader.get().getGroupingNama());
	    		data.put("groupingStatus", dataDetailGroupHeader.get().getGroupingStatus());
	    		data.put("groupingDescription", dataDetailGroupHeader.get().getGroupingDescription() != null ? dataDetailGroupHeader.get().getGroupingDescription() : "");
	    		List<JSONObject> detail = new ArrayList<>();
	    		List<Object[]> dataDetail = GroupingDetailService.detailGroupDetail(dataRequest.getInt("groupingId"), dataRequest.getInt("company_id"));
	    		if(dataDetail.size() > 0) {
	    			dataDetail.stream().forEach(col->{
	    				JSONObject data2 = new JSONObject();
	    				data2.put("nourut", col[0]);
	    				data2.put("groupingId", col[1]);
	    				data2.put("company_id", col[2]);
	    				data2.put("MGroupingId", col[3] != null ? col[3] : "");
	    				data2.put("MGroupingNama", col[5] != null ? col[5].toString() : "");
	    				data2.put("MTaskId", col[4] != null ? col[4] : "");
	    				data2.put("MTaskNama", col[6] != null ? col[6] : "");
	    				detail.add(data2);
	    			});
	    			
	    		}
	    		data.put("detail",detail);
	    		cont.add(data);
	    	}
	    	 response.put("responseCode", "00");
		      response.put("responseDesc", "Show Detail Grouping Success");
		      response.put("responseData",cont);
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		      response.put("responseDesc", "Show Detail Grouping Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	 }
	
	public String getUserId(String header )
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}
}
