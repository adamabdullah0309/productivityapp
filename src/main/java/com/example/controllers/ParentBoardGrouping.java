package com.example.controllers;
import java.util.ArrayList;
import java.util.List;

public class ParentBoardGrouping {
    private Integer subcategory;
    private Integer parentId;
    private Integer childId;
    private List<ParentBoardGrouping> childrenItems; 

    public ParentBoardGrouping() {
        this.subcategory = null;
        this.parentId = null;
        this.childrenItems = new ArrayList<ParentBoardGrouping>();
    }
    
        
    


	public Integer getSubcategory() {
		return subcategory;
	}





	public void setSubcategory(Integer subcategory) {
		this.subcategory = subcategory;
	}





	public Integer getParentId() {
		return parentId;
	}





	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}





	public Integer getChildId() {
		return childId;
	}





	public void setChildId(Integer childId) {
		this.childId = childId;
	}





	public List<ParentBoardGrouping> getChildrenItems() {
        return childrenItems;
    }
    public void setChildrenItems(List<ParentBoardGrouping> childrenItems) {
        this.childrenItems = childrenItems;
    }
    public void addChildrenItem(ParentBoardGrouping childrenItem){
        if(!this.childrenItems.contains(childrenItem))
            this.childrenItems.add(childrenItem);
    }

}