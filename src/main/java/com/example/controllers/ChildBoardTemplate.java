package com.example.controllers;
public class ChildBoardTemplate {
    private Integer projectId;
    private String projectName;
    private Integer templateId;
    private String templateNama;
    private Integer stageId;
    private String stageNama;
    private String task;
    private Integer boardId;
    private Integer totalTask;
    private Integer jmlhDisc;
    private Integer jmlhAtt;
    private Integer companyId;
    public ChildBoardTemplate() {}
    
    public ChildBoardTemplate(String projectName,Integer projectId, Integer stageId, String stageNama, Integer boardId, Integer totalTask,
    		Integer templateId, String templateNama, Integer jmlhDisc, Integer jmlhAtt,Integer companyId
    		) {
    	this.projectName = projectName;
        this.projectId = projectId;
        this.stageId = stageId;
        this.task = task;
        this.stageNama = stageNama;
        this.boardId = boardId;
        this.totalTask = totalTask;
        this.templateId = templateId;
        this.templateNama = templateNama;
        this.jmlhDisc = jmlhDisc;
        this.jmlhAtt = jmlhAtt;
        this.companyId = companyId;
    }
    
    


	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getJmlhDisc() {
		return jmlhDisc;
	}

	public void setJmlhDisc(Integer jmlhDisc) {
		this.jmlhDisc = jmlhDisc;
	}

	public Integer getJmlhAtt() {
		return jmlhAtt;
	}

	public void setJmlhAtt(Integer jmlhAtt) {
		this.jmlhAtt = jmlhAtt;
	}

	public Integer getTemplateId() {
		return templateId;
	}




	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}




	public String getTemplateNama() {
		return templateNama;
	}




	public void setTemplateNama(String templateNama) {
		this.templateNama = templateNama;
	}



	



	public Integer getTotalTask() {
		return totalTask;
	}

	public void setTotalTask(Integer totalTask) {
		this.totalTask = totalTask;
	}

	public Integer getBoardId() {
		return boardId;
	}




	public void setBoardId(Integer boardId) {
		this.boardId = boardId;
	}




	public String getStageNama() {
		return stageNama;
	}



	public void setStageNama(String stageNama) {
		this.stageNama = stageNama;
	}



	public String getTask() {
		return task;
	}



	public void setTask(String task) {
		this.task = task;
	}



	public String getProjectName() {
		return projectName;
	}



	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}



	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Integer getStageId() {
		return stageId;
	}
	public void setStageId(Integer stageId) {
		this.stageId = stageId;
	}
    
    
    
	
    
	
}