package com.example.controllers;
public class BoardDrawer {
    private Integer stateGroupId;
    private String stateGroupName;
    private Integer stageId;
    private String stageNama;
    private Integer companyId;
    public BoardDrawer(Integer stateGroupId,String stateGroupName, Integer stageId, String stageNama, Integer companyId
    		) {
    	this.stateGroupId = stateGroupId;
    	this.stateGroupName = stateGroupName;
    	this.stageId = stageId;
    	this.stageNama = stageNama;
    	this.companyId = companyId;
    }
	public Integer getStateGroupId() {
		return stateGroupId;
	}
	public void setStateGroupId(Integer stateGroupId) {
		this.stateGroupId = stateGroupId;
	}
	public String getStateGroupName() {
		return stateGroupName;
	}
	public void setStateGroupName(String stateGroupName) {
		this.stateGroupName = stateGroupName;
	}
	public Integer getStageId() {
		return stageId;
	}
	public void setStageId(Integer stageId) {
		this.stageId = stageId;
	}
	public String getStageNama() {
		return stageNama;
	}
	public void setStageNama(String stageNama) {
		this.stageNama = stageNama;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
    
    
    
    
    
    
	
    
	
}