package com.example.controllers;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.helper.Helper;
import com.example.service.ProjectDetailService;
import com.example.service.ProjectDetailTemplateService;
import com.example.service.GabunganService;
import com.example.service.ProjectDetailDependencyService;
import com.example.service.ProjectDetailKaryawanService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/api")
public class ProjectDetailController {
	private static final Logger logger = LoggerFactory.getLogger(ProjectDetailController.class);
	Object data;
	
	@Autowired
	private ProjectDetailKaryawanService ProjectDetailKaryawanService;
	
	@Autowired
	private ProjectDetailService ProjectDetailService;
	
	@Autowired
	private GabunganService GabunganService;
	
	@Autowired
	private ProjectDetailDependencyService ProjectDetailDependencyService;
	
	Helper Helper;
	
	@Autowired
	private ProjectDetailTemplateService ProjectDetailTemplateService;

	ArrayList<ProjectDetail> ProjectDetail = new ArrayList<>();
	ArrayList<ProjectDetailKaryawan> dataKaryawan = new ArrayList<>();
	
	ArrayList<ProjectDetailDependency> dataDependency = new ArrayList<>();
	int a11 = 100000000;
	
	
	@PostMapping({"/listTask/{paramAktif}"})
	public String listTask(@PathVariable("paramAktif") String paramAktif,@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    ArrayList<ProjectDetailDependency> DataProjectDetailDependency= new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    logger.info("call listTask()");
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> listTask = ProjectDetailService.listTask(dataRequest.getInt("projectId"), dataRequest.getInt("companyId"), dataRequest.getString("projectType"), dataRequest.getInt("projectDetailId"), paramAktif.toString().toLowerCase());
	        listTask.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("projectDetailId", col[0] == null ? "" : col[0]);
	        	data.put("mProjectId", col[1] == null ? "" : col[1]);
	        	data.put("ProjectName", col[2] == null ? "" : col[2]);
	        	data.put("status", col[3] == null ? "" : col[3]);
	        	data.put("mTaskId", col[4] == null ? "" : col[4]);
	        	data.put("mGroupingId", col[5] == null ? "" : col[5]);
	        	data.put("taskNama", col[6] == null ? "" : col[6]);
	        	data.put("companyId", col[7] == null ? "" : col[7]);
	        	cont.add(data);
	        });

	    	response.put("responseCode", "00");
		    response.put("responseDesc", "List Task Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "List Task Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping({"/listGroup/{paramAktif}"})
	public String listGroup(@PathVariable("paramAktif") String paramAktif,@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    ArrayList<ProjectDetailDependency> DataProjectDetailDependency= new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    logger.info("call listGroup()");
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> listGroup = ProjectDetailService.listGroup(dataRequest.getInt("projectId"), dataRequest.getInt("companyId"), dataRequest.getString("projectType"), paramAktif.toString().toLowerCase());
	        listGroup.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("mProjectId", col[0] == null ? "" : col[0]);
	        	data.put("ProjectName", col[1] == null ? "" : col[1]);
	        	data.put("mTaskId", col[2] == null ? "" : col[2]);
	        	data.put("mGroupingId", col[3] == null ? "" : col[3]);
	        	data.put("companyId", col[4] == null ? "" : col[4]);
	        	data.put("status", col[5] == null ? "" : col[5]);
	        	data.put("taskNama", col[6] == null ? "" : col[6]);
	        	data.put("projectDetailId", col[7] == null ? "" : col[7]);
	        	cont.add(data);
	        });
	        response.put("responseCode", "00");
		    response.put("responseDesc", "List Group Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "List Group Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/saveProjectTemplate")
	public String updateProjectTemplate(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    ArrayList<ProjectDetailDependency> DataProjectDetailDependency= new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        ProjectDetailTemplate data = new ProjectDetailTemplate();
	        data.setCompanyId(dataRequest.getInt("company_id"));
	        data.setmProjectId(dataRequest.getInt("mProjectId"));
	        data.setmTemplateId(dataRequest.getInt("mTemplateId"));
	        
	        ProjectDetailTemplateService.save(data);
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Save Project Template Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Detail Project Dependency Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/saveDependency")
	public String saveDependency(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    ArrayList<ProjectDetailDependency> DataProjectDetailDependency= new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        JSONArray arrayTask = dataRequest.getJSONArray("projectDetailDependencyArray");
		      
			
			if (arrayTask.size() > 0)
			{
				for (int a = 0; a < arrayTask.size(); a++) {
					ProjectDetailDependency data = new ProjectDetailDependency();
					data.setProjectDetailId(arrayTask.getJSONObject(a).getInt("projectDetailId"));
					data.setmProjectId(arrayTask.getJSONObject(a).getInt("mProjectId"));
					data.setNoDependency(arrayTask.getJSONObject(a).getInt("noDependency"));
					data.setCompanyId(arrayTask.getJSONObject(a).getInt("company_id"));
					data.setProjectDetailDependencyId(arrayTask.getJSONObject(a).getInt("projectDetailDependencyId"));
					DataProjectDetailDependency.add(data);
				}
			}
			GabunganService.saveDependency(DataProjectDetailDependency);
			response.put("responseCode", "00");
		    response.put("responseDesc", "Save Detail Project Dependency Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Detail Project Dependency Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/detailDependency")
	public String detailDependency(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    ArrayList<ProjectDetailDependency> DataProjectDetailDependency= new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<ProjectDetailDependency> findDetail = ProjectDetailDependencyService.findDetail(dataRequest.getInt("projectId"), dataRequest.getInt("projectDetailId"), dataRequest.getInt("company_id"));
	        findDetail.stream().forEach(col->{
	        	ProjectDetailDependency data = new ProjectDetailDependency();
	        	data.setCompanyId(col.getCompanyId());
	        	data.setmProjectId(col.getmProjectId());
	        	data.setNoDependency(col.getNoDependency());
	        	data.setProjectDetailDependencyId(col.getProjectDetailDependencyId());
	        	data.setProjectDetailId(col.getProjectDetailId());
	        	DataProjectDetailDependency.add(data);
	        });
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Save Detail Project Dependency Success");
		    response.put("responseData", DataProjectDetailDependency);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Detail Project Karyawan Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/saveKaryawan")
	public String saveKaryawan(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    ArrayList<ProjectDetailKaryawan> DataProjectDetailDependency= new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        JSONArray arrayTask = dataRequest.getJSONArray("projectDetailKaryawanArray");
		      
			
			if (arrayTask.size() > 0)
			{
				for (int a = 0; a < arrayTask.size(); a++) {
					ProjectDetailKaryawan data = new ProjectDetailKaryawan();
					data.setProjectDetailId(arrayTask.getJSONObject(a).getInt("projectDetailId"));
					data.setmProjectId(arrayTask.getJSONObject(a).getInt("mProjectId"));
					data.setContactId(UUID.fromString(arrayTask.getJSONObject(a).getString("contactId")));
					data.setCompanyId(arrayTask.getJSONObject(a).getInt("company_id"));
					data.setProjectDetailKaryawanId(arrayTask.getJSONObject(a).getInt("projectDetailKaryawanId"));
					DataProjectDetailDependency.add(data);
				}
			}
			GabunganService.saveKaryawan(DataProjectDetailDependency);
			response.put("responseCode", "00");
		    response.put("responseDesc", "Save Detail Project Karyawan Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Detail Project Karyawan Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping({"/SaveDetailProject"})
	public String SaveDetailProject(@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> contaa = new ArrayList<>();
	    List<ProjectDetail> dataProject = new ArrayList<>();
	    ProjectDetail = new ArrayList<>();
	    String level = "";
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    List<JSONObject> copb = new ArrayList<>();
	    dataKaryawan = new ArrayList<>();
	    dataDependency = new ArrayList<>();
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	JSONArray arrayTask = dataRequest.getJSONArray("projectDetailArray");
		      
			UUID user_uuid = UUID.fromString(getUserId(header));
			if (arrayTask.size() > 0)
			{
				
				for (int a = 0; a < arrayTask.size(); a++) {
					JSONArray dataChild = (JSONArray) JSONSerializer.toJSON(dataRequest.getJSONArray("projectDetailArray"));
					 JSONObject resultData = (JSONObject) dataChild.get(a);
	                   logger.info("resultData detail = " + resultData);
	                    ProjectDetail data = new ProjectDetail();
						data.setProjectDetailId(resultData.getInt("projectDetailId"));
						data.setmProjectId(dataRequest.getInt("mProjectId"));
						data.setParentId(resultData.getInt("parentId"));
						if(resultData.getInt("mTaskId") != 0 && (resultData.getInt("mGroupingId") == 0))
						{
							data.setmTaskId(resultData.getInt("mTaskId"));
							data.setmGroupingId(null);
						}
						else if(resultData.getInt("mTaskId") == 0 && (resultData.getInt("mGroupingId") != 0))
						{ 
							data.setmTaskId(null);
							data.setmGroupingId(resultData.getInt("mGroupingId"));
						}
						else if(resultData.getInt("mTaskId") == 0 && (resultData.getInt("mGroupingId") == 0))
						{
							data.setmTaskId(null);
							data.setmGroupingId(null);
						}
						
						if(resultData.has("description"))
						{
							data.setDescription(resultData.getString("description"));
						}
						data.setStatus(resultData.getInt("status"));
						data.setPriority(resultData.getInt("priority"));
						
						data.setCompanyId(dataRequest.getInt("company_id"));
						ProjectDetail.add(data);
						
						if(resultData.getJSONArray("childrenItems").size() > 0)
						{
							hasChild(resultData.getJSONArray("childrenItems"),dataRequest.getInt("company_id"),dataRequest.getInt("mProjectId"));
						}
					
						if(resultData.getJSONArray("karyawanDetail").size() > 0)
						{
							JSONArray DetailProject = resultData.getJSONArray("karyawanDetail");
							
							
							for (int b = 0; b < DetailProject.size(); b++) {
								ProjectDetailKaryawan data2 = new ProjectDetailKaryawan();
								data2.setCompanyId(dataRequest.getInt("company_id"));
								data2.setmProjectId(dataRequest.getInt("mProjectId"));
								data2.setProjectDetailId(resultData.getInt("projectDetailId"));
								data2.setProjectDetailKaryawanId(DetailProject.getJSONObject(b).getInt("projectDetailKaryawanId"));
								data2.setContactId(UUID.fromString(DetailProject.getJSONObject(b).getString("contactId")));
								dataKaryawan.add(data2);
							}
							
						}
						
						if(resultData.getJSONArray("relationDetail").size() > 0)
						{
							JSONArray DetailProject = resultData.getJSONArray("relationDetail");
							
							
							for (int b = 0; b < DetailProject.size(); b++) {
								ProjectDetailDependency data2 = new ProjectDetailDependency();
								data2.setCompanyId(dataRequest.getInt("company_id"));
								data2.setmProjectId(dataRequest.getInt("mProjectId"));
								data2.setProjectDetailId(resultData.getInt("projectDetailId"));
								data2.setProjectDetailDependencyId(DetailProject.getJSONObject(b).getInt("projectDetailDependencyId"));
								data2.setNoDependency(DetailProject.getJSONObject(b).getInt("noDependency"));
								dataDependency.add(data2);
							}
							
						}
					
				}
			}
//			logger.info("ProjectDetail = " + ProjectDetail.get(0).getCompanyId());
//			ProjectDetailService.saveCoba(ProjectDetail, dataRequest.getInt("mTemplateId"));

			GabunganService.saveProjectDetail(ProjectDetail, dataRequest.getInt("mProjectId"), dataRequest.getInt("company_id"), dataRequest.getInt("mTemplateId"), dataKaryawan, dataDependency);
			response.put("responseCode", "00");
		    response.put("responseDesc", "Save Detail Project Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Detail Project Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	public void  hasChild(JSONArray childData, int company, int project){
    	List<JSONObject> cont = new ArrayList<>();
		JSONObject response = new JSONObject();
		 
		 
		 
              for (int i = 0; i < childData.size(); i++) {
                   JSONObject resultData = (JSONObject) childData.get(i);
                    ProjectDetail data = new ProjectDetail();
					data.setProjectDetailId(resultData.getInt("projectDetailId"));
					data.setmProjectId(project);
					data.setParentId(resultData.getInt("parentId"));
					if(resultData.getInt("mTaskId") != 0 && (resultData.getInt("mGroupingId") == 0))
					{
						data.setmTaskId(resultData.getInt("mTaskId"));
						data.setmGroupingId(null);
					}
					else if(resultData.getInt("mTaskId") == 0 && (resultData.getInt("mGroupingId") != 0))
					{ 
						data.setmTaskId(null);
						data.setmGroupingId(resultData.getInt("mGroupingId"));
					}
					else if(resultData.getInt("mTaskId") == 0 && (resultData.getInt("mGroupingId") == 0))
					{
						data.setmTaskId(null);
						data.setmGroupingId(null);
					}
					data.setStatus(resultData.getInt("status"));
					data.setPriority(resultData.getInt("priority"));
					data.setDescription(resultData.getString("description"));
					data.setCompanyId(company);
					ProjectDetail.add(data);
					
					if(resultData.getJSONArray("karyawanDetail").size() > 0)
					{
						JSONArray DetailProject = resultData.getJSONArray("karyawanDetail");
						
						
						for (int b = 0; b < DetailProject.size(); b++) {
							ProjectDetailKaryawan data2 = new ProjectDetailKaryawan();
							data2.setCompanyId(company);
							data2.setmProjectId(project);
							data2.setProjectDetailId(resultData.getInt("projectDetailId"));
							data2.setProjectDetailKaryawanId(DetailProject.getJSONObject(b).getInt("projectDetailKaryawanId"));
							data2.setContactId(UUID.fromString(DetailProject.getJSONObject(b).getString("contactId")));
							dataKaryawan.add(data2);
						}
						
					}
					
					if(resultData.getJSONArray("relationDetail").size() > 0)
					{
						JSONArray DetailProject = resultData.getJSONArray("relationDetail");
						
						
						for (int b = 0; b < DetailProject.size(); b++) {
							ProjectDetailDependency data2 = new ProjectDetailDependency();
							data2.setCompanyId(company);
							data2.setmProjectId(project);
							data2.setProjectDetailId(resultData.getInt("projectDetailId"));
							data2.setProjectDetailDependencyId(DetailProject.getJSONObject(b).getInt("projectDetailDependencyId"));
							data2.setNoDependency(DetailProject.getJSONObject(b).getInt("noDependency"));
							dataDependency.add(data2);
						}
						
					}
//                   ListDocumentSummarySubcategoryService.save(data);
//                   logger.info("data id = " + data.getId());
//                   logger.info("data parentid = " + data.getParentId());
                   
//                   // disiini olah data nya, misal insert ke tabel
                   JSONArray dataAnotherChild = (JSONArray) JSONSerializer.toJSON(resultData.get("childrenItems"));
                   logger.info("dataAnotherChild = " + dataAnotherChild.size());
                   if(dataAnotherChild.size() > 0){
                         hasChild(dataAnotherChild,  company, project);
                         
                   }
              }
    }
	
	@PostMapping({"/DetailProjectKaryawan"})
	public String DetailProjectKaryawan(@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> contaa = new ArrayList<>();
	    String level = "";
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	List<Object[]> data1 = ProjectDetailKaryawanService.getProjectDetailKaryawa(dataRequest.getInt("projectId"), dataRequest.getInt("projectDetailId"), dataRequest.getInt("company_id") );
	    	data1.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("projectDetailId", col[0]);
	    		data.put("projectDetailKaryawanId", col[1]);
	    		data.put("projectId", col[2]);
	    		data.put("companyId", col[3]);
	    		data.put("contactId", col[4]);
	    		cont.add(data);
	    	});
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show Detail Project Karyawan Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show Detail Project Karyawan Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping({"/DetailProject"})
	public String DetailProject(@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    
	    List<JSONObject> contaa = new ArrayList<>();
	    List<JSONObject> cont = new ArrayList<>();
	    String level = "";
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	ArrayList<ProjectDetailChild> pairs = new ArrayList<ProjectDetailChild>();
	    	List<Object[]> data1 = ProjectDetailService.getProjectDetail(dataRequest.getInt("projectId"), dataRequest.getInt("company_id") );
	    	
	    	data1.stream().forEach(col1->{
	    		List<JSONObject> karyawan = new ArrayList<>();
	    		List<ProjectDetailDependency> dependency = new ArrayList<>();
	    		List<Object[]> data12 = ProjectDetailKaryawanService.getProjectDetailKaryawa(dataRequest.getInt("projectId"), Integer.parseInt(col1[0].toString()), dataRequest.getInt("company_id") );
		    	data12.stream().forEach(col->{
		    		JSONObject data = new JSONObject();
		    		data.put("projectDetailId", col[0]);
		    		data.put("projectDetailKaryawanId", col[1]);
		    		data.put("projectId", col[2]);
		    		data.put("companyId", col[3]);
		    		data.put("contactId", col[4]);
		    		karyawan.add(data);
		    	});
//		    	
		    	List<ProjectDetailDependency> findDetail = ProjectDetailDependencyService.findDetail(dataRequest.getInt("projectId"), Integer.parseInt(col1[0].toString()), dataRequest.getInt("company_id"));
		        findDetail.stream().forEach(col->{
		        	ProjectDetailDependency data = new ProjectDetailDependency();
		        	data.setCompanyId(col.getCompanyId());
		        	data.setmProjectId(col.getmProjectId());
		        	data.setNoDependency(col.getNoDependency());
		        	data.setProjectDetailDependencyId(col.getProjectDetailDependencyId());
		        	data.setProjectDetailId(col.getProjectDetailId());
		        	dependency.add(data);
		        });
		        ProjectDetailChild data = new ProjectDetailChild();
		        data.setChildId(Integer.parseInt(col1[0].toString()));
		        data.setParentId(Integer.parseInt(String.valueOf(col1[1])));
		        data.setCompanyId(Integer.parseInt(col1[2].toString()));
		        data.setmProjectId(Integer.parseInt(String.valueOf(col1[4])));
		        data.setmTaskId(col1[5] == null ? 0 : Integer.parseInt(col1[5].toString()));
		        data.setmGroupingId(col1[6] == null ? 0 :Integer.parseInt(col1[6].toString()));
		        data.setStatus(Integer.parseInt(col1[7].toString()));
		        data.setPriority(Integer.parseInt(col1[8].toString()));
		        data.setDescription(col1[9] == null ? "" : col1[9].toString() );
		        data.setTemplateId(col1[10] == null ? 0 : Integer.parseInt(col1[10].toString()));
		        data.setmTaskNama(col1[12] == null ? "" :String.valueOf(col1[12]));
		        
		        data.setmGroupingNama(col1[13] == null ? "" :String.valueOf(col1[13]));
		        
		        
		        
		        data.setJumlahKaryawan(Integer.parseInt(col1[14].toString()));
		        data.setJumlahDependency(Integer.parseInt(col1[15].toString()));
		        data.setTemplateNama(col1[16] == null ? "" :String.valueOf(col1[16]));
		        data.setKaryawanDetail(karyawan);
		        data.setRelationDetail(dependency);
		        
		        pairs.add(data);
    			
	    	});
	    	Map<Integer, ProjectDetailParent> hm = new HashMap<>();
	    	 for(ProjectDetailChild p:pairs){

		        	
		            //  ----- Child -----
				 	ProjectDetailParent mmdChild ;
		            if(hm.containsKey(p.getChildId())){
		                mmdChild = hm.get(p.getChildId());
		            }
		            else{
		                mmdChild = new ProjectDetailParent();
		                hm.put(p.getChildId(), mmdChild);
		            }  
		            mmdChild.setId(p.getChildId());
		            mmdChild.setParentId(p.getParentId());
		            mmdChild.setmProjectId(p.getmProjectId());
		            mmdChild.setmTaskId(p.getmTaskId());
		            mmdChild.setmTaskNama(p.getmTaskNama());
		            mmdChild.setmGroupingId(p.getmGroupingId());
		            mmdChild.setmGroupingNama(p.getmGroupingNama());
		            mmdChild.setStatus(p.getStatus());
		            mmdChild.setPriority(p.getPriority());
		            mmdChild.setCompanyId(p.getCompanyId());
		            mmdChild.setJumlahKaryawan(p.getJumlahKaryawan());
		            mmdChild.setJumlahDependency(p.getJumlahDependency());
		            mmdChild.setKaryawanDetail(p.getKaryawanDetail());
		            mmdChild.setRelationDetail(p.getRelationDetail());
		            mmdChild.setTemplateId(p.getTemplateId());
		            mmdChild.setTemplateNama(p.getTemplateNama());
		            mmdChild.setDescription(p.getDescription());
		            // no need to set ChildrenItems list because the constructor created a new empty list



		            // ------ Parent ----
		            ProjectDetailParent mmdParent ;
		            if(hm.containsKey(p.getParentId())){
		                mmdParent = hm.get(p.getParentId());
		            }
		            else{
		                mmdParent = new ProjectDetailParent();
		                hm.put(p.getParentId(),mmdParent);
		            }
		            
		            mmdParent.setId(p.getParentId());
		            mmdParent.setParentId(null);
		            mmdParent.addChildren(mmdChild);
		            mmdParent.setmProjectId(p.getmProjectId());
		            mmdParent.setmTaskId(p.getmTaskId());
		            mmdParent.setmTaskNama(p.getmTaskNama());
		            mmdParent.setmGroupingId(p.getmGroupingId());
		            mmdParent.setmGroupingNama(p.getmGroupingNama());
		            mmdParent.setStatus(p.getStatus());
		            mmdParent.setPriority(p.getPriority());
		            mmdParent.setCompanyId(p.getCompanyId());
		            mmdParent.setJumlahKaryawan(p.getJumlahKaryawan());
		            mmdParent.setJumlahDependency(p.getJumlahDependency());
		            mmdParent.setKaryawanDetail(p.getKaryawanDetail());
		            mmdParent.setRelationDetail(p.getRelationDetail());
		            mmdParent.setTemplateId(p.getTemplateId());
		            mmdParent.setTemplateNama(p.getTemplateNama());
		            mmdChild.setDescription(p.getDescription());
		        }
			 
			// Get the root
		        List<ProjectDetailParent> DX = new ArrayList<ProjectDetailParent>(); 
		        for(ProjectDetailParent mmd : hm.values()){
		            if(mmd.getParentId() == null)
		                DX.add(mmd);
		        }

		        // Print 
		        for(ProjectDetailParent mmd: DX){
		            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
		            
		            JSONObject data3 = new JSONObject();
		            
		            data3.put("data",mmd);
		            cont.add(data3);
		        }
		        for(int a= 0 ; a< cont.size() ; a++)
		        {
		        	level = cont.get(a).getString("data");
		        	logger.info("cont = " + cont.get(a).getString("data"));
		        }
		        
		        JSONObject dataRequestLevel = new JSONObject();
		        List<JSONObject> resultArray2 = new ArrayList<>();
		        if(!level.isEmpty())
		        {
		        	dataRequestLevel = (JSONObject)JSONSerializer.toJSON(level);
		        	List<JSONObject> list = new ArrayList<JSONObject>();
		            for (int i = 0; i < dataRequestLevel.getJSONArray("children").size(); i++) {
		                    list.add(dataRequestLevel.getJSONArray("children").getJSONObject(i));
		            }
		            
		            Collections.sort(list, new SortBasedOnMessageId());
		            List<JSONObject> order = new ArrayList<>();
		        	//digunakan untuk order by list<JSONObject> biar urut dari id 1 ke id yang lebih besar
		            order = Helper.orderBy(list);
		            resultArray2 = order;
		        }
		        
			 
		       

	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show Detail Project Success");
		    response.put("responseData", resultArray2);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show Detail Project Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	private List<JSONObject> orderBy(List<JSONObject> cont) {
		List<JSONObject> cont1 = new ArrayList<JSONObject>();
		List<JSONObject> baru = new ArrayList<JSONObject>();
        Collections.sort(cont, new SortBasedOnMessageId());
        if(cont.size()>0)
        {
        	for (int i = 0; i < cont.size(); i++) {
        		JSONArray dataAnotherChild = (JSONArray) JSONSerializer.toJSON(cont.get(i).getJSONArray("children"));
                if(dataAnotherChild.size() > 0){
                	List<JSONObject> list = new ArrayList<JSONObject>();
                	list =  orderBy(cont.get(i).getJSONArray("children"));
                	cont.get(i).put("children", list);
                      
                }
            }
        }
		return cont;
	}
	

	
	public String getUserId(String header )
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}
}
