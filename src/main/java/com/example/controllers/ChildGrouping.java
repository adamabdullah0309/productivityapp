package com.example.controllers;

import net.sf.json.JSONObject;

public class ChildGrouping {
    private Integer childId;
    private Integer parentId;
    private Integer taskId;
    private Integer company_id;
    private JSONObject data;
    public ChildGrouping(Integer childId, Integer parentId,Integer taskId,Integer company_id,JSONObject data
    		) {
        this.childId = childId;
        this.parentId = parentId;
        this.taskId = taskId;
        this.company_id = company_id;
        this.data = data;
    }
    
    
    




	public Integer getCompany_id() {
		return company_id;
	}




	public void setCompany_id(Integer company_id) {
		this.company_id = company_id;
	}




	








	public JSONObject getData() {
		return data;
	}







	public void setData(JSONObject data) {
		this.data = data;
	}







	public Integer getTaskId() {
		return taskId;
	}



	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}



	public Integer getChildId() {
		return childId;
	}

	public void setChildId(Integer childId) {
		this.childId = childId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
    
	
    
	
}