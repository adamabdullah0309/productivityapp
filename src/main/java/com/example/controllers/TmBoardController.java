package com.example.controllers;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;import static java.util.Objects.isNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.client.RestTemplate;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.service.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static java.util.stream.Collectors.toCollection;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.collectingAndThen;


@RestController
@RequestMapping("/api")
public class TmBoardController {
	private static final Logger logger = LoggerFactory.getLogger(TmBoardController.class);
	
	@Autowired
	private GabunganService GabunganService;
	
	@Autowired
	private TmBoardService TmBoardService;
	
	@Autowired
	private ProjectDetailService ProjectDetailService;
	
	@Autowired
	private MStageGroupDetailService MStageGroupDetailService;
	
	@Autowired
	private TmBoardProjectDetailDiscussionService TmBoardProjectDetailDiscussionService;
	
	@Autowired 
	private TmBoardProjectDetailService TmBoardProjectDetailService;
	
	@Autowired
	private TmBoardProjectDetailAttService TmBoardProjectDetailAttService;
	
	@Autowired
	private TmBoardProjectDetailNoteService TmBoardProjectDetailNoteService;
	
	int totalBoarding = 0;
	int totalBoardingDetail = 0;
	@PostMapping("/listBoardTemplate")
	public String listBoardTemplate(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> dataTaskAll = TmBoardService.dataTaskAllFromBoard(dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), user_uuid, dataRequest.getInt("boardId"));
	        
	        
	        ArrayList<ChildBoardTemplate> pairs = new ArrayList<ChildBoardTemplate>();
	    	ArrayList<ChildBoardTemplate> pairs2 = new ArrayList<ChildBoardTemplate>();
	    	
//	    	String projectName,Integer projectId, Integer projectDetailId, Integer stageId, String stageNama, Integer boardId, Integer totalStage,
//    		Integer templateId, String templateNama
	    	
	    	dataTaskAll.stream().forEach(col->{
	    		ChildBoardTemplate data3 = new ChildBoardTemplate();
	    		data3.setProjectName(col[1].toString());
	    		data3.setProjectId(Integer.valueOf(col[0].toString()));
	    		data3.setStageId(Integer.valueOf(col[5].toString()));
	    		data3.setStageNama(col[6].toString());
	    		data3.setBoardId(Integer.valueOf(col[10].toString()));
	    		data3.setTemplateId(Integer.valueOf(col[3].toString()));
	    		data3.setTemplateNama(col[4].toString());
	    		data3.setTotalTask(Integer.valueOf(col[7].toString()));
	    		data3.setJmlhAtt(Integer.valueOf(col[8].toString()));
	    		data3.setJmlhDisc(Integer.valueOf(col[9].toString()));
	    		data3.setCompanyId(Integer.valueOf(col[2].toString()));
	    		pairs.add(data3);
	    		pairs2.add(data3);
    		});
	    	
	    	final List<ChildBoardTemplate> duplicates = getDuplicatesTemplate(pairs);

	   		 //untuk menghapus duplikat
	   		 HashSet<Object> seen= new HashSet<>();
	   		 pairs2.removeIf(e -> !seen.add(Arrays.asList(e.getCompanyId(), e.getProjectId(), e.getTemplateId()  )));
	   		 
	   		 logger.info("duplicates = " + duplicates);
	   		 if(pairs2.size()>0)
	   		 {
	   			 
	   			 pairs2.stream().forEach(col->{
	   				 totalBoarding = 0;
	   				 JSONObject data = new JSONObject();
	   				 data.put("projectName", col.getProjectName());
	   				 data.put("projectId", col.getProjectId());
	   				 data.put("templateId", col.getTemplateId());
	   				 data.put("templateNama", col.getTemplateNama());
	   				
	   				 totalBoardingDetail = 0;
	   				 List<JSONObject> duplikasi = new ArrayList<>();
	   				 duplicates.stream().forEach(col2->{
//	   					 
	   					 if(
	   							 col.getTemplateId().equals(col2.getTemplateId()) 
	   							 && col.getProjectId().equals(col2.getProjectId()) 
	   							 && col.getCompanyId().equals(col2.getCompanyId())  
	   					) 
	   					 {
//	   						 
	   						 JSONObject data2 = new JSONObject();
	   						 data2.put("projectId", col2.getProjectId());
	   	    				 data2.put("stageId", col2.getStageId());
	   	    				 data2.put("stageNama", col2.getStageNama());
	   	    				 data2.put("totalTask", col2.getTotalTask());
	   	    				 data2.put("totalDoc", col2.getJmlhAtt());
	   	    				 data2.put("totalDisc", col2.getJmlhDisc());
	   	    				 duplikasi.add(data2);
	//   	    				 totalBoarding++;
	   	    				 totalBoarding = totalBoarding + Integer.valueOf(col2.getTotalTask().toString());
	   					 }
//	   					 
	   				 });
	   				 data.put("totalTask", totalBoarding);
	   				 data.put("detail",duplikasi);
	   				 cont.add(data);
	   			 });
	   		 }
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Board Template Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Board Template Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}

	
	@PostMapping("/listBoardGrouping")
	public String listBoardGrouping(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> baru = new ArrayList<>();
	    List<JSONObject> duplicate = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    List<ParentBoardGrouping> parent = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	    	List<Object[]> dataBoardGrouping = TmBoardService.dataBoardGrouping(dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), user_uuid, dataRequest.getInt("boardId"));
	    	
	    	ArrayList<ChildBordGrouping> pairs = new ArrayList<ChildBordGrouping>();
	    	ArrayList<ChildBordGrouping> pairs2 = new ArrayList<ChildBordGrouping>();
	    	
//	    	String projectName,Integer projectId, Integer projectDetailId, Integer stageId, String task, String stageNama, String boardId
	    	
//	    	String projectName,Integer projectId, Integer projectDetailId, Integer stageId, String task, String stageNama, Integer boardId, Integer totalStage,
//    		Integer jmlhDisc, Integer jmlhAtt, Integer companyId
    		dataBoardGrouping.stream().forEach(col->{
    			pairs.add(new ChildBordGrouping(
    					col[1].toString(),
    					Integer.valueOf(col[0].toString()),  
    					Integer.valueOf(col[2].toString()), 
    					Integer.valueOf(col[4].toString()), 
    					col[11].toString(), 
    					col[5].toString(), 
    					Integer.valueOf(col[12].toString()),
    					Integer.valueOf(col[6].toString()),
    					Integer.valueOf(col[8].toString()),
    					Integer.valueOf(col[7].toString()),
    					Integer.valueOf(col[3].toString())
    					));
    			pairs2.add(new ChildBordGrouping(
    					col[1].toString(),
    					Integer.valueOf(col[0].toString()),  
    					Integer.valueOf(col[2].toString()), 
    					Integer.valueOf(col[4].toString()), 
    					col[11].toString(), 
    					col[5].toString(), 
    					Integer.valueOf(col[12].toString()),
    					Integer.valueOf(col[6].toString()),
    					Integer.valueOf(col[8].toString()),
    					Integer.valueOf(col[7].toString()),
    					Integer.valueOf(col[3].toString())
    					));
    		});
    		
    		 final List<ChildBordGrouping> duplicates = getDuplicates(pairs);
    		 
    		 //untuk menghapus duplikat
    		 HashSet<Object> seen= new HashSet<>();
    		 pairs2.removeIf(e -> !seen.add(Arrays.asList(e.getProjectDetailId(), e.getProjectId()  )));
    		 
    		 
    		 logger.info("duplicates = " + duplicates);
    		 
    		 if(pairs2.size()>0)
    		 {
    			 
    			 pairs2.stream().forEach(col->{
    				 totalBoarding = 0;
    				 JSONObject data = new JSONObject();
    				 data.put("projectName", col.getProjectName());
    				 data.put("projectId", col.getProjectId());
    				 data.put("projectDetailId", col.getProjectDetailId());
    				 data.put("taskNama", col.getTask());
    				
    				 totalBoardingDetail = 0;
    				 List<JSONObject> duplikasi = new ArrayList<>();
    				 duplicates.stream().forEach(col2->{
    					 
    					 if(col.getProjectDetailId().equals(col2.getProjectDetailId()) 
    							 && col.getProjectId().equals(col2.getProjectId()) 
	   							 && col.getCompanyId().equals(col2.getCompanyId())  ) 
    					 {
    						 
    						 JSONObject data2 = new JSONObject();
    						 data2.put("projectId", col2.getProjectId());
    	    				 data2.put("projectDetailId", col2.getProjectDetailId());
    	    				 data2.put("stageId", col2.getStageId());
    	    				 data2.put("stageNama", col2.getStageNama());
    	    				 data2.put("totalStage", col2.getTotalStage());
    	    				 data2.put("totalDoc", col2.getJmlhDisc());
    	    				 data2.put("totalAtt", col2.getJmlhAtt());
    	    				 duplikasi.add(data2);
//    	    				 totalBoarding++;
    	    				 totalBoarding = totalBoarding + Integer.valueOf(col2.getTotalStage().toString());
    					 }
    					 
    				 });
    				 data.put("totalTask", totalBoarding);
    				 data.put("detail",duplikasi);
    				 cont.add(data);
    			 });
    		 }
    		
    		 
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Board Grouping Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Board Grouping Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	//menampilkan stage nya untuk tampilan klo dibutuhkan
	@PostMapping("/listStageBoarding")
	public String listStageBoarding(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> baru = new ArrayList<>();
	    List<JSONObject> duplicate = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    List<ParentBoardGrouping> parent = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> dataBoardStage = TmBoardService.listStageBoard(dataRequest.getInt("boardId"), dataRequest.getInt("company_id"));
	        
	        dataBoardStage.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("boardId", col[0]);
	        	data.put("stageId", col[1]);
	        	data.put("stageNama", col[2]);
	        	data.put("company_id", col[3]);
	        	data.put("boardStageId", col[4]);
	        	cont.add(data);
	        });
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Stage Board Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Stage Board Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	// list task di tab task
	@PostMapping("/listBoardTask")
	public String listBoardTask(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> baru = new ArrayList<>();
	    List<JSONObject> duplicate = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    List<ParentBoardGrouping> parent = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> dataTask = TmBoardService.listdataTask(dataRequest.getInt("projectId"), dataRequest.getInt("company_id"), user_uuid, dataRequest.getInt("boardId") );
	        
	        dataTask.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("projectId", col[0]);
	        	data.put("projectNama", col[1]);
	        	data.put("asalProjectDetailId", col[2]);
	        	data.put("projectDetailId", col[8]);
	        	data.put("taskId", col[6]);
	        	data.put("groupingId", col[7] == null ? "" : col[7]);
	        	data.put("taskNama", col[11]);
	        	data.put("stageId", col[4]);
	        	data.put("stageNama", col[5]);
	        	data.put("contactId", col[15] == null ? "" : col[15]);
	        	data.put("boardId", col[12]);
	        	data.put("role", col[16]);
	        	data.put("company_id", col[3]);
	        	data.put("jumlahDoc", col[6]);
	        	data.put("jumlahDisc", col[7]);
	        	data.put("boardStageId", col[13]);
	        	data.put("boardProjectDetailId", col[14]);
	        	cont.add(data);
	        });
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Board Task Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Board Task Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	// ini untuk ketika awal mau create sebuah project maka ambil yang ini
	@PostMapping("/listDrawerProject")
	public String listDrawerProject(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> baru = new ArrayList<>();
	    List<JSONObject> duplicate = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    List<ParentBoardGrouping> parent = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> dataListDrawerProject = TmBoardService.listDrawerProject(dataRequest.getInt("company_id"));
	        dataListDrawerProject.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("projectId", col[0] == null ? "" : col[0]);
	        	data.put("projectName", col[1] == null ? "" : col[1]);
	        	cont.add(data);
	        });
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Drawer Project Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Drawer Project Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	//ini untuk list project yang sudah ter created
	@PostMapping("/listProjectBoard")
	public String listProjectBoard(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> baru = new ArrayList<>();
	    List<JSONObject> duplicate = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    List<ParentBoardGrouping> parent = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> dataListDrawerProject = TmBoardService.listProjectBoard(dataRequest.getInt("company_id"), user_uuid);
	        dataListDrawerProject.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("boardId", col[0]);
	        	data.put("projectId", col[1]);
	        	data.put("projectName", col[2]);
	        	data.put("company_id", col[3]);
	        	cont.add(data);
	        });
	        
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Project Board Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Project Board Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	//ini untuk awalan, ketika mau create board milih stage pakek ini
	@PostMapping("/listStage")
	public String listStage(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> baru = new ArrayList<>();
	    List<JSONObject> duplicate = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    List<ParentBoardGrouping> parent = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> listStage = TmBoardService.listStageDrawer(dataRequest.getInt("company_id"));

	        ArrayList<BoardDrawer> pairs = new ArrayList<BoardDrawer>();	    		
	        ArrayList<BoardDrawer> pairs2 = new ArrayList<BoardDrawer>();
	        listStage.stream().forEach(col->{
    			pairs.add(new BoardDrawer(Integer.valueOf(col[0].toString()),col[1].toString(),  Integer.valueOf(col[2].toString())
    					, col[3].toString(), Integer.valueOf(col[4].toString())
    					));
    			pairs2.add(new BoardDrawer(Integer.valueOf(col[0].toString()),col[1].toString(),  Integer.valueOf(col[2].toString())
    					, col[3].toString(), Integer.valueOf(col[4].toString())
    					));
    		});
    		
    		 final List<BoardDrawer> duplicates = getDuplicatesDrawer(pairs);
    		 
    		 //untuk menghapus duplikat
    		 HashSet<Object> seen= new HashSet<>();
    		 pairs2.removeIf(e -> !seen.add(Arrays.asList(e.getStateGroupId(), e.getCompanyId()  )));

    		 if(pairs2.size()>0)
    		 {
    			 pairs2.stream().forEach(col->{
    				 JSONObject data = new JSONObject();
    				 data.put("stageGroupName", col.getStateGroupName());
    				 data.put("stageGroupId", col.getStateGroupId());
    				 data.put("stageId", col.getStageId());
    				 data.put("stageNama", col.getStageNama());
    				 data.put("company_id", col.getCompanyId());
    				 
    				 List<JSONObject> duplikasi = new ArrayList<>();
    				 duplicates.stream().forEach(col2->{
    					 if(col.getStateGroupId().equals(col2.getStateGroupId()) && col.getCompanyId().equals(col2.getCompanyId()) ) 
    					 {
    						 
    						 JSONObject data2 = new JSONObject();
    	    				 data2.put("stageId", col2.getStageId());
    	    				 data2.put("stageNama", col2.getStageNama());
    	    				 duplikasi.add(data2);
    	    				 
    					 }
    				 });
    				 data.put("detail",duplikasi);
    				 cont.add(data);
    			 });
    		 }
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Show List Stage Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show List Stage Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	//untuk detail attachement
	@PostMapping("/DetailBoardDetailAttachement")
	public String DetailBoardDetailAttachement(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> baru = new ArrayList<>();
	    List<JSONObject> duplicate = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    List<ParentBoardGrouping> parent = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> listBoardDetailAtt = TmBoardService.listBoardDetailAtt(dataRequest.getInt("company_id"), dataRequest.getInt("boardId"), dataRequest.getInt("boardProjectDetailId"));
	        if(listBoardDetailAtt.size()>0)
	        {
	        	listBoardDetailAtt.stream().forEach(col->{
	        		JSONObject data = new JSONObject();
	        		data.put("boardId", col[0] == null ? "" : col[0]);
	        		data.put("boardProjectDetailId", col[1] == null ? "" : col[1]);
	        		data.put("boardProjectDetailAttId", col[2] == null ? "" : col[2]);
	        		data.put("docDescription", col[3] == null ? "" : col[3]);
	        		data.put("docTypeId", col[4] == null ? "" : col[4]);
	        		data.put("company_id", col[5] == null ? "" : col[5]);
	        		data.put("filePath", col[6] == null ? "" : col[6]);
	        		data.put("datedocument", col[7] == null ? "" : col[7].toString());
	        		data.put("namaFile", col[8] == null ? "" : col[8].toString());
	        		data.put("docTypeNama", col[9] == null ? "" : col[9]);
	        		cont.add(data);
	        	});
	        }
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Board Attachement Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Board Attachement Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	//-----------------------------------------------------------------------------untuk save
	Integer nomor = 1;
	Integer nomor2 = 1;
	 int id = 0;
	 
	 //untuk save awalan ketika memilih project yang mana di board
	@PostMapping("/saveBoard")
	public String saveBoard(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        
	        id = 0;
	        TmBoard data = new TmBoard();
	        if(dataRequest.has("boardId"))
			{
				id = dataRequest.getInt("boardId");
				data.setUpdatedBy(user_uuid);
				data.setUpdatedAt(localDateTime2);
			}
			else
			{
				List<Object[]> dataNext = TmBoardService.nextval();
				id = Integer.parseInt(String.valueOf(dataNext.get(0)));
				data.setBoardId(id);
				data.setCreatedAt(localDateTime2);
				data.setCreatedBy(user_uuid);
			}
			
			data.setmProjectId(dataRequest.getInt("mProjectId"));
			data.setmStageGroupId(dataRequest.getInt("mStageGroupId"));
			data.setCompanyId(dataRequest.getInt("company_id"));
			List<Object[]> getProjectDetail = ProjectDetailService.getProjectDetailAktif(dataRequest.getInt("mProjectId"), dataRequest.getInt("company_id"));
			List<Object[]> getStageDetailAll = MStageGroupDetailService.getStageDetailAll(dataRequest.getInt("mStageGroupId"), dataRequest.getInt("company_id"));
			if(getStageDetailAll.size()>0)
			{
				nomor = 1;
				getStageDetailAll.stream().forEach(col->{
					TmBoardStage data3 = new TmBoardStage();
					data3.setTmBoardId(id);
					data3.setCompanyId(dataRequest.getInt("company_id"));
					data3.setTmBoardStageId(nomor);
					data3.setMStageId(Integer.parseInt(String.valueOf(col[2])));
					DataStage.add(data3);
					nomor++;
				});
				if(getProjectDetail.size()>0)
				{
					nomor2 = 1;
					getProjectDetail.stream().forEach(col->{
						TmBoardProjectDetail data2 = new TmBoardProjectDetail();
						data2.setTmBoardId(id);
						data2.setTmBoardStageId(1);
						data2.setTmProjectId(dataRequest.getInt("mProjectId"));
						data2.setTmBoardProjectDetailId(nomor2);
						data2.setProjectDetailId(Integer.parseInt(String.valueOf(col[0])));
						data2.setCompanyId(dataRequest.getInt("company_id"));
						DataProjectDetail.add(data2);
						nomor2++;
					});
				}
			}
			
			GabunganService.saveBoardFirst(data, DataStage, DataProjectDetail);
			response.put("responseCode", "00");
		    response.put("responseDesc", "Save Board Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Board Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}

	
	@PostMapping("/saveBoardAttachement")
	public String saveBoardAttachement(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetailAtt> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        JSONArray arrayTask = dataRequest.getJSONArray("boardDocumentArray");
		      
			
			if (arrayTask.size() > 0)
			{
				for (int a = 0; a < arrayTask.size(); a++) {
					TmBoardProjectDetailAtt dataDetail = new TmBoardProjectDetailAtt();
					dataDetail.setTmBoardId(arrayTask.getJSONObject(a).getInt("boardId"));
					dataDetail.setCompanyId(arrayTask.getJSONObject(a).getInt("company_id"));
					dataDetail.setTmBoardProjectDetailAttId(arrayTask.getJSONObject(a).getInt("boardProjectDetailAttId"));
					dataDetail.setTmBoardProjectDetailId(arrayTask.getJSONObject(a).getInt("boardProjectDetailId"));
					dataDetail.setDatedocument(localDateTime2);
					dataDetail.setDocDescription(arrayTask.getJSONObject(a).getString("docDescription"));
					dataDetail.setDocType(arrayTask.getJSONObject(a).getInt("docType"));
					dataDetail.setFilePath(arrayTask.getJSONObject(a).getString("filePath"));
					dataDetail.setNamaFile(arrayTask.getJSONObject(a).getString("namaFile"));
					DataProjectDetail.add(dataDetail);
				}
			}
			GabunganService.saveBoardAttchement(DataProjectDetail);
			response.put("responseCode", "00");
		    response.put("responseDesc", "Save Board Attachement Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Board Attachement Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/DeleteBoardAttachement")
	public String DeleteBoardAttachement(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetailAtt> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        TmBoardProjectDetailAtt dataDetail = new TmBoardProjectDetailAtt();
			dataDetail.setTmBoardId(dataRequest.getInt("boardId"));
			dataDetail.setCompanyId(dataRequest.getInt("company_id"));
			dataDetail.setTmBoardProjectDetailAttId(dataRequest.getInt("boardProjectDetailAttId"));
			dataDetail.setTmBoardProjectDetailId(dataRequest.getInt("boardProjectDetailId"));
			
			TmBoardProjectDetailAttService.deleteAtt(dataDetail);
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Delete Board Attachement Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Delete Board Attachement Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	
	//----------------------------------------------------------------------------------------------discussion task
	
	@PostMapping("/SaveDiscussionTask")
	public String SaveDiscussionTask(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequestLevel = new JSONObject();
	    String level = "";
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        TmBoardProjectDetailDiscussion data = new TmBoardProjectDetailDiscussion();
	        data.setTmBoardId(dataRequest.getInt("boardId"));
	        data.setTmBoardProjectDetailId(dataRequest.getInt("boardProjectDetailId"));
	        
	        List<Object[]> dataNext = TmBoardProjectDetailDiscussionService.nextval(dataRequest.getInt("boardId"), dataRequest.getInt("boardProjectDetailId"), dataRequest.getInt("company_id"));
	    	int id = 1;
	    	if(dataNext.size()>0)
	    	{
	    		id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    		id++;
	    	}
	        data.setTmBoardProjectDetailDisId(id);
	        
	        data.setTmBoardProjectDetailDisParentId(dataRequest.getInt("boardProjectDetailDisParentId"));
	        data.setCommentDiscussion(dataRequest.getString("commentDiscussion"));
	        data.setContactId(user_uuid);
	        data.setCompanyId(dataRequest.getInt("company_id"));
	        data.setDateComment(localDateTime2);
	        
	        TmBoardProjectDetailDiscussionService.save(data);
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Save Discussion Task Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Discussion Task Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/DeleteDiscussionTask")
	public String DeleteDiscussionTask(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequestLevel = new JSONObject();
	    String level = "";
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        TmBoardProjectDetailDiscussion data = new TmBoardProjectDetailDiscussion();
	        data.setTmBoardId(dataRequest.getInt("boardId"));
	        data.setTmBoardProjectDetailId(dataRequest.getInt("boardProjectDetailId"));
	        data.setTmBoardProjectDetailDisId(dataRequest.getInt("boardProjectDetailDisId"));
	        data.setCompanyId(dataRequest.getInt("company_id"));
	        
	        TmBoardProjectDetailDiscussionService.delete(data);
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Delete Discussion Task Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Delete Discussion Task Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/listDiscussionTask")
	public String listDiscussionTask(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequestLevel = new JSONObject();
	    String level = "";
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
//	        logger.info("user_uuid = " + user_uuid);
	        
	        List<Object[]> listDetailCategory = TmBoardService.listDiscussionTask(dataRequest.getInt("boardId"), dataRequest.getInt("boardProjectDetailId"), dataRequest.getInt("company_id"), user_uuid  );
	        ArrayList<ChildComment> pairs = new ArrayList<ChildComment>();
	        if(listDetailCategory.size()>0)
	        {
	        	listDetailCategory.stream().forEach(col->{
//	        		LocalDateTime coba = LocalDateTime.parse(col[8].toString(), formatter);
	        		ChildComment data = new ChildComment ();
	        		data.setChildId(Integer.valueOf(col[3].toString()));
	        		data.setParentId(Integer.valueOf(col[4].toString()));
	        		data.setCompany_id(Integer.valueOf(col[5].toString()));
	        		data.setCommentDiscussion(col[6].toString());
	        		data.setBoardId(Integer.valueOf(col[0].toString()));
	        		data.setBoardProjectDetailId(Integer.valueOf(col[2].toString()));
	        		data.setBoardProjectDetailDisId(Integer.valueOf(col[3].toString()));
	        		data.setContactId(col[7].toString());
	        		data.setDateComment(col[8].toString());
	        		data.setRole(col[10].toString());
	        		pairs.add(data);
	        	});
	        	
	        	Map<Integer, ParentComment> hm = new HashMap<>();
	        	
	        	// populate a Map
		        for(ChildComment p:pairs){

		        	
		            //  ----- Child -----
		        	ParentComment mmdChild ;
		            if(hm.containsKey(p.getChildId())){
		                mmdChild = hm.get(p.getChildId());
		            }
		            else{
		                mmdChild = new ParentComment();
		                hm.put(p.getChildId(), mmdChild);
		            }  
		            mmdChild.setBoardId(p.getBoardId());
		            mmdChild.setBoardProjectDetailId(p.getBoardProjectDetailId());
		            mmdChild.setBoardProjectDetailDisId(p.getBoardProjectDetailDisId());
		            mmdChild.setId(p.getChildId());
		            mmdChild.setParentId(p.getParentId());
		            mmdChild.setCommentDiscussion(p.getCommentDiscussion());
		            mmdChild.setCompany_id(p.getCompany_id());
		            mmdChild.setContactId(p.getContactId());
		            mmdChild.setDateComment(p.getDateComment());
		            mmdChild.setRole(p.getRole());
		            // no need to set ChildrenItems list because the constructor created a new empty list



		            // ------ Parent ----
		            ParentComment mmdParent ;
		            if(hm.containsKey(p.getParentId())){
		                mmdParent = hm.get(p.getParentId());
		            }
		            else{
		                mmdParent = new ParentComment();
		                hm.put(p.getParentId(),mmdParent);
		            }
		            mmdParent.setBoardId(p.getBoardId());
		            mmdParent.setId(p.getParentId());
		            mmdParent.setParentId(null);
		            mmdParent.addChildrenItem(mmdChild);
		            mmdParent.setBoardProjectDetailId(p.getBoardProjectDetailId());
		            mmdParent.setBoardProjectDetailDisId(p.getBoardProjectDetailDisId());
		            mmdParent.setCompany_id(p.getCompany_id());
		            mmdParent.setDateComment(p.getDateComment());
		            mmdParent.setContactId(p.getContactId());
		            mmdParent.setRole(p.getRole());
		        }
		        
		        // Get the root
		        List<ParentComment> DX = new ArrayList<ParentComment>(); 
		        for(ParentComment mmd : hm.values()){
		            if(mmd.getParentId() == null)
		                DX.add(mmd);
		        }

		        // Print 
		        for(ParentComment mmd: DX){
		            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
		            JSONObject data = new JSONObject();
		            
		            data.put("data",mmd);
		            cont.add(data);
		        }
		        for(int a= 0 ; a< cont.size() ; a++)
		        {
		        	level = cont.get(a).getString("data");
		        	logger.info("cont = " + cont.get(a).getString("data"));
		        }
		        for(int a= 0 ; a< cont.size() ; a++)
		        {
		        	level = cont.get(a).getString("data");
		        	logger.info("cont = " + cont.get(a).getString("data"));
		        }
		        dataRequestLevel = (JSONObject)JSONSerializer.toJSON(level);
	        }
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "List Discussion Task Success");
		    response.put("responseData", dataRequestLevel.get("childrenItems"));
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "List Discussion Task Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	//----------------------------------------------------------------------------------------------discussion task
	
	//--------------------------------------------------------------------------------------------------------------update detail board
	
	@PostMapping("/updateBoardStage")
	public String updateBoardStage(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequestLevel = new JSONObject();
	    String level = "";
	    List<JSONObject> final11 = new ArrayList<>();
	    List<TmBoardProjectDetail> dataFinal = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try { 
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        Optional <TmBoardProjectDetail> dataUpdate = TmBoardProjectDetailService.getDetail(dataRequest.getInt("boardId"), dataRequest.getInt("boardProjectDetailId"), dataRequest.getInt("company_id"));
	        if(dataUpdate.isPresent())
	        {
	        	((TmBoardProjectDetail)dataUpdate.get()).setTmBoardStageId(dataRequest.getInt("boardStageId"));
	        	TmBoardProjectDetailService.updateBoardStage(dataUpdate.get());
	        }
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Update Board Stage Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Update Board Stage Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
		return response.toString();
	}
	
	@PostMapping("/listLog")
	public String listLog(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequestLevel = new JSONObject();
	    String level = "";
	    List<JSONObject> final11 = new ArrayList<>();
	    List<JSONObject> dataHeader = new ArrayList<>();
	    List<JSONObject> dataDetail = new ArrayList<>();
	    List<TmBoardProjectDetail> dataFinal = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try { 
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> dataAll = TmBoardProjectDetailService.listLog(dataRequest.getInt("projectId"));
	        
	        dataAll.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("projectId", col[0]);
	        	data.put("timedate", col[1].toString());
	        	data.put("user_id", col[2].toString());
	        	data.put("action", col[3].toString());
	        	data.put("value", col[4].toString());
	        	dataDetail.add(data);
	        	dataHeader.add(data);
	        });
	        
	        HashSet<Object> seen= new HashSet<>();
	        dataHeader.removeIf(e -> !seen.add(Arrays.asList(e.getInt("projectId"), e.getString("timedate"))));
	        
	        
	        
	        dataHeader.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("projectId", col.getInt("projectId"));
	        	data.put("user_id", col.getString("user_id"));
	        	data.put("timedate", col.getString("timedate"));
	        	List<JSONObject> detail = new ArrayList<>();
	        	dataDetail.stream().forEach(col2->{
	        		if(col.getInt("projectId") == col2.getInt("projectId") 
	        				&& col.getString("timedate").equals(col2.getString("timedate"))
	        				&& col.getString("user_id").equals(col2.getString("user_id"))
	        				&& col.getString("action").equals(col2.getString("action"))
	        				)
	        		{
	        			JSONObject data2 = new JSONObject();
	        			data2.put("action", col2.getString("action"));
	        			data2.put("value", col2.getString("value"));
	        			detail.add(data2);
	        		}
	        	});
	        	
	        	data.put("detail", detail);
	        	cont.add(data);
	        });
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "List Log Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "List Log Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/updateBoard")
	public String updateBoard(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequestLevel = new JSONObject();
	    String level = "";
	    List<JSONObject> final11 = new ArrayList<>();
	    List<TmBoardProjectDetail> dataFinal = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        if(dataRequest.getJSONArray("detail").size() > 0)
		    {
	        	for(int a = 0 ; a < dataRequest.getJSONArray("detail").size(); a++)
		    	{
	        		TmBoardProjectDetail data = new TmBoardProjectDetail();
	     	        data.setTmBoardId(dataRequest.getJSONArray("detail").getJSONObject(a).getInt("boardId"));
	     	        data.setTmBoardStageId(dataRequest.getJSONArray("detail").getJSONObject(a).getInt("boardStageId"));
	     	        data.setTmBoardProjectDetailId(dataRequest.getJSONArray("detail").getJSONObject(a).getInt("boardProjectDetailId"));
	     	        data.setProjectDetailId(dataRequest.getJSONArray("detail").getJSONObject(a).getInt("projectDetailId"));
	     	        data.setDoneDate( dataRequest.getJSONArray("detail").getJSONObject(a).getString("doneDate").equals("") ? null : formatter11.parse(dataRequest.getJSONArray("detail").getJSONObject(a).getString("doneDate")));
	     	        data.setProgress(dataRequest.getJSONArray("detail").getJSONObject(a).getInt("progress"));
	     	        data.setTmProjectId(dataRequest.getInt("projectId"));
	     	        data.setCompanyId(dataRequest.getInt("company_id"));
	     	        dataFinal.add(data);
		    	}
		    }
	        
	       
	        
	        TmBoardProjectDetailService.updateMuch(dataFinal);
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Update Board Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Update Board Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------------------        
	
	
	@PostMapping("/saveNote")
	public String saveNote(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        
	        id = 0;
	        TmBoardProjectDetailNote data = new TmBoardProjectDetailNote();
	        if(dataRequest.has("boardProjectDetailNoteId"))
			{
				id = dataRequest.getInt("boardProjectDetailNoteId");
				data.setTmBoardProjectDetailNoteId(id);
			}
			else
			{
				List<Object[]> dataNext = TmBoardProjectDetailNoteService.nextval();
				id = Integer.parseInt(String.valueOf(dataNext.get(0)));
				data.setTmBoardProjectDetailNoteId(id);
			}
	        data.setTmBoardId(dataRequest.getInt("boardId"));
	        data.setTmBoardProjectDetailId(dataRequest.getInt("boardProjectDetailId"));
	        data.setCompanyId(dataRequest.getInt("company_id"));
	        data.setNote(dataRequest.getString("note"));
	        data.setDateNote(localDateTime2);
	        
	        TmBoardProjectDetailNoteService.save(data);
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Save Note Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Note Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	@PostMapping("/deleteNote")
	public String deleteNote(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        TmBoardProjectDetailNote data = new TmBoardProjectDetailNote();
	        data.setTmBoardId(dataRequest.getInt("boardId"));
	        data.setTmBoardProjectDetailId(dataRequest.getInt("boardProjectDetailId"));
	        data.setTmBoardProjectDetailNoteId(dataRequest.getInt("boardProjectDetailNoteId"));
	        data.setCompanyId(dataRequest.getInt("company_id"));
	        TmBoardProjectDetailNoteService.delete(data);
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Delete Note Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Delete Note Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	

	@PostMapping("/DetailNote")
	public String DetailNote(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	List<Object[]> dataAll = TmBoardProjectDetailNoteService.detailNote(dataRequest.getInt("boardId"), dataRequest.getInt("boardProjectDetailId"), dataRequest.getInt("boardProjectDetailNoteId"),dataRequest.getInt("company_id"));
	    	if(dataAll.size()>0)
	    	{
	    		dataAll.stream().forEach(col->{
	    			JSONObject data = new JSONObject();
		    		data.put("boardId", col[0]);
		    		data.put("boardProjectDetailId", col[1]);
		    		data.put("boardProjectDetailNoteId", col[2]);
		    		data.put("note", col[3]);
		    		data.put("company_id", col[4]);
		    		data.put("dateNote", col[5].toString());
		    		cont.add(data);
	    		});
	    		
	    		
	    	}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Note Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Note Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/listNote")
	public String listNote(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	List<Object[]> dataAll = TmBoardProjectDetailNoteService.listNote(dataRequest.getInt("boardId"),  dataRequest.getInt("boardProjectDetailId"),dataRequest.getInt("company_id"));
	    	if(dataAll.size()>0)
	    	{
	    		dataAll.stream().forEach(col->{
	    			JSONObject data = new JSONObject();
		    		data.put("boardId", col[0]);
		    		data.put("boardProjectDetailId", col[1]);
		    		data.put("boardProjectDetailNoteId", col[2]);
		    		data.put("note", col[3]);
		    		data.put("company_id", col[4]);
		    		data.put("dateNote", col[5].toString());
		    		cont.add(data);
	    		});
	    		
	    		
	    	}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "List Note Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "List Note Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	//-----------------------------------------------------------------------detail board task
	

	// detail member khusus grouping
	@PostMapping("/detailBoardMemberGrouping")
	public String detailBoardMemberGrouping(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> dataAll = TmBoardProjectDetailService.detailMember(dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), dataRequest.getInt("projectDetailId"), user_uuid, dataRequest.getInt("boardId"));
	    	
	    	if(dataAll != null)
	    	{
	    		dataAll.stream().forEach(col->{
		    		JSONObject data = new JSONObject();
		    		data.put("contact_id", col[0] == null ? "" : col[0]);
		    		cont.add(data);
		    	});
	    	}
	    	
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Member Board Grouping Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Member Board Grouping Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	// detail member khusus template
	@PostMapping("/detailBoardMemberTemplate")
	public String detailBoardMemberTemplate(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try 
	    {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> dataAll = TmBoardProjectDetailService.detailMemberTemplate(dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), user_uuid, dataRequest.getInt("boardId"));
	    	dataAll.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("contact_id", col[0] == null ? "" : col[0]);
	    		cont.add(data);
	    	});
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Member Board Template Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Member Board Template Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	// detail member khusus task
	@PostMapping("/detailBoardMemberTask")
	public String detailBoardMemberTask(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try 
	    {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> dataAll = TmBoardProjectDetailService.detailMemberTask(dataRequest.getInt("company_id"), dataRequest.getInt("boardId"), user_uuid, dataRequest.getInt("boardProjectDetailId"));
	    	dataAll.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("contact_id", col[0] == null ? "" : col[0]);
	    		cont.add(data);
	    	});
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Member Board Task Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Member Board Task Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	// detail attachement khusus template
	@PostMapping("/detailBoardAttachementTemplate")
	public String detailBoardAttachementTemplate(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try 
	    {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> dataAll = TmBoardProjectDetailService.detailAttachementTemplate(dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), user_uuid, dataRequest.getInt("boardId"));
	    	dataAll.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("projectId", col[0] == null ? "" : col[0]);
	    		data.put("projectDetailId", col[1] == null ? "" : col[1]);
	    		data.put("company_id", col[2] == null ? "" : col[2]);
	    		data.put("parent_id", col[3] == null ? "" : col[3]);
	    		data.put("contactId", col[4] == null ? "" : col[4]);
	    		data.put("boardId", col[5] == null ? "" : col[5]);
	    		data.put("boardStageId", col[6] == null ? "" : col[6]);
	    		data.put("boardProjectDetailId", col[7] == null ? "" : col[7]);
	    		data.put("namaTask", col[8] == null ? "" : col[8]);
	    		data.put("dateDocument", col[9] == null ? "" : col[9]);
	    		data.put("docDescription", col[10] == null ? "" : col[10]);
	    		data.put("docTypeId", col[11] == null ? "" : col[11]);
	    		data.put("docTypeNama", col[12] == null ? "" : col[12]);
	    		data.put("filePath", col[13] == null ? "" : col[13]);
	    		data.put("boardProjectDetailAttId", col[14] == null ? "" : col[14]);
	    		data.put("namaFile", col[15] == null ? "" : col[15]);
	    		cont.add(data);
	    	});
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Attachement Board Template Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Attachement Board Template Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	// detail attachement khusus grouping
	@PostMapping("/detailBoardAttachementGrouping")
	public String detailBoardAttachementGrouping(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try 
	    {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> dataAll = TmBoardProjectDetailService.detailBoardAttachementGrouping(dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), dataRequest.getInt("projectDetailId"), user_uuid, dataRequest.getInt("boardId"));
	    	dataAll.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("projectId", col[0] == null ? "" : col[0]);
	    		data.put("projectDetailId", col[1] == null ? "" : col[1]);
	    		data.put("company_id", col[2] == null ? "" : col[2]);
	    		data.put("parent_id", col[3] == null ? "" : col[3]);
	    		data.put("contactId", col[4] == null ? "" : col[4]);
	    		data.put("boardId", col[5] == null ? "" : col[5]);
	    		data.put("boardStageId", col[6] == null ? "" : col[6]);
	    		data.put("boardProjectDetailId", col[7] == null ? "" : col[7]);
	    		data.put("namaTask", col[8] == null ? "" : col[8]);
	    		data.put("dateDocument", col[9] == null ? "" : col[9]);
	    		data.put("docDescription", col[10] == null ? "" : col[10]);
	    		data.put("docTypeId", col[11] == null ? "" : col[11]);
	    		data.put("docTypeNama", col[12] == null ? "" : col[12]);
	    		data.put("filePath", col[13] == null ? "" : col[13]);
	    		data.put("boardProjectDetailAttId", col[14] == null ? "" : col[14]);
	    		data.put("namaFile", col[15] == null ? "" : col[15]);
	    		cont.add(data);
	    	});
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Attachement Board Grouping Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Attachement Board Grouping Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	// detail attachement khusus task
	@PostMapping("/detailBoardAttachementTask")
	public String detailBoardAttachementTask(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try 
	    {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> dataAll = TmBoardProjectDetailService.detailBoardAttachementTask(dataRequest.getInt("company_id"), dataRequest.getInt("boardId"), user_uuid, dataRequest.getInt("boardProjectDetailId"));
	    	dataAll.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("projectId", col[0] == null ? "" : col[0]);
	    		data.put("projectDetailId", col[1] == null ? "" : col[1]);
	    		data.put("company_id", col[2] == null ? "" : col[2]);
//	    		data.put("parent_id", col[3] == null ? "" : col[3]);
	    		data.put("contactId", col[4] == null ? "" : col[4]);
	    		data.put("boardId", col[5] == null ? "" : col[5]);
	    		data.put("boardStageId", col[6] == null ? "" : col[6]);
	    		data.put("boardProjectDetailId", col[7] == null ? "" : col[7]);
	    		data.put("namaTask", col[8] == null ? "" : col[8]);
	    		data.put("dateDocument", col[9] == null ? "" : col[9].toString());
	    		data.put("docDescription", col[10] == null ? "" : col[10]);
	    		data.put("docTypeId", col[11] == null ? "" : col[11]);
	    		data.put("docTypeNama", col[12] == null ? "" : col[12]);
	    		data.put("filePath", col[13] == null ? "" : col[13]);
	    		data.put("boardProjectDetailAttId", col[14] == null ? "" : col[14]);
	    		data.put("namaFile", col[15] == null ? "" : col[15]);
	    		cont.add(data);
	    	});
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Attachement Board Task Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Attachement Board Task Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	int a = 0;
	// detail board discussion khusus template
	@PostMapping("/detailBoardDiscussionTemplate")
	public String detailBoardDiscussionTemplate(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    List<JSONObject> finalFull = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try 
	    {
	    	String level = "";
	    	ArrayList<ChildComment> pairs = new ArrayList<ChildComment>();
	    	ArrayList<ChildComment> pairs2 = new ArrayList<ChildComment>();
	    	
	    	ArrayList<ChildComment> getData = new ArrayList<ChildComment>();
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> dataAll = TmBoardProjectDetailService.detailBoardDiscussionTemplate(dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), user_uuid, dataRequest.getInt("boardId"));
	    	dataAll.stream().forEach(col->{
	    		ChildComment data = new ChildComment();
	    		data.setChildId(Integer.valueOf(col[8].toString()));
	    		data.setParentId(Integer.valueOf(col[9].toString()));
	    		data.setBoardId(Integer.valueOf(col[5].toString()));
	    		data.setBoardProjectDetailId(Integer.valueOf(col[7].toString()));
	    		data.setBoardProjectDetailDisId(Integer.valueOf(col[8].toString()));
	    		data.setCompany_id(Integer.valueOf(col[2].toString()));
	    		data.setNamaTask(col[10].toString());
	    		data.setMProjectId(Integer.valueOf(col[0].toString()));
	    		data.setProjectDetailId(Integer.valueOf(col[1].toString()));
	    		
	    		data.setContactId(col[4] == null ? "" : col[4].toString());
	    		data.setDateComment(col[11].toString());
	    		data.setCommentDiscussion(col[12].toString());
	    		pairs.add(data);
	    		pairs2.add(data);
	    	});
	    	
	    	//untuk menghapus duplikat
	   		 HashSet<Object> seen= new HashSet<>();
	   		 pairs2.removeIf(e -> !seen.add(Arrays.asList(e.getCompany_id(), e.getBoardId(), e.getBoardProjectDetailId() )));
	    	
	   		 pairs2.stream().forEach(col->{
	   			JSONObject dataHeaderFull = new JSONObject();
	   			ArrayList<ChildComment> dataHeader = new ArrayList<ChildComment>();
	   			
	   			Map<Integer, ParentComment> hmTry = new HashMap<>();
	   			pairs.stream().forEach(col2->{
	   				
	   				if(col2.getCompany_id().equals(col.getCompany_id())
	   				   && col2.getBoardId().equals(col.getBoardId())
	   				   &&  col2.getBoardProjectDetailId().equals(col.getBoardProjectDetailId())
	   						)
	   				{
	   					ChildComment data = new ChildComment();
			    		data.setChildId(col2.getChildId());
			    		data.setParentId(col2.getParentId());
			    		data.setBoardId(col2.getBoardId());
			    		data.setBoardProjectDetailId(col2.getBoardProjectDetailId());
			    		data.setBoardProjectDetailDisId(col2.getBoardProjectDetailDisId());
			    		data.setCompany_id(col2.getCompany_id());
			    		data.setMProjectId(col2.getMProjectId());
			    		data.setProjectDetailId(col2.getProjectDetailId());
			    		data.setNamaTask(col2.getNamaTask());
			    		
			    		data.setContactId(col2.getContactId());
			    		data.setCommentDiscussion(col2.getCommentDiscussion());
			    		data.setDateComment(col2.getDateComment());
			    		dataHeader.add(data);
	   				}
	   			});
	   			dataHeaderFull.put("header", "Task > " + col.getNamaTask());
	   			dataHeaderFull.put("boardid", col.getBoardId());
	   			dataHeaderFull.put("boardprojectdetailid", col.getBoardProjectDetailId());
	   			dataHeaderFull.put("detail", dataHeader);
	   			finalFull.add(dataHeaderFull);
	   		});
	   		
	   		 
	   		
	   		//bagian att_discussioin
	   		ArrayList<ChildAttachementComment> pairsAttDisc = new ArrayList<ChildAttachementComment>();
	    	ArrayList<ChildAttachementComment> pairsAttDisc2 = new ArrayList<ChildAttachementComment>();
	   		List<Object[]> dataAllAttDisc = TmBoardProjectDetailService.detailBoardAttachementDiscussionTemplate(dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), user_uuid, dataRequest.getInt("boardId"));
	   		dataAllAttDisc.stream().forEach(col->{
	    		ChildAttachementComment data = new ChildAttachementComment();
	    		data.setChildId(Integer.valueOf(col[14].toString()));
	    		data.setParentId(Integer.valueOf(col[18].toString()));
	    		data.setBoardId(Integer.valueOf(col[4].toString()));
	    		data.setBoardProjectDetailId(Integer.valueOf(col[6].toString()));
	    		data.setBoardProjectDetailAttId(Integer.valueOf(col[7].toString()));
	    		data.setBoardProjectDetailAttDiscId(Integer.valueOf(col[14].toString()));
	    		data.setCompany_id(Integer.valueOf(col[2].toString()));
	    		data.setNamaTask(col[8].toString());
	    		data.setMProjectId(Integer.valueOf(col[0].toString()));
	    		data.setProjectDetailId(Integer.valueOf(col[1].toString()));
	    		
	    		data.setContactId(col[15].toString());
	    		data.setDateComment(col[17].toString());
	    		data.setCommentDiscussion(col[16].toString());
	    		pairsAttDisc.add(data);
	    		pairsAttDisc2.add(data);
	    	});
	   		
	   		//untuk menghapus duplikat
	   		 HashSet<Object> seenAtt= new HashSet<>();
	   		pairsAttDisc2.removeIf(e -> !seenAtt.add(Arrays.asList(e.getCompany_id(), e.getBoardId(), e.getBoardProjectDetailId(), e.getBoardProjectDetailAttId() )));
	   		
	   		pairsAttDisc2.stream().forEach(col->{
	   			JSONObject dataHeaderFull = new JSONObject();
	   			ArrayList<ChildAttachementComment> dataHeader = new ArrayList<ChildAttachementComment>();
	   			
	   			pairsAttDisc.stream().forEach(col2->{
	   				
	   				if(col2.getCompany_id().equals(col.getCompany_id())
	   				   && col2.getBoardId().equals(col.getBoardId())
	   				   &&  col2.getBoardProjectDetailId().equals(col.getBoardProjectDetailId())
	   				   && col2.getBoardProjectDetailAttId().equals(col.getBoardProjectDetailAttId())
	   						)
	   				{
	   					ChildAttachementComment data = new ChildAttachementComment();
			    		data.setChildId(col2.getChildId());
			    		data.setParentId(col2.getParentId());
			    		data.setBoardId(col2.getBoardId());
			    		data.setBoardProjectDetailId(col2.getBoardProjectDetailId());
			    		data.setBoardProjectDetailAttId(col2.getBoardProjectDetailAttId());
			    		data.setBoardProjectDetailAttDiscId(col2.getBoardProjectDetailAttDiscId());
			    		data.setCompany_id(col2.getCompany_id());
			    		data.setMProjectId(col2.getMProjectId());
			    		data.setProjectDetailId(col2.getProjectDetailId());
			    		data.setNamaTask(col2.getNamaTask());
			    		
			    		data.setContactId(col2.getContactId());
			    		data.setCommentDiscussion(col2.getCommentDiscussion());
			    		data.setDateComment(col2.getDateComment());
			    		dataHeader.add(data);
	   				}
	   			});
	   			dataHeaderFull.put("header", "Attachement > " + col.getNamaTask());
	   			dataHeaderFull.put("boardid", col.getBoardId());
	   			dataHeaderFull.put("boardprojectdetailid", col.getBoardProjectDetailId());
	   			dataHeaderFull.put("detailAttachementDiscussion", dataHeader);
	   			finalFull.add(dataHeaderFull);
	   		});
	   		a = 0;
	   		finalFull.stream().forEach(col->{
	   			List<JSONObject> hasil = new ArrayList<>();
	   			List<JSONObject> hasilAttDisc = new ArrayList<>();
	   			if(col.has("detail"))
	   			{
	   				if(col.getJSONArray("detail").size() > 0)
		   			{
		   				hasil = makeChild(col.getJSONArray("detail"));
		   			}
	   			}
	   			
	   			else if(col.has("detailAttachementDiscussion"))
	   			{
	   				if(col.getJSONArray("detailAttachementDiscussion").size() > 0)
	   				{
	   					hasilAttDisc = makeChildAttchementDiscussion(col.getJSONArray("detailAttachementDiscussion"));
	   				}
	   			}
	   			logger.info("hasilAttDisc == " + hasilAttDisc);
	   			
	   			
	   			JSONObject baru = new JSONObject ();
	   			baru.put("header", col.getString("header"));
	   			baru.put("boardId", col.getInt("boardid"));
	   			baru.put("boardProjectDetailId", col.getInt("boardprojectdetailid"));
	   			baru.put("detail", hasil.size() > 0 ? hasil : hasilAttDisc );
	   			finalFull.set(a, baru);
	   			a++;
	   		});
	   		
            response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Discussion Board Template Success");
		    response.put("responseData", finalFull);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Discussion Board Template Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	// detail discussion khusus grouping
	@PostMapping("/detailBoardDiscussionGrouping")
	public String detailBoardDiscussionGrouping(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
		{
			JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    List<JSONObject> final11 = new ArrayList<>();
		    List<JSONObject> finalFull = new ArrayList<>();
		    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
		    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    logger.info("input >>>> " + dataRequest);
		    try 
		    {
		    	String level = "";
		    	ArrayList<ChildComment> pairs = new ArrayList<ChildComment>();
		    	ArrayList<ChildComment> pairs2 = new ArrayList<ChildComment>();
		    	
		    	ArrayList<ChildComment> getData = new ArrayList<ChildComment>();
		    	UUID user_uuid = UUID.fromString(getUserId(header));
		    	List<Object[]> dataAll = TmBoardProjectDetailService.detailBoardDiscussionGrouping(dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), dataRequest.getInt("projectDetailId"), user_uuid, dataRequest.getInt("boardId"));
		    	dataAll.stream().forEach(col->{
		    		ChildComment data = new ChildComment();
		    		data.setChildId(Integer.valueOf(col[8].toString()));
		    		data.setParentId(Integer.valueOf(col[9].toString()));
		    		data.setBoardId(Integer.valueOf(col[5].toString()));
		    		data.setBoardProjectDetailId(Integer.valueOf(col[7].toString()));
		    		data.setBoardProjectDetailDisId(Integer.valueOf(col[8].toString()));
		    		data.setCompany_id(Integer.valueOf(col[2].toString()));
		    		data.setNamaTask(col[10].toString());
		    		data.setMProjectId(Integer.valueOf(col[0].toString()));
		    		data.setProjectDetailId(Integer.valueOf(col[1].toString()));
		    		
		    		data.setContactId(col[4] == null ? "" : col[4].toString());
		    		data.setDateComment(col[11].toString());
		    		data.setCommentDiscussion(col[12].toString());
		    		pairs.add(data);
		    		pairs2.add(data);
		    	});
		    	
		    	//untuk menghapus duplikat
		   		 HashSet<Object> seen= new HashSet<>();
		   		 pairs2.removeIf(e -> !seen.add(Arrays.asList(e.getCompany_id(), e.getBoardId(), e.getBoardProjectDetailId() )));
		    	
		   		 pairs2.stream().forEach(col->{
		   			JSONObject dataHeaderFull = new JSONObject();
		   			ArrayList<ChildComment> dataHeader = new ArrayList<ChildComment>();
		   			
		   			Map<Integer, ParentComment> hmTry = new HashMap<>();
		   			pairs.stream().forEach(col2->{
		   				
		   				if(col2.getCompany_id().equals(col.getCompany_id())
		   				   && col2.getBoardId().equals(col.getBoardId())
		   				   &&  col2.getBoardProjectDetailId().equals(col.getBoardProjectDetailId())
		   						)
		   				{
		   					ChildComment data = new ChildComment();
				    		data.setChildId(col2.getChildId());
				    		data.setParentId(col2.getParentId());
				    		data.setBoardId(col2.getBoardId());
				    		data.setBoardProjectDetailId(col2.getBoardProjectDetailId());
				    		data.setBoardProjectDetailDisId(col2.getBoardProjectDetailDisId());
				    		data.setCompany_id(col2.getCompany_id());
				    		data.setMProjectId(col2.getMProjectId());
				    		data.setProjectDetailId(col2.getProjectDetailId());
				    		data.setNamaTask(col2.getNamaTask());
				    		
				    		data.setContactId(col2.getContactId());
				    		data.setCommentDiscussion(col2.getCommentDiscussion());
				    		data.setDateComment(col2.getDateComment());
				    		dataHeader.add(data);
		   				}
		   			});
		   			dataHeaderFull.put("header", "Task > " + col.getNamaTask());
		   			dataHeaderFull.put("boardid", col.getBoardId());
		   			dataHeaderFull.put("boardprojectdetailid", col.getBoardProjectDetailId());
		   			dataHeaderFull.put("detail", dataHeader);
		   			finalFull.add(dataHeaderFull);
		   		});
		   		
		   		 
		   		
		   		//bagian att_discussioin
		   		ArrayList<ChildAttachementComment> pairsAttDisc = new ArrayList<ChildAttachementComment>();
		    	ArrayList<ChildAttachementComment> pairsAttDisc2 = new ArrayList<ChildAttachementComment>();
		   		List<Object[]> dataAllAttDisc = TmBoardProjectDetailService.detailBoardAttachementDiscussionGrouping(dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), dataRequest.getInt("projectDetailId"), user_uuid, dataRequest.getInt("boardId"));
		   		dataAllAttDisc.stream().forEach(col->{
		    		ChildAttachementComment data = new ChildAttachementComment();
		    		data.setChildId(Integer.valueOf(col[14].toString()));
		    		data.setParentId(Integer.valueOf(col[18].toString()));
		    		data.setBoardId(Integer.valueOf(col[4].toString()));
		    		data.setBoardProjectDetailId(Integer.valueOf(col[6].toString()));
		    		data.setBoardProjectDetailAttId(Integer.valueOf(col[7].toString()));
		    		data.setBoardProjectDetailAttDiscId(Integer.valueOf(col[14].toString()));
		    		data.setCompany_id(Integer.valueOf(col[2].toString()));
		    		data.setNamaTask(col[8].toString());
		    		data.setMProjectId(Integer.valueOf(col[0].toString()));
		    		data.setProjectDetailId(Integer.valueOf(col[1].toString()));
		    		
		    		data.setContactId(col[15].toString());
		    		data.setDateComment(col[17].toString());
		    		data.setCommentDiscussion(col[16].toString());
		    		pairsAttDisc.add(data);
		    		pairsAttDisc2.add(data);
		    	});
		   		
		   		//untuk menghapus duplikat
		   		 HashSet<Object> seenAtt= new HashSet<>();
		   		pairsAttDisc2.removeIf(e -> !seenAtt.add(Arrays.asList(e.getCompany_id(), e.getBoardId(), e.getBoardProjectDetailId(), e.getBoardProjectDetailAttId() )));
		   		
		   		pairsAttDisc2.stream().forEach(col->{
		   			JSONObject dataHeaderFull = new JSONObject();
		   			ArrayList<ChildAttachementComment> dataHeader = new ArrayList<ChildAttachementComment>();
		   			
		   			pairsAttDisc.stream().forEach(col2->{
		   				
		   				if(col2.getCompany_id().equals(col.getCompany_id())
		   				   && col2.getBoardId().equals(col.getBoardId())
		   				   &&  col2.getBoardProjectDetailId().equals(col.getBoardProjectDetailId())
		   				   && col2.getBoardProjectDetailAttId().equals(col.getBoardProjectDetailAttId())
		   						)
		   				{
		   					ChildAttachementComment data = new ChildAttachementComment();
				    		data.setChildId(col2.getChildId());
				    		data.setParentId(col2.getParentId());
				    		data.setBoardId(col2.getBoardId());
				    		data.setBoardProjectDetailId(col2.getBoardProjectDetailId());
				    		data.setBoardProjectDetailAttId(col2.getBoardProjectDetailAttId());
				    		data.setBoardProjectDetailAttDiscId(col2.getBoardProjectDetailAttDiscId());
				    		data.setCompany_id(col2.getCompany_id());
				    		data.setMProjectId(col2.getMProjectId());
				    		data.setProjectDetailId(col2.getProjectDetailId());
							data.setNamaTask(col2.getNamaTask());
				    		
				    		data.setContactId(col2.getContactId());
				    		data.setCommentDiscussion(col2.getCommentDiscussion());
				    		data.setDateComment(col2.getDateComment());
				    		dataHeader.add(data);
		   				}
		   			});
		   			dataHeaderFull.put("header", "Attachement > " + col.getNamaTask());
		   			dataHeaderFull.put("boardid", col.getBoardId());
		   			dataHeaderFull.put("boardprojectdetailid", col.getBoardProjectDetailId());
		   			dataHeaderFull.put("detailAttachementDiscussion", dataHeader);
		   			finalFull.add(dataHeaderFull);
		   		});
		   		a = 0;
		   		finalFull.stream().forEach(col->{
		   			List<JSONObject> hasil = new ArrayList<>();
		   			List<JSONObject> hasilAttDisc = new ArrayList<>();
		   			if(col.has("detail"))
		   			{
		   				if(col.getJSONArray("detail").size() > 0)
			   			{
			   				hasil = makeChild(col.getJSONArray("detail"));
			   			}
		   			}
		   			
		   			else if(col.has("detailAttachementDiscussion"))
		   			{
		   				if(col.getJSONArray("detailAttachementDiscussion").size() > 0)
		   				{
		   					hasilAttDisc = makeChildAttchementDiscussion(col.getJSONArray("detailAttachementDiscussion"));
		   				}
		   			}
		   			logger.info("hasilAttDisc == " + hasilAttDisc);
		   			
		   			
		   			JSONObject baru = new JSONObject ();
		   			baru.put("header", col.getString("header"));
		   			baru.put("boardId", col.getInt("boardid"));
		   			baru.put("boardProjectDetailId", col.getInt("boardprojectdetailid"));
		   			baru.put("detail", hasil.size() > 0 ? hasil : hasilAttDisc );
		   			finalFull.set(a, baru);
		   			a++;
		   		});
		   		
	            response.put("responseCode", "00");
			    response.put("responseDesc", "Detail Discussion Board Grouping Success");
			    response.put("responseData", finalFull);
		    }
		    catch(Exception e)
		    {
		    	response.put("responseCode", "99");
			    response.put("responseDesc", "Detail Discussion Board Grouping Failed");
			    response.put("responseError", e.getMessage());
			    logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
		}
	
	@PostMapping("/detailBoardDiscussionTask")
	public String detailBoardDiscussionTask(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    List<JSONObject> finalFull = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try 
	    {
	    	String level = "";
	    	ArrayList<ChildComment> pairs = new ArrayList<ChildComment>();
	    	ArrayList<ChildComment> pairs2 = new ArrayList<ChildComment>();
	    	
	    	ArrayList<ChildComment> getData = new ArrayList<ChildComment>();
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> dataAll = TmBoardProjectDetailService.detailBoardDiscussionTask(dataRequest.getInt("company_id"),  dataRequest.getInt("boardId"), dataRequest.getInt("boardProjectDetailId"), user_uuid);
	    	dataAll.stream().forEach(col->{
	    		ChildComment data = new ChildComment();
	    		data.setChildId(Integer.valueOf(col[8].toString()));
	    		data.setParentId(Integer.valueOf(col[9].toString()));
	    		data.setBoardId(Integer.valueOf(col[5].toString()));
	    		data.setBoardProjectDetailId(Integer.valueOf(col[7].toString()));
	    		data.setBoardProjectDetailDisId(Integer.valueOf(col[8].toString()));
	    		data.setCompany_id(Integer.valueOf(col[2].toString()));
	    		data.setNamaTask(col[10].toString());
	    		data.setMProjectId(Integer.valueOf(col[0].toString()));
	    		data.setProjectDetailId(col[1] == null ? 0 : Integer.valueOf(col[1].toString()));
	    		
	    		data.setContactId(col[4] == null ? "" : col[4].toString());
	    		data.setDateComment(col[11].toString());
	    		data.setCommentDiscussion(col[12].toString());
	    		
	    		pairs.add(data);
	    		pairs2.add(data);
	    	});
	    	
	    	//untuk menghapus duplikat
	   		 HashSet<Object> seen= new HashSet<>();
	   		 pairs2.removeIf(e -> !seen.add(Arrays.asList(e.getCompany_id(), e.getBoardId(), e.getBoardProjectDetailId() )));
	    	
	   		 pairs2.stream().forEach(col->{
	   			JSONObject dataHeaderFull = new JSONObject();
	   			ArrayList<ChildComment> dataHeader = new ArrayList<ChildComment>();
	   			
	   			Map<Integer, ParentComment> hmTry = new HashMap<>();
	   			pairs.stream().forEach(col2->{
	   				
	   				if(col2.getCompany_id().equals(col.getCompany_id())
	   				   && col2.getBoardId().equals(col.getBoardId())
	   				   &&  col2.getBoardProjectDetailId().equals(col.getBoardProjectDetailId())
	   						)
	   				{
	   					ChildComment data = new ChildComment();
			    		data.setChildId(col2.getChildId());
			    		data.setParentId(col2.getParentId());
			    		data.setBoardId(col2.getBoardId());
			    		data.setBoardProjectDetailId(col2.getBoardProjectDetailId());
			    		data.setBoardProjectDetailDisId(col2.getBoardProjectDetailDisId());
			    		data.setCompany_id(col2.getCompany_id());
			    		data.setMProjectId(col2.getMProjectId());
			    		data.setProjectDetailId(col2.getProjectDetailId());
			    		data.setParentId(col2.getParentId());
			    		data.setMProjectId(col2.getMProjectId());
			    		data.setProjectDetailId(col2.getProjectDetailId());
			    		data.setNamaTask(col2.getNamaTask());
			    		
			    		data.setContactId(col2.getContactId());
			    		data.setCommentDiscussion(col2.getCommentDiscussion());
			    		data.setDateComment(col2.getDateComment());
			    		dataHeader.add(data);
	   				}
	   			});
	   			dataHeaderFull.put("header", "Task > " + col.getNamaTask());
	   			dataHeaderFull.put("boardid", col.getBoardId());
	   			dataHeaderFull.put("boardprojectdetailid", col.getBoardProjectDetailId());
	   			dataHeaderFull.put("detail", dataHeader);
	   			finalFull.add(dataHeaderFull);
	   		});
	   		 
	   		 //bagian att_discussioin
		   		ArrayList<ChildAttachementComment> pairsAttDisc = new ArrayList<ChildAttachementComment>();
		    	ArrayList<ChildAttachementComment> pairsAttDisc2 = new ArrayList<ChildAttachementComment>();
		   		List<Object[]> dataAllAttDisc = TmBoardProjectDetailService.detailBoardAttachementDiscussionTask(dataRequest.getInt("company_id"),  dataRequest.getInt("boardId"),  dataRequest.getInt("boardProjectDetailId"), user_uuid);
		   		dataAllAttDisc.stream().forEach(col->{
		    		ChildAttachementComment data = new ChildAttachementComment();
		    		data.setChildId(Integer.valueOf(col[14].toString()));
		    		data.setParentId(Integer.valueOf(col[18].toString()));
		    		data.setBoardId(Integer.valueOf(col[4].toString()));
		    		data.setBoardProjectDetailId(Integer.valueOf(col[6].toString()));
		    		data.setBoardProjectDetailAttId(Integer.valueOf(col[7].toString()));
		    		data.setBoardProjectDetailAttDiscId(Integer.valueOf(col[14].toString()));
		    		data.setCompany_id(Integer.valueOf(col[2].toString()));
		    		data.setNamaTask(col[8].toString());
		    		data.setMProjectId(Integer.valueOf(col[0].toString()));
		    		data.setProjectDetailId(Integer.valueOf(col[1].toString()));
		    		
		    		data.setContactId(col[15].toString());
		    		data.setDateComment(col[17].toString());
		    		data.setCommentDiscussion(col[16].toString());
		    		pairsAttDisc.add(data);
		    		pairsAttDisc2.add(data);
		    	});
		   		
		   		//untuk menghapus duplikat
		   		HashSet<Object> seenAtt= new HashSet<>();
		   		pairsAttDisc2.removeIf(e -> !seenAtt.add(Arrays.asList(e.getCompany_id(), e.getBoardId(), e.getBoardProjectDetailId(), e.getBoardProjectDetailAttId() )));
		   		
		   		pairsAttDisc2.stream().forEach(col->{
		   			JSONObject dataHeaderFull = new JSONObject();
		   			ArrayList<ChildAttachementComment> dataHeader = new ArrayList<ChildAttachementComment>();
		   			
		   			pairsAttDisc.stream().forEach(col2->{
		   				
		   				if(col2.getCompany_id().equals(col.getCompany_id())
		   				   && col2.getBoardId().equals(col.getBoardId())
		   				   &&  col2.getBoardProjectDetailId().equals(col.getBoardProjectDetailId())
		   				   && col2.getBoardProjectDetailAttId().equals(col.getBoardProjectDetailAttId())
		   						)
		   				{
		   					ChildAttachementComment data = new ChildAttachementComment();
				    		data.setChildId(col2.getChildId());
				    		data.setParentId(col2.getParentId());
				    		data.setBoardId(col2.getBoardId());
				    		data.setBoardProjectDetailId(col2.getBoardProjectDetailId());
				    		data.setBoardProjectDetailAttId(col2.getBoardProjectDetailAttId());
				    		data.setBoardProjectDetailAttDiscId(col2.getBoardProjectDetailAttDiscId());
				    		data.setCompany_id(col2.getCompany_id());
				    		data.setMProjectId(col2.getMProjectId());
				    		data.setProjectDetailId(col2.getProjectDetailId());
				    		data.setParentId(col2.getParentId());
				    		data.setMProjectId(col2.getMProjectId());
				    		data.setProjectDetailId(col2.getProjectDetailId());
				    		data.setNamaTask(col2.getNamaTask());
				    		
				    		data.setContactId(col2.getContactId());
				    		data.setCommentDiscussion(col2.getCommentDiscussion());
				    		data.setDateComment(col2.getDateComment());
				    		dataHeader.add(data);
		   				}
		   			});
		   			dataHeaderFull.put("header", "Attachement > " + col.getNamaTask());
		   			dataHeaderFull.put("boardid", col.getBoardId());
		   			dataHeaderFull.put("boardprojectdetailid", col.getBoardProjectDetailId());
		   			dataHeaderFull.put("detailAttachementDiscussion", dataHeader);
		   			finalFull.add(dataHeaderFull);
		   		});
		   		a = 0;
		   		finalFull.stream().forEach(col->{
		   			List<JSONObject> hasil = new ArrayList<>();
		   			List<JSONObject> hasilAttDisc = new ArrayList<>();
		   			if(col.has("detail"))
		   			{
		   				if(col.getJSONArray("detail").size() > 0)
			   			{
			   				hasil = makeChild(col.getJSONArray("detail"));
			   			}
		   			}
		   			
		   			else if(col.has("detailAttachementDiscussion"))
		   			{
		   				if(col.getJSONArray("detailAttachementDiscussion").size() > 0)
		   				{
		   					hasilAttDisc = makeChildAttchementDiscussion(col.getJSONArray("detailAttachementDiscussion"));
		   				}
		   			}
		   			logger.info("hasilAttDisc == " + hasilAttDisc);
		   			
		   			
		   			JSONObject baru = new JSONObject ();
		   			baru.put("header", col.getString("header"));
		   			baru.put("boardId", col.getInt("boardid"));
		   			baru.put("boardProjectDetailId", col.getInt("boardprojectdetailid"));
		   			baru.put("detail", hasil.size() > 0 ? hasil : hasilAttDisc );
		   			finalFull.set(a, baru);
		   			a++;
		   		});
	   		 
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Discussion Board Task Success");
		    response.put("responseData", finalFull);
	    }
	    catch(Exception e )
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Discussion Board Task Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
		return response.toString();
	}
		
	public List<JSONObject>  makeChild(JSONArray jsonArray){
		String level = "";
	    List<JSONObject> cont = new ArrayList<>();
    	ArrayList<ChildComment> pairs = new ArrayList<ChildComment>();
		for (int i = 0; i < jsonArray.size(); i++) {
			 JSONObject resultData = (JSONObject) jsonArray.get(i);
			 ChildComment data = new ChildComment();
			 data.setChildId(resultData.getInt("childId"));
	    		data.setParentId(resultData.getInt("parentId"));
	    		data.setBoardId(resultData.getInt("boardId"));
	    		data.setBoardProjectDetailId(resultData.getInt("boardProjectDetailId"));
	    		data.setBoardProjectDetailDisId(resultData.getInt("boardProjectDetailDisId"));
	    		data.setCompany_id(resultData.getInt("company_id"));
	    		data.setContactId(resultData.getString("contactId"));
	    		data.setDateComment(resultData.getString("dateComment"));
	    		
	    		data.setCommentDiscussion(resultData.getString("commentDiscussion"));
	    		data.setMProjectId(resultData.getInt("MProjectId"));
	    		data.setProjectDetailId(resultData.getInt("projectDetailId"));
	    		data.setNamaTask(resultData.getString("namaTask"));
	    		pairs.add(data);
		 }
		 Map<Integer, ParentComment> hm = new HashMap<>();
    	 for(ChildComment p:pairs){ 

	        	
	            //  ----- Child -----
    		 	ParentComment mmdChild ;
	            if(hm.containsKey(p.getChildId())){
	                mmdChild = hm.get(p.getChildId());
	            }
	            else{
	                mmdChild = new ParentComment();
	                hm.put(p.getChildId(), mmdChild);
	            }  
	            mmdChild.setId(p.getChildId());
	            mmdChild.setParentId(p.getParentId());
	            mmdChild.setBoardProjectDetailDisId(p.getBoardProjectDetailDisId());
	            mmdChild.setBoardId(p.getBoardId());
	            mmdChild.setBoardProjectDetailId(p.getBoardProjectDetailId());
	            mmdChild.setBoardProjectDetailDisId(p.getBoardProjectDetailDisId());
	            mmdChild.setCompany_id(p.getCompany_id());
	            mmdChild.setMProjectId(p.getMProjectId());
	            mmdChild.setProjectDetailId(p.getProjectDetailId());
	            mmdChild.setNamaTask(p.getNamaTask());
	            
	            mmdChild.setContactId(p.getContactId());
	            mmdChild.setCommentDiscussion(p.getCommentDiscussion());
	            mmdChild.setDateComment(p.getDateComment());
	            // no need to set ChildrenItems list because the constructor created a new empty list



	            // ------ Parent ----
	            ParentComment mmdParent ;
	            if(hm.containsKey(p.getParentId())){
	                mmdParent = hm.get(p.getParentId());
	            }
	            else{
	                mmdParent = new ParentComment();
	                hm.put(p.getParentId(),mmdParent);
	            }
	            
	            mmdParent.setId(p.getParentId());
	            mmdParent.setParentId(null);
	            mmdParent.addChildrenItem(mmdChild);
	            mmdParent.setBoardProjectDetailDisId(p.getBoardProjectDetailDisId());
	            mmdParent.setBoardId(p.getBoardId());
	            mmdParent.setBoardProjectDetailId(p.getBoardProjectDetailId());
	            
	            mmdParent.setCompany_id(p.getCompany_id());
	            mmdParent.setMProjectId(p.getMProjectId());
	            mmdParent.setProjectDetailId(p.getProjectDetailId());
	            mmdParent.setNamaTask(p.getNamaTask());
	            
	            mmdParent.setContactId(p.getContactId());
	            mmdParent.setCommentDiscussion(p.getCommentDiscussion());
	            mmdParent.setDateComment(p.getDateComment());
	        }
    	 List<ParentComment> DX = new ArrayList<ParentComment>(); 
        for(ParentComment mmd : hm.values()){
            if(mmd.getParentId() == null)
                DX.add(mmd);
        }

        // Print 
        for(ParentComment mmd: DX){
            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
            JSONObject data3 = new JSONObject();
            
            data3.put("data",mmd);
            cont.add(data3);
        }
        for(int a= 0 ; a< cont.size() ; a++)
        {
        	level = cont.get(a).getString("data");
        	logger.info("cont = " + cont.get(a).getString("data"));
        }
        JSONObject dataRequestLevel = (JSONObject)JSONSerializer.toJSON(level);
	 
        List<JSONObject> list = new ArrayList<JSONObject>();
        for (int i = 0; i < dataRequestLevel.getJSONArray("childrenItems").size(); i++) {
                list.add(dataRequestLevel.getJSONArray("childrenItems").getJSONObject(i));
        }
        
        Collections.sort(list, new SortBasedOnMessageId());
        
        List<JSONObject> resultArray2 = list;
//		        List<JSONObject> resultArray2 = new ArrayList<>();
        return resultArray2;
		}
	
	public List<JSONObject>  makeChildAttchementDiscussion(JSONArray jsonArray){
		String level = "";
	    List<JSONObject> cont = new ArrayList<>();
    	ArrayList<ChildAttachementComment> pairs = new ArrayList<ChildAttachementComment>();
		for (int i = 0; i < jsonArray.size(); i++) {
			 JSONObject resultData = (JSONObject) jsonArray.get(i);
			 ChildAttachementComment data = new ChildAttachementComment();
			 data.setChildId(resultData.getInt("childId"));
	    		data.setParentId(resultData.getInt("parentId"));
	    		data.setBoardId(resultData.getInt("boardId"));
	    		data.setBoardProjectDetailId(resultData.getInt("boardProjectDetailId"));
	    		data.setBoardProjectDetailAttId(resultData.getInt("boardProjectDetailAttId"));
	    		data.setBoardProjectDetailAttDiscId(resultData.getInt("boardProjectDetailAttDiscId"));
	    		data.setCompany_id(resultData.getInt("company_id"));
	    		data.setContactId(resultData.getString("contactId"));
	    		data.setDateComment(resultData.getString("dateComment"));
				data.setCommentDiscussion(resultData.getString("commentDiscussion"));
				data.setMProjectId(resultData.getInt("MProjectId"));
				data.setProjectDetailId(resultData.getInt("projectDetailId"));
				data.setNamaTask(resultData.getString("namaTask"));
	    		pairs.add(data);
		 }
		 Map<Integer, ParentAttachementComment> hm = new HashMap<>();
    	 for(ChildAttachementComment p:pairs){

	        	
	            //  ----- Child -----
    		 ParentAttachementComment mmdChild ;
	            if(hm.containsKey(p.getChildId())){
	                mmdChild = hm.get(p.getChildId());
	            }
	            else{
	                mmdChild = new ParentAttachementComment();
	                hm.put(p.getChildId(), mmdChild);
	            }  
	            mmdChild.setId(p.getChildId());
				mmdChild.setParentId(p.getParentId());
				mmdChild.setProjectDetailId(p.getProjectDetailId());
				mmdChild.setNamaTask(p.getNamaTask());
				mmdChild.setMProjectId(p.getMProjectId());

	            mmdChild.setBoardProjectDetailAttId(p.getBoardProjectDetailAttId());
	            mmdChild.setBoardProjectDetailAttDiscId(p.getBoardProjectDetailAttDiscId());
	            mmdChild.setBoardId(p.getBoardId());
	            mmdChild.setBoardProjectDetailId(p.getBoardProjectDetailId());
	            mmdChild.setCompany_id(p.getCompany_id());
	            
	            mmdChild.setContactId(p.getContactId());
	            mmdChild.setCommentDiscussion(p.getCommentDiscussion());
	            mmdChild.setDateComment(p.getDateComment());
	            // no need to set ChildrenItems list because the constructor created a new empty list



	            // ------ Parent ----
	            ParentAttachementComment mmdParent ;
	            if(hm.containsKey(p.getParentId())){
	                mmdParent = hm.get(p.getParentId());
	            }
	            else{
	                mmdParent = new ParentAttachementComment();
	                hm.put(p.getParentId(),mmdParent);
	            }
	            
	            mmdParent.setId(p.getParentId());
	            mmdParent.setParentId(null);
				mmdParent.addChildrenItem(mmdChild);
				mmdParent.setMProjectId(p.getMProjectId());
				mmdParent.setProjectDetailId(p.getProjectDetailId());
				mmdParent.setNamaTask(p.getNamaTask());
	            mmdParent.setBoardProjectDetailAttId(p.getBoardProjectDetailAttId());
	            mmdParent.setBoardProjectDetailAttDiscId(p.getBoardProjectDetailAttDiscId());
	            mmdParent.setBoardId(p.getBoardId());
	            mmdParent.setBoardProjectDetailId(p.getBoardProjectDetailId());
	            mmdParent.setCompany_id(p.getCompany_id());
	            
	            mmdParent.setContactId(p.getContactId());
	            mmdParent.setCommentDiscussion(p.getCommentDiscussion());
	            mmdParent.setDateComment(p.getDateComment());
	        }
    	 List<ParentAttachementComment> DX = new ArrayList<ParentAttachementComment>(); 
        for(ParentAttachementComment mmd : hm.values()){
            if(mmd.getParentId() == null)
                DX.add(mmd);
        }

        // Print 
        for(ParentAttachementComment mmd: DX){
            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
            JSONObject data3 = new JSONObject();
            
            data3.put("data",mmd);
            cont.add(data3);
        }
        for(int a= 0 ; a< cont.size() ; a++)
        {
        	level = cont.get(a).getString("data");
        	logger.info("cont = " + cont.get(a).getString("data"));
        }
        JSONObject dataRequestLevel = (JSONObject)JSONSerializer.toJSON(level);
	 
        List<JSONObject> list = new ArrayList<JSONObject>();
        for (int i = 0; i < dataRequestLevel.getJSONArray("childrenItems").size(); i++) {
                list.add(dataRequestLevel.getJSONArray("childrenItems").getJSONObject(i));
        }
        
        Collections.sort(list, new SortBasedOnMessageId());
        
        List<JSONObject> resultArray2 = list;
        return resultArray2;
		}
		
	@PostMapping("/detailTask")
	public String detailTask(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> dataAll = TmBoardProjectDetailService.detailTask(dataRequest.getInt("boardId"), dataRequest.getInt("boardProjectDetailId"),dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), user_uuid);
	    	if(dataAll.size()>0)
	    	{
	    		dataAll.stream().forEach(col->{
	    			JSONObject data = new JSONObject();
		    		data.put("boardId", col[0]);
		    		data.put("boardStageId", col[1]);
		    		data.put("boardProjectDetailId", col[2]);
		    		data.put("ProjectDetailId", col[3]);
		    		data.put("projectId", col[4]);
		    		data.put("doneDate", col[5] == null ? "" : col[5].toString());
		    		data.put("progress", col[6] == null ? "" : col[6]);
		    		data.put("company_id", col[7] == null ? "" : col[7]);
		    		data.put("jumlahNote", col[8] == null ? "" : col[8]);
		    		data.put("jumlahAttchement", col[9] == null ? "" : col[9]);
		    		data.put("jumlahDiscussion", col[10] == null ? "" : col[10]);
		    		
		    		data.put("priority", col[11] == null ? "" : col[11]);
		    		data.put("MTaskId", col[12] == null ? "" : col[12]);
		    		data.put("MTaskNama", col[13] == null ? "" : col[13]);
		    		data.put("MGroupingId", col[14] == null ? "" : col[14]);
		    		data.put("MGroupingNama", col[15] == null ? "" : col[15]);
		    		data.put("dateLine", col[16] == null ? "" : col[16].toString());
		    		data.put("contactId", col[17] == null ? "" : col[17].toString());
		    		data.put("MStageId", col[18] == null ? "" : col[18]);
		    		data.put("MStageNama", col[19] == null ? "" : col[19]);
		    		cont.add(data);
	    		});
	    	}
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Task Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Task Board");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/detailGroupingAndTemplate")
	public String detailGroupingAndTemplate(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    ArrayList<TmBoardProjectDetail> DataProjectDetail= new ArrayList<>();
	    ArrayList<TmBoardStage> DataStage= new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	List<Object[]> dataAll = TmBoardProjectDetailService.detailGroupingAndTemplate(dataRequest.getInt("boardId"), dataRequest.getInt("company_id"), dataRequest.getInt("projectId"), dataRequest.getInt("projectDetailId"), user_uuid);
	    	if(dataAll.size()>0)
	    	{
	    		dataAll.stream().forEach(col->{
	    			JSONObject data = new JSONObject();
		        	data.put("projectId", col[0]);
		        	data.put("asalProjectDetailId", col[2]);
		        	data.put("projectDetailId", col[8]);
		        	data.put("taskId", col[6] == null ? "" : col[6]);
		        	data.put("groupingId", col[7] == null ? "" : col[7]);
		        	data.put("taskNama", col[11]);
		        	data.put("stageId", col[4]);
		        	data.put("stageNama", col[5]);
		        	data.put("contactId", col[15] == null ? "" : col[15]);
		        	data.put("boardId", col[12]);
		        	data.put("role", col[16]);
		        	data.put("company_id", col[3]);
		        	data.put("jumlahDoc", col[6]);
		        	data.put("jumlahDisc", col[7]);
		        	data.put("boardStageId", col[13]);
		        	data.put("boardProjectDetailId", col[14]);
		        	data.put("priority", col[17] == null ? "" : col[17]);
		        	data.put("doneDate", col[18] == null ? "" : col[18].toString() );
		        	data.put("progress", col[19] == null ? "" : col[19] );
		        	data.put("dateLine", col[20] == null ? "" : col[20].toString() );
		        	data.put("jumlahNote", col[21] == null ? "" : col[21] );
		        	
		        	
		    		cont.add(data);
	    		});
	    	}
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Task Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Task Board");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	//-----------------------------------------------------------------------detail board task
	
	//------------------------------------------------------------------------stage drawer
	public static List<BoardDrawer> getDuplicatesDrawer(final List<BoardDrawer> personList) {
	  return getDuplicatesMapDrawer(personList).values().stream()
	      .filter(duplicates -> duplicates.size() > 1)
	      .flatMap(Collection::stream)
	      .collect(Collectors.toList());
	}
		
		 public static List<BoardDrawer> removeDuplicatesDrawer(List<BoardDrawer> personList) {
		 return personList
	                .stream()
//		                .filter(duplicateByKey(ChildBordGrouping::getProjectId))
	                .filter(duplicateByKeyDrawer(BoardDrawer::getStateGroupId))
	                .collect(Collectors.toList());

		}
		 
		 private static <T> Predicate<T> duplicateByKeyDrawer(final Function<? super T, Object> keyExtractor) {
	        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
	        return t -> isNull(seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE));

	    }


		private static Map<Integer, List<BoardDrawer>> getDuplicatesMapDrawer(List<BoardDrawer> personList) {
		    return personList.stream().collect(Collectors.groupingBy(TmBoardController::uniqueAttributesDrawer));
		}

		private static Integer uniqueAttributesDrawer(BoardDrawer person){

		    if(Objects.isNull(person)){
		        return null;
		    }

		    return (person.getStateGroupId()) ;
		}
	//-------------------------------------------------------------------------
	
	//-----------------------------------------------------------------------grouping
	public static List<ChildBordGrouping> getDuplicates(final List<ChildBordGrouping> personList) {
	  return getDuplicatesMap(personList).values().stream()
	      .filter(duplicates -> duplicates.size() >= 1)
	      .flatMap(Collection::stream)
	      .collect(Collectors.toList());
	}
	
	 public static List<ChildBordGrouping> removeDuplicates(List<ChildBordGrouping> personList) {
	 return personList
                .stream()
	            .filter(duplicateByKey(ChildBordGrouping::getProjectId))
                .filter(duplicateByKey(ChildBordGrouping::getProjectDetailId))
                .collect(Collectors.toList());

	}
	 
	 private static <T> Predicate<T> duplicateByKey(final Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> isNull(seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE));

    }


	private static Map<Integer, List<ChildBordGrouping>> getDuplicatesMap(List<ChildBordGrouping> personList) {
	    return personList.stream().collect(Collectors.groupingBy(TmBoardController::uniqueAttributes));
	}

	private static Integer uniqueAttributes(ChildBordGrouping person){

	    if(Objects.isNull(person)){
	        return null;
	    }

	    return (person.getProjectId()) + (person.getProjectDetailId()) ;
	}
	//----------------------------------------------------------------------------

	//----------------------------------------------------------------------template
	
	public static List<ChildBoardTemplate> getDuplicatesTemplate(final List<ChildBoardTemplate> personList) {
		  return getDuplicatesMapTemplate(personList).values().stream()
		      .filter(duplicates -> duplicates.size() >= 1)
		      .flatMap(Collection::stream)
		      .collect(Collectors.toList());
		}
		
		 public static List<ChildBoardTemplate> removeDuplicatesTemplate(List<ChildBoardTemplate> personList) {
		 return personList
	                .stream()
		            .filter(duplicateByKeyTemplate(ChildBoardTemplate::getProjectId))
	                .filter(duplicateByKeyTemplate(ChildBoardTemplate::getCompanyId))
	                .filter(duplicateByKeyTemplate(ChildBoardTemplate::getTemplateId))
	                .collect(Collectors.toList());

		}
		 
		 private static <T> Predicate<T> duplicateByKeyTemplate(final Function<? super T, Object> keyExtractor) {
	        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
	        return t -> isNull(seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE));

	    }


		private static Map<Integer, List<ChildBoardTemplate>> getDuplicatesMapTemplate(List<ChildBoardTemplate> personList) {
		    return personList.stream().collect(Collectors.groupingBy(TmBoardController::uniqueAttributesTemplate));
		}

		private static Integer uniqueAttributesTemplate(ChildBoardTemplate person){

		    if(Objects.isNull(person)){
		        return null;
		    }

		    return (person.getProjectId()) + (person.getTemplateId()) ;
		}
	
	//------------------------------------------------------------------------
	public String getUserId(String header)
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}

}
