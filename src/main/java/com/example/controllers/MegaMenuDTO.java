package com.example.controllers;
import java.util.ArrayList;
import java.util.List;

public class MegaMenuDTO {

	private String subcategory;
    private String value;
    private String subsubcategory;
    private Integer parentId;
    private Integer Id;
    private Integer nourut;
    private List<MegaMenuDTO> childrenItems; 

    public MegaMenuDTO() {
        this.subcategory = "";
        this.value = "";     
        this.subsubcategory = "";
        this.parentId = null;
        this.childrenItems = new ArrayList<MegaMenuDTO>();
        this.Id = null;
        this.nourut = null;
    }
    
        
    


	public int getNourut() {
		return nourut;
	}





	public void setNourut(int nourut) {
		this.nourut = nourut;
	}





	public Integer getId() {
		return Id;
	}





	public void setId(Integer id) {
		Id = id;
	}





	public String getSubcategory() {
		return subcategory;
	}





	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}





	public String getValue() {
		return value;
	}





	public void setValue(String value) {
		this.value = value;
	}





	public String getSubsubcategory() {
		return subsubcategory;
	}





	public void setSubsubcategory(String subsubcategory) {
		this.subsubcategory = subsubcategory;
	}





	public Integer getParentId() {
		return parentId;
	}



	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}



	public List<MegaMenuDTO> getChildrenItems() {
        return childrenItems;
    }
    public void setChildrenItems(List<MegaMenuDTO> childrenItems) {
        this.childrenItems = childrenItems;
    }
    public void addChildrenItem(MegaMenuDTO childrenItem){
        if(!this.childrenItems.contains(childrenItem))
            this.childrenItems.add(childrenItem);
    }

}