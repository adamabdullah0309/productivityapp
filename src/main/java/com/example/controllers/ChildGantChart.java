package com.example.controllers;

import java.util.List;

import net.sf.json.JSONObject;

import com.example.entity.ProjectDetailDependency;
import com.example.entity.TmProjectTimeframe;

public class ChildGantChart {
    private Integer childId;
    private Integer parentId;
    private Integer mProjectId;
    private Integer projectParty;
    private String projectName;
    private Integer projectDetailId;
    private Integer company_id;
    private Integer mTaskId;
    private Integer mGroupingId;
    private String task;
    private Integer timeframeId;
    private String startdate;
    private String dateline;
    private String assignTo;
    private String dateBudgetHour;
    private Float BudgetHour;
    private Integer projectStatus;
	private List<JSONObject> dataTimeframeBudgetHour;
	private List<JSONObject> dataDependency;
	
	
	
	public List<JSONObject> getDataDependency() {
		return dataDependency;
	}
	public void setDataDependency(List<JSONObject> dataDependency) {
		this.dataDependency = dataDependency;
	}
	public Integer getProjectStatus() {
		return projectStatus;
	}
	public void setProjectStatus(Integer projectStatus) {
		this.projectStatus = projectStatus;
	}
	public Integer getCompany_id() {
		return company_id;
	}
	public void setCompany_id(Integer company_id) {
		this.company_id = company_id;
	}
	public Integer getProjectDetailId() {
		return projectDetailId;
	}
	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}
	public Integer getProjectParty() {
		return projectParty;
	}
	public void setProjectParty(Integer projectParty) {
		this.projectParty = projectParty;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Integer getChildId() {
		return childId;
	}
	public void setChildId(Integer childId) {
		this.childId = childId;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public Integer getmProjectId() {
		return mProjectId;
	}
	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}
	public Integer getmTaskId() {
		return mTaskId;
	}
	public void setmTaskId(Integer mTaskId) {
		this.mTaskId = mTaskId;
	}
	public Integer getmGroupingId() {
		return mGroupingId;
	}
	public void setmGroupingId(Integer mGroupingId) {
		this.mGroupingId = mGroupingId;
	}
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public Integer getTimeframeId() {
		return timeframeId;
	}
	public void setTimeframeId(Integer timeframeId) {
		this.timeframeId = timeframeId;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getDateline() {
		return dateline;
	}
	public void setDateline(String dateline) {
		this.dateline = dateline;
	}
	public String getAssignTo() {
		return assignTo;
	}
	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}
	public String getDateBudgetHour() {
		return dateBudgetHour;
	}
	public void setDateBudgetHour(String dateBudgetHour) {
		this.dateBudgetHour = dateBudgetHour;
	}
	public Float getBudgetHour() {
		return BudgetHour;
	}
	public void setBudgetHour(Float budgetHour) {
		BudgetHour = budgetHour;
	}
	public List<JSONObject> getDataTimeframeBudgetHour() {
		return dataTimeframeBudgetHour;
	}
	public void setDataTimeframeBudgetHour(List<JSONObject> dataTimeframeBudgetHour) {
		this.dataTimeframeBudgetHour = dataTimeframeBudgetHour;
	} 
    
    	
}