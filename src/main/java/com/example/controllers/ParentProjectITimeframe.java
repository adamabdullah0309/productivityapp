package com.example.controllers;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.example.entity.TmProjectTimeframeBudgethour;

import net.sf.json.JSONObject;
import java.util.Date;
public class ParentProjectITimeframe {

	private Integer mProjectId;
	private Integer parentId;
    private Integer projectDetailId;
    private String nama;
    private String startdate;
    private String dateline;
    private Integer company_id;
    private Float budgetHour;
    private Integer timeframeId;
    private List<ParentProjectITimeframe> children; 
    private String user_uuid;
    private List<JSONObject> dataDetail;
    public ParentProjectITimeframe() {
        this.parentId = null;
        this.children = new ArrayList<ParentProjectITimeframe>();
        this.mProjectId = null;
        this.parentId = null;
        this.company_id = null;
        this.projectDetailId = null;
        this.nama = "";
        this.startdate = null;
        this.dateline = null;
        this.dataDetail = new ArrayList<>();
        this.budgetHour = null;
        this.timeframeId = null;
        this.user_uuid = "" ;
    }
    
    
    
    public String getUser_uuid() {
		return user_uuid;
	}



	public void setUser_uuid(String user_uuid) {
		this.user_uuid = user_uuid;
	}



	public Float getBudgetHour() {
		return budgetHour;
	}



	public void setBudgetHour(Float budgetHour) {
		this.budgetHour = budgetHour;
	}



	public Integer getmProjectId() {
		return mProjectId;
	}



	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}



	public Integer getParentId() {
		return parentId;
	}



	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}



	public Integer getProjectDetailId() {
		return projectDetailId;
	}



	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}



	public String getNama() {
		return nama;
	}



	public void setNama(String nama) {
		this.nama = nama;
	}


	


	public String getStartdate() {
		return startdate;
	}



	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}



	public String getDateline() {
		return dateline;
	}



	public void setDateline(String dateline) {
		this.dateline = dateline;
	}



	public Integer getTimeframeId() {
		return timeframeId;
	}



	public void setTimeframeId(Integer timeframeId) {
		this.timeframeId = timeframeId;
	}



	public Integer getCompany_id() {
		return company_id;
	}



	public void setCompany_id(Integer company_id) {
		this.company_id = company_id;
	}
	
	

	public List<JSONObject> getDataDetail() {
		return dataDetail;
	}



	public void setDataDetail(List<JSONObject> dataDetail) {
		this.dataDetail = dataDetail;
	}



	public List<ParentProjectITimeframe> getChildren() {
        return children;
    }
    public void setChildren(List<ParentProjectITimeframe> children) {
        this.children = children;
    }
    public void addChildren(ParentProjectITimeframe childrenItem){
        if(!this.children.contains(childrenItem))
            this.children.add(childrenItem);
    }

}