package com.example.controllers;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;import static java.util.Objects.isNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.client.RestTemplate;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.service.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static java.util.stream.Collectors.toCollection;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.collectingAndThen;


@RestController
@RequestMapping("/api")
public class DetailAttachementCommentController {
private static final Logger logger = LoggerFactory.getLogger(DetailAttachementCommentController.class);
	
	@Autowired
	private GabunganService GabunganService;
	
	@Autowired 
	private TmBoardProjectDetailService TmBoardProjectDetailService;
	
	@Autowired
	private TmBoardProjectDetailAttachementDiscussionService TmBoardProjectDetailAttachementDiscussionService;
	
	int a = 0;
	@PostMapping("/detailAttachementComment")
	public String detailAttachementComment(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> finalFull = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        String level = "";
	    	ArrayList<ChildComment> pairs = new ArrayList<ChildComment>();
	    	ArrayList<ChildComment> pairs2 = new ArrayList<ChildComment>();
	    	
	    	ArrayList<ChildComment> getData = new ArrayList<ChildComment>();
	    	
	        
	    	ArrayList<ChildAttachementComment> pairsAttDisc = new ArrayList<ChildAttachementComment>();
	    	ArrayList<ChildAttachementComment> pairsAttDisc2 = new ArrayList<ChildAttachementComment>();
	   		List<Object[]> dataAllAttDisc = TmBoardProjectDetailAttachementDiscussionService.detailBoardAttachementDiscussion(dataRequest.getInt("company_id"),  dataRequest.getInt("boardId"),  dataRequest.getInt("boardProjectDetailId"),  dataRequest.getInt("boardProjectDetailAttId") , user_uuid);
	   		dataAllAttDisc.stream().forEach(col->{
	    		ChildAttachementComment data = new ChildAttachementComment();
	    		data.setChildId(Integer.valueOf(col[14].toString()));
	    		data.setParentId(Integer.valueOf(col[18].toString()));
	    		data.setBoardId(Integer.valueOf(col[4].toString()));
	    		data.setBoardProjectDetailId(Integer.valueOf(col[6].toString()));
	    		data.setBoardProjectDetailAttId(Integer.valueOf(col[7].toString()));
	    		data.setBoardProjectDetailAttDiscId(Integer.valueOf(col[14].toString()));
	    		data.setCompany_id(Integer.valueOf(col[2].toString()));
	    		data.setNamaTask(col[8].toString());
	    		data.setMProjectId(Integer.valueOf(col[0].toString()));
	    		data.setProjectDetailId(Integer.valueOf(col[1].toString()));
	    		data.setRole(col[20].toString());
	    		data.setContactId(col[15].toString());
	    		data.setDateComment(col[17].toString());
	    		data.setCommentDiscussion(col[16].toString());
	    		pairsAttDisc.add(data);
	    		pairsAttDisc2.add(data);
	    	});
	   		
	   		//untuk menghapus duplikat
	   		HashSet<Object> seenAtt= new HashSet<>();
	   		pairsAttDisc2.removeIf(e -> !seenAtt.add(Arrays.asList(e.getCompany_id(), e.getBoardId(), e.getBoardProjectDetailId(), e.getBoardProjectDetailAttId() )));
	   		
	   		pairsAttDisc2.stream().forEach(col->{
	   			JSONObject dataHeaderFull = new JSONObject();
	   			ArrayList<ChildAttachementComment> dataHeader = new ArrayList<ChildAttachementComment>();
	   			
	   			pairsAttDisc.stream().forEach(col2->{
	   				
	   				if(col2.getCompany_id().equals(col.getCompany_id())
	   				   && col2.getBoardId().equals(col.getBoardId())
	   				   &&  col2.getBoardProjectDetailId().equals(col.getBoardProjectDetailId())
	   				   && col2.getBoardProjectDetailAttId().equals(col.getBoardProjectDetailAttId())
	   						)
	   				{
	   					ChildAttachementComment data = new ChildAttachementComment();
			    		data.setChildId(col2.getChildId());
			    		data.setParentId(col2.getParentId());
			    		data.setBoardId(col2.getBoardId());
			    		data.setBoardProjectDetailId(col2.getBoardProjectDetailId());
			    		data.setBoardProjectDetailAttId(col2.getBoardProjectDetailAttId());
			    		data.setBoardProjectDetailAttDiscId(col2.getBoardProjectDetailAttDiscId());
			    		data.setCompany_id(col2.getCompany_id());
			    		data.setMProjectId(col2.getMProjectId());
			    		data.setProjectDetailId(col2.getProjectDetailId());
			    		data.setNamaTask(col2.getNamaTask());
			    		
			    		data.setContactId(col2.getContactId());
			    		data.setCommentDiscussion(col2.getCommentDiscussion());
			    		data.setDateComment(col2.getDateComment());
			    		data.setRole(col2.getRole());
			    		dataHeader.add(data);
	   				}
	   			});
	   			dataHeaderFull.put("header", "Attachement > " + col.getNamaTask());
	   			dataHeaderFull.put("boardid", col.getBoardId());
	   			dataHeaderFull.put("boardprojectdetailid", col.getBoardProjectDetailId());
	   			dataHeaderFull.put("boardProjectDetailAttId", col.getBoardProjectDetailAttId());
	   			dataHeaderFull.put("boardProjectDetailAttDiscId", col.getBoardProjectDetailAttDiscId());
	   			dataHeaderFull.put("detailAttachementDiscussion", dataHeader);
	   			finalFull.add(dataHeaderFull);
	   		});
	   		a = 0;
	   		finalFull.stream().forEach(col->{
	   			List<JSONObject> hasil = new ArrayList<>();
	   			List<JSONObject> hasilAttDisc = new ArrayList<>();
	   			if(col.has("detailAttachementDiscussion"))
	   			{
	   				if(col.getJSONArray("detailAttachementDiscussion").size() > 0)
	   				{
	   					hasilAttDisc = makeChildAttchementDiscussion(col.getJSONArray("detailAttachementDiscussion"));
	   				}
	   			}
	   			logger.info("hasilAttDisc == " + hasilAttDisc);
	   			
	   			
	   			JSONObject baru = new JSONObject ();
	   			baru.put("header", col.getString("header"));
	   			baru.put("boardId", col.getInt("boardid"));
	   			baru.put("boardProjectDetailId", col.getInt("boardprojectdetailid"));
	   			baru.put("boardProjectDetailAttId", col.getInt("boardProjectDetailAttId"));
	   			baru.put("boardProjectDetailAttDiscId", col.getInt("boardProjectDetailAttDiscId"));
	   			baru.put("detail", hasilAttDisc );
	   			finalFull.set(a, baru);
	   			a++;
	   		});
	   		 
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Show Detail Attachement Comment Success");
		    response.put("responseData", finalFull);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show Detail Attachement Comment Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	@PostMapping("/saveAttachementComment")
	public String saveAttachementComment(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> finalFull = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> dataNext = TmBoardProjectDetailAttachementDiscussionService.nextval(dataRequest.getInt("boardId"), dataRequest.getInt("boardProjectDetailId"), dataRequest.getInt("boardProjectDetailAttId"), dataRequest.getInt("company_id"));
	        int id = 1;
	        if(dataNext.size()>0)
	        {
	        	id =  Integer.parseInt(String.valueOf(dataNext.get(0)));
//	        	id++;
	        }
	        
	        TmBoardProjectDetailAttchementDiscussion data = new TmBoardProjectDetailAttchementDiscussion();
	        data.setTmBoardId(dataRequest.getInt("boardId"));
	        data.setTmBoardProjectDetailId(dataRequest.getInt("boardProjectDetailId"));
	        data.setTmBoardProjectDetailAttId(dataRequest.getInt("boardProjectDetailAttId"));
	        data.setTmBoardProjectDetailAttDiscId(id);
	        data.setContactId(user_uuid);
	        data.setCompanyId(dataRequest.getInt("company_id"));
	        data.setCommentDiscussion(dataRequest.getString("commentDiscussion"));
	        data.setTm_board_project_detail_att_disc_parent_id(dataRequest.getInt("parentId"));
	        data.setDateComment(localDateTime2);
	        TmBoardProjectDetailAttachementDiscussionService.save(data);
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Save Attachement Comment Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Attachement Comment Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/deleteAttachementComment")
	public String deleteAttachementComment(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> finalFull = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        TmBoardProjectDetailAttachementDiscussionService.delete(dataRequest.getInt("boardId"),  dataRequest.getInt("boardProjectDetailId"), dataRequest.getInt("boardProjectDetailAttId"),  dataRequest.getInt("boardProjectDetailAttDiscId"), dataRequest.getInt("company_id"));
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Delete Attachement Comment Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Attachement Comment Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	
	public List<JSONObject>  makeChildAttchementDiscussion(JSONArray jsonArray){
		String level = "";
	    List<JSONObject> cont = new ArrayList<>();
    	ArrayList<ChildAttachementComment> pairs = new ArrayList<ChildAttachementComment>();
		for (int i = 0; i < jsonArray.size(); i++) {
			 JSONObject resultData = (JSONObject) jsonArray.get(i);
			 ChildAttachementComment data = new ChildAttachementComment();
			 data.setChildId(resultData.getInt("childId"));
	    		data.setParentId(resultData.getInt("parentId"));
	    		data.setBoardId(resultData.getInt("boardId"));
	    		data.setBoardProjectDetailId(resultData.getInt("boardProjectDetailId"));
	    		data.setBoardProjectDetailAttId(resultData.getInt("boardProjectDetailAttId"));
	    		data.setBoardProjectDetailAttDiscId(resultData.getInt("boardProjectDetailAttDiscId"));
	    		data.setCompany_id(resultData.getInt("company_id"));
	    		data.setContactId(resultData.getString("contactId"));
	    		data.setDateComment(resultData.getString("dateComment"));
	    		data.setCommentDiscussion(resultData.getString("commentDiscussion"));
	    		data.setRole(resultData.getString("role"));
	    		
	    		data.setMProjectId(resultData.getInt("MProjectId"));
	    		data.setProjectDetailId(resultData.getInt("projectDetailId"));
	    		data.setNamaTask(resultData.getString("namaTask"));
	    		pairs.add(data);
		 }
		 Map<Integer, ParentAttachementComment> hm = new HashMap<>();
    	 for(ChildAttachementComment p:pairs){

	        	
	            //  ----- Child -----
    		 ParentAttachementComment mmdChild ;
	            if(hm.containsKey(p.getChildId())){
	                mmdChild = hm.get(p.getChildId());
	            }
	            else{
	                mmdChild = new ParentAttachementComment();
	                hm.put(p.getChildId(), mmdChild);
	            }  
	            mmdChild.setId(p.getChildId());
	            mmdChild.setParentId(p.getParentId());
	            mmdChild.setBoardProjectDetailAttId(p.getBoardProjectDetailAttId());
	            mmdChild.setBoardProjectDetailAttDiscId(p.getBoardProjectDetailAttDiscId());
	            mmdChild.setBoardId(p.getBoardId());
	            mmdChild.setBoardProjectDetailId(p.getBoardProjectDetailId());
	            mmdChild.setCompany_id(p.getCompany_id());
	            mmdChild.setMProjectId(p.getMProjectId());
	            mmdChild.setProjectDetailId(p.getProjectDetailId());
	            mmdChild.setNamaTask(p.getNamaTask());
	            
	            mmdChild.setContactId(p.getContactId());
	            mmdChild.setCommentDiscussion(p.getCommentDiscussion());
	            mmdChild.setDateComment(p.getDateComment());
	            mmdChild.setRole(p.getRole());
	            // no need to set ChildrenItems list because the constructor created a new empty list



	            // ------ Parent ----
	            ParentAttachementComment mmdParent ;
	            if(hm.containsKey(p.getParentId())){
	                mmdParent = hm.get(p.getParentId());
	            }
	            else{
	                mmdParent = new ParentAttachementComment();
	                hm.put(p.getParentId(),mmdParent);
	            }
	            
	            mmdParent.setId(p.getParentId());
	            mmdParent.setParentId(null);
	            mmdParent.addChildrenItem(mmdChild);
	            mmdParent.setBoardProjectDetailAttId(p.getBoardProjectDetailAttId());
	            mmdParent.setBoardProjectDetailAttDiscId(p.getBoardProjectDetailAttDiscId());
	            mmdParent.setBoardId(p.getBoardId());
	            mmdParent.setBoardProjectDetailId(p.getBoardProjectDetailId());
	            mmdParent.setCompany_id(p.getCompany_id());
	            mmdParent.setMProjectId(p.getMProjectId());
	            mmdParent.setProjectDetailId(p.getProjectDetailId());
	            mmdParent.setNamaTask(p.getNamaTask());
	            
	            mmdParent.setContactId(p.getContactId());
	            mmdParent.setCommentDiscussion(p.getCommentDiscussion());
	            mmdParent.setDateComment(p.getDateComment());
	            mmdParent.setRole(p.getRole());
	        }
    	 List<ParentAttachementComment> DX = new ArrayList<ParentAttachementComment>(); 
        for(ParentAttachementComment mmd : hm.values()){
            if(mmd.getParentId() == null)
                DX.add(mmd);
        }

        // Print 
        for(ParentAttachementComment mmd: DX){
            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
            JSONObject data3 = new JSONObject();
            
            data3.put("data",mmd);
            cont.add(data3);
        }
        for(int a= 0 ; a< cont.size() ; a++)
        {
        	level = cont.get(a).getString("data");
        	logger.info("cont = " + cont.get(a).getString("data"));
        }
        JSONObject dataRequestLevel = (JSONObject)JSONSerializer.toJSON(level);
	 
        List<JSONObject> list = new ArrayList<JSONObject>();
        for (int i = 0; i < dataRequestLevel.getJSONArray("childrenItems").size(); i++) {
                list.add(dataRequestLevel.getJSONArray("childrenItems").getJSONObject(i));
        }
        
        Collections.sort(list, new SortBasedOnMessageId());
        
        List<JSONObject> resultArray2 = list;
        return resultArray2;
		}
	
	//------------------------------------------------------------------------
		public String getUserId(String header)
		{
			final String uri = "http://54.169.109.123:3003/apps/me";
	        
	        RestTemplate restTemplate = new RestTemplate();
	         
	        HttpHeaders headers = new HttpHeaders();
	        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	        headers.set("x-access-code", header);
	        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
	        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
	        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
	        
	        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
		      
		    return user_uuid;
		}

}
