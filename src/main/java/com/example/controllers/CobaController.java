package com.example.controllers;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.service.*;
import com.example.service.ListDocumentSummaryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
@RestController
@RequestMapping("/api")
public class CobaController {
	private static final Logger logger = LoggerFactory.getLogger(CobaController.class);
	Object data;
	
	List<JSONObject> dataAKhir = new ArrayList<>();
	
//	private Channel channel;
	  private String requestQueueName = "rpc_queue";
	@Autowired
	private ListDocumentSummarySubcategoryService ListDocumentSummarySubcategoryService;
	
	@Autowired
	private ListDocumentSummaryService ListDocumentSummaryService;
	

	@Autowired
	private TmProjectTimeframeService TmProjectTimeframeService;
	
//	@Autowired
//	private RabbitMQSenderService rabbitMQSender;
	
//	@Autowired
//	private AmqpTemplate amqpTemplate;
	
//	@Autowired
//	private EmployeeRegistrationSource employeeRegistrationSource;
	
	@PostMapping("/hello")
	public String hello(@RequestBody String request)
	{
		return "hello";
	}
	
	@PostMapping("/cobarabbitv2")
	public String cobarabbitv2(@RequestBody String request,  @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
//			 final String corrId = UUID.randomUUID().toString();
//			 	channel.exchangeDeclare("coba-exchange", "direct", true);
//		        String replyQueueName = channel.queueDeclare().getQueue();
//		        AMQP.BasicProperties props = new AMQP.BasicProperties
//		                .Builder()
//		                .correlationId(corrId)
//		                .replyTo(replyQueueName)
//		                .build();
//		        String message = "halo";
//		        logger.info("message.getBytes(\"UTF-8\") = " + message.getBytes("UTF-8"));
//		        channel.basicPublish(exchange, routingKey, props, body);
//		        channel.basicPublish("", routingKey, props, body);
//		        logger.info("replyQueueName = " + replyQueueName);
//		        channel.basicPublish("", replyQueueName, props, message.getBytes("UTF-8"));
//
//		        final BlockingQueue<String> response1 = new ArrayBlockingQueue<>(1);

//		        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
//		            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
//		                response1.offer(new String(delivery.getBody(), "UTF-8"));
//		            }
//		        }, consumerTag -> {
//		        });
//
//		        String result = response1.take();
			response.put("responseCode", "00");
			response.put("responseDesc", "cobarabbitv2 Success");
//			response.put("result",result);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "cobarabbitv2 Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/cobarabbit")
	public String cobarabbit(@RequestBody String request,  @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
//			JSONObject data = new JSONObject();
//			data.put("dataRequest", dataRequest);
//			data.put("user_uuid", header);
//			data.put("API", "DetailProject");
//			String hasil = rabbitMQSender.send(data);
			
			
			response.put("responseCode", "00");
			response.put("responseDesc", "cobaProcedure Success");
			 
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "cobaProcedure Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/cobaProcedure")
	public String cobaProcedure(@RequestBody String request)
	{
		JSONObject response = new JSONObject();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> dataProcedure = TmProjectTimeframeService.cobaProcedure();
			logger.info("dataProcedure = " + dataProcedure);
			
			 response.put("responseCode", "00");
			 response.put("responseDesc", "cobaProcedure Success");
			 response.put("data", dataProcedure);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "cobaProcedure Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	
	public  JSONObject listData(JSONArray childData){
    	List<JSONObject> cont = new ArrayList<>();
    	JSONObject data2 = new JSONObject();
    	data2.put("taskId",1);
    	List<JSONObject> contAkhir = new ArrayList<>();
		JSONObject response = new JSONObject();
		return data2;
    }
	
	
	//ini fungsi untuk memanggil API lain
	@PostMapping("/calling")
	private static void getEmployees()
	{	        
	        
	     // request url
	        String url = "http://18.189.31.121:8080/project-management-0.0.1-SNAPSHOT/api/listProject/a";

	        // create an instance of RestTemplate
	        RestTemplate restTemplate = new RestTemplate();

	        // create headers
	        HttpHeaders headers = new HttpHeaders();
	        // set `content-type` header
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        headers.set("User-Access", "3ZnZ5WJzGzcgZZX5KqyqSgYkGJ52");
	        // set `accept` header
//	        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

	        // request body parameters
	        Map<String, Object> map = new HashMap<>();
	        map.put("company_id", 1);

	        // build the request
	        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

	        // send POST request
	        ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
	        
	        JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(response);
	        
//	        JSONArray last = dataRequest.getJSONObject("body").getJSONArray("responseData");
	        if(dataRequest.getJSONObject("body").getJSONArray("responseData").getJSONObject(0).getInt("projectId") != 2)
	        {
	        	//klo mau menghapus
	        	dataRequest.getJSONObject("body").getJSONArray("responseData").remove(0);
	        }
//	        dataRequest.getJSONObject("body").getJSONArray("responseData").getJSONObject(0).put("coba", "halooo");
	        logger.info("response = " +  dataRequest.getJSONObject("body").getJSONArray("responseData"));
	    
	}
	
	
	@PostMapping("/read")
	public String read(@RequestBody String request)
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    
	    if(dataRequest.getJSONArray("detailData").size() > 0)
	    {
	    	ListDocumentSummaryService.delete(dataRequest.getInt("listDocumentId"), dataRequest.getInt("company_id"));
	    	ListDocumentSummarySubcategoryService.delete(dataRequest.getInt("listDocumentId"), dataRequest.getInt("company_id"));
	    	for(int a = 0 ; a < dataRequest.getJSONArray("detailData").size(); a++)
	    	{
	    		JSONArray dataChild = (JSONArray) JSONSerializer.toJSON(dataRequest.getJSONArray("detailData").getJSONObject(a).getJSONArray("detail"));
	    		
	    		
	    		ListDocumentSummary data = new ListDocumentSummary();
	    		data.setDocumentNoId(dataRequest.getInt("listDocumentId"));
	    		data.setIdCompany(dataRequest.getInt("company_id"));
	    		data.setIdCategory(dataRequest.getJSONArray("detailData").getJSONObject(a).getInt("category"));
//	    		ListDocumentSummaryService.save(data);
	    		
	    		hasChild(dataChild,dataRequest.getInt("listDocumentId"),dataRequest.getJSONArray("detailData").getJSONObject(a).getInt("category"),dataRequest.getInt("company_id"));
	    	}
	    }
		return "halo";
	}
	
	 public void  hasChild(JSONArray childData, int doc, int category, int company){
	    	List<JSONObject> cont = new ArrayList<>();
			JSONObject response = new JSONObject();
			 ListDocumentSummarySubcategory data = new ListDocumentSummarySubcategory();

	              for (int i = 0; i < childData.size(); i++) {
	                   JSONObject resultData = (JSONObject) childData.get(i);
	                   
	                   
	                   data.setDocumentNoId(doc);
	                   data.setIdCompany(company);
	                   data.setIdCategory(category);
	                   data.setIdSubcategory(resultData.getInt("subcategory"));
	                   data.setValue(resultData.getString("value"));
	                   data.setIdSubsubcategory(resultData.getInt("subsubcategory"));
	                   data.setNourut(resultData.getInt("nourut"));
	                   data.setParentId(resultData.getInt("parentId"));
	                   data.setId(resultData.getInt("id"));
//	                   ListDocumentSummarySubcategoryService.save(data);
	                   logger.info("data = " + data.getId());
	                   
//	                   // disiini olah data nya, misal insert ke tabel
	                   JSONArray dataAnotherChild = (JSONArray) JSONSerializer.toJSON( resultData .get("childrenItems"));
	                   if(dataAnotherChild.size() > 0){
	                         hasChild(dataAnotherChild, doc, category, company);
	                         
	                   }
	              }
	    	
	    }
	 
	 public String getUserId(String header )
		{
			final String uri = "http://54.169.109.123:3003/apps/me";
	        
	        RestTemplate restTemplate = new RestTemplate();
	         
	        HttpHeaders headers = new HttpHeaders();
	        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	        headers.set("x-access-code", header);
	        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
	        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
	        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
	        
	        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
		      
		    return user_uuid;
		}
}
