package com.example.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.entity.TaskList;
import com.example.service.MTaskListService;

@RestController
@RequestMapping({"/api"})
public class MTaskListController {
	private static final Logger logger = LoggerFactory.getLogger(MTaskListController.class);
	
	@Autowired
	private MTaskListService MTaskListService;
	
	
	
	
	
	
	@RequestMapping(value = "/MTaskList/{paramAktif}", method = RequestMethod.POST,
            consumes = {"application/json"})
	@ResponseBody
	
	public String MTaskList(
			@PathVariable("paramAktif") String paramAktif, 
			@RequestHeader(value = "User-Access") String header,
			@RequestBody String request
			) 
	{
		
		JSONObject response = new JSONObject();
		List<JSONObject> cont = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		List<JSONObject> taskListData = new ArrayList<>();
		logger.info("input request >>>> " + dataRequest);
		try {
			UUID user_uuid = UUID.fromString(getUserId(header));
			if (paramAktif.toString().toLowerCase().equals("y")) {
				List<TaskList> MTaskListServiceData = MTaskListService.findByStatusAndCompanyId(1, dataRequest.getInt("company_id"), user_uuid);
				for (TaskList dataTaskList : MTaskListServiceData) {
					JSONObject data = new JSONObject();
					
					data.put("tasklistId",dataTaskList.getTasklistId());
					data.put("tasklistNama", dataTaskList.getTasklistNama());
					data.put("tasklistStatus", dataTaskList.getStatus());
					data.put("tasklistDescription", dataTaskList.getDescription() == null || dataTaskList.getDescription() == "" ? "" : dataTaskList.getDescription() );
					data.put("companyId", dataTaskList.getCompanyId());
					taskListData.add(data);
				}
			} 
			else if (paramAktif.toString().toLowerCase().equals("n")) {
				List<TaskList> MTaskListServiceData = MTaskListService.findByStatusAndCompanyId(0, dataRequest.getInt("company_id"), user_uuid);
				for (TaskList dataTaskList : MTaskListServiceData) {
					JSONObject data = new JSONObject();
					data.put("tasklistId",dataTaskList.getTasklistId());
					data.put("tasklistNama", dataTaskList.getTasklistNama());
					data.put("tasklistStatus", dataTaskList.getStatus());
					data.put("tasklistDescription", dataTaskList.getDescription() == null || dataTaskList.getDescription() == "" ? "" : dataTaskList.getDescription() );
					data.put("companyId", dataTaskList.getCompanyId());
					taskListData.add(data);
				}
			} else {
				List<TaskList> MTaskListServiceData = MTaskListService.findByCompanyId(dataRequest.getInt("company_id"), user_uuid);
				for (TaskList dataTaskList : MTaskListServiceData) {
					JSONObject data = new JSONObject();
					data.put("tasklistId",dataTaskList.getTasklistId());
					data.put("tasklistNama", dataTaskList.getTasklistNama());
					data.put("tasklistStatus", dataTaskList.getStatus());
					data.put("tasklistDescription", dataTaskList.getDescription() == null || dataTaskList.getDescription() == "" ? "" : dataTaskList.getDescription() );
					data.put("companyId", dataTaskList.getCompanyId());
					taskListData.add(data);
				}
			} 
			Object data;
//			return new ApiResponse<>(00, "Login Success", data);
			response.put("responseCode", "00");
			response.put("responseDesc", "Get Task List Success");
			response.put("responseData", taskListData);
		} 
		catch (Exception e) {
			response.put("responseCode", "99");
			response.put("responseDesc", "Get Task List Failed");
			response.put("responseError", e.getMessage());
			logger.info("error #### " + e.getMessage());
    	} 
		return response.toString();
	}
	
	@PostMapping({"/MTaskListDetail"})
	  public String MTaskListDetail(@RequestBody String request) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input request >>>> " + dataRequest);
	    try {
	      List<JSONObject> detail_data = new ArrayList<>();
	      Optional<TaskList> DataDetailTaskList = MTaskListService.FindByTaskListIdAndCompanyId(dataRequest.getInt("tasklistId"), dataRequest.getInt("company_id"));
	      if (DataDetailTaskList.isPresent()) {
	        JSONObject data = new JSONObject();
	        data.put("tasklistId", ((TaskList)DataDetailTaskList.get()).getTasklistId());
	        data.put("tasklistNama", ((TaskList)DataDetailTaskList.get()).getTasklistNama());
	        data.put("tasklistStatus", ((TaskList)DataDetailTaskList.get()).getStatus());
	        data.put("tasklistDescription", ((TaskList)DataDetailTaskList.get()).getDescription() == null || ((TaskList)DataDetailTaskList.get()).getDescription() == "" ? "" : ((TaskList)DataDetailTaskList.get()).getDescription() );
	        data.put("companyId", ((TaskList)DataDetailTaskList.get()).getCompanyId());
	        detail_data.add(data);
	      } 
	      response.put("responseCode", "00");
	      response.put("responseDesc", "Get Detail Task List Success");
	      response.put("responseData", detail_data);
	    } catch (Exception e) {
	      response.put("responseCode", "99");
	      response.put("responseDesc", "Get Detail Task List Failed");
	      response.put("responseError", e.getMessage());
	      logger.info("error #### " + e.getMessage());
	    } 
	    return response.toString();
	  }
	
	@PostMapping({"/saveTaskList"})
	  public String TMSaveMasterNeedsType(@RequestBody String request,
				@RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	     
	      LocalDateTime localDateTime = LocalDateTime.now();
	      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	      String formatDateTime = localDateTime.format(formatter);
	      LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	      
	      JSONArray arrayTask = dataRequest.getJSONArray("taskArray");
	      
	      UUID user_uuid = UUID.fromString(getUserId(header));
	      if (arrayTask.size() > 0)
	      {
	    	  ArrayList<TaskList> isiArray = new ArrayList<>();
    		  for (int a = 0; a < arrayTask.size(); a++) {
    			  
    			  	TaskList masterTaskList = new TaskList(
    			  	arrayTask.getJSONObject(a).has("tasklistId") == false ?  00 : arrayTask.getJSONObject(a).getInt("tasklistId"), 
	                arrayTask.getJSONObject(a).getString("tasklistNama"),
	                user_uuid,
	                localDateTime2,
	                dataRequest.getInt("company_id"),
	                arrayTask.getJSONObject(a).getString("tasklistDescription"),
	                arrayTask.getJSONObject(a).getInt("tasklistStatus")
	                );
	            isiArray.add(masterTaskList);
	            
	           
	            
	            
	          }
    		  
    		  logger.info("isiArray = " + isiArray);
    		  MTaskListService.save(isiArray);
    		  
    		  
    		  
    		  
    		  
	       }
	      
	      response.put("responseCode", "00");
	      response.put("responseDesc", "Save Task List Success");
	    } catch (Exception e) {
	      response.put("responseCode", "99");
	      response.put("responseDesc", "Save Task List Failed");
	      response.put("responseError", e.getMessage());
	      logger.info("ERROR #### " + e.getMessage());
	    } 
	    return response.toString();
	  }
	
	@PostMapping({"/updateTask"})
	  public String updateTask(@RequestBody String request,@RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try {
	     
	      LocalDateTime localDateTime = LocalDateTime.now();
	      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	      String formatDateTime = localDateTime.format(formatter);
	      LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	      
	      JSONArray arrayTask = dataRequest.getJSONArray("taskArray");
	      UUID user_uuid = UUID.fromString(getUserId(header));
	      
	      if (arrayTask.size() > 0)
	      {
	    	  ArrayList<TaskList> isiArray = new ArrayList<>();
	    	  TaskList isiArray2 = new TaskList();
	    	  for (int a = 0; a < arrayTask.size(); a++) {
	    		  
	    		  Optional<TaskList> dataTaskList = MTaskListService.FindByTaskListIdAndCompanyId(arrayTask.getJSONObject(a).getInt("tasklistId"), dataRequest.getInt("company_id"));
	    		  if(dataTaskList.isPresent())
	    		  {
	    			  ((TaskList)dataTaskList.get()).setTasklistNama(arrayTask.getJSONObject(a).getString("tasklistNama"));
	    			  ((TaskList)dataTaskList.get()).setDescription(arrayTask.getJSONObject(a).getString("tasklistDescription"));
	    			  ((TaskList)dataTaskList.get()).setStatus(arrayTask.getJSONObject(a).getInt("tasklistStatus"));
	    			  ((TaskList)dataTaskList.get()).setUpdatedBy(user_uuid);
	    			  ((TaskList)dataTaskList.get()).setUpdatedDate(localDateTime2);
	    			  isiArray.add(dataTaskList.get());  
	    		  } 
	          }
	    	  MTaskListService.save(isiArray);
//  		  MTaskListService.save(isiArray);
  		  
	       }
	      
	      response.put("responseCode", "00");
	      response.put("responseDesc", "Update Task List Success");
	    } catch (Exception e) {
	      response.put("responseCode", "99");
	      response.put("responseDesc", "Update Task List Failed");
	      response.put("responseError", e.getMessage());
	    } 
	    return response.toString();
	  }
	
	public String getUserId(String header )
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}
}
