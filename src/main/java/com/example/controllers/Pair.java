package com.example.controllers;
public class Pair {
    private Integer childId;
    private Integer parentId;
    private String subcategory;
    private String value;
    private String subsubcategory;
    private int nourut;
    public Pair(Integer childId, Integer parentId, String subcategory, String value, String subsubcategory,int nourut) {
        this.childId = childId;
        this.parentId = parentId;
        this.subcategory = subcategory;
        this.value = value;
        this.subsubcategory = subsubcategory;
        this.nourut = nourut;
    }
    
    
	public int getNourut() {
		return nourut;
	}


	public void setNourut(int nourut) {
		this.nourut = nourut;
	}


	public Integer getChildId() {
		return childId;
	}
	public void setChildId(Integer childId) {
		this.childId = childId;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public String getSubcategory() {
		return subcategory;
	}
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getSubsubcategory() {
		return subsubcategory;
	}
	public void setSubsubcategory(String subsubcategory) {
		this.subsubcategory = subsubcategory;
	}
	
	
    
    
	
}