package com.example.controllers;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.client.RestTemplate;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.helper.Helper;
import com.example.service.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/api")
public class TimeframeController {
	private static final Logger logger = LoggerFactory.getLogger(TimeframeController.class);
	
	@Autowired
	private GabunganService GabunganService;
	
	@Autowired
	private ProjectDetailService ProjectDetailService;
	
	@Autowired
	private TmProjectTimeframeService TmProjectTimeframeService;
	
	@Autowired
	private TmProjectTimeframeBudgethourService TmProjectTimeframeBudgethourService;
	
	//untuk mendapatkan timeframe combobox di planner
	//untuk mendapatkan semua timeframe
	@PostMapping({"/listTimeframeCombobox"})
		public String listTimeframeCombobox(@RequestBody String request,  @RequestHeader(value = "User-Access") String header2) {
		    JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    List<JSONObject> header = new ArrayList<>();
		    List<JSONObject> headerAlone = new ArrayList<>();
		    
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    logger.info("input >>>> " + dataRequest);
		    try {
		    	UUID user_uuid = UUID.fromString(getUserId(header2));
		    	
		    }
		    catch(Exception e)
		    {
		    	
		    }
		    return response.toString();
		}
		//-------------------------
	//
	
	ArrayList<TmProjectTimeframe> TimeframeDetail = new ArrayList<>();
	ArrayList<TmProjectTimeframeBudgethour> TimeframeDetailBudgetHour = new ArrayList<>();
	@PostMapping("/saveTimeframe")
	public String saveTimeframe(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    TimeframeDetail = new ArrayList<>();
	    TimeframeDetailBudgetHour = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        JSONArray arrayTask = dataRequest.getJSONArray("projectDetailTimeframeArray");
		      
			
			if (arrayTask.size() > 0)
			{
				for (int a = 0; a < arrayTask.size(); a++) {
					JSONArray dataChild = (JSONArray) JSONSerializer.toJSON(dataRequest.getJSONArray("projectDetailTimeframeArray"));
					JSONObject resultData = (JSONObject) dataChild.get(a);
					TmProjectTimeframe data = new TmProjectTimeframe();
					data.setProjectDetailId(resultData.getInt("projectDetailId"));
					data.setTmProjectId(resultData.getInt("mProjectId"));
					data.setCompanyId(resultData.getInt("company_id"));
					data.setTimeframeId(resultData.getInt("timeframeId"));
					
					try {
						data.setDateline(formatter11.parse(resultData.getString("dateline")));
						data.setStartdate(formatter11.parse(resultData.getString("startdate")));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					data.setBudgetHour(Float.valueOf(Double.toString(resultData.getDouble("budgetHour"))));
					TimeframeDetail.add(data);
					if(resultData.getJSONArray("children").size() > 0)
					{
						hasChild(resultData.getJSONArray("children"));
					}
					JSONArray detailBudgethour = resultData.getJSONArray("dataDetail");
					if(detailBudgethour.size()>0)
					{
						
						for (int b = 0; b < detailBudgethour.size(); b++) {
							TmProjectTimeframeBudgethour dataDetail = new TmProjectTimeframeBudgethour();
							dataDetail.setTmProjectId(resultData.getInt("mProjectId"));
							dataDetail.setCompanyId(resultData.getInt("company_id"));
							dataDetail.setTimeframeId(resultData.getInt("timeframeId"));
							dataDetail.setProjectDetailId(resultData.getInt("projectDetailId"));
							try {
								dataDetail.setDate(formatter11.parse(detailBudgethour.getJSONObject(b).getString("date")));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							dataDetail.setBudgetHour(Float.valueOf(Double.toString(detailBudgethour.getJSONObject(b).getDouble("budgetHour"))));
							TimeframeDetailBudgetHour.add(dataDetail);
						}
					}
//					JSONArray dataChild = (JSONArray) JSONSerializer.toJSON(dataRequest.getJSONArray("projectDetailTimeframeArray"));
//					hasChild(dataChild);
				}
			}
			GabunganService.saveTimeframeDetail(TimeframeDetail, TimeframeDetailBudgetHour);
			response.put("responseCode", "00");
		    response.put("responseDesc", "Save Timeframe BudgetHour Success");
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Save Timeframe BudgetHour Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	public Date coba1(String input) throws ParseException {
		SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
		return formatter11.parse(input);
	}
	
	public void  hasChild(JSONArray childData){
    	List<JSONObject> cont = new ArrayList<>();
		JSONObject response = new JSONObject();
		SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
		 
		Date start =null;
	      for (int i = 0; i < childData.size(); i++) {
	    	  
	    	  
	           JSONObject resultData = (JSONObject) childData.get(i);
	           if(resultData.getInt("timeframeId") != 0 )
	           {
	        	   	TmProjectTimeframe data = new TmProjectTimeframe();
					data.setProjectDetailId(resultData.getInt("projectDetailId"));
					data.setTmProjectId(resultData.getInt("mProjectId"));
					data.setCompanyId(resultData.getInt("company_id"));
					data.setTimeframeId(resultData.getInt("timeframeId"));
					
					try {
						data.setDateline(formatter11.parse(resultData.getString("dateline")));
						data.setStartdate(formatter11.parse(resultData.getString("startdate")));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					data.setBudgetHour(Float.valueOf(Double.toString(resultData.getDouble("budgetHour"))));
					TimeframeDetail.add(data);
					
					JSONArray detailBudgethour = resultData.getJSONArray("dataDetail");
					if(detailBudgethour.size()>0)
					{
						
						for (int b = 0; b < detailBudgethour.size(); b++) {
							TmProjectTimeframeBudgethour dataDetail = new TmProjectTimeframeBudgethour();
							dataDetail.setTmProjectId(resultData.getInt("mProjectId"));
							dataDetail.setCompanyId(resultData.getInt("company_id"));
							dataDetail.setTimeframeId(resultData.getInt("timeframeId"));
							dataDetail.setProjectDetailId(resultData.getInt("projectDetailId"));
							try {
								dataDetail.setDate(formatter11.parse(detailBudgethour.getJSONObject(b).getString("date")));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							dataDetail.setBudgetHour(Float.valueOf(Double.toString(detailBudgethour.getJSONObject(b).getDouble("budgetHour"))));
							TimeframeDetailBudgetHour.add(dataDetail);
						}
					}
	           }
	//         disiini olah data nya, misal insert ke tabel
	           JSONArray dataAnotherChild = (JSONArray) JSONSerializer.toJSON(resultData.get("children"));
	           if(dataAnotherChild.size() > 0){
	                 hasChild(dataAnotherChild);
	           }
	      }
    }
	
	@PostMapping({"/DetailTimeframe"})
	public String DetailTimeframe(@RequestBody String request,  @RequestHeader(value = "User-Access") String header) {
	    JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> contaa = new ArrayList<>();
	    String level = "";
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try {
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	ArrayList<ChildrenProjectTimeframe> pairs = new ArrayList<ChildrenProjectTimeframe>();
			List<Object[]> dataHeader = TmProjectTimeframeService.getTimeframe(dataRequest.getInt("projectId"), dataRequest.getInt("company_id"));
			List<Object[]> dataDetail = TmProjectTimeframeService.getTimeframeBudgetHour(dataRequest.getInt("projectId"), dataRequest.getInt("company_id"));
			dataHeader.stream().forEach(col1->{
//				logger.info("col1 = " + formatter11.parse(col1[4].toString()));
				logger.info("project = " + Integer.parseInt(String.valueOf(col1[0])));
				logger.info("parent = " + col1[1]);
				logger.info("projectDetail = " + Integer.parseInt(String.valueOf(col1[2])));
				ChildrenProjectTimeframe data = new ChildrenProjectTimeframe();
				data.setmProjectId(Integer.valueOf(col1[0].toString()));
				data.setParentId(Integer.valueOf(col1[1].toString()));
				data.setProjectDetailId(Integer.valueOf(col1[2].toString()));
				data.setNama(col1[3].toString());
				data.setStartdate(col1[4] == null ? "" : String.valueOf(col1[4]));
				data.setDateline(col1[5] == null ? "" : String.valueOf(col1[5]));
				data.setBudgetHour(col1[6] == null ? null : Float.valueOf(String.valueOf(col1[6])));
				data.setCompany_id(Integer.valueOf(col1[7].toString()));
				data.setTimeframeId(col1[8] == null ? null : Integer.valueOf(col1[8].toString()));
				data.setUser_uuid(col1[10] == null ? null : col1[10].toString());
				List<JSONObject> TimeframeBudgethour = new ArrayList<>();
				
				dataDetail.stream().forEach(col2->{
					if(
							Integer.valueOf(col2[0].toString()).equals(Integer.valueOf(col1[2].toString())) 
							&& col1[8] != null &&  col1[8].equals(col2[2])
							&& Integer.valueOf(col1[7].toString()).equals(Integer.valueOf(col2[3].toString()))
							)
					{
						JSONObject data21 = new JSONObject();
						data21.put("date", col2[4].toString());
						data21.put("budgetHour", Float.valueOf(col2[5].toString()));
						TimeframeBudgethour.add(data21);
					}
					
				});
				data.setDataDetail(TimeframeBudgethour);
				pairs.add(data);
				
				
				
			});
			logger.info("pairs = " + pairs.size());
			Map<Integer, ParentProjectITimeframe> hm = new HashMap<>();
	    	 for(ChildrenProjectTimeframe p:pairs){

		        	
		            //  ----- Child -----
	    		 ParentProjectITimeframe mmdChild ;
		            if(hm.containsKey(p.getProjectDetailId())){
		                mmdChild = hm.get(p.getProjectDetailId());
		            }
		            else{
		                mmdChild = new ParentProjectITimeframe();
		                hm.put(p.getProjectDetailId(), mmdChild);
		            }
		            mmdChild.setDataDetail(p.getDataDetail());
		            mmdChild.setProjectDetailId(p.getProjectDetailId());
		            mmdChild.setmProjectId(p.getmProjectId());
		            mmdChild.setParentId(p.getParentId());
		            mmdChild.setNama(p.getNama());
		            mmdChild.setStartdate(p.getStartdate());
		            mmdChild.setDateline(p.getDateline());
		            mmdChild.setBudgetHour(p.getBudgetHour());
		            mmdChild.setCompany_id(p.getCompany_id());
		            mmdChild.setTimeframeId(p.getTimeframeId());
		            mmdChild.setUser_uuid(p.getUser_uuid());
		            // no need to set ChildrenItems list because the constructor created a new empty list



		            // ------ Parent ----
		            ParentProjectITimeframe mmdParent ;
		            if(hm.containsKey(p.getParentId())){
		                mmdParent = hm.get(p.getParentId());
		            }
		            else{
		                mmdParent = new ParentProjectITimeframe();
		                hm.put(p.getParentId(),mmdParent);
		            }
		            mmdParent.setProjectDetailId(p.getParentId());
		            mmdParent.setParentId(null);
		            mmdParent.addChildren(mmdChild);
		            mmdParent.setmProjectId(p.getmProjectId());
		            
		            mmdParent.setBudgetHour(p.getBudgetHour());
		            mmdParent.setNama(p.getNama());
		            mmdParent.setDateline(p.getDateline());
		            mmdParent.setStartdate(p.getStartdate());
		            mmdParent.setDataDetail(p.getDataDetail());
		            mmdParent.setTimeframeId(p.getTimeframeId());
		            mmdParent.setUser_uuid(p.getUser_uuid());
		        }
	    	 
	    	 	List<ParentProjectITimeframe> DX = new ArrayList<ParentProjectITimeframe>(); 
		        for(ParentProjectITimeframe mmd : hm.values()){
		            if(mmd.getParentId() == null)
		                DX.add(mmd);
		        }

		        // Print 
		        for(ParentProjectITimeframe mmd: DX){
		            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
		            JSONObject data3 = new JSONObject();
		            
		            data3.put("data",mmd);
		            cont.add(data3);
		        }
		        for(int a= 0 ; a< cont.size() ; a++)
		        {
		        	level = cont.get(a).getString("data");
		        	logger.info("cont = " + cont.get(a).getString("data"));
		        }
		        JSONObject dataRequestLevel = new JSONObject();
		        List<JSONObject> resultArray2 = new ArrayList<>();
		        if(!level.isEmpty())
		        {
		        	dataRequestLevel = (JSONObject)JSONSerializer.toJSON(level);
		        	List<JSONObject> list = new ArrayList<JSONObject>();
		            for (int i = 0; i < dataRequestLevel.getJSONArray("children").size(); i++) {
		                    list.add(dataRequestLevel.getJSONArray("children").getJSONObject(i));
		            }
		            
		            Collections.sort(list, new SortBasedOnMessageId());
		            List<JSONObject> order = new ArrayList<>();
		        	//digunakan untuk order by list<JSONObject> biar urut dari id 1 ke id yang lebih besar
		            order = Helper.orderByTimeframe(list);
		            resultArray2 = order;
		        }
	            
//	            Collections.sort(list, new SortBasedOnMessageId());
//
//	            List<JSONObject> resultArray2 = list;
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Show Detail Timeframe Success"); 
		    response.put("responseData", resultArray2);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Show Detail Project Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	public String getUserId(String header)
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}
	
}
