package com.example.controllers;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import com.example.entity.ProjectDetailDependency;
import com.example.entity.TmProjectTimeframe;

public class ParentGantChart {

	private Integer Id;
    private Integer parentId;
    private Integer mProjectId;
    private Integer projectParty;
    private String projectName;
    private Integer projectDetailId;
    private Integer mTaskId;
    private Integer mGroupingId;
    private String task;
    private Integer company_id;
    private Integer timeframeId;
    private String startdate;
    private String dateline;
    private String assignTo;
    private String dateBudgetHour;
    private Float BudgetHour;
	
	private List<JSONObject> dataTimeframeBudgetHour;
	private List<JSONObject> dataDependency;
    private List<ParentGantChart> children; 
    public ParentGantChart() {
    	this.parentId = null;
    	this.Id = null;
        this.mProjectId = null;
        this.projectDetailId = null;
        this.projectName = "";
        this.projectParty = null;
        this.company_id = null;
        this.mTaskId = null;
        this.mGroupingId = null;
        this.task = "";
        this.timeframeId = null;
        this.startdate = "";
        this.dateline = "";
        this.assignTo = "";
        this.dateBudgetHour = "";
        this.BudgetHour = null;
        this.children = new ArrayList<ParentGantChart>();
        this.dataTimeframeBudgetHour = new ArrayList<JSONObject>();
        this.dataDependency = new ArrayList<JSONObject>();
    }

	
	
    


	public List<JSONObject> getDataDependency() {
		return dataDependency;
	}






	public void setDataDependency(List<JSONObject> dataDependency) {
		this.dataDependency = dataDependency;
	}






	public Integer getCompany_id() {
		return company_id;
	}






	public void setCompany_id(Integer company_id) {
		this.company_id = company_id;
	}






	public Integer getProjectDetailId() {
		return projectDetailId;
	}






	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}






	public Integer getProjectParty() {
		return projectParty;
	}






	public void setProjectParty(Integer projectParty) {
		this.projectParty = projectParty;
	}






	public String getProjectName() {
		return projectName;
	}






	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}






	public Integer getId() {
		return Id;
	}






	public void setId(Integer id) {
		Id = id;
	}






	public Integer getParentId() {
		return parentId;
	}






	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}






	public Integer getmProjectId() {
		return mProjectId;
	}






	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}






	public Integer getmTaskId() {
		return mTaskId;
	}






	public void setmTaskId(Integer mTaskId) {
		this.mTaskId = mTaskId;
	}






	public Integer getmGroupingId() {
		return mGroupingId;
	}






	public void setmGroupingId(Integer mGroupingId) {
		this.mGroupingId = mGroupingId;
	}






	public String getTask() {
		return task;
	}






	public void setTask(String task) {
		this.task = task;
	}






	public Integer getTimeframeId() {
		return timeframeId;
	}






	public void setTimeframeId(Integer timeframeId) {
		this.timeframeId = timeframeId;
	}






	public String getStartdate() {
		return startdate;
	}






	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}






	public String getDateline() {
		return dateline;
	}






	public void setDateline(String dateline) {
		this.dateline = dateline;
	}






	public String getAssignTo() {
		return assignTo;
	}






	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}






	public String getDateBudgetHour() {
		return dateBudgetHour;
	}






	public void setDateBudgetHour(String dateBudgetHour) {
		this.dateBudgetHour = dateBudgetHour;
	}






	public Float getBudgetHour() {
		return BudgetHour;
	}






	public void setBudgetHour(Float budgetHour) {
		BudgetHour = budgetHour;
	}






	public List<JSONObject> getDataTimeframeBudgetHour() {
		return dataTimeframeBudgetHour;
	}






	public void setDataTimeframeBudgetHour(List<JSONObject> dataTimeframeBudgetHour) {
		this.dataTimeframeBudgetHour = dataTimeframeBudgetHour;
	}






	public List<ParentGantChart> getChildren() {
        return children;
    }
    public void setChildren(List<ParentGantChart> children) {
        this.children = children;
    }
    public void addChildren(ParentGantChart children){
        if(!this.children.contains(children))
            this.children.add(children);
    }

}