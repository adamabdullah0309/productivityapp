package com.example.controllers;

import java.util.List;

import net.sf.json.JSONObject;

import com.example.entity.ProjectDetailDependency;
import com.example.entity.TmProjectTimeframe;

public class ProjectDetailChild {
    private Integer childId;
    private Integer parentId;
    private Integer mProjectId;
    
    private Integer mTaskId;
    private String mTaskNama;
    private Integer mGroupingId;
    private String mGroupingNama;
    
    
    private Integer status;
	private Integer priority;
	private Integer companyId;
	
	private Integer jumlahKaryawan;
	private Integer jumlahDependency;
	
	private List<JSONObject> dataTimeframe; 
	private List<JSONObject> karyawanDetail; 
	private List<ProjectDetailDependency> relationDetail; 
	private String description;
	private Integer templateId;
	private String templateNama;
    public ProjectDetailChild(Integer childId, Integer parentId, Integer mProjectId, Integer mTaskId, String mTaskNama, 
    		Integer mGroupingId,String mGroupingNama,Integer status,Integer priority,Integer companyId,Integer jumlahKaryawan,Integer jumlahDependency,
    		List<JSONObject> karyawanDetail, List<ProjectDetailDependency> relationDetail, String description
    		) {
        this.childId = childId;
        this.parentId = parentId;
        this.mProjectId = mProjectId;
        this.mTaskId = mTaskId;
        this.mTaskNama = mTaskNama;
        this.mGroupingId = mGroupingId;
        this.mGroupingNama = mGroupingNama;
        this.status = status;
        this.priority = priority;
        this.companyId = companyId;
        this.jumlahKaryawan = jumlahKaryawan;
        this.jumlahDependency = jumlahDependency;
        this.karyawanDetail = karyawanDetail;
        this.relationDetail = relationDetail;
        this.description = description;
    }
    
    public ProjectDetailChild() {}
    
    
    
    


	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getTemplateNama() {
		return templateNama;
	}

	public void setTemplateNama(String templateNama) {
		this.templateNama = templateNama;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ProjectDetailDependency> getRelationDetail() {
		return relationDetail;
	}

	public void setRelationDetail(List<ProjectDetailDependency> relationDetail) {
		this.relationDetail = relationDetail;
	}

	public List<JSONObject> getKaryawanDetail() {
		return karyawanDetail;
	}

	public void setKaryawanDetail(List<JSONObject> karyawanDetail) {
		this.karyawanDetail = karyawanDetail;
	}


	public Integer getJumlahDependency() {
		return jumlahDependency;
	}

	public void setJumlahDependency(Integer jumlahDependency) {
		this.jumlahDependency = jumlahDependency;
	}

	public List<JSONObject> getDataTimeframe() {
		return dataTimeframe;
	}

	public void setDataTimeframe(List<JSONObject> untukDetail) {
		this.dataTimeframe = untukDetail;
	}

	public Integer getChildId() {
		return childId;
	}
	public void setChildId(Integer childId) {
		this.childId = childId;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public Integer getmProjectId() {
		return mProjectId;
	}
	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}
	public Integer getmTaskId() {
		return mTaskId;
	}
	public void setmTaskId(Integer mTaskId) {
		this.mTaskId = mTaskId;
	}
	public String getmTaskNama() {
		return mTaskNama;
	}
	public void setmTaskNama(String mTaskNama) {
		this.mTaskNama = mTaskNama;
	}
	public Integer getmGroupingId() {
		return mGroupingId;
	}
	public void setmGroupingId(Integer mGroupingId) {
		this.mGroupingId = mGroupingId;
	}
	public String getmGroupingNama() {
		return mGroupingNama;
	}
	public void setmGroupingNama(String mGroupingNama) {
		this.mGroupingNama = mGroupingNama;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getJumlahKaryawan() {
		return jumlahKaryawan;
	}
	public void setJumlahKaryawan(Integer jumlahKaryawan) {
		this.jumlahKaryawan = jumlahKaryawan;
	}
    
    
	
	
    
	
}