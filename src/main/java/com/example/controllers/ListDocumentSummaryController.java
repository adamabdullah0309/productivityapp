package com.example.controllers;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.entity.ListDocument;
import com.example.entity.ListDocumentShared;
import com.example.entity.ListDocumentSummary;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.entity.MasterInitial;
import com.example.repository.ListDocumentRepository;
import com.example.service.GabunganService;
import com.example.service.ListDocumentService;
import com.example.service.ListDocumentSharedService;
import com.example.service.ListDocumentSummaryService;
import com.example.service.ListDocumentSummarySubcategoryService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class ListDocumentSummaryController {
	
	ArrayList<ListDocumentSummarySubcategory> masterSharedDocument = new ArrayList<>();
	
	@Autowired
	private ListDocumentSummaryService ListDocumentSummaryService;
	
	@Autowired
	private ListDocumentSummarySubcategoryService ListDocumentSummarySubcategoryService;
	
	@Autowired
	private ListDocumentService ListDocumentService;
	
	@Autowired
	private GabunganService GabunganService;
	
	List<JSONObject> contAkhir = new ArrayList<>();
	
	private static final Logger logger = LoggerFactory.getLogger(ListDocumentSummaryController.class);
	
	@PostMapping("/saveListDocumentSummary")
	public String saveListDocumentSummary(@RequestBody String request,  @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> final11 = new ArrayList<>();
	    masterSharedDocument = new ArrayList<>();
	    ArrayList<ListDocumentSummarySubcategory> newData = new ArrayList<>();
	    ArrayList<ListDocumentSummary> DataCategory = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    try
	    {
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	ListDocument dataHeader = new ListDocument();
	    	dataHeader.setListDocumentTitle(dataRequest.getString("title"));
	    	dataHeader.setListDocumentType(dataRequest.getInt("type"));
	    	dataHeader.setListDocumentStatus(dataRequest.getBoolean("status"));
	    	if(dataRequest.getInt("shared") != 0)
	    	{
	    		dataHeader.setListDocumentShared(dataRequest.getInt("shared"));
	    	}
	    	
	    	dataHeader.setCreatedBy(user_uuid);
	    	dataHeader.setCreatedAt(localDateTime2);
	    	dataHeader.setIdCompany(dataRequest.getInt("company_id"));
	    	dataHeader.setListConfidential(dataRequest.getBoolean("confidential"));
	    	dataHeader.setListDocumentPathfile(dataRequest.getString("pathFile"));
//	    	ListDocument LastId = ListDocumentService.saveDoc(dataHeader);
	    	if(dataRequest.getJSONArray("detailData").size() > 0)
		    {
//		    	ListDocumentSummaryService.delete(LastId.getListDocumentId(), dataRequest.getInt("company_id"));
//		    	ListDocumentSummarySubcategoryService.delete(LastId.getListDocumentId(), dataRequest.getInt("company_id"));
		    	for(int a = 0 ; a < dataRequest.getJSONArray("detailData").size(); a++)
		    	{
		    		JSONArray dataChild = (JSONArray) JSONSerializer.toJSON(dataRequest.getJSONArray("detailData").getJSONObject(a).getJSONArray("detail"));
		    		
		    		
		    		ListDocumentSummary data = new ListDocumentSummary();
		    		data.setIdCompany(dataRequest.getInt("company_id"));
		    		data.setIdCategory(dataRequest.getJSONArray("detailData").getJSONObject(a).getInt("category"));
//		    		ListDocumentSummaryService.save(data);
		    		DataCategory.add(data);
		    		newData = hasChild(dataChild,dataRequest.getJSONArray("detailData").getJSONObject(a).getInt("category"),dataRequest.getInt("company_id"));
		    	}
		    }
	    	
	    	GabunganService.saveSummary(dataHeader, DataCategory,newData);
	    	
	    	 response.put("responseCode", "00");
		      response.put("responseDesc", "Save List Document Summary Success");
	    }
	    catch(Exception e)
	    {
	    	  response.put("responseCode", "99");
		      response.put("responseDesc", "Save List Document Summary Failed");
		      response.put("responseError", e.getMessage());
		      logger.info("ERROR #### " + e.getMessage());
	    }
	    
	    
		return response.toString();
	}
	
	 public ArrayList<ListDocumentSummarySubcategory>  hasChild(JSONArray childData, int category, int company){
	    	List<JSONObject> cont = new ArrayList<>();
			JSONObject response = new JSONObject();
			 
			 ListDocumentSummarySubcategory data = new ListDocumentSummarySubcategory();

	              for (int i = 0; i < childData.size(); i++) {
	                   JSONObject resultData = (JSONObject) childData.get(i);
	                   
	                   
//	                   data.setDocumentNoId(doc);
	                   data.setIdCompany(company);
	                   data.setIdCategory(category);
	                   data.setIdSubcategory(resultData.getInt("subcategory"));
	                   data.setValue(resultData.getString("value"));
	                   data.setIdSubsubcategory(resultData.getInt("subsubcategory"));
	                   data.setNourut(resultData.getInt("nourut"));
	                   data.setParentId(resultData.getInt("parentId"));
	                   data.setId(resultData.getInt("id"));
	                   masterSharedDocument.add(data);
//	                   ListDocumentSummarySubcategoryService.save(data);
	                   logger.info("data = " + data.getId());
	                   
//	                   // disiini olah data nya, misal insert ke tabel
	                   JSONArray dataAnotherChild = (JSONArray) JSONSerializer.toJSON( resultData .get("childrenItems"));
	                   if(dataAnotherChild.size() > 0){
	                         hasChild(dataAnotherChild,  category, company);
	                         
	                   }
	              }
				return masterSharedDocument;
	    }
	
	 @PostMapping("/updateListDocumentSummary")
		public String updateListDocumentSummary(@RequestBody String request,  @RequestHeader(value = "User-Access") String header)
		{
			JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    List<JSONObject> final11 = new ArrayList<>();
		    contAkhir = new ArrayList<>();
		    masterSharedDocument = new ArrayList<>();
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    
		    Optional<ListDocument> listUpdate = ListDocumentService.findIdAndIdCompany(dataRequest.getInt("listDocumentId"), dataRequest.getInt("company_id"));
		    ArrayList<ListDocumentSummarySubcategory> newData = new ArrayList<>();
		    ArrayList<ListDocumentSummary> DataCategory = new ArrayList<>();
		    logger.info("input >>>> " + dataRequest);
		    try
		    {
		    	LocalDateTime localDateTime = LocalDateTime.now();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
		        String formatDateTime = localDateTime.format(formatter);
		        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
		        
		    	UUID user_uuid = UUID.fromString(getUserId(header));
		    	
		    	 

		    	
		    	if(listUpdate.isPresent())
		    	{
		    		
			    	
		    		((ListDocument)listUpdate.get()).setListDocumentTitle(dataRequest.getString("title"));
		    		((ListDocument)listUpdate.get()).setListDocumentType(dataRequest.getInt("type"));
		    		((ListDocument)listUpdate.get()).setListDocumentStatus(dataRequest.getBoolean("status"));
		    		if(dataRequest.getInt("shared") != 0)
			    	{
		    			((ListDocument)listUpdate.get()).setListDocumentShared(dataRequest.getInt("shared"));
			    	}
		    		((ListDocument)listUpdate.get()).setUpdatedBy(user_uuid);
		    		((ListDocument)listUpdate.get()).setUpdatedAt(localDateTime2);
		    		((ListDocument)listUpdate.get()).setIdCompany(dataRequest.getInt("company_id"));
		    		((ListDocument)listUpdate.get()).setListConfidential(dataRequest.getBoolean("confidential"));
		    		((ListDocument)listUpdate.get()).setListDocumentPathfile(dataRequest.getString("pathFile"));
		    	}
		    	
		    	
//		    	ListDocument LastId = ListDocumentService.saveDoc(listUpdate.get());
		    	
		    	if(dataRequest.getJSONArray("detailData").size() > 0)
			    {
//			    	ListDocumentSummaryService.delete(dataRequest.getInt("listDocumentId"), dataRequest.getInt("company_id"));
//			    	ListDocumentSummarySubcategoryService.delete(dataRequest.getInt("listDocumentId"), dataRequest.getInt("company_id"));
			    	for(int a = 0 ; a < dataRequest.getJSONArray("detailData").size(); a++)
			    	{
			    		JSONArray dataChild = (JSONArray) JSONSerializer.toJSON(dataRequest.getJSONArray("detailData").getJSONObject(a).getJSONArray("detail"));
			    		
			    		
			    		ListDocumentSummary data = new ListDocumentSummary();
//			    		data.setDocumentNoId(dataRequest.getInt("listDocumentId"));
			    		data.setIdCompany(dataRequest.getInt("company_id"));
			    		data.setIdCategory(dataRequest.getJSONArray("detailData").getJSONObject(a).getInt("category"));
			    		DataCategory.add(data);
//			    		ListDocumentSummaryService.save(data);
			    		
			    		newData=hasChild(dataChild,dataRequest.getJSONArray("detailData").getJSONObject(a).getInt("category"),dataRequest.getInt("company_id"));
			    		
//			    		logger.info("hasil = " + hasil);
			    	}
			    }
		    	
		    	GabunganService.updateSummary(listUpdate.get(), DataCategory,newData );
		    	 response.put("responseCode", "00");
			      response.put("responseDesc", "Update List Document Summary Success");
		    }
		    catch(Exception e)
		    {
		    	  response.put("responseCode", "99");
			      response.put("responseDesc", "Update List Document Summary Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		    
		    
			return response.toString();
		}
	 
	 @PostMapping("/detailListDocumentSummary")
		public String detailListDocumentSummary(@RequestBody String request) {
			JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    List<JSONObject> final11 = new ArrayList<>();
		    
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    logger.info("input >>>> " + dataRequest);
		    try {
		    	
		    	List<Object[]> listDetailCategory = ListDocumentService.listDetailCategory(dataRequest.getInt("company_id"), dataRequest.getInt("listDocumentId"));
		    	
		    	listDetailCategory.stream().forEach(col1->{
		    		ArrayList<Child> pairs = new ArrayList<Child>();
		    		String level = "";
		    		JSONObject data2 = new JSONObject();
		    		data2.put("category",col1[0]);
		    		List<Object[]> listDetailHierarchy = ListDocumentSummarySubcategoryService.listDetailHierarchy(dataRequest.getInt("company_id"), dataRequest.getInt("listDocumentId"), Integer.parseInt(String.valueOf(col1[0])));
//		    		(Integer childId, Integer parentId, Integer subcategory, String value, Integer subsubcategory,int nourut
//			    	Integer childId adalah id sesungguhnya, Integer parentId, String subcategory, String value, String subsubcategory , nourut
//		    		Integer idDatatype, String namaDatatype, String namaSubcategory, String namaSubsubcategory
			    	listDetailHierarchy.stream().forEach(col->{
			    		
			    		 pairs.add(new Child( 
			    				 Integer.parseInt(col[2].toString()),
			    				 col[0] != null ? Integer.parseInt(String.valueOf(col[0])) : 0,
			    					Integer.parseInt(col[11].toString()), 
				    	    		col[7].toString() , 	
				    	    		Integer.parseInt(col[12].toString()) ,
				    	    		Integer.parseInt(col[10].toString()),
				    	    		Integer.parseInt(col[13].toString()),
				    	    		col[14].toString(),
				    	    		col[6].toString(),
				    	    		col[8].toString()
				    	    		));
			    	});

			    	Map<Integer, Parent> hm = new HashMap<>();


			        // you are using MegaMenuDTO as Linked list with next and before link 

			        // populate a Map
			        for(Child p:pairs){

			        	
			            //  ----- Child -----
			        	Parent mmdChild ;
			            if(hm.containsKey(p.getChildId())){
			                mmdChild = hm.get(p.getChildId());
			            }
			            else{
			                mmdChild = new Parent();
			                hm.put(p.getChildId(), mmdChild);
			            }  
			            mmdChild.setSubcategory(p.getSubcategory());
			            mmdChild.setValue(p.getValue());
			            mmdChild.setSubsubcategory(p.getSubsubcategory());
			            mmdChild.setId(p.getChildId());
			            mmdChild.setParentId(p.getParentId());
			            mmdChild.setNourut(p.getNourut());
			            
			            mmdChild.setIdDatatype(p.getIdDatatype());
			            mmdChild.setNamaDatatype(p.getNamaDatatype());
			            mmdChild.setNamaSubcategory(p.getNamaSubcategory());
			            mmdChild.setNamaSubsubcategory(p.getNamaSubsubcategory());
			            // no need to set ChildrenItems list because the constructor created a new empty list



			            // ------ Parent ----
			            Parent mmdParent ;
			            if(hm.containsKey(p.getParentId())){
			                mmdParent = hm.get(p.getParentId());
			            }
			            else{
			                mmdParent = new Parent();
			                hm.put(p.getParentId(),mmdParent);
			            }
			            mmdParent.setId(p.getParentId());
			            mmdParent.setParentId(null);
			            mmdParent.addChildrenItem(mmdChild);
			            mmdParent.setSubcategory(p.getSubcategory());
			            mmdParent.setValue(p.getValue());
			            mmdParent.setSubsubcategory(p.getSubsubcategory());
			            mmdParent.setNourut(p.getNourut());

			            mmdParent.setIdDatatype(p.getIdDatatype());
			            mmdParent.setNamaDatatype(p.getNamaDatatype());
			            mmdParent.setNamaSubcategory(p.getNamaSubcategory());
			            mmdParent.setNamaSubsubcategory(p.getNamaSubsubcategory());
			        }
			        
			     // Get the root
			        List<Parent> DX = new ArrayList<Parent>(); 
			        for(Parent mmd : hm.values()){
			            if(mmd.getParentId() == null)
			                DX.add(mmd);
			        }

			        // Print 
			        for(Parent mmd: DX){
			            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
			            JSONObject data = new JSONObject();
			            
			            data.put("data",mmd);
			            cont.add(data);
			        }
			        for(int a= 0 ; a< cont.size() ; a++)
			        {
			        	level = cont.get(a).getString("data");
			        	logger.info("cont = " + cont.get(a).getString("data"));
			        }
			        JSONObject dataRequestLevel = new JSONObject();
			        if(level.isEmpty() == false) {
			        	dataRequestLevel = (JSONObject)JSONSerializer.toJSON(level);
			        	data2.put("detail",dataRequestLevel.get("childrenItems"));
			        }
			        else
			        {
			        	data2.put("detail","");
			        }
		    		final11.add(data2);
		    	});
		    	response.put("responseCode", "00");
			      response.put("responseDesc", "Show Detail Document Summary Success");
			      response.put("responseData", final11);
		    }
		    catch(Exception e)
		    {
		    	 response.put("responseCode", "99");
			      response.put("responseDesc", "Show Detail Document Summary Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
		}

	 @PostMapping("/listSubcategory/{paramAktif}")
		public String listSubcategory(@PathVariable("paramAktif") String paramAktif,@RequestBody String request)
		{
			JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    List<JSONObject> final11 = new ArrayList<>();
		    
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    logger.info("input >>>> " + dataRequest);
		    try
		    {
		    	 List<Object[]> listSubcategory = ListDocumentSummaryService.listSubcategory(paramAktif, dataRequest.getInt("company_id"));
		    	 listSubcategory.stream().forEach(col->{
		    		 JSONObject data = new JSONObject();
		    		 data.put("idSubcategory", col[0]);
		    		 data.put("namaSubcategory", col[1]);
		    		 data.put("statusSubcategory", col[2]);
		    		 data.put("idCompany", col[3]);
		    		 data.put("idDatatype", col[4]);
		    		 data.put("namaDatatype", col[5]);
		    		 cont.add(data);
		    	 });
		    	 response.put("responseCode", "00");
			      response.put("responseDesc", "Show Combobox Subcategory Success");
			      response.put("responseData", cont);
		    }
		    catch(Exception e)
		    {
		    	 response.put("responseCode", "99");
			      response.put("responseDesc", "Show Combobox Subcategory Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
		}
	 
	 @PostMapping("/listSubSubcategory")
		public String listSubSubcategory(@RequestBody String request)
		{
			JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    List<JSONObject> final11 = new ArrayList<>();
		    
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    logger.info("input >>>> " + dataRequest);
		    try
		    {
		    	 List<Object[]> listSubcategory = ListDocumentSummaryService.listSubSubcategory(dataRequest.getInt("company_id"), dataRequest.getInt("subcategoryId"));
		    	 listSubcategory.stream().forEach(col->{
		    		 JSONObject data = new JSONObject();
		    		 data.put("idSubSubcategory", col[0]);
		    		 data.put("namaSubSubcategory", col[1]);
		    		 
		    		 data.put("idSubcategory", col[2]);
		    		 data.put("namaSubcategory", col[3]);
		    		 cont.add(data);
		    	 });
		    	 response.put("responseCode", "00");
			      response.put("responseDesc", "Show Combobox Subsubcategory Success");
			      response.put("responseData", cont);
		    }
		    catch(Exception e)
		    {
		    	 response.put("responseCode", "99");
			      response.put("responseDesc", "Show Combobox Subsubcategory Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
		}
	 
	 @PostMapping("/listCategory/{paramAktif}")
		public String listCategory(@PathVariable("paramAktif") String paramAktif, @RequestBody String request)
		{
			JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    List<JSONObject> final11 = new ArrayList<>();
		    
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    logger.info("input >>>> " + dataRequest);
		    try
		    {
		    	 List<Object[]> listSubcategory = ListDocumentSummaryService.listCategory(paramAktif, dataRequest.getInt("company_id"));
		    	 listSubcategory.stream().forEach(col->{
		    		 JSONObject data = new JSONObject();
		    		 data.put("idCategory", col[0]);
		    		 data.put("namaCategory", col[1]);
		    		 data.put("idCompany", col[2]);
		    		 data.put("status", col[3]);
		    		 cont.add(data);
		    	 });
		    	 response.put("responseCode", "00");
			      response.put("responseDesc", "Show Combobox Category Success");
			      response.put("responseData", cont);
		    }
		    catch(Exception e)
		    {
		    	 response.put("responseCode", "99");
			      response.put("responseDesc", "Show Combobox Category Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
		}
	 
	 @PostMapping("/listDocumentType/{paramAktif}")
		public String listDocumentType(@PathVariable("paramAktif") String paramAktif, @RequestBody String request)
		{
			JSONObject response = new JSONObject();
		    List<JSONObject> cont = new ArrayList<>();
		    List<JSONObject> final11 = new ArrayList<>();
		    
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		    logger.info("input >>>> " + dataRequest);
		    try
		    {
		    	 List<Object[]> listSubcategory = ListDocumentSummaryService.listDocumentType(paramAktif, dataRequest.getInt("company_id"));
		    	 listSubcategory.stream().forEach(col->{
		    		 JSONObject data = new JSONObject();
		    		 data.put("idDocumentType", col[0]);
		    		 data.put("namaDocumentType", col[1]);
		    		 data.put("idCompany", col[2]);
		    		 data.put("status", col[3]);
		    		 cont.add(data);
		    	 });
		    	 response.put("responseCode", "00");
			      response.put("responseDesc", "Show Combobox Category Success");
			      response.put("responseData", cont);
		    }
		    catch(Exception e)
		    {
		    	 response.put("responseCode", "99");
			      response.put("responseDesc", "Show Combobox Category Failed");
			      response.put("responseError", e.getMessage());
			      logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
		}
	 
	 public String getUserId(String header )
		{
			final String uri = "http://54.169.109.123:3003/apps/me";
	        
	        RestTemplate restTemplate = new RestTemplate();
	         
	        HttpHeaders headers = new HttpHeaders();
	        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	        headers.set("x-access-code", header);
	        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
	        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
	        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
	        
	        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
		      
		    return user_uuid;
		}
}
