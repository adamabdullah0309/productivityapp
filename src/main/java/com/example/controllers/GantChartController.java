package com.example.controllers;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;import static java.util.Objects.isNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.client.RestTemplate;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import com.example.controllers.Helpers;
import com.example.entity.*;
import com.example.service.GantChartService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static java.util.stream.Collectors.toCollection;

import java.math.BigDecimal;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.collectingAndThen;
import com.example.helper.Helper;


@RestController
@RequestMapping("/api")
public class GantChartController {
	private static final Logger logger = LoggerFactory.getLogger(GantChartController.class);
	
	@Autowired
	private GantChartService GantChartService;
	
	
	Helper Helper;
	
	int a = 0;
	String date = "";
	String dateDateline = "";
	@PostMapping("/ListGantChart")
	public String ListGantChart(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> cont1 = new ArrayList<>();
	    JSONObject dataRequestLevel = new JSONObject();
	    String level = "";
	    List<JSONObject> final11 = new ArrayList<>();
	    List<JSONObject> dataHeader = new ArrayList<>();
	    List<JSONObject> dataDetail = new ArrayList<>();
	    List<TmBoardProjectDetail> dataFinal = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    logger.info("call ListGantChart()");
	    try { 
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> dataAll = GantChartService.listGantChart(dataRequest.getInt("company_id"), user_uuid, dataRequest.getJSONArray("projectId"), 
	        		dataRequest.getJSONArray("projectPartyId"));
	        dataAll.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("projectName", col[0].toString());
	        	data.put("projectParty", col[1] == null ? "" : Integer.valueOf(col[1].toString()));
	        	data.put("mProjectId", col[2] == null ? "": Integer.valueOf(col[2].toString()));
	        	data.put("projectDetailId", col[3] == null ? "" : Integer.valueOf(col[3].toString()));
	        	data.put("parentId", col[4] == null ? "" : Integer.valueOf(col[4].toString()));
	        	data.put("mTaskId", col[5] == null ? 0 : Integer.valueOf(col[5].toString()));
	        	data.put("mGroupingId", col[6] == null ?0 : Integer.valueOf(col[6].toString()));
	        	data.put("company_id", col[7] == null ? "" : Integer.valueOf(col[7].toString()));
	        	data.put("task", col[8] == null ? "" : col[8].toString());
//	        	data.put("number", col[9] == null ? "" : Integer.valueOf(col[9].toString()));
	        	data.put("timeframeId", col[10] == null ? 0 : Integer.valueOf(col[10].toString()));
	        	data.put("startdate", col[11] == null ? "" : col[11].toString());
	        	data.put("dateline", col[12] == null ? "" : col[12].toString());
	        	data.put("assignTo", col[13] == null ? "" : col[13].toString());
	        	data.put("dateBudgetHour", col[14] == null ? "" : col[14].toString());
	        	data.put("BudgetHour", col[15] == null ? "" : Float.parseFloat(col[15].toString()));
	        	data.put("headerBudgetHour", col[16] == null ? "" : Float.parseFloat(col[16].toString()));
	        	data.put("projectStatus",  col[17] == null ? 0 : Integer.valueOf(col[17].toString()));
	        	dataHeader.add(data);
	        	dataDetail.add(data);
	        });
//	        
	        HashSet<Object> seen= new HashSet<>();
	        dataHeader.removeIf(e -> !seen.add(Arrays.asList(e.getInt("mProjectId"), e.getInt("projectDetailId"), e.getInt("company_id"), e.getInt("timeframeId"))));
	        
	        dataHeader.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("projectName", col.getString("projectName") == null ? "" : col.getString("projectName"));
	        	data.put("projectParty",  col.getInt("projectParty") == 0 ? 0 : col.getInt("projectParty"));
	        	data.put("mProjectId", col.getInt("mProjectId") == 0 ? 0 : col.getInt("mProjectId"));
	        	data.put("projectDetailId", col.getInt("projectDetailId") == 0 ? 0 : col.getInt("projectDetailId"));
	        	data.put("parentId", col.getInt("parentId") == 0 ? 0 : col.getInt("parentId"));
	        	data.put("mTaskId", col.getInt("mTaskId") == 0 ? 0 : col.getInt("mTaskId"));
	        	data.put("mGroupingId", col.getInt("mGroupingId") == 0 ? 0 : col.getInt("mGroupingId"));
	        	data.put("company_id", col.getInt("company_id") == 0 ? 0 : col.getInt("company_id"));
	        	data.put("task", col.getString("task") == "" ? "" : col.getString("task"));
//	        	data.put("number", col.getInt("number") == 0 ? 0 : col.getInt("number"));
	        	data.put("timeframeId", col.getInt("timeframeId") == 0 ? 0 : col.getInt("timeframeId"));
	        	data.put("startdate", col.getString("startdate") == "" ? "" : col.getString("startdate"));
	        	data.put("dateline", col.getString("dateline") == "" ? "" : col.getString("dateline"));
	        	data.put("assignTo", col.getString("assignTo") == "" ? "" : col.getString("assignTo"));
	        	data.put("BudgetHour",col.getString("BudgetHour") == "" ? 0 : Float.parseFloat(col.getString("BudgetHour")));
	        	data.put("headerBudgetHour", col.getString("headerBudgetHour") == "" ? 0 : Float.parseFloat(col.getString("headerBudgetHour")));
	        	data.put("projectStatus", col.getInt("projectStatus"));
	        	List<JSONObject> detail = new ArrayList<>();
	        	dataDetail.stream().forEach(col2->{
	        		if(col.getInt("mProjectId") == col2.getInt("mProjectId") 
	        				&& col.getInt("projectDetailId") == col2.getInt("projectDetailId")
	        				&& col.getInt("company_id") == col2.getInt("company_id")
	        				&& col.getInt("timeframeId") == col2.getInt("timeframeId")
	        				&& (!col.getString("BudgetHour").isEmpty() || !col.getString("dateBudgetHour").isEmpty())
	        				
	        				)
	        			
	        			
	        		{
	        			JSONObject data2 = new JSONObject();
	        			data2.put("dateBudgetHour", col2.getString("dateBudgetHour"));
	        			data2.put("BudgetHour", col2.getString("BudgetHour") == "" ? 0 : Float.parseFloat(col2.getString("BudgetHour")));
	        			detail.add(data2);
	        		}
	        	});
	        	
	        	data.put("detail", detail);
	        	cont.add(data);
	        });
	        ArrayList<ChildGantChart> pairs = new ArrayList<ChildGantChart>();
	        ArrayList<ChildGantChart> pairsKhususproject = new ArrayList<ChildGantChart>();
	        ArrayList<ChildGantChart> pairsDetail = new ArrayList<ChildGantChart>();
	        cont.stream().forEach(col->{
	        	ChildGantChart dataChild = new ChildGantChart();
	        	dataChild.setProjectStatus(col.getInt("projectStatus"));
	        	dataChild.setChildId(col.getInt("projectDetailId") == 0 ? 0 : col.getInt("projectDetailId"));
	        	dataChild.setProjectName(col.getString("projectName") == null ? "" : col.getString("projectName"));
	        	dataChild.setProjectParty(col.getInt("projectParty") == 0 ? 0 : col.getInt("projectParty"));
	        	dataChild.setmProjectId(col.getInt("mProjectId") == 0 ? 0 : col.getInt("mProjectId"));
	        	dataChild.setProjectDetailId(col.getInt("projectDetailId") == 0 ? 0 : col.getInt("projectDetailId"));
	        	dataChild.setTimeframeId( col.getInt("timeframeId") == 0 ? 0 : col.getInt("timeframeId"));
	        	dataChild.setParentId(col.getInt("parentId") == 0 ? 0 : col.getInt("parentId"));
	        	dataChild.setmTaskId(col.getInt("mTaskId") == 0 ? 0 : col.getInt("mTaskId"));
	        	dataChild.setmGroupingId(col.getInt("mGroupingId") == 0 ? 0 : col.getInt("mGroupingId"));
	        	dataChild.setCompany_id(col.getInt("company_id") == 0 ? 0 : col.getInt("company_id"));
	        	dataChild.setTask(col.getString("task") == "" ? "" : col.getString("task"));
	        	dataChild.setTimeframeId(col.getInt("timeframeId") == 0 ? 0 : col.getInt("timeframeId"));
	        	dataChild.setStartdate(col.getString("startdate") == "" ? "" : col.getString("startdate"));
	        	dataChild.setDateline(col.getString("dateline") == "" ? "" : col.getString("dateline"));
	        	dataChild.setAssignTo(col.getString("assignTo") == "" ? "" : col.getString("assignTo"));
	        	// ini masih salah lur.............
	        	if(col.getString("headerBudgetHour") == "" || col.getString("BudgetHour") == "")
	        	{
	        		dataChild.setBudgetHour((float) 0);
	        	}
	        	else if(!col.getString("headerBudgetHour").isEmpty() || col.getString("BudgetHour") == "")
	        	{
	        		dataChild.setBudgetHour(Float.parseFloat(col.getString("headerBudgetHour")));
	        	}
	        	else if(col.getString("headerBudgetHour").isEmpty() || !col.getString("BudgetHour").isEmpty())
	        	{
	        		dataChild.setBudgetHour(Float.parseFloat(col.getString("BudgetHour")));
	        	}
//	        	dataChild.setBudgetHour( col.getString("headerBudgetHour") == "" ? 0 : Float.parseFloat(col.getString("headerBudgetHour")));
	        	dataChild.setDataTimeframeBudgetHour(col.getJSONArray("detail"));
	        	pairs.add(dataChild);
	        	pairsKhususproject.add(dataChild);
	        	pairsDetail.add(dataChild);
	        });
	        
	        HashSet<Object> seen2= new HashSet<>();
	        pairsKhususproject.removeIf(e -> !seen2.add(Arrays.asList(e.getCompany_id(), e.getmProjectId() )));
	        
	        List<JSONObject> akhir = new ArrayList<>();
	        
	        pairsKhususproject.stream().forEach(col->{
//	        	if(col.getmProjectId() == dataRequest.getInt("mProjectId") && col.getCompany_id() == dataRequest.getInt("company_id"))
//	        	{
	        		JSONObject data = new JSONObject();
		        	data.put("mProjectId", col.getmProjectId());
		        	data.put("projectName", col.getProjectName());
		        	data.put("company_id", col.getCompany_id());
		        	data.put("projectParty", col.getProjectParty());
//		        	data.put("projectPeriode", )
		        	date = "";
		        	dateDateline = "";
		        	data.put("projectStatus", col.getProjectStatus());
		        	ArrayList<ChildGantChart> pairsDetail2 = new ArrayList<ChildGantChart>();
		        	ArrayList<JSONObject> listKaryawan = new ArrayList<JSONObject>();
		        	for(ChildGantChart p:pairsDetail){
		        		
		        		if(col.getCompany_id().equals(p.getCompany_id()) && col.getmProjectId().equals(p.getmProjectId()))
		        		{
		        			JSONObject dataKaryawan = new JSONObject();
		        			if(p.getAssignTo() != "")
		        			{
		        				
			        			dataKaryawan.put("assignTo", p.getAssignTo());
			        			listKaryawan.add(dataKaryawan);
		        			}
		        			
		        			if(p.getmTaskId() == 0 && p.getmGroupingId() == 0 || p.getProjectDetailId() == 1 && date == "")
		        			{
		        				logger.info("kenaak"); 
		        				
		        				date = p.getStartdate();
		        				dateDateline = p.getDateline();
		        			}
		        			else
		        			{
		        				date = "";
		        				dateDateline = "";
		        			}
		        			ChildGantChart dataChild = new ChildGantChart();
		        			dataChild.setChildId(p.getChildId());
		        			dataChild.setProjectName(p.getProjectName());
		        			dataChild.setProjectParty(p.getProjectParty());
		        			dataChild.setmProjectId(p.getmProjectId());
		        			dataChild.setProjectDetailId(p.getProjectDetailId());
		        			dataChild.setParentId(p.getParentId());
		        			dataChild.setmTaskId(p.getmTaskId());
		        			dataChild.setmGroupingId(p.getmGroupingId());
		        			dataChild.setCompany_id(p.getCompany_id());
		        			dataChild.setTask(p.getTask());
		        			dataChild.setTimeframeId(p.getTimeframeId());
		        			dataChild.setStartdate(p.getStartdate());
		        			dataChild.setDateline(p.getDateline());
		        			dataChild.setAssignTo(p.getAssignTo());
		        			dataChild.setDataTimeframeBudgetHour(p.getDataTimeframeBudgetHour());
		        			dataChild.setBudgetHour(p.getBudgetHour());
		        			pairsDetail2.add(dataChild);
		        		}
		        	}
		        	data.put("projectStart", date != null ? date : "");
		        	data.put("projectEnd", dateDateline != null ? dateDateline : "");
		        	data.put("detail", pairsDetail2);
		        	akhir.add(data);
//	        	}
	        	
	        });
	        if(akhir.size()>0)
	        {
	        	for(JSONObject z:akhir)
	        	{
	        		logger.info("z = " + z.getJSONArray("detail"));
	        		List<JSONObject> hasil = new ArrayList<>();
	        		if(z.has("detail"))
	        		{
	        			if(z.getJSONArray("detail").size()>0)
		        		{
		        			hasil = makeChild(z.getJSONArray("detail"));
		        		}
	        		}
	        		z.remove("detail");
//	        		z.replace("detail", hasil);
	        	}
	        }
	        
	        
//	        if(dataRequest.getInt("mProjectId") != 0)
//	        {
//	        	if(akhir.size()>0)
//		        {
//		        	for(int b = 0 ; b < akhir.size() ; b++)
//		        	{
//		        		if(akhir.get(b).getInt("mProjectId") != dataRequest.getInt("mProjectId"))
//		        		{
//		        			akhir.remove(b);
//		        		}
//		        	}
//		        }
//	        }
//	        
	        
	        
//	        logger.info("dataRequest.getJSONArray(\"projectDetailId\") = " + dataRequest.getJSONArray("projectDetailId"));
//	        if(dataRequest.getJSONArray("projectDetailId").size() > 0)
//	        {
//	        	if(akhir.size()>0)
//		        {
//		        	for(int b = 0 ; b < akhir.size() ; b++)
//		        	{
//		        		if(akhir.get(b).getJSONArray("detail").size() > 0)
//		        		{
//		        			if(akhir.get(b).getJSONArray("detail").getJSONObject(0).getJSONArray("children").size() > 0)
//		        			{
//		        				for(int z = 0 ; z < akhir.get(b).getJSONArray("detail").getJSONObject(0).getJSONArray("children").size() ; z ++)
//		        				{
//		        					for(int y = 0; y < dataRequest.getJSONArray("projectDetailId").size() ; y++)
//		        					{
//		        						if(akhir.get(b).getJSONArray("detail").getJSONObject(0).getJSONArray("children").getJSONObject(z).getInt("projectDetailId") != Integer.valueOf(dataRequest.getJSONArray("projectDetailId").get(y).toString()))
//			        					{
//			        						akhir.get(b).getJSONArray("detail").getJSONObject(0).getJSONArray("children").remove(z);
//			        					}
//		        					}
//		        					
//		        				}
//		        			}
//		        		}
//		        		
//		        	}
//		        }
//	        }
//	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "List Gant Chart Success");
		    response.put("responseData", akhir);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "List Gant Chart Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/ListGantChartDetail")
	public String ListGantChartDetail(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> cont1 = new ArrayList<>();
	    JSONObject dataRequestLevel = new JSONObject();
	    String level = "";
	    List<JSONObject> final11 = new ArrayList<>();
	    List<JSONObject> dataHeader = new ArrayList<>();
	    List<JSONObject> dataDetail = new ArrayList<>();
	    List<JSONObject> dataDependency = new ArrayList<>();
	    List<TmBoardProjectDetail> dataFinal = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    logger.info("call ListGantChart()");
	    try { 
	    	SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
	    	UUID user_uuid = UUID.fromString(getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	        String formatDateTime = localDateTime.format(formatter);
	        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	        
	        List<Object[]> dataDependencyMentah = GantChartService.dataDependency(dataRequest.getInt("mProjectId"), dataRequest.getInt("company_id"));
	        dataDependencyMentah.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("projectDetailId", col[0]);
	        	data.put("mProjectId", col[1]);
	        	data.put("projectDetailDependencyId", col[2]);
	        	data.put("noDependency", col[3]);
	        	data.put("company_id", col[4]);
	        	dataDependency.add(data);
	        });
	        List<Object[]> dataAll = GantChartService.detailGantChart(
	        		dataRequest.getInt("company_id"), 
	        		dataRequest.getInt("mProjectId"),
	        		dataRequest.getJSONArray("projectDetailId"), 
	        		dataRequest.getJSONArray("taskId"), 
	        		dataRequest.getJSONArray("contactIdArray"),
	        		user_uuid
	        		);
	        
	        dataAll.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("mProjectId", col[0] == null ? "": Integer.valueOf(col[0].toString()));
	        	data.put("projectDetailId", col[1] == null ? "" : Integer.valueOf(col[1].toString()));
	        	data.put("parentId", col[2] == null ? "" : Integer.valueOf(col[2].toString()));
	        	data.put("company_id", col[3] == null ? "" : Integer.valueOf(col[3].toString()));
	        	data.put("mTaskId", col[4] == null ? 0 : Integer.valueOf(col[4].toString()));
	        	data.put("mGroupingId", col[5] == null ?0 : Integer.valueOf(col[5].toString()));
	        	data.put("dateBudgetHour", col[9] == null ? "" : col[9].toString());
	        	data.put("BudgetHour", col[10] == null ? "" : Float.parseFloat(col[10].toString()));
	        	data.put("headerBudgetHour", col[11] == null ? 0 : Float.parseFloat(col[11].toString()));
	        	data.put("assignTo", col[12] == null ? "" : col[12].toString());
	        	data.put("timeframeId", col[13] == null ? 0 : Integer.valueOf(col[13].toString()));
	        	data.put("startdate", col[14] == null ? "" : col[14].toString());
	        	data.put("dateline", col[15] == null ? "" : col[15].toString());
	        	data.put("task", col[16] == null ? "" : col[16].toString());
	        	dataHeader.add(data);
	        	dataDetail.add(data);
	        });
	        
	        HashSet<Object> seen= new HashSet<>();
	        dataHeader.removeIf(e -> !seen.add(Arrays.asList(e.getInt("mProjectId"), e.getInt("projectDetailId"), e.getInt("company_id"), e.getInt("timeframeId"))));
	        
	        dataHeader.stream().forEach(col->{
	        	JSONObject data = new JSONObject();
	        	data.put("mProjectId", col.getInt("mProjectId") == 0 ? 0 : col.getInt("mProjectId"));
	        	data.put("projectDetailId", col.getInt("projectDetailId") == 0 ? 0 : col.getInt("projectDetailId"));
	        	data.put("parentId", col.getInt("parentId") == 0 ? 0 : col.getInt("parentId"));
	        	data.put("mTaskId", col.getInt("mTaskId") == 0 ? 0 : col.getInt("mTaskId"));
	        	data.put("mGroupingId", col.getInt("mGroupingId") == 0 ? 0 : col.getInt("mGroupingId"));
	        	data.put("company_id", col.getInt("company_id") == 0 ? 0 : col.getInt("company_id"));
	        	data.put("task", col.getString("task") == "" ? "" : col.getString("task"));
	        	data.put("timeframeId", col.getInt("timeframeId") == 0 ? 0 : col.getInt("timeframeId"));
	        	data.put("startdate", col.getString("startdate") == "" ? "" : col.getString("startdate"));
	        	data.put("dateline", col.getString("dateline") == "" ? "" : col.getString("dateline"));
	        	data.put("assignTo", col.getString("assignTo") == "" ? "" : col.getString("assignTo"));
	        	data.put("BudgetHour",col.getString("BudgetHour") == "" ? 0 : Float.parseFloat(col.getString("BudgetHour")));
	        	data.put("headerBudgetHour", col.getString("headerBudgetHour") == "" ? 0 : Float.parseFloat(col.getString("headerBudgetHour")));
	        	List<JSONObject> detail = new ArrayList<>();
	        	List<JSONObject> detailDependency = new ArrayList<>();
	        	dataDependency.stream().forEach(col2->{
	        		if(col.getInt("mProjectId") == col2.getInt("mProjectId")
	        				&& col.getInt("projectDetailId") == col2.getInt("projectDetailId")
	        				&& col.getInt("company_id") == col2.getInt("company_id"))
	        		{
	        			detailDependency.add(col2);
	        		}
	        	});
	        	dataDetail.stream().forEach(col2->{
	        		if(col.getInt("mProjectId") == col2.getInt("mProjectId") 
	        				&& col.getInt("projectDetailId") == col2.getInt("projectDetailId")
	        				&& col.getInt("company_id") == col2.getInt("company_id")
	        				&& col.getInt("timeframeId") == col2.getInt("timeframeId")
	        				&& (!col.getString("BudgetHour").isEmpty() || !col.getString("dateBudgetHour").isEmpty())
	        				
	        				)
	        			
	        			
	        		{
	        			JSONObject data2 = new JSONObject();
	        			data2.put("dateBudgetHour", col2.getString("dateBudgetHour"));
	        			data2.put("BudgetHour", col2.getString("BudgetHour") == "" ? 0 : Float.parseFloat(col2.getString("BudgetHour")));
	        			detail.add(data2);
	        		}
	        	});
	        	
	        	data.put("dataTimeframeBudgetHour", detail);
	        	data.put("dataDependency", detailDependency);
	        	cont.add(data);
	        });
	        
	        List<JSONObject> hasil = new ArrayList<>();
	        List<JSONObject> order = new ArrayList<>();
	        if(cont.size() >0)
	        {
	        	hasil = makeChildDetail(cont);
	        	//digunakan untuk order by list<JSONObject> biar urut dari id 1 ke id yang lebih besar
	        	order = Helper.orderBy(hasil); 
	        }
	        
	        
	        
	        response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Gant Chart Success");
		    response.put("responseData", order);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Gant Chart Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	private List<JSONObject> makeChildDetail(List<JSONObject> cont) {
		// TODO Auto-generated method stub
		String level = "";
		List<JSONObject> cont1 = new ArrayList<>();
    	ArrayList<ChildGantChart> pairs = new ArrayList<ChildGantChart>();
    	for (int i = 0; i < cont.size(); i++) {
    		JSONObject resultData = (JSONObject) cont.get(i);
    		ChildGantChart dataChild = new ChildGantChart();
	    	dataChild.setChildId(resultData.getInt("projectDetailId") == 0 ? 0 : resultData.getInt("projectDetailId"));
	    	dataChild.setmProjectId(resultData.getInt("mProjectId") == 0 ? 0 : resultData.getInt("mProjectId"));
	    	dataChild.setProjectDetailId(resultData.getInt("projectDetailId") == 0 ? 0 : resultData.getInt("projectDetailId"));
	    	dataChild.setParentId(resultData.getInt("parentId") == 0 ? 0 : resultData.getInt("parentId"));
	    	dataChild.setmTaskId(resultData.getInt("mTaskId") == 0 ? 0 : resultData.getInt("mTaskId"));
	    	dataChild.setmGroupingId(resultData.getInt("mGroupingId") == 0 ? 0 : resultData.getInt("mGroupingId"));
	    	dataChild.setCompany_id(resultData.getInt("company_id") == 0 ? 0 : resultData.getInt("company_id"));
	    	dataChild.setTask(resultData.getString("task") == "" ? "" : resultData.getString("task"));
	    	dataChild.setTimeframeId(resultData.getInt("timeframeId") == 0 ? 0 : resultData.getInt("timeframeId"));
	    	dataChild.setStartdate(resultData.getString("startdate") == "" ? "" : resultData.getString("startdate"));
	    	dataChild.setDateline(resultData.getString("dateline") == "" ? "" : resultData.getString("dateline"));
	    	dataChild.setAssignTo(resultData.getString("assignTo") == "" ? "" : resultData.getString("assignTo"));
	    	dataChild.setBudgetHour(resultData.getString("headerBudgetHour") == "" ? 0 : Float.parseFloat(resultData.getString("headerBudgetHour")));
	    	dataChild.setDataTimeframeBudgetHour(resultData.getJSONArray("dataTimeframeBudgetHour"));
	    	dataChild.setDataDependency(resultData.getJSONArray("dataDependency"));
	    	pairs.add(dataChild);
    	}
    	
    	Map<Integer, ParentGantChart> hm = new HashMap<>();
        for(ChildGantChart p:pairs){

        	
            //  ----- Child -----
        	ParentGantChart mmdChild ;
            if(hm.containsKey(p.getChildId())){
                mmdChild = hm.get(p.getChildId());
            }
            else{
                mmdChild = new ParentGantChart();
                hm.put(p.getChildId(), mmdChild);
            }  
            mmdChild.setId(p.getChildId());
            mmdChild.setParentId(p.getParentId());
            mmdChild.setProjectName(p.getProjectName());
            mmdChild.setProjectParty(p.getProjectParty());
            mmdChild.setmProjectId(p.getmProjectId());
            mmdChild.setmTaskId(p.getmTaskId());
            mmdChild.setmGroupingId(p.getmGroupingId());
            mmdChild.setCompany_id(p.getCompany_id());
            mmdChild.setTask(p.getTask());
            mmdChild.setTimeframeId(p.getTimeframeId());
            mmdChild.setStartdate(p.getStartdate());
            mmdChild.setDateline(p.getDateline());
            mmdChild.setAssignTo(p.getAssignTo());
            mmdChild.setDataTimeframeBudgetHour(p.getDataTimeframeBudgetHour());
            mmdChild.setBudgetHour(p.getBudgetHour());
            mmdChild.setProjectDetailId(p.getProjectDetailId());
            mmdChild.setProjectParty(p.getProjectParty());
            mmdChild.setDataDependency(p.getDataDependency());
            // no need to set ChildrenItems list because the constructor created a new empty list



            // ------ Parent ----
            ParentGantChart mmdParent ;
            if(hm.containsKey(p.getParentId())){
                mmdParent = hm.get(p.getParentId());
            }
            else{
                mmdParent = new ParentGantChart();
                hm.put(p.getParentId(),mmdParent);
            }
            
            mmdParent.setId(p.getParentId());
            mmdParent.setParentId(null);
            mmdParent.addChildren(mmdChild);
            mmdParent.setProjectParty(p.getProjectParty());
            mmdParent.setBudgetHour(p.getBudgetHour());
            mmdParent.setProjectDetailId(p.getProjectDetailId());
            mmdParent.setProjectName(p.getProjectName());
            mmdParent.setmProjectId(p.getmProjectId());
            mmdParent.setmTaskId(p.getmTaskId());
            mmdParent.setmGroupingId(p.getmGroupingId());
            mmdParent.setCompany_id(p.getCompany_id());
            mmdParent.setTask(p.getTask());
            mmdParent.setTimeframeId(p.getTimeframeId());
            mmdParent.setStartdate(p.getStartdate());
            mmdParent.setDateline(p.getDateline());
            mmdParent.setAssignTo(p.getAssignTo());
            mmdParent.setDataTimeframeBudgetHour(p.getDataTimeframeBudgetHour());
            mmdParent.setDataDependency(p.getDataDependency());
        }
        
        List<ParentGantChart> DX = new ArrayList<ParentGantChart>(); 
        for(ParentGantChart mmd : hm.values()){
            if(mmd.getParentId() == null)
                DX.add(mmd);
        }

        // Print 
        for(ParentGantChart mmd: DX){
            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
            
            JSONObject data3 = new JSONObject();
            
            data3.put("data",mmd);
            cont1.add(data3);
        }
        for(int a= 0 ; a< cont1.size() ; a++)
        {
        	level = cont1.get(a).getString("data");
        	logger.info("cont = " + cont1.get(a).getString("data"));
        }
        JSONObject dataRequestLevel1 = (JSONObject)JSONSerializer.toJSON(level);
	 
        List<JSONObject> list = new ArrayList<JSONObject>();
        for (int i = 0; i < dataRequestLevel1.getJSONArray("children").size(); i++) {
                list.add(dataRequestLevel1.getJSONArray("children").getJSONObject(i));
        }
        
        Collections.sort(list, new SortBasedOnMessageId());

        List<JSONObject> resultArray2 = list;
        
        
        
		return resultArray2;
	}

	private List<JSONObject> makeChild(JSONArray jsonArray) {
		// TODO Auto-generated method stub
		String level = "";
		List<JSONObject> cont1 = new ArrayList<>();
    	ArrayList<ChildGantChart> pairs = new ArrayList<ChildGantChart>();
    	for (int i = 0; i < jsonArray.size(); i++) {
    		
    		JSONObject resultData = (JSONObject) jsonArray.get(i);
    		ChildGantChart dataChild = new ChildGantChart();
	    	dataChild.setChildId(resultData.getInt("projectDetailId") == 0 ? 0 : resultData.getInt("projectDetailId"));
	    	dataChild.setProjectName(resultData.getString("projectName") == null ? "" : resultData.getString("projectName"));
	    	dataChild.setProjectParty(resultData.getInt("projectParty") == 0 ? 0 : resultData.getInt("projectParty"));
	    	dataChild.setmProjectId(resultData.getInt("mProjectId") == 0 ? 0 : resultData.getInt("mProjectId"));
	    	dataChild.setProjectDetailId(resultData.getInt("projectDetailId") == 0 ? 0 : resultData.getInt("projectDetailId"));
	    	dataChild.setParentId(resultData.getInt("parentId") == 0 ? 0 : resultData.getInt("parentId"));
	    	dataChild.setmTaskId(resultData.getInt("mTaskId") == 0 ? 0 : resultData.getInt("mTaskId"));
	    	dataChild.setmGroupingId(resultData.getInt("mGroupingId") == 0 ? 0 : resultData.getInt("mGroupingId"));
	    	dataChild.setCompany_id(resultData.getInt("company_id") == 0 ? 0 : resultData.getInt("company_id"));
	    	dataChild.setTask(resultData.getString("task") == "" ? "" : resultData.getString("task"));
	    	dataChild.setTimeframeId(resultData.getInt("timeframeId") == 0 ? 0 : resultData.getInt("timeframeId"));
	    	dataChild.setStartdate(resultData.getString("startdate") == "" ? "" : resultData.getString("startdate"));
	    	dataChild.setDateline(resultData.getString("dateline") == "" ? "" : resultData.getString("dateline"));
	    	dataChild.setAssignTo(resultData.getString("assignTo") == "" ? "" : resultData.getString("assignTo"));
	    	dataChild.setBudgetHour(resultData.getString("budgetHour") == "" ? 0 : Float.parseFloat(resultData.getString("budgetHour")));
	    	dataChild.setDataTimeframeBudgetHour(resultData.getJSONArray("dataTimeframeBudgetHour"));
	    	pairs.add(dataChild);
    	}
    	
    	Map<Integer, ParentGantChart> hm = new HashMap<>();
        for(ChildGantChart p:pairs){

        	
            //  ----- Child -----
        	ParentGantChart mmdChild ;
            if(hm.containsKey(p.getChildId())){
                mmdChild = hm.get(p.getChildId());
            }
            else{
                mmdChild = new ParentGantChart();
                hm.put(p.getChildId(), mmdChild);
            }  
            mmdChild.setId(p.getChildId());
            mmdChild.setParentId(p.getParentId());
            mmdChild.setProjectName(p.getProjectName());
            mmdChild.setProjectParty(p.getProjectParty());
            mmdChild.setmProjectId(p.getmProjectId());
            mmdChild.setmTaskId(p.getmTaskId());
            mmdChild.setmGroupingId(p.getmGroupingId());
            mmdChild.setCompany_id(p.getCompany_id());
            mmdChild.setTask(p.getTask());
            mmdChild.setTimeframeId(p.getTimeframeId());
            mmdChild.setStartdate(p.getStartdate());
            mmdChild.setDateline(p.getDateline());
            mmdChild.setAssignTo(p.getAssignTo());
            mmdChild.setDataTimeframeBudgetHour(p.getDataTimeframeBudgetHour());
            mmdChild.setBudgetHour(p.getBudgetHour());
            mmdChild.setProjectDetailId(p.getProjectDetailId());
            mmdChild.setProjectParty(p.getProjectParty());
            // no need to set ChildrenItems list because the constructor created a new empty list



            // ------ Parent ----
            ParentGantChart mmdParent ;
            if(hm.containsKey(p.getParentId())){
                mmdParent = hm.get(p.getParentId());
            }
            else{
                mmdParent = new ParentGantChart();
                hm.put(p.getParentId(),mmdParent);
            }
            
            mmdParent.setId(p.getParentId());
            mmdParent.setParentId(null);
            mmdParent.addChildren(mmdChild);
            mmdParent.setProjectParty(p.getProjectParty());
            mmdParent.setBudgetHour(p.getBudgetHour());
            mmdParent.setProjectDetailId(p.getProjectDetailId());
            mmdParent.setProjectName(p.getProjectName());
            mmdParent.setmProjectId(p.getmProjectId());
            mmdParent.setmTaskId(p.getmTaskId());
            mmdParent.setmGroupingId(p.getmGroupingId());
            mmdParent.setCompany_id(p.getCompany_id());
            mmdParent.setTask(p.getTask());
            mmdParent.setTimeframeId(p.getTimeframeId());
            mmdParent.setStartdate(p.getStartdate());
            mmdParent.setDateline(p.getDateline());
            mmdParent.setAssignTo(p.getAssignTo());
            mmdParent.setDataTimeframeBudgetHour(p.getDataTimeframeBudgetHour());
        }
        
        List<ParentGantChart> DX = new ArrayList<ParentGantChart>(); 
        for(ParentGantChart mmd : hm.values()){
            if(mmd.getParentId() == null)
                DX.add(mmd);
        }

        // Print 
        for(ParentGantChart mmd: DX){
            System.out.println("DX contains "+DX.size()+" that are : "+ mmd);
            
            JSONObject data3 = new JSONObject();
            
            data3.put("data",mmd);
            cont1.add(data3);
        }
        for(int a= 0 ; a< cont1.size() ; a++)
        {
        	level = cont1.get(a).getString("data");
        	logger.info("cont = " + cont1.get(a).getString("data"));
        }
        JSONObject dataRequestLevel1 = (JSONObject)JSONSerializer.toJSON(level);
	 
        List<JSONObject> list = new ArrayList<JSONObject>();
        for (int i = 0; i < dataRequestLevel1.getJSONArray("children").size(); i++) {
                list.add(dataRequestLevel1.getJSONArray("children").getJSONObject(i));
        }
        
        Collections.sort(list, new SortBasedOnMessageId());
        
        

        List<JSONObject> resultArray2 = list;
		return resultArray2;
	}

	@PostMapping("/ListGantChartSubgroup")
	public String ListGantChartSubgroup(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> cont1 = new ArrayList<>();
	    JSONObject dataRequestLevel = new JSONObject();
	    String level = "";
	    List<JSONObject> final11 = new ArrayList<>();
	    List<JSONObject> dataHeader = new ArrayList<>();
	    List<JSONObject> dataDetail = new ArrayList<>();
	    List<TmBoardProjectDetail> dataFinal = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    logger.info("call ListGantChartSubgroup()");
	    try { 
	    	List<Object[]> dataCombobox = GantChartService.ListGantChartSubgroup(dataRequest.getInt("company_id"),dataRequest.getInt("projectId"));
	    	dataCombobox.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("projectDetailId", col[0]);
	    		data.put("nama", col[11] == null ? col[11].toString() : col[11].toString());
	    		cont.add(data);
	    	});
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "List Gant Chart Combobox Subgroup Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "List Gant Chart Combobox Subgroup Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/ListGantChartTask")
	public String ListGantChartTask(@RequestBody String request, @RequestHeader(value = "User-Access") String header) 
	{
		JSONObject response = new JSONObject();
	    List<JSONObject> cont = new ArrayList<>();
	    List<JSONObject> cont1 = new ArrayList<>();
	    JSONObject dataRequestLevel = new JSONObject();
	    String level = "";
	    List<JSONObject> final11 = new ArrayList<>();
	    List<JSONObject> dataHeader = new ArrayList<>();
	    List<JSONObject> dataDetail = new ArrayList<>();
	    List<TmBoardProjectDetail> dataFinal = new ArrayList<>();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    logger.info("input >>>> " + dataRequest);
	    logger.info("call ListGantChartTask()");
	    try { 
	    	List<Object[]> dataCombobox = GantChartService.ListGantChartTask(dataRequest.getInt("company_id"),dataRequest.getInt("projectId"),  dataRequest.getJSONArray("projectDetailId"));
	    	dataCombobox.stream().forEach(col->{
	    		JSONObject data = new JSONObject();
	    		data.put("taskId", col[3]);
	    		data.put("nama", col[6].toString());
	    		cont.add(data);
	    	});
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "List Gant Chart Combobox Task Success");
		    response.put("responseData", cont);
	    }
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "List Gant Chart Combobox Task Failed");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	
	
	//------------------------------------------------------------------------
	public String getUserId(String header)
	{
		final String uri = "http://54.169.109.123:3003/apps/me";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-access-code", header);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers); 
        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        JSONObject hasilVerivy = (JSONObject)JSONSerializer.toJSON(result.getBody());
        
        String user_uuid = hasilVerivy.getJSONObject("data").getString("user_uuid");
	      
	    return user_uuid;
	}

}
