package com.example.controllers;
import java.util.Date;
import java.time.LocalDateTime;
import java.util.List;

import net.sf.json.JSONObject;

import com.example.entity.TmProjectTimeframeBudgethour;

public class ChildrenProjectTimeframe {
	private Integer mProjectId;
	private Integer parentId;
    private Integer projectDetailId;
    private String nama;
    private String startdate;
    private String dateline;
    private Integer company_id;
    private Float budgetHour;
    private Integer timeframeId;
    private List<JSONObject> dataDetail;
    private String user_uuid;
    
    
    
	public String getUser_uuid() {
		return user_uuid;
	}
	public void setUser_uuid(String user_uuid) {
		this.user_uuid = user_uuid;
	}
	public Integer getTimeframeId() {
		return timeframeId;
	}
	public void setTimeframeId(Integer timeframeId) {
		this.timeframeId = timeframeId;
	}
	public Float getBudgetHour() {
		return budgetHour;
	}
	public void setBudgetHour(Float budgetHour) {
		this.budgetHour = budgetHour;
	}
	
	
	public List<JSONObject> getDataDetail() {
		return dataDetail;
	}
	public void setDataDetail(List<JSONObject> dataDetail) {
		this.dataDetail = dataDetail;
	}
	public Integer getmProjectId() {
		return mProjectId;
	}
	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public Integer getProjectDetailId() {
		return projectDetailId;
	}
	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	
	
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getDateline() {
		return dateline;
	}
	public void setDateline(String dateline) {
		this.dateline = dateline;
	}
	public Integer getCompany_id() {
		return company_id;
	}
	public void setCompany_id(Integer company_id) {
		this.company_id = company_id;
	}
    
    
}