package com.example.controllers;
public class ChildBordGrouping {
    private Integer projectId;
    private String projectName;
    private Integer projectDetailId;
    private Integer stageId;
    private String stageNama;
    private String task;
    private Integer boardId;
    private Integer totalStage;
    private Integer jmlhDisc;
    private Integer jmlhAtt;
    private Integer companyId;
    public ChildBordGrouping(String projectName,Integer projectId, Integer projectDetailId, Integer stageId, String task, String stageNama, Integer boardId, Integer totalStage,
    		Integer jmlhDisc, Integer jmlhAtt, Integer companyId
    		) {
    	this.projectName = projectName;
        this.projectId = projectId;
        this.projectDetailId = projectDetailId;
        this.stageId = stageId;
        this.task = task;
        this.stageNama = stageNama;
        this.boardId = boardId;
        this.totalStage = totalStage;
        this.jmlhDisc = jmlhDisc;
        this.jmlhAtt = jmlhAtt;
        this.companyId = companyId;
    }
    
    
    


	public Integer getJmlhDisc() {
		return jmlhDisc;
	}





	public void setJmlhDisc(Integer jmlhDisc) {
		this.jmlhDisc = jmlhDisc;
	}





	public Integer getJmlhAtt() {
		return jmlhAtt;
	}





	public void setJmlhAtt(Integer jmlhAtt) {
		this.jmlhAtt = jmlhAtt;
	}





	public Integer getCompanyId() {
		return companyId;
	}





	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}





	public Integer getTotalStage() {
		return totalStage;
	}




	public void setTotalStage(Integer totalStage) {
		this.totalStage = totalStage;
	}




	public Integer getBoardId() {
		return boardId;
	}




	public void setBoardId(Integer boardId) {
		this.boardId = boardId;
	}




	public String getStageNama() {
		return stageNama;
	}



	public void setStageNama(String stageNama) {
		this.stageNama = stageNama;
	}



	public String getTask() {
		return task;
	}



	public void setTask(String task) {
		this.task = task;
	}



	public String getProjectName() {
		return projectName;
	}



	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}



	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public Integer getProjectDetailId() {
		return projectDetailId;
	}
	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}
	public Integer getStageId() {
		return stageId;
	}
	public void setStageId(Integer stageId) {
		this.stageId = stageId;
	}
    
    
    
	
    
	
}