package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.StopwatchSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "stopwatch")
@IdClass(StopwatchSerializable.class)
public class Stopwatch implements Serializable{
	@Id
	@Column(name ="stopwatch_id")
	private Integer stopwatchId;
  
  @Column(name = "stopwatch_date")
  private Date stopwatchDate;
  
  @Column(name = "stopwatch_from")
  private Time stopwatchFrom;
  
  @Column(name = "stopwatch_to")
  private Time stopwatchTo;
  
  @Column(name = "stopwatch_party")
  private Integer stopwatchParty;
  
  @Column(name = "stopwatch_project_detail_id")
  private Integer stopwatchProjectDetailId;
  
  @Column(name = "stopwatch_project_id")
  private Integer stopwatchProjectId;
  
  @Column(name = "stopwatch_task_id")
  private Integer stopwatchTaskId;
  
  @Id
  @Column(name = "stopwatch_company_id")
  private Integer stopwatchCompanyId;
  
  @Column(name = "stopwatch_building")
  private String stopwatchBuilding;
  
  @Column(name = "stopwatch_address")
  private String stopwatchAddress;
  
  @Column(name = "stopwatch_status")
  private Integer stopwatchStatus;
  
  @Column(name = "stopwatch_hour")
  private Float stopwatchHour;
  
  
  @Column(name = "stopwatch_contact_id")
  private UUID stopwatchContactId;


public Integer getStopwatchId() {
	return stopwatchId;
}


public void setStopwatchId(Integer stopwatchId) {
	this.stopwatchId = stopwatchId;
}


public Date getStopwatchDate() {
	return stopwatchDate;
}


public void setStopwatchDate(Date stopwatchDate) {
	this.stopwatchDate = stopwatchDate;
}


public Time getStopwatchFrom() {
	return stopwatchFrom;
}


public void setStopwatchFrom(Time stopwatchFrom) {
	this.stopwatchFrom = stopwatchFrom;
}


public Time getStopwatchTo() {
	return stopwatchTo;
}


public void setStopwatchTo(Time stopwatchTo) {
	this.stopwatchTo = stopwatchTo;
}


public Integer getStopwatchParty() {
	return stopwatchParty;
}


public void setStopwatchParty(Integer stopwatchParty) {
	this.stopwatchParty = stopwatchParty;
}


public Integer getStopwatchProjectDetailId() {
	return stopwatchProjectDetailId;
}


public void setStopwatchProjectDetailId(Integer stopwatchProjectDetailId) {
	this.stopwatchProjectDetailId = stopwatchProjectDetailId;
}


public Integer getStopwatchProjectId() {
	return stopwatchProjectId;
}


public void setStopwatchProjectId(Integer stopwatchProjectId) {
	this.stopwatchProjectId = stopwatchProjectId;
}


public Integer getStopwatchTaskId() {
	return stopwatchTaskId;
}


public void setStopwatchTaskId(Integer stopwatchTaskId) {
	this.stopwatchTaskId = stopwatchTaskId;
}


public Integer getStopwatchCompanyId() {
	return stopwatchCompanyId;
}


public void setStopwatchCompanyId(Integer stopwatchCompanyId) {
	this.stopwatchCompanyId = stopwatchCompanyId;
}


public String getStopwatchBuilding() {
	return stopwatchBuilding;
}


public void setStopwatchBuilding(String stopwatchBuilding) {
	this.stopwatchBuilding = stopwatchBuilding;
}


public String getStopwatchAddress() {
	return stopwatchAddress;
}


public void setStopwatchAddress(String stopwatchAddress) {
	this.stopwatchAddress = stopwatchAddress;
}


public Integer getStopwatchStatus() {
	return stopwatchStatus;
}


public void setStopwatchStatus(Integer stopwatchStatus) {
	this.stopwatchStatus = stopwatchStatus;
}


public Float getStopwatchHour() {
	return stopwatchHour;
}


public void setStopwatchHour(Float stopwatchHour) {
	this.stopwatchHour = stopwatchHour;
}


public UUID getStopwatchContactId() {
	return stopwatchContactId;
}


public void setStopwatchContactId(UUID stopwatchContactId) {
	this.stopwatchContactId = stopwatchContactId;
}
  
  

  
}
