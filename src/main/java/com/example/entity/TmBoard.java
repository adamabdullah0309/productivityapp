package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TmBoardSerializable;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_board")
@IdClass(TmBoardSerializable.class)
public class TmBoard implements Serializable{
	
	@Id
	@Column(name ="board_id")
	private Integer boardId;
  
  @Column(name = "m_project_id")
  private Integer mProjectId;
  
  
    
  @Column(name = "created_by")
  private UUID createdBy;
  
  @Column(name = "created_at")
  private LocalDateTime createdAt;
  
  @Column(name = "updated_by")
  private UUID updatedBy;
  
  @Column(name = "updated_at")
  private LocalDateTime updatedAt;
  
  @Column(name = "m_stage_group_id")
  private int mStageGroupId;
  
  @Id
  @Column(name = "company_id")
  private int companyId;

public Integer getBoardId() {
	return boardId;
}

public void setBoardId(Integer boardId) {
	this.boardId = boardId;
}

public Integer getmProjectId() {
	return mProjectId;
}

public void setmProjectId(Integer mProjectId) {
	this.mProjectId = mProjectId;
}

public UUID getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(UUID createdBy) {
	this.createdBy = createdBy;
}

public LocalDateTime getCreatedAt() {
	return createdAt;
}

public void setCreatedAt(LocalDateTime createdAt) {
	this.createdAt = createdAt;
}

public UUID getUpdatedBy() {
	return updatedBy;
}

public void setUpdatedBy(UUID updatedBy) {
	this.updatedBy = updatedBy;
}

public LocalDateTime getUpdatedAt() {
	return updatedAt;
}

public void setUpdatedAt(LocalDateTime updatedAt) {
	this.updatedAt = updatedAt;
}

public int getmStageGroupId() {
	return mStageGroupId;
}

public void setmStageGroupId(int mStageGroupId) {
	this.mStageGroupId = mStageGroupId;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}

  
  
}
