package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TmBoardProjectDetailAttSerializable;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_board_project_detail_attachement")
@IdClass(TmBoardProjectDetailAttSerializable.class)
public class TmBoardProjectDetailAtt implements Serializable{
	
	@Id
	@Column(name ="tm_board_id")
	private Integer TmBoardId;
  
  
	@Id
	@Column(name = "tm_board_project_detail_id")
	private Integer TmBoardProjectDetailId;
	
	@Id
	@Column(name = "tm_board_project_detail_att_id")
	private Integer TmBoardProjectDetailAttId;
	
	@Column(name = "datedocument")
	private LocalDateTime datedocument;
    
	@Id
	@Column(name = "company_id")
	private int companyId;
	
	@Column(name = "doc_description")
	private String docDescription;
	
	@Column(name = "doc_type")
	private Integer docType;
	
	@Column(name = "file_path")
	private String filePath;
	
	@Column(name = "nama_file")
	private String namaFile;
	
	
	

	public String getNamaFile() {
		return namaFile;
	}

	public void setNamaFile(String namaFile) {
		this.namaFile = namaFile;
	}

	public LocalDateTime getDatedocument() {
		return datedocument;
	}

	public void setDatedocument(LocalDateTime datedocument) {
		this.datedocument = datedocument;
	}

	public String getDocDescription() {
		return docDescription;
	}

	public void setDocDescription(String docDescription) {
		this.docDescription = docDescription;
	}

	public Integer getDocType() {
		return docType;
	}

	public void setDocType(Integer docType) {
		this.docType = docType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Integer getTmBoardId() {
		return TmBoardId;
	}

	public void setTmBoardId(Integer tmBoardId) {
		TmBoardId = tmBoardId;
	}

	public Integer getTmBoardProjectDetailId() {
		return TmBoardProjectDetailId;
	}

	public void setTmBoardProjectDetailId(Integer tmBoardProjectDetailId) {
		TmBoardProjectDetailId = tmBoardProjectDetailId;
	}

	public Integer getTmBoardProjectDetailAttId() {
		return TmBoardProjectDetailAttId;
	}

	public void setTmBoardProjectDetailAttId(Integer tmBoardProjectDetailAttId) {
		TmBoardProjectDetailAttId = tmBoardProjectDetailAttId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	
	
}
