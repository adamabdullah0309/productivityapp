package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.StopwatchNoteSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "stopwatch_note")
@IdClass(StopwatchNoteSerializable.class)
public class StopwatchNote implements Serializable{
	@Id
	@Column(name ="stopwatch_id")
	private Integer stopwatchId;
	
	@Id
	@Column(name ="stopwatch_note_id")
	private int stopwatchNoteId;
	
	@Column(name = "date_note")
	private LocalDateTime dateNote;
  
	@Column(name = "note")
	private String note;
	
  @Id
  @Column(name = "company_id")
  private int companyId;

public Integer getStopwatchId() {
	return stopwatchId;
}

public void setStopwatchId(Integer stopwatchId) {
	this.stopwatchId = stopwatchId;
}

public int getStopwatchNoteId() {
	return stopwatchNoteId;
}

public void setStopwatchNoteId(int stopwatchNoteId) {
	this.stopwatchNoteId = stopwatchNoteId;
}

public LocalDateTime getDateNote() {
	return dateNote;
}

public void setDateNote(LocalDateTime dateNote) {
	this.dateNote = dateNote;
}

public String getNote() {
	return note;
}

public void setNote(String note) {
	this.note = note;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}
  
  
  

  
}
