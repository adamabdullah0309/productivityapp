package com.example.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.type.PostgresUUIDType;

import com.example.serializable.ListDocumentSummarySerializable;

import org.hibernate.annotations.DynamicInsert;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "list_document_summary")
@IdClass(ListDocumentSummarySerializable.class)
public class ListDocumentSummary {
	@Id
	@Column(name ="document_no_id")
	private Integer documentNoId;
	
	@Column(name = "id_company")
	private int idCompany;
	
	@Id
	@Column(name = "id_category")
	private int idCategory;
	
	public ListDocumentSummary() {}
	
	public ListDocumentSummary(Integer documentNoId, int idCompany, int idCategory) 
	{
		this.documentNoId = documentNoId;
		this.idCompany = idCompany;
		this.idCategory = idCategory;
	}

	public Integer getDocumentNoId() {
		return documentNoId;
	}

	public void setDocumentNoId(Integer documentNoId) {
		this.documentNoId = documentNoId;
	}

	public int getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(int idCompany) {
		this.idCompany = idCompany;
	}

	public int getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}

	
	
	
}
