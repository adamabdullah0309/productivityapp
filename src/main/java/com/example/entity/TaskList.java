package com.example.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import com.example.serializable.MTaskListSerializable;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "task")
@IdClass(MTaskListSerializable.class)
@NamedQueries
	(
		{
			@NamedQuery(name="TaskList.findByStatusAndCompanyIdAndCreatedBy", query = "FROM TaskList e WHERE e.status =?1 and e.companyId = ?2 and e.createdBy = ?3 order by createdDate desc"),
	    	@NamedQuery(name="TaskList.findByCompanyIdAndCreatedBy", query = "FROM TaskList e WHERE e.companyId = ?1 and e.createdBy = ?2 order by createdDate desc"),
//	    	@NamedQuery(name = "TaskList.findByBarangIdAndCompanyId", query = "FROM Barang e where e.barangId = ?1 and e.companyId =?2")
		}
	)
public class TaskList {
  @Id
  @Column(name = "task_id")
  private Integer tasklistId;
  
  @Id
  @Column(name = "company_id")
  private Integer companyId;
  
  @Column(name = "task_nama")
  private String tasklistNama;
  
  @Column(name = "created_by")
  private UUID createdBy;
  
  @Column(name = "created_at")
  private LocalDateTime createdDate;
  
  @Column(name = "updated_by")
  private UUID updatedBy;
  
//  @UpdateTimestamp
  @Column(name = "updated_at")
  private LocalDateTime updatedDate;
  
  @Column(name = "task_description")
  private String description;
  
  @Column(name = "task_status")
  private int status;
  
  public TaskList(int tasklistId, String tasklistNama, UUID createdBy, LocalDateTime createdDate, 
		  Integer companyId, String description, int status) {
	    this.tasklistId = tasklistId;
	    this.tasklistNama = tasklistNama;
	    this.createdBy = createdBy;
	    this.createdDate = createdDate;
	    this.companyId = companyId;
	    this.description = description;
	    this.status = status;
  }
  
  public TaskList() {}

public Integer getTasklistId() {
	return tasklistId;
}

public void setTasklistId(Integer tasklistId) {
	this.tasklistId = tasklistId;
}



public Integer getCompanyId() {
	return companyId;
}

public void setCompanyId(Integer companyId) {
	this.companyId = companyId;
}

public String getTasklistNama() {
	return tasklistNama;
}

public void setTasklistNama(String tasklistNama) {
	this.tasklistNama = tasklistNama;
}

public UUID getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(UUID createdBy) {
	this.createdBy = createdBy;
}

public LocalDateTime getCreatedDate() {
	return createdDate;
}

public void setCreatedDate(LocalDateTime createdDate) {
	this.createdDate = createdDate;
}

public UUID getUpdatedBy() {
	return updatedBy;
}

public void setUpdatedBy(UUID updatedBy) {
	this.updatedBy = updatedBy;
}

public LocalDateTime getUpdatedDate() {
	return updatedDate;
}

public void setUpdatedDate(LocalDateTime updatedDate) {
	this.updatedDate = updatedDate;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public int getStatus() {
	return status;
}

public void setStatus(int status) {
	this.status = status;
}
  
  
}
