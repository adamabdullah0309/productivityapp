package com.example.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.type.PostgresUUIDType;

import com.example.serializable.TemplateDetailSerializable;

import org.hibernate.annotations.DynamicInsert;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "template_detail")
@IdClass(TemplateDetailSerializable.class)
public class TemplateDetail {
	@Id
	@Column(name = "nourut")
	private Integer nourut;
	
	@Column(name = "m_template_id")
	private int mTemplateId;
	
	@Column(name = "company_id")
	private int companyId;
	
	@Column(name = "grouping_child_id")
	private Integer groupingChildId;
	
	@Column(name = "task_child_id")
	private Integer taskChildId;
	
	public TemplateDetail() {}
	
	public TemplateDetail(
			Integer nourut, int mTemplateId, int companyId, Integer groupingChildId, Integer taskChildId
			) 
	{
		this.nourut = nourut;
		this.mTemplateId = mTemplateId;
		this.companyId = companyId;
		this.groupingChildId = groupingChildId;
		this.taskChildId = taskChildId;
	}

	public Integer getNourut() {
		return nourut;
	}

	public void setNourut(Integer nourut) {
		this.nourut = nourut;
	}

	

	public int getmTemplateId() {
		return mTemplateId;
	}

	public void setmTemplateId(int mTemplateId) {
		this.mTemplateId = mTemplateId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getGroupingChildId() {
		return groupingChildId;
	}

	public void setGroupingChildId(Integer groupingChildId) {
		this.groupingChildId = groupingChildId;
	}

	public int getTaskChildId() {
		return taskChildId;
	}

	public void setTaskChildId(Integer taskChildId) {
		this.taskChildId = taskChildId;
	}

	

	
	
	
}
