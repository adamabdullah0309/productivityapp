package com.example.entity;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.example.serializable.TmProjectTimeframeSerializable;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_project_timeframe")
@IdClass(TmProjectTimeframeSerializable.class)
@NamedQueries
	(
		{
//			@NamedQuery(name="SubGroup.findByStatusAndCompanyId", query = "FROM SubGroup e WHERE e.status =?1 and e.companyId = ?2 order by createdDate desc"),
//	    	@NamedQuery(name="SubGroup.findByCompanyId", query = "FROM SubGroup e WHERE e.companyId = ?1 order by createdDate desc"),
//	    	@NamedQuery(name = "SubGroupDetail.findByMSubGroupIdAndCompanyId", query = "FROM SubGroupDetail e where e.MSubgroupId = ?1 and e.companyId =?2 order by nourut asc")
		}
	)
public class TmProjectTimeframe {
  @Id
  @Column(name = "tm_project_id")
  private Integer TmProjectId;
  
  @Id
  @Column(name = "project_detail_id")
  private Integer projectDetailId;
  
  @Id
  @Column(name = "company_id")
  private Integer companyId;

  @Id
  @Column(name = "timeframe_id")
  private Integer timeframeId;
  
  @Column(name = "startdate")
  private Date startdate;
  
  @Column(name = "dateline")
  private Date dateline;
  
  @Column(name = "budget_hour")
  private Float budgetHour;
  
  
  public TmProjectTimeframe() {}
  
  


public Integer getTmProjectId() {
	return TmProjectId;
}


public void setTmProjectId(Integer tmProjectId) {
	TmProjectId = tmProjectId;
}


public Integer getProjectDetailId() {
	return projectDetailId;
}


public void setProjectDetailId(Integer projectDetailId) {
	this.projectDetailId = projectDetailId;
}


public Integer getCompanyId() {
	return companyId;
}


public void setCompanyId(Integer companyId) {
	this.companyId = companyId;
}


public Integer getTimeframeId() {
	return timeframeId;
}


public void setTimeframeId(Integer timeframeId) {
	this.timeframeId = timeframeId;
}


public Date getStartdate() {
	return startdate;
}


public void setStartdate(Date startdate) {
	this.startdate = startdate;
}


public Date getDateline() {
	return dateline;
}


public void setDateline(Date dateline) {
	this.dateline = dateline;
}


public Float getBudgetHour() {
	return budgetHour;
}


public void setBudgetHour(Float budgetHour) {
	this.budgetHour = budgetHour;
}
  
  



  
}
