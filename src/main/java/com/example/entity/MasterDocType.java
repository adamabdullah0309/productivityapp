package com.example.entity;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;
import com.example.serializable.MasterDocTypeSerializable;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "m_doc_type")
@IdClass(MasterDocTypeSerializable.class)
//@NamedQueries
//	(
//		{
//			@NamedQuery(name="TaskList.findByStatusAndCompanyId", query = "FROM TaskList e WHERE e.status =?1 and e.companyId = ?2 order by createdDate desc"),
//	    	@NamedQuery(name="TaskList.findByCompanyId", query = "FROM TaskList e WHERE e.companyId = ?1 order by createdDate desc"),
////	    	@NamedQuery(name = "TaskList.findByBarangIdAndCompanyId", query = "FROM Barang e where e.barangId = ?1 and e.companyId =?2")
//		}
//	)
public class MasterDocType {
  @Id
  @Column(name = "doc_type_id")
  private Integer docTypeId;
  
  @Column(name = "doc_type_name")
  private String docTypeName;
  
  @Column(name = "doc_type_status")
  private String docTypeStatus;
  
  @Column(name = "created_by")
  private String createdBy;
  
  @Column(name = "created_at")
  private LocalDateTime createdAt;
  
  @Column(name = "updated_by")
  private String updatedBy;
  
  @Column(name = "updated_at")
  private LocalDateTime updatedAt;
  
  @Id
  @Column(name = "company_id")
  private Integer companyId;

public Integer getDocTypeId() {
	return docTypeId;
}

public void setDocTypeId(Integer docTypeId) {
	this.docTypeId = docTypeId;
}

public String getDocTypeName() {
	return docTypeName;
}

public void setDocTypeName(String docTypeName) {
	this.docTypeName = docTypeName;
}

public String getDocTypeStatus() {
	return docTypeStatus;
}

public void setDocTypeStatus(String docTypeStatus) {
	this.docTypeStatus = docTypeStatus;
}

public String getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}

public LocalDateTime getCreatedAt() {
	return createdAt;
}

public void setCreatedAt(LocalDateTime createdAt) {
	this.createdAt = createdAt;
}

public String getUpdatedBy() {
	return updatedBy;
}

public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
}

public LocalDateTime getUpdatedAt() {
	return updatedAt;
}

public void setUpdatedAt(LocalDateTime updatedAt) {
	this.updatedAt = updatedAt;
}

public Integer getCompanyId() {
	return companyId;
}

public void setCompanyId(Integer companyId) {
	this.companyId = companyId;
}


  
  
  
}
