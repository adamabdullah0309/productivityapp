package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TmBoardProjectDetailSerializable;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_board_project_detail")
@IdClass(TmBoardProjectDetailSerializable.class)
public class TmBoardProjectDetailV2 implements Serializable{
	
	@Id
	@Column(name ="tm_board_id")
	private Integer TmBoardId;
  
	
  
	@Id
	@Column(name = "tm_board_project_detail_id")
	private Integer TmBoardProjectDetailId;
	
	
	@Column(name ="tm_project_id")
	private int tmProjectId;
	
	
	@Column(name = "project_detail_id")
	private Integer projectDetailId;
    
	@Id
	@Column(name = "company_id")
	private int companyId;
	
	@Column(name = "done_date")
	private Date doneDate;
	
	@Column(name = "progress")
	private Integer progress;
	
	

	public int getTmProjectId() {
		return tmProjectId;
	}

	public void setTmProjectId(int tmProjectId) {
		this.tmProjectId = tmProjectId;
	}

	public Date getDoneDate() {
		return doneDate;
	}

	public void setDoneDate(Date doneDate) {
		this.doneDate = doneDate;
	}

	public Integer getProgress() {
		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}

	public Integer getTmBoardId() {
		return TmBoardId;
	}

	public void setTmBoardId(Integer tmBoardId) {
		TmBoardId = tmBoardId;
	}

	public Integer getTmBoardProjectDetailId() {
		return TmBoardProjectDetailId;
	}

	public void setTmBoardProjectDetailId(Integer tmBoardProjectDetailId) {
		TmBoardProjectDetailId = tmBoardProjectDetailId;
	}

	

	public Integer getProjectDetailId() {
		return projectDetailId;
	}

	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	
}
