package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TmBoardProjectDetailDiscussionSerializable;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_board_project_detail_discussion")
@IdClass(TmBoardProjectDetailDiscussionSerializable.class)
public class TmBoardProjectDetailDiscussion implements Serializable{
	
	@Id
	@Column(name ="tm_board_id")
	private Integer TmBoardId;
    
	@Id
	@Column(name = "tm_board_project_detail_id")
	private Integer TmBoardProjectDetailId;
	
	@Id
	@Column(name = "tm_board_project_detail_dis_id")
	private Integer TmBoardProjectDetailDisId;
	
	@Column(name = "tm_board_project_detail_dis_parent_id")
	private Integer TmBoardProjectDetailDisParentId;
    
	@Id
	@Column(name = "company_id")
	private int companyId;
	
	
	@Column(name = "contact_id")
	private UUID contactId;
	
	@Column(name = "date_comment")
	private LocalDateTime dateComment;
	
	@Column(name = "comment_discussion")
	private String commentDiscussion;
	
	

	public String getCommentDiscussion() {
		return commentDiscussion;
	}

	public void setCommentDiscussion(String commentDiscussion) {
		this.commentDiscussion = commentDiscussion;
	}

	public Integer getTmBoardProjectDetailDisParentId() {
		return TmBoardProjectDetailDisParentId;
	}

	public void setTmBoardProjectDetailDisParentId(Integer tmBoardProjectDetailDisParentId) {
		TmBoardProjectDetailDisParentId = tmBoardProjectDetailDisParentId;
	}

	public Integer getTmBoardId() {
		return TmBoardId;
	}

	public void setTmBoardId(Integer tmBoardId) {
		TmBoardId = tmBoardId;
	}

	public Integer getTmBoardProjectDetailId() {
		return TmBoardProjectDetailId;
	}

	public void setTmBoardProjectDetailId(Integer tmBoardProjectDetailId) {
		TmBoardProjectDetailId = tmBoardProjectDetailId;
	}

	public Integer getTmBoardProjectDetailDisId() {
		return TmBoardProjectDetailDisId;
	}

	public void setTmBoardProjectDetailDisId(Integer tmBoardProjectDetailDisId) {
		TmBoardProjectDetailDisId = tmBoardProjectDetailDisId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public UUID getContactId() {
		return contactId;
	}

	public void setContactId(UUID contactId) {
		this.contactId = contactId;
	}

	public LocalDateTime getDateComment() {
		return dateComment;
	}

	public void setDateComment(LocalDateTime dateComment) {
		this.dateComment = dateComment;
	}
	
	
	
}
