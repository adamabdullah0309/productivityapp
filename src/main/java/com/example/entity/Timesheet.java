package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TimesheetSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "timesheet")
@IdClass(TimesheetSerializable.class)
public class Timesheet implements Serializable{
	@Id
	@Column(name ="timesheet_id")
	private Integer timesheetId;
  
  @Column(name = "timesheet_date")
  private Date timesheetDate;
  
  @Column(name = "timesheet_from")
  private Time timesheetFrom;
  
  @Column(name = "timesheet_to")
  private Time timesheetTo;
  
  @Column(name = "timesheet_party")
  private Integer timesheetParty;
  
  @Column(name = "timesheet_project_detail_id")
  private Integer timesheetProjectDetailId;
  
  @Column(name = "timesheet_project_id")
  private Integer timesheetProjectId;
  
  @Column(name = "timesheet_task_id")
  private Integer timesheetTaskId;
  
  @Id
  @Column(name = "timesheet_company_id")
  private Integer timesheetCompanyId;
  
  @Column(name = "timesheet_building")
  private String timesheetBuilding;
  
  @Column(name = "timesheet_address")
  private String timesheetAddress;
  
  @Column(name = "timesheet_status")
  private Integer timesheetStatus;
  
  @Column(name = "timesheet_overtime")
  private Float timesheetOvertime;
  
  @Column(name = "timesheet_request_budget")
  private Float timesheetRequestBudget;
  
  @Column(name = "timesheet_actual")
  private Float timesheetActual;
  
  @Column(name = "timesheet_contact_id")
  private UUID timesheetContactId;
  
  

public Float getTimesheetRequestBudget() {
	return timesheetRequestBudget;
}

public void setTimesheetRequestBudget(Float timesheetRequestBudget) {
	this.timesheetRequestBudget = timesheetRequestBudget;
}

public Integer getTimesheetId() {
	return timesheetId;
}

public void setTimesheetId(Integer timesheetId) {
	this.timesheetId = timesheetId;
}

public Date getTimesheetDate() {
	return timesheetDate;
}

public void setTimesheetDate(Date timesheetDate) {
	this.timesheetDate = timesheetDate;
}

public Time getTimesheetFrom() {
	return timesheetFrom;
}

public void setTimesheetFrom(Time timesheetFrom) {
	this.timesheetFrom = timesheetFrom;
}

public Time getTimesheetTo() {
	return timesheetTo;
}

public void setTimesheetTo(Time timesheetTo) {
	this.timesheetTo = timesheetTo;
}

public Integer getTimesheetParty() {
	return timesheetParty;
}

public void setTimesheetParty(Integer timesheetParty) {
	this.timesheetParty = timesheetParty;
}

public Integer getTimesheetProjectDetailId() {
	return timesheetProjectDetailId;
}

public void setTimesheetProjectDetailId(Integer timesheetProjectDetailId) {
	this.timesheetProjectDetailId = timesheetProjectDetailId;
}

public Integer getTimesheetProjectId() {
	return timesheetProjectId;
}

public void setTimesheetProjectId(Integer timesheetProjectId) {
	this.timesheetProjectId = timesheetProjectId;
}

public Integer getTimesheetTaskId() {
	return timesheetTaskId;
}

public void setTimesheetTaskId(Integer timesheetTaskId) {
	this.timesheetTaskId = timesheetTaskId;
}

public Integer getTimesheetCompanyId() {
	return timesheetCompanyId;
}

public void setTimesheetCompanyId(Integer timesheetCompanyId) {
	this.timesheetCompanyId = timesheetCompanyId;
}

public String getTimesheetBuilding() {
	return timesheetBuilding;
}

public void setTimesheetBuilding(String timesheetBuilding) {
	this.timesheetBuilding = timesheetBuilding;
}

public String getTimesheetAddress() {
	return timesheetAddress;
}

public void setTimesheetAddress(String timesheetAddress) {
	this.timesheetAddress = timesheetAddress;
}

public Integer getTimesheetStatus() {
	return timesheetStatus;
}

public void setTimesheetStatus(Integer timesheetStatus) {
	this.timesheetStatus = timesheetStatus;
}

public Float getTimesheetOvertime() {
	return timesheetOvertime;
}

public void setTimesheetOvertime(Float timesheetOvertime) {
	this.timesheetOvertime = timesheetOvertime;
}

public Float getTimesheetActual() {
	return timesheetActual;
}

public void setTimesheetActual(Float timesheetActual) {
	this.timesheetActual = timesheetActual;
}

public UUID getTimesheetContactId() {
	return timesheetContactId;
}

public void setTimesheetContactId(UUID timesheetContactId) {
	this.timesheetContactId = timesheetContactId;
}

  
}
