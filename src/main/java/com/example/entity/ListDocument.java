package com.example.entity;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.DynamicInsert;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "list_document")
@NamedQueries(  
	    {  
	    	
//	    	@NamedQuery(name = "findByCompanyId", query ="FROM MasterJabatan e where e.companyId = ?1"),
	    	@NamedQuery(name = "findByListDocumentIdAndIdCompany", query="FROM ListDocument e where e.listDocumentId = ?1 and e.idCompany = ?2")
	    	
	    }
	)
public class ListDocument {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_list_doc")
	@SequenceGenerator(name = "generator_list_doc", sequenceName = "list_document_sequence", allocationSize = 1)
	@Column(name ="list_document_id")
	private Integer listDocumentId;
	
	@Column(name="list_document_title")
	private String listDocumentTitle;
	
	@Column(name="list_document_type")
	private int listDocumentType;
	
	@Column(name="list_document_status")
	private boolean listDocumentStatus ;
	
	@Column(name = "list_document_shared")
	private Integer listDocumentShared;
	
	@Column(name="created_by")
	private UUID createdBy;
	
	@Column(name="created_at")
	private LocalDateTime createdAt;
	
	@Column(name="updated_by")
	private UUID updatedBy;
	
	@Column(name="updated_at")
	private LocalDateTime updatedAt;
	
	@Column(name="id_company")
	private int idCompany;

	@Column(name="list_confidential")
	private boolean listConfidential;
	
	@Column(name="list_document_pathfile")
	private String listDocumentPathfile;

	public Integer getListDocumentId() {
		return listDocumentId;
	}

	public void setListDocumentId(Integer listDocumentId) {
		this.listDocumentId = listDocumentId;
	}

	public String getListDocumentTitle() {
		return listDocumentTitle;
	}

	public void setListDocumentTitle(String listDocumentTitle) {
		this.listDocumentTitle = listDocumentTitle;
	}

	public int getListDocumentType() {
		return listDocumentType;
	}

	public void setListDocumentType(int listDocumentType) {
		this.listDocumentType = listDocumentType;
	}

	public boolean isListDocumentStatus() {
		return listDocumentStatus;
	}

	public void setListDocumentStatus(boolean listDocumentStatus) {
		this.listDocumentStatus = listDocumentStatus;
	}

	public Integer getListDocumentShared() {
		return listDocumentShared;
	}

	public void setListDocumentShared(Integer listDocumentShared) {
		this.listDocumentShared = listDocumentShared;
	}

	public UUID getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UUID createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public UUID getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(UUID updatedBy) {
		this.updatedBy = updatedBy;
	}

	
	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(int idCompany) {
		this.idCompany = idCompany;
	}

	public boolean isListConfidential() {
		return listConfidential;
	}

	public void setListConfidential(boolean listConfidential) {
		this.listConfidential = listConfidential;
	}

	public String isListDocumentPathfile() {
		return listDocumentPathfile;
	}

	public void setListDocumentPathfile(String listDocumentPathfile) {
		this.listDocumentPathfile = listDocumentPathfile;
	}
	
	
	
	
}
