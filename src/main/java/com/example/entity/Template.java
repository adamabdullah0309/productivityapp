package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TemplateSerializable;
import com.example.serializable.TimesheetSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "template")
@IdClass(TemplateSerializable.class)
public class Template implements Serializable{
	@Id
	@Column(name ="template_id")
	private Integer templateId;
	
	@Column(name ="template_nama")
	private String templateNama;
	
	@Column(name ="template_description")
	private String templateDescription;
	
	@Column(name ="template_status")
	private int templateStatus;
	
	
  
  @Column(name = "created_by")
  private UUID createdBy;
  
  @Column(name = "created_at")
  private LocalDateTime createdAt;
  
  @Column(name = "updated_by")
  private UUID updatedBy;
  
  @Column(name = "updated_at")
  private LocalDateTime updatedAt;
  
  @Id
  @Column(name = "company_id")
  private int companyId;
  
  

public Integer getTemplateId() {
	return templateId;
}

public void setTemplateId(Integer templateId) {
	this.templateId = templateId;
}

public String getTemplateNama() {
	return templateNama;
}

public void setTemplateNama(String templateNama) {
	this.templateNama = templateNama;
}

public String getTemplateDescription() {
	return templateDescription;
}

public void setTemplateDescription(String templateDescription) {
	this.templateDescription = templateDescription;
}

public int getTemplateStatus() {
	return templateStatus;
}

public void setTemplateStatus(int templateStatus) {
	this.templateStatus = templateStatus;
}

public UUID getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(UUID createdBy) {
	this.createdBy = createdBy;
}

public LocalDateTime getCreatedAt() {
	return createdAt;
}

public void setCreatedAt(LocalDateTime createdAt) {
	this.createdAt = createdAt;
}

public UUID getUpdatedBy() {
	return updatedBy;
}

public void setUpdatedBy(UUID updatedBy) {
	this.updatedBy = updatedBy;
}

public LocalDateTime getUpdatedAt() {
	return updatedAt;
}

public void setUpdatedAt(LocalDateTime updatedAt) {
	this.updatedAt = updatedAt;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}


  

  
}
