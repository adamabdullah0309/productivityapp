package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TemplateSerializable;
import com.example.serializable.ProjectDetailSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "project_detail")
@IdClass(ProjectDetailSerializable.class)
public class ProjectDetail implements Serializable{
	@Id
	@Column(name ="project_detail_id")
	private Integer projectDetailId;
	
	@Id
	@Column(name ="m_project_id")
	private Integer mProjectId;
	
	@Column(name ="parent_id")
	private Integer parentId;
	
	@Column(name ="m_task_id")
	private Integer mTaskId;
	
	@Column(name ="m_grouping_id")
	private Integer mGroupingId;
	
	@Column(name ="status")
	private int status;
	
	@Column(name ="priority")
	private int priority;
	
  
	@Id
	@Column(name = "company_id")
	private int companyId;
	
	@Column(name = "description")
	private String description;
	
	public ProjectDetail() {}
	
	public ProjectDetail(
			Integer projectDetailId, Integer mProjectId, Integer parentId, Integer mTaskId, Integer mGroupingId, int companyId, int status, int priority, String description
			) 
	{
		this.projectDetailId = projectDetailId;
		this.mProjectId = mProjectId;
		this.parentId = parentId;
		this.mTaskId = mTaskId;
		this.mGroupingId = mGroupingId;
		this.companyId = companyId;
		this.status = status;
		this.priority = priority;
		this.description = description;
	}

	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
	public Integer getProjectDetailId() {
		return projectDetailId;
	}

	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}

	public Integer getmProjectId() {
		return mProjectId;
	}

	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getmTaskId() {
		return mTaskId;
	}

	public void setmTaskId(Integer mTaskId) {
		this.mTaskId = mTaskId;
	}

	public Integer getmGroupingId() {
		return mGroupingId;
	}

	public void setmGroupingId(Integer mGroupingId) {
		this.mGroupingId = mGroupingId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	
  


  

  
}
