package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TemplateSerializable;
import com.example.serializable.ProjectDetailDependencySerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "project_detail_dependency")
@IdClass(ProjectDetailDependencySerializable.class)
public class ProjectDetailDependency implements Serializable{
	@Id
	@Column(name ="project_detail_id")
	private Integer projectDetailId;
	
	@Id
	@Column(name ="m_project_id")
	private Integer mProjectId;
	
	@Id
	@Column(name ="project_detail_dependency_id")
	private Integer projectDetailDependencyId;
	
	@Column(name ="no_dependency")
	private int noDependency;
	
	@Id
	@Column(name = "company_id")
	private int companyId;
	
	public ProjectDetailDependency() {}
	
	public ProjectDetailDependency(
			Integer projectDetailId, Integer mProjectId, int noDependency,int companyId, Integer projectDetailDependencyId
			) 
	{
		this.projectDetailId = projectDetailId;
		this.mProjectId = mProjectId;
		this.noDependency = noDependency;
		this.companyId = companyId;
		this.projectDetailDependencyId = projectDetailDependencyId;
	}

	public Integer getProjectDetailId() {
		return projectDetailId;
	}

	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}

	public Integer getmProjectId() {
		return mProjectId;
	}

	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}

	public Integer getProjectDetailDependencyId() {
		return projectDetailDependencyId;
	}

	public void setProjectDetailDependencyId(Integer projectDetailDependencyId) {
		this.projectDetailDependencyId = projectDetailDependencyId;
	}

	
	
	public int getNoDependency() {
		return noDependency;
	}

	public void setNoDependency(int noDependency) {
		this.noDependency = noDependency;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	

	
	
	
  


  

  
}
