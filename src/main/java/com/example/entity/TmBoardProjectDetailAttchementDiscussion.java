package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TmBoardProjectDetailAttchementDiscussionSerializable;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_board_project_detail_attachement_discussion")
@IdClass(TmBoardProjectDetailAttchementDiscussionSerializable.class)
public class TmBoardProjectDetailAttchementDiscussion implements Serializable{
	
	@Id
	@Column(name ="tm_board_id")
	private Integer TmBoardId;
  
	@Id
	@Column(name = "tm_board_project_detail_id")
	private Integer TmBoardProjectDetailId;
	
	@Id
	@Column(name = "tm_board_project_detail_att_id")
	private Integer TmBoardProjectDetailAttId;
	
	@Id
	@Column(name = "tm_board_project_detail_att_disc_id")
	private Integer TmBoardProjectDetailAttDiscId;
	
	@Column(name = "contact_id")
	private UUID contactId;
    
	@Id
	@Column(name = "company_id")
	private int companyId;
	
	@Column(name = "comment_discussion")
	private String commentDiscussion;
	
	@Column(name = "tm_board_project_detail_att_disc_parent_id")
	private Integer tm_board_project_detail_att_disc_parent_id;
	
	@Column(name = "date_comment")
	private LocalDateTime dateComment;

	public Integer getTmBoardId() {
		return TmBoardId;
	}

	public void setTmBoardId(Integer tmBoardId) {
		TmBoardId = tmBoardId;
	}

	public Integer getTmBoardProjectDetailId() {
		return TmBoardProjectDetailId;
	}

	public void setTmBoardProjectDetailId(Integer tmBoardProjectDetailId) {
		TmBoardProjectDetailId = tmBoardProjectDetailId;
	}

	public Integer getTmBoardProjectDetailAttId() {
		return TmBoardProjectDetailAttId;
	}

	public void setTmBoardProjectDetailAttId(Integer tmBoardProjectDetailAttId) {
		TmBoardProjectDetailAttId = tmBoardProjectDetailAttId;
	}

	public Integer getTmBoardProjectDetailAttDiscId() {
		return TmBoardProjectDetailAttDiscId;
	}

	public void setTmBoardProjectDetailAttDiscId(Integer tmBoardProjectDetailAttDiscId) {
		TmBoardProjectDetailAttDiscId = tmBoardProjectDetailAttDiscId;
	}

	public UUID getContactId() {
		return contactId;
	}

	public void setContactId(UUID contactId) {
		this.contactId = contactId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCommentDiscussion() {
		return commentDiscussion;
	}

	public void setCommentDiscussion(String commentDiscussion) {
		this.commentDiscussion = commentDiscussion;
	}

	public Integer getTm_board_project_detail_att_disc_parent_id() {
		return tm_board_project_detail_att_disc_parent_id;
	}

	public void setTm_board_project_detail_att_disc_parent_id(Integer tm_board_project_detail_att_disc_parent_id) {
		this.tm_board_project_detail_att_disc_parent_id = tm_board_project_detail_att_disc_parent_id;
	}

	public LocalDateTime getDateComment() {
		return dateComment;
	}

	public void setDateComment(LocalDateTime dateComment) {
		this.dateComment = dateComment;
	}
	
	
	
	
}
