package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TemplateSerializable;
import com.example.serializable.ProjectDetailKaryawanSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "project_detail_karyawan")
@IdClass(ProjectDetailKaryawanSerializable.class)
public class ProjectDetailKaryawan implements Serializable{
	@Id
	@Column(name ="project_detail_id")
	private Integer projectDetailId;
	
	@Id
	@Column(name ="project_id")
	private Integer mProjectId;
	
	@Id
	@Column(name ="project_detail_karyawan_id")
	private Integer projectDetailKaryawanId;
	
	@Column(name ="contact_id")
	private UUID contactId;
	
	@Id
	@Column(name = "company_id")
	private int companyId;
	
	public ProjectDetailKaryawan() {}
	
	public ProjectDetailKaryawan(
			Integer projectDetailId, Integer mProjectId, UUID contactId,int companyId, Integer projectDetailKaryawanId
			) 
	{
		this.projectDetailId = projectDetailId;
		this.mProjectId = mProjectId;
		this.contactId = contactId;
		this.companyId = companyId;
		this.projectDetailKaryawanId = projectDetailKaryawanId;
	}

	public Integer getProjectDetailId() {
		return projectDetailId;
	}

	public void setProjectDetailId(Integer projectDetailId) {
		this.projectDetailId = projectDetailId;
	}

	public Integer getmProjectId() {
		return mProjectId;
	}

	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}

	public Integer getProjectDetailKaryawanId() {
		return projectDetailKaryawanId;
	}

	public void setProjectDetailKaryawanId(Integer projectDetailKaryawanId) {
		this.projectDetailKaryawanId = projectDetailKaryawanId;
	}

	public UUID getContactId() {
		return contactId;
	}

	public void setContactId(UUID contactId) {
		this.contactId = contactId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	

	
	
	
  


  

  
}
