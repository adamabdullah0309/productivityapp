package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.GroupingSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "grouping")
@IdClass(GroupingSerializable.class)
public class Grouping implements Serializable{
	@Id
	@Column(name ="grouping_id")
	private Integer groupingId;
  
  @Column(name = "grouping_nama")
  private String groupingNama;
  
  @Column(name = "grouping_description")
  private String groupingDescription;
  
  @Column(name = "grouping_status")
  private int groupingStatus;
  
  
  
  @Column(name = "created_by")
  private UUID createdBy;
  
  @Column(name = "created_at")
  private LocalDateTime createdAt;
  
  @Column(name = "updated_by")
  private UUID updatedBy;
  
  @Column(name = "updated_at")
  private LocalDateTime updatedAt;
  
  @Id
  @Column(name = "company_id")
  private int companyId;

public Integer getGroupingId() {
	return groupingId;
}

public void setGroupingId(Integer groupingId) {
	this.groupingId = groupingId;
}

public String getGroupingNama() {
	return groupingNama;
}

public void setGroupingNama(String groupingNama) {
	this.groupingNama = groupingNama;
}

public String getGroupingDescription() {
	return groupingDescription;
}

public void setGroupingDescription(String groupingDescription) {
	this.groupingDescription = groupingDescription;
}

public int getGroupingStatus() {
	return groupingStatus;
}

public void setGroupingStatus(int groupingStatus) {
	this.groupingStatus = groupingStatus;
}

public UUID getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(UUID createdBy) {
	this.createdBy = createdBy;
}

public LocalDateTime getCreatedAt() {
	return createdAt;
}

public void setCreatedAt(LocalDateTime createdAt) {
	this.createdAt = createdAt;
}

public UUID getUpdatedBy() {
	return updatedBy;
}

public void setUpdatedBy(UUID updatedBy) {
	this.updatedBy = updatedBy;
}

public LocalDateTime getUpdatedAt() {
	return updatedAt;
}

public void setUpdatedAt(LocalDateTime updatedAt) {
	this.updatedAt = updatedAt;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}

  
  

  
}
