package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TmPlannerAloneSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_planner_alone")
@IdClass(TmPlannerAloneSerializable.class)
public class TmPlannerAlone implements Serializable{
	@Id
	@Column(name ="planner_alone_id")
	private Integer plannerAloneId;
	
	@Column(name ="party_id")
	private Integer partyId;
	
	@Column(name ="task_id")
	private Integer taskId;
	
	@Column(name = "date_planner_internal")
	private Date datePlannerInternal;
	
	
  
	@Column(name = "start_hour")
	private Time startHour;
  
  @Column(name = "end_hour")
  private Time endHour;
  
  @Column(name = "note")
  private String note;
  
  @Column(name = "address")
  private String address;
  
  @Column(name = "building")
  private String building;
  
  @Column(name = "planned_hour")
  private Float plannedHour;
  
  @Column(name = "overtime_hour")
  private Float overtimeHour;
  
  @Column(name = "request_budget_hour")
  private Float requestBudgetHour;
  
  @Id
  @Column(name = "company_id")
  private int companyId;
  
  @Column(name = "contact_id")
  private UUID contactId;
  
  @Column(name = "status")
  private int status;
  
  

public int getStatus() {
	return status;
}

public void setStatus(int status) {
	this.status = status;
}

public String getBuilding() {
	return building;
}

public void setBuilding(String building) {
	this.building = building;
}

public Integer getPlannerAloneId() {
	return plannerAloneId;
}

public void setPlannerAloneId(Integer plannerAloneId) {
	this.plannerAloneId = plannerAloneId;
}

public Integer getPartyId() {
	return partyId;
}

public void setPartyId(Integer partyId) {
	this.partyId = partyId;
}

public Integer getTaskId() {
	return taskId;
}

public void setTaskId(Integer taskId) {
	this.taskId = taskId;
}

public Date getDatePlannerInternal() {
	return datePlannerInternal;
}

public void setDatePlannerInternal(Date datePlannerInternal) {
	this.datePlannerInternal = datePlannerInternal;
}

public Time getStartHour() {
	return startHour;
}

public void setStartHour(Time startHour) {
	this.startHour = startHour;
}

public Time getEndHour() {
	return endHour;
}

public void setEndHour(Time endHour) {
	this.endHour = endHour;
}

public String getNote() {
	return note;
}

public void setNote(String note) {
	this.note = note;
}



public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public Float getPlannedHour() {
	return plannedHour;
}

public void setPlannedHour(Float plannedHour) {
	this.plannedHour = plannedHour;
}

public Float getOvertimeHour() {
	return overtimeHour;
}

public void setOvertimeHour(Float overtimeHour) {
	this.overtimeHour = overtimeHour;
}

public Float getRequestBudgetHour() {
	return requestBudgetHour;
}

public void setRequestBudgetHour(Float requestBudgetHour) {
	this.requestBudgetHour = requestBudgetHour;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}

public UUID getContactId() {
	return contactId;
}

public void setContactId(UUID contactId) {
	this.contactId = contactId;
}
  
  
  
  
  
}
