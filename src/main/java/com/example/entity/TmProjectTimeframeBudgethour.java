package com.example.entity;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.example.serializable.TmProjectTimeframeBudgethourSerializable;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_project_timeframe_budgethour")
@IdClass(TmProjectTimeframeBudgethourSerializable.class)
@NamedQueries
	(
		{
//			@NamedQuery(name="SubGroup.findByStatusAndCompanyId", query = "FROM SubGroup e WHERE e.status =?1 and e.companyId = ?2 order by createdDate desc"),
//	    	@NamedQuery(name="SubGroup.findByCompanyId", query = "FROM SubGroup e WHERE e.companyId = ?1 order by createdDate desc"),
//	    	@NamedQuery(name = "SubGroupDetail.findByMSubGroupIdAndCompanyId", query = "FROM SubGroupDetail e where e.MSubgroupId = ?1 and e.companyId =?2 order by nourut asc")
		}
	)
public class TmProjectTimeframeBudgethour {
	@Id
	@Column(name = "tm_project_id")
	private Integer TmProjectId;
	
	@Id
	@Column(name = "project_detail_id")
	private Integer projectDetailId;
	  
	@Id
	@Column(name = "company_id")
	private Integer companyId;
	
	@Id
	@Column(name = "timeframe_id")
	private Integer timeframeId;
	
	@Id
	@Column(name = "date_budgethour")
	private Date date;
  
	@Column(name = "budget_hour")
	private Float budgetHour;
  
  


public TmProjectTimeframeBudgethour(Integer tmProjectId, Date date,
		Float budgetHour, Integer companyId, Integer projectDetailId) {
	this.TmProjectId = tmProjectId;
	this.date = date;
	this.budgetHour = budgetHour;
	this.companyId = companyId;
}
  
public TmProjectTimeframeBudgethour() {}



public Integer getProjectDetailId() {
	return projectDetailId;
}

public void setProjectDetailId(Integer projectDetailId) {
	this.projectDetailId = projectDetailId;
}

public Integer getTmProjectId() {
	return TmProjectId;
}

public void setTmProjectId(Integer tmProjectId) {
	TmProjectId = tmProjectId;
}

public Integer getCompanyId() {
	return companyId;
}

public void setCompanyId(Integer companyId) {
	this.companyId = companyId;
}

public Integer getTimeframeId() {
	return timeframeId;
}

public void setTimeframeId(Integer timeframeId) {
	this.timeframeId = timeframeId;
}

public Date getDate() {
	return date;
}

public void setDate(Date date) {
	this.date = date;
}

public Float getBudgetHour() {
	return budgetHour;
}

public void setBudgetHour(Float budgetHour) {
	this.budgetHour = budgetHour;
}


}
