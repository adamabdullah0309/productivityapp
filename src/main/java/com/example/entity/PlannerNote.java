package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.PlannerSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_planner_note")
@IdClass(PlannerSerializable.class)
public class PlannerNote implements Serializable{
	@Id
	@Column(name ="planner_id")
	private Integer plannerId;
	
	@Id
	@Column(name ="project_id")
	private int projectId;
	
	@Id
	@Column(name ="project_detail_id")
	private int projectDetailId;
	
	@Column(name = "date_note")
	private LocalDateTime dateNote;
  
	@Column(name = "note")
	private String note;
	
	@Column(name = "tm_planner_note_id")
	private int tmPlannerNoteId;
	
  @Id
  @Column(name = "timeframe_id")
  private int timeframeId;
  
  @Id
  @Column(name = "company_id")
  private int companyId;
  
  @Column(name = "date_budgethour_timeframe")
  private Date dateBudgethourTimeframe;

public Integer getPlannerId() {
	return plannerId;
}

public void setPlannerId(Integer plannerId) {
	this.plannerId = plannerId;
}

public int getProjectId() {
	return projectId;
}

public void setProjectId(int projectId) {
	this.projectId = projectId;
}

public int getProjectDetailId() {
	return projectDetailId;
}

public void setProjectDetailId(int projectDetailId) {
	this.projectDetailId = projectDetailId;
}



public LocalDateTime getDateNote() {
	return dateNote;
}

public void setDateNote(LocalDateTime dateNote) {
	this.dateNote = dateNote;
}

public String getNote() {
	return note;
}

public void setNote(String note) {
	this.note = note;
}

public int getTmPlannerNoteId() {
	return tmPlannerNoteId;
}

public void setTmPlannerNoteId(int tmPlannerNoteId) {
	this.tmPlannerNoteId = tmPlannerNoteId;
}
public int getTimeframeId() {
	return timeframeId;
}

public void setTimeframeId(int timeframeId) {
	this.timeframeId = timeframeId;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}

public Date getDateBudgethourTimeframe() {
	return dateBudgethourTimeframe;
}

public void setDateBudgethourTimeframe(Date dateBudgethourTimeframe) {
	this.dateBudgethourTimeframe = dateBudgethourTimeframe;
}
  
  
  
  
  
  

  
}
