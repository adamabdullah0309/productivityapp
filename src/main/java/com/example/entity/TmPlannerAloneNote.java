package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.PlannerAloneNoteSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_planner_alone_note")
@IdClass(PlannerAloneNoteSerializable.class)
public class TmPlannerAloneNote implements Serializable{
	@Id
	@Column(name ="planner_alone_id")
	private Integer plannerAloneId;
	
	@Id
	@Column(name ="planner_alone_note_id")
	private Integer plannerAloneNoteId;
	
	@Column(name ="note")
	private String note;
	
	@Column(name = "date_note")
	private LocalDateTime dateNote;
	
  @Id
  @Column(name = "company_id")
  private int companyId;

public Integer getPlannerAloneId() {
	return plannerAloneId;
}

public void setPlannerAloneId(Integer plannerAloneId) {
	this.plannerAloneId = plannerAloneId;
}

public Integer getPlannerAloneNoteId() {
	return plannerAloneNoteId;
}

public void setPlannerAloneNoteId(Integer plannerAloneNoteId) {
	this.plannerAloneNoteId = plannerAloneNoteId;
}

public String getNote() {
	return note;
}

public void setNote(String note) {
	this.note = note;
}



public LocalDateTime getDateNote() {
	return dateNote;
}

public void setDateNote(LocalDateTime dateNote) {
	this.dateNote = dateNote;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}
  
  
  
  
  
  
  
}
