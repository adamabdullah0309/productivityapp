package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TmBoardStageSerializable;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_board_stage")
@IdClass(TmBoardStageSerializable.class)
public class TmBoardStage implements Serializable{
	
	@Id
	@Column(name ="tm_board_id")
	private Integer TmBoardId;
  
	@Id
	@Column(name = "tm_board_stage_id")
	private Integer TmBoardStageId;
  
	@Column(name = "m_stage_id")
	private Integer MStageId;
    
	@Id
	@Column(name = "company_id")
	private int companyId;

public Integer getTmBoardId() {
	return TmBoardId;
}

public void setTmBoardId(Integer tmBoardId) {
	TmBoardId = tmBoardId;
}

public Integer getTmBoardStageId() {
	return TmBoardStageId;
}

public void setTmBoardStageId(Integer tmBoardStageId) {
	TmBoardStageId = tmBoardStageId;
}

public Integer getMStageId() {
	return MStageId;
}

public void setMStageId(Integer mStageId) {
	MStageId = mStageId;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}

  
  
}
