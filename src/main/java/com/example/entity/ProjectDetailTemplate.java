package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TemplateSerializable;
import com.example.serializable.ProjectDetailTemplateSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "project_template_detail")
@IdClass(ProjectDetailTemplateSerializable.class)
public class ProjectDetailTemplate implements Serializable{
	
	@Id
	@Column(name ="m_project_id")
	private Integer mProjectId;
	
	@Column(name ="m_template_id")
	private Integer mTemplateId;
	
	@Id
	@Column(name = "company_id")
	private int companyId;

	public Integer getmProjectId() {
		return mProjectId;
	}

	public void setmProjectId(Integer mProjectId) {
		this.mProjectId = mProjectId;
	}

	public Integer getmTemplateId() {
		return mTemplateId;
	}

	public void setmTemplateId(Integer mTemplateId) {
		this.mTemplateId = mTemplateId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	


  

  
}
