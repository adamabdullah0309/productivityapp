package com.example.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.type.PostgresUUIDType;

import com.example.serializable.GroupingDetailSerializable;

import org.hibernate.annotations.DynamicInsert;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "subgroup_detail")
@IdClass(GroupingDetailSerializable.class)
public class GroupingDetail {
	@Id
	@Column(name = "nourut")
	private Integer nourut;
	
	@Column(name = "m_grouping_id")
	private int mGroupingId;
	
	@Column(name = "company_id")
	private int companyId;
	
	@Column(name = "grouping_child_id")
	private Integer groupingChildId;
	
	@Column(name = "task_child_id")
	private Integer taskChildId;
	
	public GroupingDetail() {}
	
	public GroupingDetail(
			Integer nourut, int mGroupingId, int companyId, Integer groupingChildId, Integer taskChildId
			) 
	{
		this.nourut = nourut;
		this.mGroupingId = mGroupingId;
		this.companyId = companyId;
		this.groupingChildId = groupingChildId;
		this.taskChildId = taskChildId;
	}

	public Integer getNourut() {
		return nourut;
	}

	public void setNourut(Integer nourut) {
		this.nourut = nourut;
	}

	public int getmGroupingId() {
		return mGroupingId;
	}

	public void setmGroupingId(int mGroupingId) {
		this.mGroupingId = mGroupingId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getGroupingChildId() {
		return groupingChildId;
	}

	public void setGroupingChildId(Integer groupingChildId) {
		this.groupingChildId = groupingChildId;
	}

	public int getTaskChildId() {
		return taskChildId;
	}

	public void setTaskChildId(Integer taskChildId) {
		this.taskChildId = taskChildId;
	}

	

	
	
	
}
