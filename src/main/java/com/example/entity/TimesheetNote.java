package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TimesheetNoteSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "timesheet_note")
@IdClass(TimesheetNoteSerializable.class)
public class TimesheetNote implements Serializable{
	@Id
	@Column(name ="timesheet_id")
	private Integer timesheetId;
	
	@Id
	@Column(name ="timesheet_note_id")
	private Integer timesheetNoteId;
	
	@Column(name ="note")
	private String note;
	
	@Column(name = "date_note")
	private LocalDateTime dateNote;
	
  @Id
  @Column(name = "timesheet_company_id")
  private int timesheetCompanyId;

public Integer getTimesheetId() {
	return timesheetId;
}

public void setTimesheetId(Integer timesheetId) {
	this.timesheetId = timesheetId;
}

public Integer getTimesheetNoteId() {
	return timesheetNoteId;
}

public void setTimesheetNoteId(Integer timesheetNoteId) {
	this.timesheetNoteId = timesheetNoteId;
}

public String getNote() {
	return note;
}

public void setNote(String note) {
	this.note = note;
}

public LocalDateTime getDateNote() {
	return dateNote;
}

public void setDateNote(LocalDateTime dateNote) {
	this.dateNote = dateNote;
}

public int getTimesheetCompanyId() {
	return timesheetCompanyId;
}

public void setTimesheetCompanyId(int timesheetCompanyId) {
	this.timesheetCompanyId = timesheetCompanyId;
}

  
  
  
  
  
  
  
}
