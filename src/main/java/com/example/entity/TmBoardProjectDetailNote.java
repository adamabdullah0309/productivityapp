package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.TmBoardProjectDetailNoteSerializable;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_board_project_detail_note")
@IdClass(TmBoardProjectDetailNoteSerializable.class)
public class TmBoardProjectDetailNote implements Serializable{
	
	@Id
	@Column(name ="tm_board_id")
	private int TmBoardId;
	
	@Id
	@Column(name ="tm_board_project_detail_id")
	private int TmBoardProjectDetailId;
	
	@Id
	@Column(name ="tm_board_project_detail_note_id")
	private int TmBoardProjectDetailNoteId;
	
	
	@Column(name = "note")
	private String note;
	
	@Column(name = "date_note")
	private LocalDateTime dateNote;
    
	@Id
	@Column(name = "company_id")
	private int companyId;

	public int getTmBoardId() {
		return TmBoardId;
	}

	public void setTmBoardId(int tmBoardId) {
		TmBoardId = tmBoardId;
	}

	public int getTmBoardProjectDetailId() {
		return TmBoardProjectDetailId;
	}

	public void setTmBoardProjectDetailId(int tmBoardProjectDetailId) {
		TmBoardProjectDetailId = tmBoardProjectDetailId;
	}

	public int getTmBoardProjectDetailNoteId() {
		return TmBoardProjectDetailNoteId;
	}

	public void setTmBoardProjectDetailNoteId(int tmBoardProjectDetailNoteId) {
		TmBoardProjectDetailNoteId = tmBoardProjectDetailNoteId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public LocalDateTime getDateNote() {
		return dateNote;
	}

	public void setDateNote(LocalDateTime dateNote) {
		this.dateNote = dateNote;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	
	
	
}
