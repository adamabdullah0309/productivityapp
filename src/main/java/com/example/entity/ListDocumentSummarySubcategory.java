package com.example.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.type.PostgresUUIDType;

import com.example.serializable.ListDocumentSummarySubcategorySerializable;

import org.hibernate.annotations.DynamicInsert;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "list_document_summary_subcategory")
@IdClass(ListDocumentSummarySubcategorySerializable.class)
public class ListDocumentSummarySubcategory {
	@Id
	@Column(name ="document_no_id")
	private Integer documentNoId;
	
	@Column(name = "id_company")
	private int idCompany;
	
	@Column(name = "id_category")
	private int idCategory;
	
	@Column(name = "id_subcategory")
	private int idSubcategory;
	
	@Column(name = "value")
	private String value;
	
	@Column(name = "id_subsubcategory")
	private Integer idSubsubcategory;
	
	@Column(name = "nourut")
	private int nourut;
	
	@Column(name = "parent_id")
	private int parentId;
	
	@Id
	@Column(name = "id")
	private int id;
	
	public ListDocumentSummarySubcategory() {}
	
	public ListDocumentSummarySubcategory(
			Integer documentNoId, int idCompany, int idCategory, int idSubcategory, String value, Integer idSubsubcategory, int nourut, int parentId,
			int id
			) 
	{
		this.documentNoId = documentNoId;
		this.idCompany = idCompany;
		this.idCategory = idCategory;
		this.idSubcategory = idSubcategory;
		this.value = value;
		this.idSubsubcategory = idSubsubcategory;
		this.nourut = nourut;
		this.parentId = parentId;
		this.id = id;
	}

	public Integer getDocumentNoId() {
		return documentNoId;
	}

	public void setDocumentNoId(Integer documentNoId) {
		this.documentNoId = documentNoId;
	}

	public int getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(int idCompany) {
		this.idCompany = idCompany;
	}

	public int getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}

	public int getIdSubcategory() {
		return idSubcategory;
	}

	public void setIdSubcategory(int idSubcategory) {
		this.idSubcategory = idSubcategory;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getIdSubsubcategory() {
		return idSubsubcategory;
	}

	public void setIdSubsubcategory(Integer idSubsubcategory) {
		this.idSubsubcategory = idSubsubcategory;
	}

	public int getNourut() {
		return nourut;
	}

	public void setNourut(int nourut) {
		this.nourut = nourut;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	
	
}
