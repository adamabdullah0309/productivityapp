package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.PlannerSerializable;



@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_planner")
@IdClass(PlannerSerializable.class)
public class Planner implements Serializable{
	@Id
	@Column(name ="planner_id")
	private Integer plannerId;
	
	@Id
	@Column(name ="project_id")
	private int projectId;
	
	@Id
	@Column(name ="project_detail_id")
	private int projectDetailId;
	
	@Column(name = "date_planner")
	private Date datePlanner;
	
	@Column(name = "status")
	private Integer status;
  
	@Column(name = "to_planner")
	private Time to_planner;
	
	@Column(name = "planned_hour")
	private Float plannedHour;
	
	@Column(name = "overtime_hour")
    private Float overtime_hour;
	
	@Column(name = "request_budget_hour")
	private Float request_budget_hour;
	
	@Column(name = "note_planner")
	  private String notePlanner;
	
	@Column(name = "building_planner")
	  private String buildingPlanner;
	
  @Column(name = "from_planner")
  private Time fromplanner;
  
  @Column(name = "contact_id")
  private UUID contactId;
  
  @Id
  @Column(name = "timeframe_id")
  private int timeframeId;
  
  @Id
  @Column(name = "company_id")
  private int companyId;
  
  @Column(name = "date_budgethour_timeframe")
  private Date dateBudgethourTimeframe;
  
  @Column(name = "address_planner")
  private String addressPlanner;

  
  
public Integer getStatus() {
	return status;
}

public void setStatus(Integer status) {
	this.status = status;
}

public Integer getPlannerId() {
	return plannerId;
}

public void setPlannerId(Integer plannerId) {
	this.plannerId = plannerId;
}

public int getProjectId() {
	return projectId;
}

public void setProjectId(int projectId) {
	this.projectId = projectId;
}

public int getProjectDetailId() {
	return projectDetailId;
}

public void setProjectDetailId(int projectDetailId) {
	this.projectDetailId = projectDetailId;
}

public Date getDatePlanner() {
	return datePlanner;
}

public void setDatePlanner(Date datePlanner) {
	this.datePlanner = datePlanner;
}

public Time getTo_planner() {
	return to_planner;
}

public void setTo_planner(Time to_planner) {
	this.to_planner = to_planner;
}

public Float getPlannedHour() {
	return plannedHour;
}

public void setPlannedHour(Float plannedHour) {
	this.plannedHour = plannedHour;
}

public Float getOvertime_hour() {
	return overtime_hour;
}

public void setOvertime_hour(Float overtime_hour) {
	this.overtime_hour = overtime_hour;
}

public Float getRequest_budget_hour() {
	return request_budget_hour;
}

public void setRequest_budget_hour(Float request_budget_hour) {
	this.request_budget_hour = request_budget_hour;
}

public String getNotePlanner() {
	return notePlanner;
}

public void setNotePlanner(String notePlanner) {
	this.notePlanner = notePlanner;
}

public String getBuildingPlanner() {
	return buildingPlanner;
}

public void setBuildingPlanner(String buildingPlanner) {
	this.buildingPlanner = buildingPlanner;
}

public Time getFromplanner() {
	return fromplanner;
}

public void setFromplanner(Time fromplanner) {
	this.fromplanner = fromplanner;
}

public UUID getContactId() {
	return contactId;
}

public void setContactId(UUID contactId) {
	this.contactId = contactId;
}

public int getTimeframeId() {
	return timeframeId;
}

public void setTimeframeId(int timeframeId) {
	this.timeframeId = timeframeId;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}

public Date getDateBudgethourTimeframe() {
	return dateBudgethourTimeframe;
}

public void setDateBudgethourTimeframe(Date dateBudgethourTimeframe) {
	this.dateBudgethourTimeframe = dateBudgethourTimeframe;
}


public String getAddressPlanner() {
	return addressPlanner;
}

public void setAddressPlanner(String addressPlanner) {
	this.addressPlanner = addressPlanner;
}

  
  
  
  
  

  
}
