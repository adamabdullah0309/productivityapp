package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.MStageGroupDetailSerializable;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "m_stage_group_detail")
@IdClass(MStageGroupDetailSerializable.class)
public class MStageGroupDetail implements Serializable{
	
	@Id
	@Column(name ="m_stage_id")
	private Integer stageId;
  
  @Id
  @Column(name = "stage_detail_id")
  private int stageDetailId;
  
  @Id
  @Column(name = "company_id")
  private int companyId;

public Integer getStageId() {
	return stageId;
}

public void setStageId(Integer stageId) {
	this.stageId = stageId;
}

public int getStageDetailId() {
	return stageDetailId;
}

public void setStageDetailId(int stageDetailId) {
	this.stageDetailId = stageDetailId;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}

  

  

  
}
