package com.example.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.io.Serializable;
import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.example.serializable.MStageSerializable;


@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "m_stage")
@IdClass(MStageSerializable.class)
public class MStage implements Serializable{
	
	@Id
	@Column(name ="stage_id")
	private Integer stageId;
  
  

  @Column(name = "stage_name")
  private String stageName;
  
  @Column(name = "stage_color")
  private String stageColor;
  
  @Column(name = "stage_status")
  private int stageStatus;
    
  @Column(name = "created_by")
  private UUID createdBy;
  
  @Column(name = "created_at")
  private LocalDateTime createdAt;
  
  @Column(name = "updated_by")
  private UUID updatedBy;
  
  @Column(name = "updated_at")
  private LocalDateTime updatedAt;
  
  @Id
  @Column(name = "company_id")
  private int companyId;

public Integer getStageId() {
	return stageId;
}

public void setStageId(Integer stageId) {
	this.stageId = stageId;
}

public String getStageName() {
	return stageName;
}

public void setStageName(String stageName) {
	this.stageName = stageName;
}

public String getStageColor() {
	return stageColor;
}

public void setStageColor(String stageColor) {
	this.stageColor = stageColor;
}

public int getStageStatus() {
	return stageStatus;
}

public void setStageStatus(int stageStatus) {
	this.stageStatus = stageStatus;
}

public UUID getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(UUID createdBy) {
	this.createdBy = createdBy;
}

public LocalDateTime getCreatedAt() {
	return createdAt;
}

public void setCreatedAt(LocalDateTime createdAt) {
	this.createdAt = createdAt;
}

public UUID getUpdatedBy() {
	return updatedBy;
}

public void setUpdatedBy(UUID updatedBy) {
	this.updatedBy = updatedBy;
}

public LocalDateTime getUpdatedAt() {
	return updatedAt;
}

public void setUpdatedAt(LocalDateTime updatedAt) {
	this.updatedAt = updatedAt;
}

public int getCompanyId() {
	return companyId;
}

public void setCompanyId(int companyId) {
	this.companyId = companyId;
}

  

  
}
