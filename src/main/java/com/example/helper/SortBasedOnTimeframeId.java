package com.example.helper;

import java.util.Comparator;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public class SortBasedOnTimeframeId implements Comparator {

	@Override
	public int compare(Object lhs, Object rhs) {
		// TODO Auto-generated method stub
		try {
            return ((JSONObject) lhs).getInt("projectDetailId") > ((JSONObject) rhs).getInt("projectDetailId") ? 1 : (((JSONObject) lhs)
                .getInt("projectDetailId") < ((JSONObject) rhs).getInt("projectDetailId") ? -1 : 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
	}
}
