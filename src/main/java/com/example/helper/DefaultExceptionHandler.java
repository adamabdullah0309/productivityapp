package com.example.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import io.jsonwebtoken.lang.Assert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@ControllerAdvice
@SuppressWarnings("WeakerAccess")
public class DefaultExceptionHandler extends ResponseEntityExceptionHandler {

	 
    private final Logger log = LoggerFactory.getLogger("DefaultExceptionHandler");

    private final MessageSourceAccessor messageSource;

    @Autowired
    public DefaultExceptionHandler(MessageSourceAccessor messageSource) {
        Assert.notNull(messageSource, "messageSource must not be null");
        this.messageSource = messageSource;
     }

      @ExceptionHandler(DataIntegrityViolationException.class)
      public ResponseEntity<Object> handleApplicationSpecificException(DataIntegrityViolationException ex) {
    	  String message = NestedExceptionUtils.getMostSpecificCause(ex).getMessage();
//          ErrorMessage errorMessage = new ErrorMessage(message); 
          return new ResponseEntity<>(message, HttpStatus.CONFLICT);
      }
}