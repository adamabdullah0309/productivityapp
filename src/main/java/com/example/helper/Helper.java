package com.example.helper;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.controllers.PlannerController;
import com.example.controllers.SortBasedOnMessageId;
import com.example.helper.SortBasedOnTimeframeId;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class Helper {
	private static final Logger logger = LoggerFactory.getLogger(Helper.class);
	public static List<JSONObject> orderBy(List<JSONObject> cont) {
		List<JSONObject> cont1 = new ArrayList<JSONObject>();
		List<JSONObject> baru = new ArrayList<JSONObject>();
        Collections.sort(cont, new SortBasedOnMessageId());
        if(cont.size()>0)
        {
        	for (int i = 0; i < cont.size(); i++) {
        		JSONArray dataAnotherChild = (JSONArray) JSONSerializer.toJSON(cont.get(i).getJSONArray("children"));
                if(dataAnotherChild.size() > 0){
                	List<JSONObject> list = new ArrayList<JSONObject>();
                	list =  Helper.orderBy(cont.get(i).getJSONArray("children"));
                	cont.get(i).put("children", list);
                      
                }
            }
        }
		return cont;
	}
	
	public static List<JSONObject> orderByTimeframe(List<JSONObject> cont) {
		List<JSONObject> cont1 = new ArrayList<JSONObject>();
		List<JSONObject> baru = new ArrayList<JSONObject>();
        Collections.sort(cont, new SortBasedOnTimeframeId());
        if(cont.size()>0)
        {
        	for (int i = 0; i < cont.size(); i++) {
        		JSONArray dataAnotherChild = (JSONArray) JSONSerializer.toJSON(cont.get(i).getJSONArray("children"));
                if(dataAnotherChild.size() > 0){
                	List<JSONObject> list = new ArrayList<JSONObject>();
                	list =  Helper.orderByTimeframe(cont.get(i).getJSONArray("children"));
                	cont.get(i).put("children", list);
                      
                }
            }
        } 
		return cont;
	}
	
	public static JSONObject getdiff(String fromTimeParam, String toTimeParam, Float timeframe, Float budgetHour, String actual)
	{
		//actual adalah dataRequest
		// budgetHour dari database
		// timeframe dari list planner
		JSONObject data = new JSONObject();

//    	timeframe = budgetHour - timeframe;
    	Time from = Time.valueOf(fromTimeParam);
		Time to  =  Time.valueOf(toTimeParam);
		Time batas = Time.valueOf("17:00:00");
		Time masuk = Time.valueOf("08:00:00");
    	Float actualTime = 0.0f;
    	Float requestTime = 0.0f;
    	Float overtimeTime = 0.0f;
    	LocalTime fromTime = LocalTime.parse(fromTimeParam );
		LocalTime toTime = LocalTime.parse(toTimeParam);
		LocalTime batasTime = LocalTime.parse("17:00:00");
		LocalTime masukTime = LocalTime.parse("08:00:00");
		//kondisi 1 utk planner. dia masuk jam kantor semua (bisa )
		//kondisi kedua . dia masuk diantara overtime dan planner contoh from 16:00 to 20:00. yg from planner dan yg to overtime (bisa)
		//kondisi ketiga dia masuk overtime semua (bisa)
		//kondisi keempat from masih di overtime tapi to masuk ke planner. cnth from= 07:00 to 10:00 (bisa)
		logger.info("ini normal = ("+
		masukTime.equals(fromTime) +
		masukTime.isBefore(fromTime)+")" + batasTime.isAfter(fromTime) + " " + 
		masukTime.isBefore(toTime) +"("+ batasTime.isAfter(toTime) + batasTime.equals(toTime)+")" );//harus pakek kondisi equals utk awal dan akhir karena klo jamnya sama jam 5 itu dianggep sama
		logger.info("kondisi kedua = " + 
				(masukTime.isBefore(fromTime) || masukTime.equals(fromTime)) +
		batasTime.isAfter(fromTime)
		+ masukTime.isBefore(toTime) + batasTime.isBefore(toTime)  ) ;
//		logger.info("kondisi ketiga = " +
//		masukTime.isBefore(fromTime)
//				);
		logger.info("kondisi ketiga = "
				+ "(" + masukTime.isBefore(fromTime) + masukTime.equals(toTime)+masukTime.isAfter(fromTime)+")"
				+"("+batasTime.isBefore(fromTime) + batasTime.isAfter(fromTime) + batasTime.equals(fromTime) +")" 
				+" "+"("+ batasTime.isBefore(toTime)+batasTime.isAfter(toTime) +")"
				
				);
		
		logger.info("kondisi keempat = " 
				+ "(" + masukTime.isAfter(fromTime) + masukTime.equals(toTime)+")"
				+"("+batasTime.isBefore(fromTime) + batasTime.isAfter(fromTime) + batasTime.equals(fromTime) +")"
				
				+ masukTime.isBefore(toTime) +"("+ batasTime.isAfter(toTime) + batasTime.equals(toTime)+")" 
				);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		//bisa membedakan mana yang masuk tapi pembagian waktunya gimana ?
		if( (masukTime.equals(fromTime) || masukTime.isBefore(fromTime)) 
				&& masukTime.isBefore(toTime) 
				&& (batasTime.isAfter(toTime) || batasTime.equals(toTime)) )
		{
			logger.info("masuk planner semua");
			// 1. user + list
			// 2. bandingkan user + list dengan batas
			// jika lebih banyak batas maka user masuk semua
			// jika sama dengan batas maka masih masuk semua
			// jika lebih banyak user maka (list planner + user)
			Float hasil = Float.valueOf(actual) + timeframe;
			if(hasil <= budgetHour)
			{
				actualTime = Float.valueOf(actual);
			}
			else
			{
				Float hasil1 = Float.valueOf(actual) + timeframe;
				requestTime = hasil1 - budgetHour;
				actualTime = budgetHour - timeframe;
			}
			
		}
		else if((masukTime.isBefore(fromTime) || masukTime.equals(fromTime)) 
				&& batasTime.isAfter(fromTime) 
				&& ((masukTime.isBefore(toTime) 
				&& batasTime.isBefore(toTime)) || batasTime.isAfter(toTime))
				
				)
		{
			//actual adalah dataRequest
			// budgetHour dari database
			// timeframe dari list planner
			logger.info("kedua");
			// antara overtime dan planner
			//cara menentukan berapa jam overtime, planner dan request budget
			// cari berapa yang masih bisa jadi actualnya
			// cari antara jam actual ke overtime
			// klo masih ada jam nya cari overtime
			// kemudian klo sudah habis semua maka jadikan request semua
			Float hasil = Float.valueOf(actual) + timeframe;
			if(hasil <= budgetHour)
			{
				Float sementaraActual = Float.valueOf(actual);
				
				long differenceFromInitial1 = batas.getTime() - from.getTime();
				int hours = (int) (differenceFromInitial1/(1000 * 60 * 60));
                int mins = (int) ((differenceFromInitial1/(1000*60)) % 60);
                
                int totalSemua = (int) ((hours * 60) + mins);
                Float akhirActual = (float) totalSemua/60;
                String diff = hours + "." + mins; 
                
				actualTime = akhirActual;
				overtimeTime = sementaraActual - actualTime;
			}
			else if(timeframe >= budgetHour)
			{
				requestTime = Float.valueOf(actual);
				logger.info("lebih besar lur");
			}
			else
			{
				Float hasil1 = Float.valueOf(actual) + timeframe;
				requestTime = hasil1 - budgetHour;
				
				Float sementaraActual = budgetHour - timeframe;
				
				long differenceFromInitial1 = batas.getTime() - from.getTime();
				int hours = (int) (differenceFromInitial1/(1000 * 60 * 60));
                int mins = (int) ((differenceFromInitial1/(1000*60)) % 60);
                
                int totalSemua = (int) ((hours * 60) + mins);
                Float akhirActual = (float) totalSemua/60;
                String diff = hours + "." + mins; 
                
				actualTime = akhirActual;
				overtimeTime = sementaraActual - actualTime;
				//cara mencari actual
				// 1. hitung jarak jam sampek ke toko tutup
				// 2. klo sudah itu jadi actual
				// 3. sisa nya overtime
			}
		}
		else if( (masukTime.isAfter(fromTime) || masukTime.equals(toTime)) && 
				  (batasTime.isBefore(fromTime) || batasTime.isAfter(fromTime) || batasTime.equals(fromTime))
				  && masukTime.isBefore(toTime) && (  batasTime.isAfter(toTime) || batasTime.equals(toTime) )
				)
			{
			//kondisi keempat from masih di overtime tapi to masuk ke planner. cnth from= 07:00 to 10:00 
			Float hasil = Float.valueOf(actual) + timeframe;
			logger.info("keempat");
			if(hasil <= budgetHour)
			{
				Float sementaraActual = Float.valueOf(actual);
				
				long differenceFromInitial1 = to.getTime() -  masuk.getTime();
				int hours = (int) (differenceFromInitial1/(1000 * 60 * 60));
                int mins = (int) ((differenceFromInitial1/(1000*60)) % 60);
                
                int totalSemua = (int) ((hours * 60) + mins);
                Float akhirActual = (float) totalSemua/60;
                String diff = hours + "." + mins; 

                logger.info("total semua = " + sementaraActual);
				actualTime = akhirActual;
				overtimeTime = sementaraActual - actualTime;
			}
			else if(timeframe >= budgetHour)
			{
				requestTime = Float.valueOf(actual);
				logger.info("lebih besar lur");
			}
			else
			{
				Float hasil1 = Float.valueOf(actual) + timeframe;
				requestTime = hasil1 - budgetHour;
				
				Float sementaraActual = budgetHour - timeframe;
				logger.info("sementaraActual == " + sementaraActual);
				long differenceFromInitial1 = masuk.getTime() -  from.getTime();
				int hours = (int) (differenceFromInitial1/(1000 * 60 * 60));
                int mins = (int) ((differenceFromInitial1/(1000*60)) % 60);
                
                int totalSemua = (int) ((hours * 60) + mins);
                Float akhirActual = (float) totalSemua/60;
                String diff = hours + "." + mins; 
                
				overtimeTime = akhirActual;
				actualTime = sementaraActual-akhirActual ;
			}
			}
		else
		{
			logger.info("ketiga");
			Float hasil = Float.valueOf(actual) + timeframe;
			if(hasil <= budgetHour)
			{
				overtimeTime = Float.valueOf(actual);
			}
			else
			{
				Float hasil1 = Float.valueOf(actual) + timeframe;
				requestTime = hasil1 - budgetHour;
				overtimeTime = budgetHour - timeframe;
			}
		}
		data.put("requestTime", requestTime);
		data.put("overtimeTime", overtimeTime);
		data.put("actualTime", actualTime);
		return data;
	}

}
