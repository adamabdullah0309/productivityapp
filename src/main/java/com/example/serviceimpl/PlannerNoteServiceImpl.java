package com.example.serviceimpl;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.PlannerNote;
import com.example.repository.PlannerNoteRepository;
import com.example.service.PlannerNoteService;
import com.example.service.PlannerService;

@Service
public class PlannerNoteServiceImpl implements PlannerNoteService {
	private static final Logger logger = LoggerFactory.getLogger(PlannerService.class);
	

	@Autowired
	private PlannerNoteRepository PlannerNoteRepository;
	
	@Autowired
	EntityManager em;

	@Override
	@Transactional
	public void delete(ArrayList<PlannerNote> isiPlannerNote) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from tm_planner_note where "
				+ "planner_id = :id "
				+ "and project_id = :project_id "
				+ "and project_detail_id = :project_detail_id"
				+ " and timeframe_id = :timeframe_id"
				+ " "
				+ " and company_id = :company_id "
				+ "  ")
		.setParameter("id", isiPlannerNote.get(0).getPlannerId())
		.setParameter("project_id", isiPlannerNote.get(0).getProjectId())
		.setParameter("project_detail_id", isiPlannerNote.get(0).getProjectDetailId())
		.setParameter("timeframe_id", isiPlannerNote.get(0).getTimeframeId())
		.setParameter("company_id", isiPlannerNote.get(0).getCompanyId())
		.executeUpdate();
	}

	@Override
	public void save(ArrayList<PlannerNote> isiPlannerNote) {
		// TODO Auto-generated method stub
		PlannerNoteRepository.saveAll(isiPlannerNote);
	}


}
