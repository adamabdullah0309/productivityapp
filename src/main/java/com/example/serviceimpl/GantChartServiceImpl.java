package com.example.serviceimpl;

import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.repository.GantChartRepository;
import com.example.service.GantChartService;

import net.sf.json.JSONArray;

@Service
public class GantChartServiceImpl implements GantChartService {
	@Autowired
	EntityManager em;
	
	@Autowired
	private GantChartRepository GantChartRepository;

	@Override
	public List<Object[]> listGantChart(int int1, UUID user_uuid,JSONArray projectId, JSONArray projectPartyId) {
		// TODO Auto-generated method stub
		String query = "select * from list_gant_chart("+int1+", '"+user_uuid+"', array"+projectId+"\\:\\:integer[], array"+projectPartyId+"\\:\\:integer[])\r\n" + 
				"";
		return em.createNativeQuery(query).getResultList();
	}

	@Override
	public List<Object[]> detailGantChart(int company_id, int mProjectId, JSONArray projectDetailId, JSONArray taskId,
			JSONArray contactIdArray, UUID user_uuid) {
		// TODO Auto-generated method stub
		String query = "select * from contoh_gantchart("+company_id+", array"+projectDetailId+"\\:\\:integer[], array"+taskId+"\\:\\:integer[], array"+contactIdArray.toString().replace("\"" , "\\'")+"\\:\\:uuid[], "+mProjectId+", '"+user_uuid+"')\r\n" + 
				"";
		return em.createNativeQuery(query).getResultList();
//		return GantChartRepository.detailGantChart(company_id, mProjectId, projectDetailId, taskId,
//			contactIdArray, user_uuid);
	}

	@Override
	public List<Object[]> ListGantChartSubgroup(int company, int project) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select coba.*, b.task_id, case when c.grouping_nama is not null then c.grouping_nama else b.task_nama end as nama from project_detail as coba\r\n" + 
				"				left join task as b on coba.m_task_id = b.task_id and coba.company_id = b.company_id\r\n" + 
				"				left join grouping as c on c.grouping_id = coba.m_grouping_id and c.company_id = coba.company_id\r\n" + 
				"				where\r\n" + 
				"				coba.m_project_id = :project\r\n" + 
				"				and coba.parent_id = 1\r\n" + 
				"				and coba.company_id = :company"
				+ " and EXISTS ( SELECT 1 FROM project_detail AS b1 WHERE coba.project_detail_id = b1.parent_id and coba.m_project_id = b1.m_project_id and coba.company_id = b1.company_id )  ")
				.setParameter("project", project)
				.setParameter("company", company)
				.getResultList();
	}

	@Override
	public List<Object[]> ListGantChartTask(int company, int project, JSONArray projectDetail) {
		// TODO Auto-generated method stub
		String projectDetailString = "";
		if(projectDetail.size()>0)
		{
			String formattedString = projectDetail.toString()
				    .replace("[", "")
				    .replace("]", "");
			projectDetailString = " and parent_id in ( "+formattedString+" )";
		}
		else
		{
			projectDetailString = "";
		}
		return em.createNativeQuery("WITH RECURSIVE subordinates AS (\r\n" + 
				"	SELECT\r\n" + 
				"		project_detail_id,\r\n" + 
				"		m_project_id,\r\n" + 
				"		parent_id,\r\n" + 
				"		m_task_id,\r\n" + 
				"		m_grouping_id,\r\n" + 
				"		company_id\r\n" + 
				"	FROM\r\n" + 
				"		project_detail\r\n" + 
				"	WHERE\r\n" + 
				"		m_project_id = :project\r\n" 
				+ projectDetailString + 
				"		and company_id = :company\r\n" + 
				"	UNION\r\n" + 
				"		SELECT\r\n" + 
				"			e.project_detail_id,\r\n" + 
				"			e.m_project_id,\r\n" + 
				"			e.parent_id,\r\n" + 
				"			e.m_task_id,\r\n" + 
				"			e.m_grouping_id,\r\n" + 
				"			e.company_id\r\n" + 
				"		FROM\r\n" + 
				"			project_detail e\r\n" + 
				"		INNER JOIN subordinates s ON s.project_detail_id = e.parent_id\r\n" + 
				"		where\r\n" + 
				"		e.m_project_id = :project\r\n" + 
				"		and e.company_id = :company\r\n" + 
				") SELECT\r\n" + 
				"	subordinates.*, b.task_nama, c.grouping_nama\r\n" + 
				"FROM\r\n" + 
				"	subordinates\r\n" + 
				"	left join task as b on subordinates.m_task_id = b.task_id and subordinates.company_id = b.company_id\r\n" + 
				"	left join grouping as c on c.grouping_id = subordinates.m_grouping_id and c.company_id = subordinates.company_id\r\n" + 
				"	where\r\n" + 
				"	NOT EXISTS ( SELECT 1 FROM project_detail AS b1 WHERE subordinates.project_detail_id = b1.parent_id and subordinates.m_project_id = b1.m_project_id and subordinates.company_id = b1.company_id ) ")
				.setParameter("company", company)
				.setParameter("project", project)
//				.setParameter("projectDetail", projectDetail)
				.getResultList();
	}

	@Override
	public List<Object[]> dataDependency(int project, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select * from project_detail_dependency\r\n" + 
				"where m_project_id = :project\r\n" + 
				"and company_id = :company"
				+ " order by\r\n" + 
				"project_detail_id, no_dependency ")
				.setParameter("project", project)
				.setParameter("company", company)
				.getResultList();
	}

}
