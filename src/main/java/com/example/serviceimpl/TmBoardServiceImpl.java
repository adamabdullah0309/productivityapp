package com.example.serviceimpl;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.TmBoard;
import com.example.entity.TmProjectTimeframe;
import com.example.entity.TmProjectTimeframeBudgethour;
import com.example.repository.TmBoardRepository;
import com.example.repository.TmProjectTimeframeBudgethourRepository;
import com.example.service.TmBoardService;

@Service
public class TmBoardServiceImpl implements TmBoardService {
	private static final Logger logger = LoggerFactory.getLogger(TmBoardServiceImpl.class);
	
	@Autowired
	private TmBoardRepository TmBoardRepository;
	
	@Autowired
	EntityManager em;

	@Override
	public List<Object[]> dataTaskAllFromBoard(int company, int project, UUID user_uuid, int boardId) {
		// TODO Auto-generated method stub
		return TmBoardRepository.listBoardTemplate(company, project,user_uuid, boardId);
	}

	@Override
	public List<Object[]> dataBoardGrouping(int company, int project, UUID user_uuid, int boardId) {
		// TODO Auto-generated method stub
		return TmBoardRepository.listBoardGrouping(company, project,user_uuid, boardId);
	}

	@Override
	public List<Object[]> listStageBoard(int projectId, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"	j.board_id,\r\n" + 
				"	l.m_stage_id,\r\n" + 
				"	m.stage_name,\r\n" + 
				"	j.company_id,"
				+ " l.tm_board_stage_id \r\n" + 
				"FROM\r\n" + 
				"	tm_board AS j\r\n" + 
				"	LEFT JOIN tm_board_stage AS l ON l.tm_board_id = j.board_id \r\n" + 
				"	AND l.company_id = j.company_id \r\n" + 
				"	right join m_stage as m on m.stage_id = l.m_stage_id and m.company_id = l.company_id\r\n" + 
				"WHERE\r\n" + 
				"	l.tm_board_id = :project \r\n" + 
				"	AND l.company_id = :company")
				.setParameter("company", company)
				.setParameter("project", projectId)
				.getResultList();
				
	}

	@Override
	public List<Object[]> listdataTask(int projectId, int company, UUID user_uuid, int boardId) {
		// TODO Auto-generated method stub
		return TmBoardRepository.listBoardTask(company, projectId, user_uuid, boardId);
	}

	@Override
	public List<Object[]> listDrawerProject(int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.project_id,\r\n" + 
				"	A.project_name \r\n" + 
				"FROM\r\n" + 
				"	dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', 'SELECT project_id, project_name, company_id, project_status, project_step, project_step_productivitapp, status_timeframe FROM project ' ) AS A ( project_id INTEGER, project_name VARCHAR, company_id INTEGER, project_status INTEGER, project_step integer, project_step_productivitapp integer, status_timeframe integer )\r\n" + 
				"	LEFT JOIN tm_board AS b ON b.m_project_id = A.project_id \r\n" + 
				"	AND b.company_id = A.company_id \r\n" + 
				"WHERE\r\n" + 
				"	b.board_id IS NULL \r\n" + 
				"	AND A.company_id = :company \r\n" + 
				"	and a.status_timeframe = 3\r\n" + 
				"	and a.project_step = 2\r\n" + 
				"	and a.project_step_productivitapp = 2")
				.setParameter("company", company)
				.getResultList();
	}

	@Override
	public List<Object[]> listProjectBoard(int company, UUID user_uuid) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select\r\n" + 
				"b.board_id,\r\n" + 
				"a.project_id,\r\n" + 
				"a.project_name,\r\n" + 
				"a.company_id\r\n" + 
				"from \r\n" + 
				"dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', 'SELECT project_id, project_name, company_id, project_status, status_timeframe, project_step_productivitapp, project_step FROM project ' ) AS a ( project_id INTEGER, project_name VARCHAR, company_id INTEGER, project_status integer, status_timeframe INTEGER, project_step_productivitapp integer, project_step integer ) \r\n" + 
				"left join tm_board as b on b.m_project_id= a.project_id and b.company_id = a.company_id\r\n" + 
				"left join project_detail as c on a.project_id = c.m_project_id and c.company_id = c.company_id \r\n" + 
				"left join project_detail_karyawan as d on d.project_id = c.m_project_id and d.project_detail_id = c.project_detail_id and d.company_id = c.company_id\r\n" + 
				"where \r\n" + 
				"b.board_id is not null\r\n" + 
				"and a.company_id = :company \r\n" + 
				"and d.contact_id = :user_uuid"
				+ " and a.status_timeframe = 3 "
				+ " and a.project_step = 2 "
				+ " and a.project_step_productivitapp = 2  \r\n" + 
				"GROUP BY\r\n" + 
				"b.board_id,\r\n" + 
				"a.project_id,\r\n" + 
				"a.project_name,\r\n" + 
				"a.company_id")
				.setParameter("company", company)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
	}

	@Override
	public List<Object[]> listStageDrawer(int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select a.stage_group_id, a.stage_group_name, c.stage_id, c.stage_name, a.company_id  from m_stage_group as a\r\n" + 
				"left join m_stage_group_detail as b on b.m_stage_group_id = a.stage_group_id and b.company_id = a.company_id\r\n" + 
				"left join m_stage as c on c.stage_id = b.m_stage_id and c.company_id = b.company_id\r\n" + 
				"where\r\n" + 
				"a.stage_group_status = 1\r\n" + 
				"and a.company_id = :company")
				.setParameter("company", company)
				.getResultList();
	}

	@Override
	public List<Object[]> nextval() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('board_sequence')").getResultList();
	}

	@Override
	public void save(TmBoard data) {
		// TODO Auto-generated method stub
		TmBoardRepository.save(data);
	}

	@Override
	public List<Object[]> listBoardDetailAtt(int company, int board, int boardProjectDetailId) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT a.*, b.doc_type_name FROM tm_board_project_detail_attachement as a\r\n" + 
				"left join m_doc_type as b on a.doc_type = b.doc_type_id and a.company_id = b.company_id\r\n" + 
				"where \r\n" + 
				"a.tm_board_id = :board \r\n" + 
				"and a.tm_board_project_detail_id = :boardProjectDetailId "
				+ " and a.company_id = :company\r\n" + 
				"\r\n" + 
				"")
				.setParameter("company", company)
				.setParameter("boardProjectDetailId", boardProjectDetailId)
				.setParameter("board", board)
				.getResultList();
	}

	@Override
	public List<Object[]> listDiscussionTask(int boardId,  int boardProjectDetailId, int company_id, UUID user_uuid) {
		// TODO Auto-generated method stub
		return TmBoardRepository.listDiscussionTask(user_uuid, company_id, boardId, boardProjectDetailId);
	}

	

}
