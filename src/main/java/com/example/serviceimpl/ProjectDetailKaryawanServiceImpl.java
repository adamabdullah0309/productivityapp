package com.example.serviceimpl;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Planner;
import com.example.entity.ProjectDetail;
import com.example.entity.ProjectDetailKaryawan;
import com.example.service.PlannerService;
import com.example.service.ProjectDetailKaryawanService;
import com.example.service.ProjectDetailService;
import com.example.repository.ProjectDetailKaryawanRepository;
import com.example.serializable.PlannerSerializable;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
//@CacheConfig(cacheNames={"projectCaching"})
public class ProjectDetailKaryawanServiceImpl implements ProjectDetailKaryawanService{
	private static final Logger logger = LoggerFactory.getLogger(ProjectDetailKaryawanService.class);
	

	@Autowired
	private ProjectDetailKaryawanRepository ProjectDetailKaryawanRepository;
	
	@Autowired
	EntityManager em;

	@Override
//	@Cacheable("findProjectDetailKaryawan")
	public List<Object[]> getProjectDetailKaryawa(int project, int detail, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select a.project_detail_id, a.project_detail_karyawan_id, a.project_id, a.company_id, cast(a.contact_id as varchar) from project_detail_karyawan as a "
				+ " where a.project_id = :project and a.project_detail_id = :detail and a.company_id = :company ")
				.setParameter("project", project)
				.setParameter("detail", detail)
				.setParameter("company", company)
				.getResultList();
	}

	@Override
//	@CachePut(value = "findProjectDetailKaryawan", key = "#ProjectDetailKaryawan.projectDetailId + #ProjectDetailKaryawan.mProjectId + #ProjectDetail.companyId + #ProjectDetailKaryawan.projectDetailKaryawanId")
	public void save(List<ProjectDetailKaryawan> dataKaryawan) {
		// TODO Auto-generated method stub
		ProjectDetailKaryawanRepository.saveAll(dataKaryawan);
//		for (ProjectDetailKaryawan tasklistLooping : dataKaryawan) {
////			em.persist(tasklistLooping);
//			ProjectDetailKaryawanRepository.save(tasklistLooping);
//		}
	}
	
	@Override
	@Transactional
	public void delete(int project,  int company) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from project_detail_karyawan where project_id = :project and company_id = :company")
		.setParameter("project", project).setParameter("company", company).executeUpdate();
	}

}
