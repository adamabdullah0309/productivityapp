package com.example.serviceimpl;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Timesheet;
import com.example.service.TimesheetService;
import com.example.repository.TimesheetRepository;
import com.example.serializable.TimesheetSerializable;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class TimesheetServiceImpl implements TimesheetService {
	private static final Logger logger = LoggerFactory.getLogger(TimesheetServiceImpl.class);
	

	@Autowired
	private TimesheetRepository TimesheetRepository;
	
	@Autowired
	EntityManager em;


	@Override
	public void save(Timesheet data) {
		// TODO Auto-generated method stub
		TimesheetRepository.save(data);
	}


	@Override
	public List<Object[]> nextval() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('timesheet_sequence')").getResultList();
	}


	@Override
	public Optional<Timesheet> findIdAndIdCompany(int int1, int int2) {
		// TODO Auto-generated method stub
		return TimesheetRepository.findById(new TimesheetSerializable(int1, int2));
	}


	@Override
	public List<Object[]> listContent(String paramAktif, int int1, UUID user_uuid) {
		// TODO Auto-generated method stub
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and a.status = 1 ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and a.status = 0 ";
		}
		else
		{
			cari = "";
		}
		return em.createNativeQuery("select \r\n" + 
				"a.timesheet_id,\r\n" + 
				"a.from_timesheet,\r\n" + 
				"a.to_timesheet,\r\n" + 
				"a.project_name_timesheet,\r\n" + 
				"a.party,\r\n" + 
				"a.task,\r\n" + 
				"a.tag,\r\n" + 
				"a.note,\r\n" + 
				"a.location_timesheet,\r\n" + 
				"a.billable_timesheet,\r\n" + 
				"cast(a.created_by as varchar),\r\n" + 
				"a.created_at,\r\n" + 
				"a.company_id,\r\n" + 
				"a.status,"
				+ "a.date_timesheet\r\n" + 
				"from timesheet as a where\r\n" + 
				"a.created_by = :user \r\n" + 
				"and a.company_id = :company\r\n" + 
				cari + 
				" ")
				.setParameter("user", user_uuid)
				.setParameter("company", int1)
				.getResultList();
	}


	@Override
	public List<Object[]> listDetailTimesheet(int int1, int int2, UUID user_uuid) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"a.timesheet_id,\r\n" + 
				"a.timesheet_date,\r\n" + 
				"a.timesheet_from,\r\n" + 
				"a.timesheet_to,\r\n" + 
				"a.timesheet_party,\r\n" + 
				"a.timesheet_project_detail_id,\r\n" + 
				"a.timesheet_project_id,\r\n" + 
				"a.timesheet_company_id,\r\n" + 
				"a.timesheet_building,\r\n" + 
				"a.timesheet_address,\r\n" + 
				"a.timesheet_status,\r\n" + 
				"a.timesheet_overtime,\r\n" + 
				"a.timesheet_actual,\r\n" + 
				" cast(a.timesheet_contact_id as varchar),\r\n" + 
				"	case when c.task_id is not null then c.task_id else d.task_id end as task_id,\r\n" + 
				"	case when c.task_nama is not null then c.task_nama else d.task_nama end as task_nama,\r\n" + 
				"	b.m_grouping_id,\r\n" + 
				"	e.grouping_nama,\r\n" + 
				"	f.timesheet_note_id,\r\n" + 
				"	f.note,\r\n" + 
				"	f.date_note,"
				+ " a.timesheet_request_budget\r\n" + 
				"FROM\r\n" + 
				"	timesheet\r\n" + 
				"	AS A LEFT JOIN project_detail AS b ON A.timesheet_project_id = b.m_project_id \r\n" + 
				"	AND A.timesheet_project_detail_id = b.project_detail_id \r\n" + 
				"	AND A.timesheet_company_id = b.company_id\r\n" + 
				"	LEFT JOIN task AS C ON C.task_id = A.timesheet_task_id \r\n" + 
				"	AND C.company_id = A.timesheet_company_id\r\n" + 
				"	LEFT JOIN task AS d ON d.task_id = b.m_task_id \r\n" + 
				"	AND d.company_id = b.company_id\r\n" + 
				"	LEFT JOIN GROUPING AS e ON e.grouping_id = b.m_grouping_id \r\n" + 
				"	AND e.company_id = b.company_id\r\n" + 
				"	left join timesheet_note as f on f.timesheet_id = a.timesheet_id and f.timesheet_company_id = a.timesheet_company_id\r\n" + 
				"	where\r\n" + 
				"	1 = 1\r\n" + 
				"	and a.timesheet_contact_id = :user\r\n" + 
				"	and a.timesheet_company_id = :company\r\n" + 
				"	and a.timesheet_id = :id \r\n" + 
				"	" + 
				" ")
				.setParameter("user", user_uuid)
				.setParameter("id", int1)
				.setParameter("company", int2)
				.getResultList();
	}


	@Override
	public List<Object[]> listTimeframe(String paramAktif, int int1, UUID user_uuid, String startDate, String endDate) {
		// TODO Auto-generated method stub
		String parameter = "";
		String startDateString = "";
		String endDateString = "";
		if(paramAktif.toLowerCase().equals("y"))
		{
			parameter = " and b.status = 1 ";
		}
		else if (paramAktif.toLowerCase().equals("n")) {
			parameter = " and b.status = 0 ";
		}
		else
		{
			parameter = "";
		}
		
		if(!startDate.isEmpty())
		{
			startDateString = " and a.date_budgethour >= '" + startDate +"' ";
		}
		else
		{
			startDateString = "";
		}
		
		if(!endDate.isEmpty())
		{
			endDateString = " and a.date_budgethour <= '" + endDate+"' ";
		}
		else
		{
			endDateString = "";
		}
		
		
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.date_budgethour,\r\n" + 
				"	A.tm_project_id,\r\n" + 
				"	f.project_name,\r\n" + 
				"	A.project_detail_id,\r\n" + 
				"	b.m_task_id,\r\n" + 
				"	b.m_grouping_id,\r\n" + 
				"	case when c.grouping_nama is null then n.task_nama else c.grouping_nama end as task,\r\n" + 
				"	A.budget_hour,\r\n" + 
				"	cast (d.contact_id as varchar),\r\n" + 
				"	 a.timeframe_id,\r\n" + 
				"	 d.company_id,"
				+ " f.project_party\r\n" + 
				"FROM\r\n" + 
				"	tm_project_timeframe_budgethour\r\n" + 
				"	AS A LEFT JOIN project_detail AS b ON b.project_detail_id = A.project_detail_id \r\n" + 
				"	AND A.tm_project_id = b.m_project_id \r\n" + 
				"	AND A.company_id = b.company_id\r\n" + 
				"	LEFT JOIN project_detail_karyawan AS d ON d.project_detail_id = b.project_detail_id \r\n" + 
				"	AND d.project_id = b.m_project_id\r\n" + 
				"	LEFT JOIN GROUPING AS C ON C.grouping_id = b.m_grouping_id \r\n" + 
				"	AND C.company_id = b.company_id\r\n" + 
				"	LEFT JOIN task AS n ON b.m_task_id = n.task_id \r\n" + 
				"	AND b.company_id = n.company_id\r\n" + 
				"	LEFT JOIN dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', '"
				+ "SELECT project_id, project_name, company_id, project_status, project_party, project_step_productivitapp, status_timeframe FROM project ' ) AS f ( project_id INTEGER, project_name VARCHAR, company_id INTEGER, project_status INTEGER, project_party integer, project_step_productivitapp integer, status_timeframe integer) ON A.company_id = f.company_id \r\n" + 
				"	AND A.tm_project_id = f.project_id \r\n" + 
				"WHERE\r\n" + 
				"	1 = 1 \r\n" + 
				"	and d.contact_id = :user_uuid\r\n" + 
				"	and d.company_id = :company\r\n" + startDateString + endDateString +
				"	AND NOT EXISTS (\r\n" + 
				"	SELECT\r\n" + 
				"		1 \r\n" + 
				"	FROM\r\n" + 
				"		project_detail AS b1 \r\n" + 
				"	WHERE\r\n" + 
				"		A.project_detail_id = b1.parent_id \r\n" + 
				"		AND A.tm_project_id = b1.m_project_id \r\n" + 
				"	AND A.company_id = b1.company_id \r\n" + 
				"	) "
				+ " AND f.project_status = 1 \r\n" + 
				"					AND f.project_step_productivitapp = 2 \r\n" + 
				"					AND f.status_timeframe = 3   \r\n" + 
				"	" + parameter)
				.setParameter("company", int1)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
	}


	@Override
	public List<Object[]> listTimesheet(String paramAktif, int int1, UUID user_uuid, String string, String string2) {
		// TODO Auto-generated method stub
		String parameter = "";
		String startDateString = "";
		String endDateString = "";
		if(paramAktif.toLowerCase().equals("y"))
		{
			parameter = " and a.timesheet_status = 1 ";
		}
		else if (paramAktif.toLowerCase().equals("n")) {
			parameter = " and a.timesheet_status = 0 ";
		}
		else
		{
			parameter = "";
		}
		
		if(!string.isEmpty())
		{
			startDateString = " and a.timesheet_date >= '" + string +"' ";
		}
		else
		{
			startDateString = "";
		}
		
		if(!string2.isEmpty())
		{
			endDateString = " and a.timesheet_date <= '" + string2+"' ";
		}
		else
		{
			endDateString = "";
		}
		return em.createNativeQuery("SELECT\r\n" + 
				"				a.timesheet_id,\r\n" + 
				"				a.timesheet_date,\r\n" + 
				"				a.timesheet_from,\r\n" + 
				"				a.timesheet_to,\r\n" + 
				"				a.timesheet_party,\r\n" + 
				"				a.timesheet_project_detail_id,\r\n" + 
				"				a.timesheet_project_id,\r\n" + 
				"				a.timesheet_company_id,\r\n" + 
				"				a.timesheet_building,\r\n" + 
				"				a.timesheet_address,\r\n" + 
				"				a.timesheet_status,\r\n" + 
				"				a.timesheet_overtime,\r\n" + 
				"				a.timesheet_actual,\r\n" + 
				"				 cast (a.timesheet_contact_id as varchar),\r\n" + 
				"					case when c.task_id is not null then c.task_id else d.task_id end as task_id,\r\n" + 
				"					case when c.task_nama is not null then c.task_nama else d.task_nama end as task_nama,\r\n" + 
				"					b.m_grouping_id,\r\n" + 
				"					e.grouping_nama,\r\n" + 
				"				  a.timesheet_request_budget,\r\n" + 
				"					\r\n" + 
				"					f.project_name\r\n" + 
				"				FROM\r\n" + 
				"					timesheet\r\n" + 
				"					AS A LEFT JOIN project_detail AS b ON A.timesheet_project_id = b.m_project_id \r\n" + 
				"					AND A.timesheet_project_detail_id = b.project_detail_id \r\n" + 
				"					AND A.timesheet_company_id = b.company_id\r\n" + 
				"					LEFT JOIN task AS C ON C.task_id = A.timesheet_task_id \r\n" + 
				"					AND C.company_id = A.timesheet_company_id\r\n" + 
				"					LEFT JOIN task AS d ON d.task_id = b.m_task_id \r\n" + 
				"					AND d.company_id = b.company_id\r\n" + 
				"					LEFT JOIN GROUPING AS e ON e.grouping_id = b.m_grouping_id \r\n" + 
				"					AND e.company_id = b.company_id\r\n" + 
				"					LEFT JOIN dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', 'SELECT project_id, project_name, company_id, project_status FROM project ' ) AS f ( project_id INTEGER, project_name VARCHAR, company_id INTEGER, project_status INTEGER ) ON a.timesheet_company_id = f.company_id \r\n" + 
				"				AND a.timesheet_project_id = f.project_id " + 
				"	where\r\n" + 
				"	1 = 1\r\n" + parameter + startDateString + endDateString+ 
				"	and a.timesheet_contact_id = :user_uuid\r\n" + 
				"	and a.timesheet_company_id = :int1")
				.setParameter("int1", int1)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
	}


	@Override
	public List<Object[]> listTimesheet(int project, int projectDetail, UUID user_uuid, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT timesheet_actual, timesheet_overtime, timesheet_request_budget FROM \"timesheet\"\r\n" + 
				"where\r\n" + 
				"timesheet_project_id = :project\r\n" + 
				"and timesheet_project_detail_id = :projectDetail\r\n" + 
				"and timesheet_company_id = :company\r\n" + 
				"and timesheet_contact_id = :user_uuid")
				.setParameter("project", project)
				.setParameter("projectDetail", projectDetail)
				.setParameter("user_uuid", user_uuid)
				.setParameter("company", company)
				.getResultList();
	}


	@Override
	public List<Object[]> listTimesheetId(int project, int projectDetail, UUID user_uuid, int company, int timesheetId) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT timesheet_actual, timesheet_overtime, timesheet_request_budget FROM \"timesheet\"\r\n" + 
				"where\r\n" + 
				"timesheet_project_id = :project\r\n" + 
				"and timesheet_project_detail_id = :projectDetail\r\n" + 
				"and timesheet_company_id = :company\r\n" + 
				"and timesheet_contact_id = :user_uuid\r\n" + 
				"and timesheet_id != :timesheetId")
				.setParameter("project", project)
				.setParameter("projectDetail", projectDetail)
				.setParameter("user_uuid", user_uuid)
				.setParameter("company", company)
				.setParameter("timesheetId", timesheetId)
				.getResultList();
	}




	

	
	
}
