package com.example.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.ProjectDetail;
import com.example.entity.TmProjectTimeframe;
import com.example.repository.TmProjectTimeframeRepository;
import com.example.service.MStageGroupDetailService;

@Service
public class MStageGroupDetailServiceImpl implements MStageGroupDetailService {
	private static final Logger logger = LoggerFactory.getLogger(MStageGroupDetailServiceImpl.class);

	@Autowired
	EntityManager em;
	
	@Override
	public List<Object[]> getStageDetail(int stageId, int companyid) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select m_stage_id from m_stage_group_detail as a\r\n" + 
				"where\r\n" + 
				"a.m_stage_group_id = :stageId \r\n" + 
				"and a.company_id = :companyid"
				+ " and stage_detail_id = 1 ")
				.setParameter("stageId", stageId)
				.setParameter("companyid", companyid)
				.getResultList();
	}

	@Override
	public List<Object[]> getStageDetailAll(int stageId, int companyid) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select * from m_stage_group_detail as a\r\n" + 
				"where\r\n" + 
				"a.m_stage_group_id = :stageId \r\n" + 
				"and a.company_id = :companyid"
				+ " order by m_stage_id "
				+ "  ")
				.setParameter("stageId", stageId)
				.setParameter("companyid", companyid)
				.getResultList();
	}

	



}
