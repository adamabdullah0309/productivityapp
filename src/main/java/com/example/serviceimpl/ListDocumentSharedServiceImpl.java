package com.example.serviceimpl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.ListDocumentShared;
import com.example.repository.ListDocumentSharedRepository;
import com.example.service.ListDocumentSharedService;
import com.example.serializable.ListDocumentSharedSerializable;

@Service
public class ListDocumentSharedServiceImpl implements ListDocumentSharedService {
	private static final Logger logger = LoggerFactory.getLogger(ListDocumentSharedServiceImpl.class);
	
	@Autowired
	EntityManager em;

	@Autowired
	private ListDocumentSharedRepository ListDocumentSharedRepository;


	@Override
	@Transactional
	public void save(List<ListDocumentShared> masterSharedDocument) {
		// TODO Auto-generated method stub
		
		em.createNativeQuery("delete from list_document_shared where list_document_id = :id")
		.setParameter("id", masterSharedDocument.get(0).getListDocumentId()).executeUpdate();
		
		for (ListDocumentShared tasklistLooping : masterSharedDocument) {
//			em.persist(tasklistLooping);
			
			ListDocumentSharedRepository.save(tasklistLooping);
		}
	}


	@Override
	public List<Object[]> detailDocumentShared(UUID user_uuid, int doc_id) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select "
				+ "list_document_share_id,"
				+ "cast(list_document_share_user as varchar),"
				+ " list_document_share_start_date,"
				+ "list_document_share_end_date,"
				+ "list_document_share_role,"
				+ "list_document_share_user_group,"
				+ "list_document_share_so,"
				+ "list_document_id,"
				+ "cast (user_id as varchar)"
				+ ", company_id "
				+ "from list_document_shared where list_document_id = :doc_id and user_id = :user_uuid")
				.setParameter("user_uuid", user_uuid)
				.setParameter("doc_id", doc_id)
				.getResultList();
	}


	@Override
	public List<Object[]> listSharedWithMe(UUID user_uuid, int company_id, int group_id, int so_id, Date startDate,
			Date endDate, String status) {
		// TODO Auto-generated method stub
		String Status = "";
		if (status.equals("y"))
		{
			Status  = " and a.list_document_status = \'t\' ";
		}
		else if(status.equals("n"))
		{
			Status  = " and a.list_document_status = \'f\' ";
		}
		else
		{
			Status = "";
		}
		
		String userId = "";
//		if (user_uuid.toString().isEmpty() != true)
//		{
			userId  = " b.list_document_share_user = \'" + user_uuid.toString() + "\' ";
//		}
//		else
//		{
//			userId = "";
//		}
		
		String groupuserId = "";
//		if (group_id != 0)
//		{
			groupuserId  = " or b.list_document_share_user_group =  " + group_id + " ";
//		}
//		else
//		{
//			groupuserId = "";
//		}
		
		String soId = "";
//		if (so_id != 0)
//		{
			soId  = " or b.list_document_share_so =  " + so_id + " ";
//		}
//		else
//		{
//			soId = "";
//		}
		return em.createNativeQuery("SELECT DISTINCT ON\r\n" + 
				"	( FINAL.list_document_id )\r\n" + 
				"	FINAL.list_document_id,\r\n" + 
				"	FINAL.list_document_title,\r\n" + 
				"	e.caption as doc_type,\r\n" + 
				"	FINAL.category,\r\n" + 
				"	FINAL.list_document_share_role,\r\n" + 
				"	cast (FINAL.list_document_share_user as varchar),\r\n" + 
				"	FINAL.list_document_share_user_group,\r\n" + 
				"	FINAL.list_document_share_so ,\r\n" + 
				"	final.list_document_share_start_date,\r\n" + 
				"	final.list_document_share_end_date\r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT A\r\n" + 
				"		.list_document_id,\r\n" + 
				"		A.list_document_title,\r\n" + 
				"		list_document_type,\r\n" + 
				"		b.list_document_share_role,\r\n" + 
				"		(\r\n" + 
				"		SELECT\r\n" + 
				"			string_agg ( b1.caption, ', ' ) \r\n" + 
				"		FROM\r\n" + 
				"			list_document_summary AS A1\r\n" + 
				"			LEFT JOIN m_category AS b1 ON A1.id_category = b1.ID \r\n" + 
				"		WHERE\r\n" + 
				"			A1.document_no_id = A.list_document_id \r\n" + 
				"			AND A1.id_company = A.id_company \r\n" + 
				"		) AS category,\r\n" + 
				"		b.list_document_share_user,\r\n" + 
				"		b.list_document_share_user_group,\r\n" + 
				"		b.list_document_share_so ,\r\n" + 
				"		b.list_document_share_start_date,\r\n" + 
				"		b.list_document_share_end_date\r\n" + 
				"	FROM\r\n" + 
				"		list_document\r\n" + 
				"		AS A LEFT JOIN list_document_shared AS b ON A.list_document_id = b.list_document_id \r\n" + 
				"	WHERE\r\n" + 
				"		1 = 1 \r\n" + 
				"		AND ( "+userId+" "+groupuserId+" "+soId+" \r\n" + 
				"		)\r\n" + 
				"	and (  b.list_document_share_end_date is null or  b.list_document_share_end_date >= :startDate \r\n" + 
				"	 )\r\n" + 
				"	 and b.list_document_share_start_date <= :endDate "
				+ " and a.list_confidential = 'f' \r\n" + 
				"	"+Status+" \r\n" + 
				"	 and a.id_company = :company_id \r\n" + 
				"	 and b.company_id = :company_id \r\n" + 
				"	) AS FINAL \r\n" + 
				"	left join m_document_type as e on e.\"id\" = final.list_document_type\r\n" + 
				"ORDER BY\r\n" + 
				"	FINAL.list_document_id,\r\n" + 
				"	FINAL.list_document_share_role asc ")
				.setParameter("startDate", startDate)
				.setParameter("endDate", endDate)
				.setParameter("company_id", company_id)
				.getResultList();
	}

}
