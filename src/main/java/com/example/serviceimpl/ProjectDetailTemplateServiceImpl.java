package com.example.serviceimpl;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.example.entity.ProjectDetailTemplate;
import com.example.service.ProjectDetailTemplateService;
import com.example.repository.ProjectDetailTemplateRepository;
import com.example.serializable.ProjectDetailTemplateSerializable;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProjectDetailTemplateServiceImpl implements ProjectDetailTemplateService {
	private static final Logger logger = LoggerFactory.getLogger(ProjectDetailTemplateServiceImpl.class);

	@Autowired
	private ProjectDetailTemplateRepository ProjectDetailTemplateRepository;
	
	@Autowired
	EntityManager em;

	@Override
	public void save(ProjectDetailTemplate data) {
		// TODO Auto-generated method stub
		ProjectDetailTemplateRepository.save(data);
	}

}
