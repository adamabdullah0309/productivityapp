package com.example.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.ProjectDetail;
import com.example.entity.TmProjectTimeframe;
import com.example.repository.TmProjectTimeframeRepository;
import com.example.serializable.TmProjectTimeframeSerializable;
import com.example.service.TmProjectTimeframeService;

@Service
public class TmProjectTimeframeServiceImpl implements TmProjectTimeframeService {
	private static final Logger logger = LoggerFactory.getLogger(TmProjectTimeframeServiceImpl.class);
	

	@Autowired
	private TmProjectTimeframeRepository TmProjectTimeframeRepository;
	
	@Autowired
	EntityManager em;

	@Override
	public void save(ArrayList<TmProjectTimeframe> dataTimeframe) {
		// TODO Auto-generated method stub
		for (TmProjectTimeframe tasklistLooping : dataTimeframe) {
//			em.persist(tasklistLooping);
			TmProjectTimeframeRepository.save(tasklistLooping);
		}
	}

	@Override
	@Transactional
	public void delete(Integer tmProjectId, Integer companyId) {
		// TODO Auto-generated method stub
		em.createNativeQuery("DELETE FROM tm_project_timeframe WHERE tm_project_id=:project and company_id = :company_id")
		.setParameter("project", tmProjectId)
		.setParameter("company_id", companyId)
		.executeUpdate();
	}

	@Override
	public List<Object[]> getTimeframeDetail(int project, int detail, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT * FROM tm_project_timeframe as a \r\n" + 
				"where\r\n" + 
				"a.tm_project_id = :project and\r\n" + 
				"a.project_detail_id = :detail \r\n" + 
				"and a.company_id = :company_id ")
				.setParameter("project", project)
				.setParameter("detail", detail)
				.setParameter("company_id", company)
				.getResultList();
	}

	@Override
	public List<Object[]> listPlanner() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.m_project_id,\r\n" + 
				"	A.project_detail_id,\r\n" + 
				"	cast(b.contact_id as varchar),\r\n" + 
				"	A.m_task_id,\r\n" + 
				"	A.m_grouping_id,\r\n" + 
				"	e.task_nama,\r\n" + 
				"	f.grouping_nama,\r\n" + 
				"	C.budget_hour,\r\n" + 
				"	C.startdate,\r\n" + 
				"	C.dateline,\r\n" + 
				"	d.date_budgethour \r\n" + 
				"FROM\r\n" + 
				"	project_detail\r\n" + 
				"	AS A RIGHT JOIN project_detail_karyawan AS b ON A.project_detail_id = b.project_detail_id \r\n" + 
				"	AND A.m_project_id = b.project_id \r\n" + 
				"	AND A.company_id = b.company_id\r\n" + 
				"	LEFT JOIN tm_project_timeframe AS C ON C.project_detail_id = A.project_detail_id \r\n" + 
				"	AND C.tm_project_id = A.m_project_id \r\n" + 
				"	AND A.company_id = C.company_id\r\n" + 
				"	LEFT JOIN tm_project_timeframe_budgethour AS d ON d.project_detail_id = C.project_detail_id \r\n" + 
				"	AND d.tm_project_id = C.tm_project_id \r\n" + 
				"	AND d.timeframe_id = C.timeframe_id \r\n" + 
				"	AND d.company_id = C.company_id\r\n" + 
				"	LEFT JOIN task AS e ON e.company_id = A.company_id \r\n" + 
				"	AND e.task_id = A.m_task_id\r\n" + 
				"	LEFT JOIN GROUPING AS f ON f.company_id = A.company_id \r\n" + 
				"	AND f.grouping_id = A.m_grouping_id")
				.getResultList();
	}

	@Override
	public List<Object[]> getTimeframe(int project, int company) {
		// TODO Auto-generated method stub
		return TmProjectTimeframeRepository.getTimeframe(project, company);
	}

	@Override
	public List<Object[]> getTimeframeBudgetHour(int project, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select * from tm_project_timeframe_budgethour as a\r\n" + 
				"where\r\n" + 
				"a.tm_project_id = :project\r\n" + 
				"and a.company_id = :company")
				.setParameter("project", project)
				.setParameter("company", company)
				.getResultList();
	}

	@Override
	public List<Object[]> cobaProcedure() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select foo()").getResultList();
	}

	@Override
	public Optional<TmProjectTimeframe> getDetail(int project, int detail, int timeframeId, int company) {
		// TODO Auto-generated method stub
		return TmProjectTimeframeRepository.findById(new TmProjectTimeframeSerializable(project, timeframeId, company, detail));
	}


}
