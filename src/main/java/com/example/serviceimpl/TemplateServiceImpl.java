package com.example.serviceimpl;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.Grouping;
import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Template;
import com.example.service.TemplateService;
import com.example.service.ListDocumentService;
import com.example.repository.TemplateRepository;
import com.example.serializable.TemplateSerializable;

@Service
public class TemplateServiceImpl implements TemplateService {
	private static final Logger logger = LoggerFactory.getLogger(TemplateServiceImpl.class);

	@Autowired
	private TemplateRepository TemplateRepository;
	

	@Autowired
	EntityManager em;


	@Override
	public Optional<Template> detailTemplateHeader(int int1, int int2) {
		// TODO Auto-generated method stub
		return TemplateRepository.findById(new TemplateSerializable(int1, int2));
	}


	@Override
	public List<Object[]> detailTemplateDetail(int group, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.*,\r\n" + 
				"	b.grouping_nama,\r\n" + 
				"	C.task_nama \r\n" + 
				"FROM\r\n" + 
				"	\"template_detail\"\r\n" + 
				"	AS A LEFT JOIN GROUPING AS b ON A.grouping_child_id = b.grouping_id \r\n" + 
				"	AND A.company_id = b.company_id\r\n" + 
				"	LEFT JOIN task AS C ON C.task_id = A.task_child_id \r\n" + 
				"	AND C.company_id = A.company_id \r\n" + 
				"WHERE\r\n" + 
				"	A.company_id = :company \r\n" + 
				"	AND A.m_template_id = :group ")
				.setParameter("company", company)
				.setParameter("group", group)
				.getResultList();
				
	}


	@Override
	public Template save(Template dataHeader) {
		// TODO Auto-generated method stub
		return TemplateRepository.save(dataHeader);
	}


	@Override
	public List<Object[]> nextval() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('template_sequence')").getResultList();
	}


	@Override
	public List<Object[]> getHeaderTemplate(int int1, UUID user_uuid, String paramAktif) {
		// TODO Auto-generated method stub
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and a.template_status = 1 ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and a.template_status = 0 ";
		} 
		else
		{
			cari = "";
		}
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.template_id,\r\n" + 
				"	A.template_nama,\r\n" + 
				"	A.template_description,\r\n" + 
				"	A.template_status,\r\n" + 
				"	A.company_id \r\n" + 
				"FROM\r\n" + 
				"	\"template\" AS A \r\n" + 
				"WHERE\r\n" + 
				"	A.company_id = :company \r\n" + 
				"	" + cari).setParameter("company", int1).getResultList();
	}


	@Override
	public List<Object[]> getSubgroup(Integer integer, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select\r\n" + 
				"z.*,\r\n" + 
				"ROW_NUMBER ( ) OVER ( ORDER BY 0 ) AS NUMBER\r\n" + 
				"from\r\n" + 
				"(\r\n" + 
				"WITH RECURSIVE descendants AS (\r\n" + 
				"	SELECT\r\n" + 
				"		grouping_child_id,\r\n" + 
				"		m_template_id AS asal,\r\n" + 
				"		task_child_id,\r\n" + 
				"		company_id,\r\n" + 
				"		nourut,\r\n" + 
				"		1 AS LEVEL,\r\n" + 
				"		m_template_id \r\n" + 
				"	FROM\r\n" + 
				"		template_detail UNION ALL\r\n" + 
				"	SELECT\r\n" + 
				"		s.grouping_child_id,\r\n" + 
				"		s.m_grouping_id AS asal,\r\n" + 
				"		s.task_child_id,\r\n" + 
				"		s.company_id,\r\n" + 
				"		s.nourut,\r\n" + 
				"		d.LEVEL + 1,\r\n" + 
				"		d.m_template_id \r\n" + 
				"	FROM\r\n" + 
				"		descendants AS d\r\n" + 
				"		JOIN subgroup_detail s ON d.grouping_child_id = s.m_grouping_id "
				+ " where \r\n" + 
				"	d.level < 10\r\n" + 
				"	) SELECT\r\n" + 
				"	descendants.grouping_child_id,\r\n" + 
				"	descendants.asal,\r\n" + 
				"	descendants.task_child_id,\r\n" + 
				"	descendants.company_id,\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"		WHEN descendants.task_child_id IS NOT NULL THEN\r\n" + 
				"		d.task_nama ELSE C.grouping_nama \r\n" + 
				"	END AS nama,\r\n" + 
				"	descendants.m_template_id \r\n" + 
				"	\r\n" + 
				"FROM\r\n" + 
				"	descendants\r\n" + 
				"	LEFT JOIN GROUPING AS C ON C.grouping_id = descendants.grouping_child_id \r\n" + 
				"	AND C.company_id = descendants.company_id\r\n" + 
				"	LEFT JOIN task AS d ON descendants.task_child_id = d.task_id \r\n" + 
				"	AND d.company_id = descendants.company_id \r\n" + 
				"WHERE\r\n" + 
				"	descendants.company_id = :company "
				+ " \r\n" + 
				"ORDER BY\r\n" + 
				"-- 	m_grouping_id,\r\n" + 
				"	m_template_id,\r\n" + 
				"	LEVEL, \r\n" + 
				"	grouping_child_id,\r\n" + 
				"	nourut\r\n" + 
				"	) as z\r\n" + 
				"	order by number desc")
				.setParameter("company", company)
				.getResultList();
	}


	@Override
	public List<Object[]> getDetailTemplate(Integer templateDetailId, int companyId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Object[]> callDetailTemplate(Integer id, Integer company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"	a.nourut,\r\n" + 
				"	a.m_template_id,\r\n" + 
				"	a.grouping_child_id,\r\n" + 
				"	a.task_child_id ,"
				+ " a.company_id,\r\n" + 
				"	case when a.grouping_child_id is not null then b.grouping_nama else c.task_nama end as task_nama\r\n" + 
				"FROM\r\n" + 
				"	template_detail\r\n" + 
				"	AS A LEFT JOIN GROUPING AS b ON A.grouping_child_id = b.grouping_id \r\n" + 
				"	AND A.company_id = b.company_id\r\n" + 
				"	LEFT JOIN task AS C ON A.task_child_id = C.task_id \r\n" + 
				"	AND A.company_id = C.company_id \r\n" + 
				"WHERE\r\n" + 
				"	A.m_template_id = :id \r\n" + 
				"	AND A.company_id = :company")
				.setParameter("id", id)
				.setParameter("company", company)
				.getResultList();
	}


	
}
