package com.example.serviceimpl;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Stopwatch;
import com.example.entity.StopwatchNote;
import com.example.entity.Timesheet;
import com.example.service.StopwatchService;
import com.example.service.TimesheetService;
import com.example.repository.StopwatchNoteRepository;
import com.example.repository.StopwatchRepository;
import com.example.serializable.StopwatchSerializable;
import com.example.serializable.TimesheetSerializable;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class StopwatchServiceImpl implements StopwatchService {
	private static final Logger logger = LoggerFactory.getLogger(StopwatchServiceImpl.class);
	

	@Autowired
	private StopwatchRepository StopwatchRepository;
	
	@Autowired
	private StopwatchNoteRepository StopwatchNoteRepository;
	
	@Autowired
	EntityManager em;


	@Override
	@Transactional
	public void save(Stopwatch data, List<StopwatchNote> note) {
		// TODO Auto-generated method stub
		StopwatchRepository.save(data);
		
		em.createNativeQuery("delete from stopwatch_note where "
				+ "stopwatch_id = :stopwatch_id "
				+ "and company_id = :company_id "
				+ "  ")
		.setParameter("stopwatch_id", data.getStopwatchId())
		.setParameter("company_id", data.getStopwatchCompanyId())
		.executeUpdate();
		
		StopwatchNoteRepository.saveAll(note);
	}


	@Override
	public List<Object[]> nextval() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('stopwatch_sequence')").getResultList();
	}


	@Override
	public Optional<Stopwatch> findIdAndIdCompany(int int1, int int2) {
		// TODO Auto-generated method stub
		return StopwatchRepository.findById(new StopwatchSerializable(int1, int2));
	}


	@Override
	public List<Object[]> listContent(String paramAktif, int int1, UUID user_uuid) {
		// TODO Auto-generated method stub
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and a.status = 1 ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and a.status = 0 ";
		}
		else
		{
			cari = "";
		}
		return em.createNativeQuery("select \r\n" + 
				"a.stopwatch_id,\r\n" + 
				"a.from_stopwatch,\r\n" + 
				"a.to_stopwatch,\r\n" + 
				"a.project_name_stopwatch,\r\n" + 
				"a.party,\r\n" + 
				"a.task,\r\n" + 
				"a.tag,\r\n" + 
				"a.note,\r\n" + 
				"a.location_stopwatch,\r\n" + 
				"0 as bil,\r\n" + 
				"cast(a.created_by as varchar),\r\n" + 
				"a.created_at,\r\n" + 
				"a.company_id,\r\n" + 
				"a.status,"
				+ "a.date_stopwatch\r\n" + 
				"from stopwatch as a where\r\n" + 
				"a.created_by = :user \r\n" + 
				"and a.company_id = :company\r\n" + 
				cari + 
				" ")
				.setParameter("user", user_uuid)
				.setParameter("company", int1)
				.getResultList();
	}


	@Override
	public List<Object[]> listDetailStopwatch(int int1, int int2, UUID user_uuid) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select \r\n" + 
				"a.stopwatch_id,\r\n" + 
				"a.from_stopwatch,\r\n" + 
				"a.to_stopwatch,\r\n" + 
				"a.project_name_stopwatch,\r\n" + 
				"a.party,\r\n" + 
				"a.task,\r\n" + 
				"a.tag,\r\n" + 
				"a.note,\r\n" + 
				"a.location_stopwatch,\r\n" + 
				"0 as bil,\r\n" + 
				"cast(a.created_by as varchar),\r\n" + 
				"a.created_at,\r\n" + 
				"a.company_id,\r\n" + 
				"a.status,"
				+ "a.date_stopwatch\r\n" + 
				"from stopwatch as a where\r\n" + 
				"a.created_by = :user \r\n" + 
				"and a.company_id = :company\r\n" + 
				"and a.stopwatch_id = :id " + 
				" ")
				.setParameter("user", user_uuid)
				.setParameter("id", int1)
				.setParameter("company", int2)
				.getResultList();
	}


	@Override
	public List<Object[]> listStopwatch(String paramAktif, int int1, UUID user_uuid) {
		// TODO Auto-generated method stub
		String parameter = "";
		if(paramAktif.toLowerCase().equals("y"))
		{
			parameter = " and a.stopwatch_status = 1 ";
		}
		else if (paramAktif.toLowerCase().equals("n")) {
			parameter = " and a.stopwatch_status = 0 ";
		}
		else
		{
			parameter = "";
		}
		return em.createNativeQuery("SELECT\r\n" + 
				"				a.stopwatch_id,\r\n" + 
				"				a.stopwatch_date,\r\n" + 
				"				a.stopwatch_from,\r\n" + 
				"				a.stopwatch_to,\r\n" + 
				"				a.stopwatch_party,\r\n" + 
				"				a.stopwatch_project_detail_id,\r\n" + 
				"				a.stopwatch_project_id,\r\n" + 
				"				a.stopwatch_company_id,\r\n" + 
				"				a.stopwatch_building,\r\n" + 
				"				a.stopwatch_address,\r\n" + 
				"				a.stopwatch_status,\r\n" + 
				"				 cast (a.stopwatch_contact_id as varchar),\r\n" + 
				"					case when c.task_id is not null then c.task_id else d.task_id end as task_id,\r\n" + 
				"					case when c.task_nama is not null then c.task_nama else d.task_nama end as task_nama,\r\n" + 
				"					b.m_grouping_id,\r\n" + 
				"					e.grouping_nama,\r\n" + 
				"					a.stopwatch_hour,"
				+ " f.project_name, (select count(*) from stopwatch_note as b1 where b1.stopwatch_id = a.stopwatch_id and b1.company_id = a.stopwatch_company_id) as jumlah_note " + 
				"				FROM\r\n" + 
				"					stopwatch\r\n" + 
				"					AS A LEFT JOIN project_detail AS b ON A.stopwatch_project_id = b.m_project_id \r\n" + 
				"					AND A.stopwatch_project_detail_id = b.project_detail_id \r\n" + 
				"					AND A.stopwatch_company_id = b.company_id\r\n" + 
				"					LEFT JOIN task AS C ON C.task_id = A.stopwatch_task_id \r\n" + 
				"					AND C.company_id = A.stopwatch_company_id\r\n" + 
				"					LEFT JOIN task AS d ON d.task_id = b.m_task_id \r\n" + 
				"					AND d.company_id = b.company_id\r\n" + 
				"					LEFT JOIN GROUPING AS e ON e.grouping_id = b.m_grouping_id \r\n" + 
				"					AND e.company_id = b.company_id "
				+ " LEFT JOIN dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', 'SELECT project_name, project_status, project_step_productivitapp, status_timeframe, project_id, company_id, project_party from project ' ) AS f ( project_name VARCHAR, project_status INTEGER, project_step_productivitapp INTEGER, status_timeframe INTEGER, project_id INTEGER, company_id INTEGER, project_party INTEGER ) ON f.project_id = A.stopwatch_project_id " + 
				"				AND f.company_id = A.stopwatch_company_id " + 
				"					where\r\n" + 
				"					1 = 1 " +  parameter +
				"					and a.stopwatch_contact_id = :con " + 
				"					and a.stopwatch_company_id = :company")
				.setParameter("con", user_uuid)
				.setParameter("company", int1)
				.getResultList();
	}


	@Override
	public List<Object[]> detail(int stopwatch, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT a.stopwatch_id,\r\n" + 
				"a.stopwatch_date,\r\n" + 
				"a.stopwatch_from,\r\n" + 
				"a.stopwatch_to,\r\n" + 
				"a.stopwatch_party,\r\n" + 
				"a.stopwatch_project_detail_id,\r\n" + 
				"a.stopwatch_project_id,\r\n" + 
				"a.stopwatch_task_id,\r\n" + 
				"a.stopwatch_company_id,\r\n" + 
				"a.stopwatch_building,\r\n" + 
				"a.stopwatch_address,\r\n" + 
				"a.stopwatch_status,\r\n" + 
				"a.stopwatch_hour,\r\n" + 
				" cast (a.stopwatch_contact_id as varchar)\r\n" + 
				" ,\r\n" + 
				"	C.m_task_id,\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"		WHEN n.task_nama IS NOT NULL THEN\r\n" + 
				"		n.task_nama ELSE P.task_nama \r\n" + 
				"	END AS task_nama,\r\n" + 
				"	d.project_name,\r\n" + 
				"	d.project_party,"
				+ " b.stopwatch_note_id,\r\n" + 
				"	b.note,\r\n" + 
				"	b.date_note \r\n" + 
				"FROM\r\n" + 
				"	stopwatch\r\n" + 
				"	AS A LEFT JOIN stopwatch_note AS b ON A.stopwatch_id = b.stopwatch_id \r\n" + 
				"	AND A.stopwatch_company_id = b.company_id\r\n" + 
				"	LEFT JOIN project_detail AS C ON A.stopwatch_project_id = C.m_project_id \r\n" + 
				"	AND A.stopwatch_project_detail_id = C.project_detail_id \r\n" + 
				"	AND A.stopwatch_company_id = C.company_id\r\n" + 
				"	LEFT JOIN task AS n ON C.m_task_id = n.task_id \r\n" + 
				"	AND C.company_id = n.company_id\r\n" + 
				"	LEFT JOIN task AS P ON A.stopwatch_task_id = P.task_id \r\n" + 
				"	AND A.stopwatch_company_id = P.company_id\r\n" + 
				"	LEFT JOIN dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', 'SELECT project_name, project_status, project_step_productivitapp, status_timeframe, project_id, company_id, project_party from project ' ) AS d ( project_name VARCHAR, project_status INTEGER, project_step_productivitapp INTEGER, status_timeframe INTEGER, project_id INTEGER, company_id INTEGER, project_party INTEGER ) ON d.project_id = A.stopwatch_project_id \r\n" + 
				"	AND d.company_id = A.stopwatch_company_id\r\n" + 
				"	where\r\n" + 
				"	a.stopwatch_id = :stopwatch\r\n" + 
				"	and a.stopwatch_company_id = :company")
				.setParameter("stopwatch", stopwatch)
				.setParameter("company", company)
				.getResultList();
	}
	


	

	
	
}
