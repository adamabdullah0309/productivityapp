package com.example.serviceimpl;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.example.entity.Grouping;
import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.service.GroupingService;
import com.example.service.ListDocumentService;
import com.example.repository.GroupingRepository;
import com.example.serializable.GroupingSerializable;

@Service
//@CacheConfig(cacheNames={"groupingCaching"})
public class GroupingServiceImpl implements GroupingService {
	private static final Logger logger = LoggerFactory.getLogger(GroupingServiceImpl.class);

	@Autowired
	private GroupingRepository GroupingRepository;
	

	@Autowired
	EntityManager em;


	@Override
	public Optional<Grouping> detailGroupHeader(int int1, int int2) {
		// TODO Auto-generated method stub
		return GroupingRepository.findById(new GroupingSerializable(int1, int2));
	}

	@Override
//	@Cacheable("findHeaderSubgroup")
	public List<Object[]> getHeaderSubgroup(int int1, UUID user_uuid, String paramAktif) {
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and a.grouping_status = 1 ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and a.grouping_status = 0 ";
		}
		else
		{
			cari = "";
		}
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.grouping_id,\r\n" + 
				"	A.grouping_nama,\r\n" + 
				"	A.grouping_description,\r\n" + 
				"	A.grouping_status,\r\n" + 
				"	A.company_id \r\n" + 
				"FROM\r\n" + 
				"	\"grouping\" AS A \r\n" + 
				"WHERE\r\n" + 
				"	A.company_id = :company \r\n" + 
				"	" + cari).setParameter("company", int1).getResultList();
	}
	
	
	@Override
	public List<Object[]> getSubgroupCoba(int company) {
		return GroupingRepository.listSubgroup(company);
	}

	
	@Override
	@Transactional
//	@Caching(
//			evict = 
//				{
//						@CacheEvict(value = "findHeaderSubgroup", allEntries = true),
//						@CacheEvict(value = "findDetailSubgroup", allEntries = true),
//						@CacheEvict(value = "findDetailGroupDetail", allEntries = true)
//				}
//			)
	public Grouping save(Grouping dataHeader) {
		// TODO Auto-generated method stub
		return GroupingRepository.save(dataHeader);
	}


	@Override
	public List<Object[]> nextval() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('grouping_sequence')").getResultList();
	}

	@Override
	public List<Object[]> getDetailSubgroup(int company, int group) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"	a.nourut,\r\n" + 
				"	a.m_grouping_id,\r\n" + 
				"	a.grouping_child_id,\r\n" + 
				"	a.task_child_id,"
				+ " a.company_id,\r\n" + 
				"	case when \r\n" + 
				"	a.grouping_child_id is not null then b.grouping_nama else c.task_nama end as task_nama\r\n" + 
				"FROM\r\n" + 
				"	subgroup_detail as a \r\n" + 
				"	left join grouping as b on a.grouping_child_id = b.grouping_id and a.company_id = b.company_id\r\n" + 
				"	left join task as c on a.task_child_id = c.task_id and a.company_id = c.company_id\r\n" + 
				"WHERE\r\n" + 
				"	a.m_grouping_id = :group \r\n" + 
				"	AND a.company_id = :company")
				.setParameter("group", group)
				.setParameter("company", company).getResultList();
	}

	@Override
	public List<Object[]> checkDuplicate(int company, int group) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select \r\n" + 
				"				z.*, \r\n" + 
				"				ROW_NUMBER ( ) OVER ( ORDER BY 0 ) AS NUMBER \r\n" + 
				"				from \r\n" + 
				"				( \r\n" + 
				"				WITH RECURSIVE descendants AS ( \r\n" + 
				"					SELECT \r\n" + 
				"						grouping_child_id, \r\n" + 
				"						m_grouping_id AS asal, \r\n" + 
				"						task_child_id, \r\n" + 
				"						company_id, \r\n" + 
				"						nourut, \r\n" + 
				"						1 AS LEVEL, \r\n" + 
				"						m_grouping_id  \r\n" + 
				"					FROM \r\n" + 
				"						subgroup_detail \r\n" + 
				"						where m_grouping_id = :group"
				+ " 					and company_id = :company \r\n" + 
				"						UNION ALL \r\n" + 
				"					SELECT \r\n" + 
				"						s.grouping_child_id, \r\n" + 
				"						s.m_grouping_id AS asal, \r\n" + 
				"						s.task_child_id, \r\n" + 
				"						s.company_id, \r\n" + 
				"						s.nourut, \r\n" + 
				"						d.LEVEL + 1, \r\n" + 
				"						d.m_grouping_id  \r\n" + 
				"					FROM \r\n" + 
				"						descendants AS d \r\n" + 
				"						JOIN subgroup_detail s ON d.grouping_child_id = s.m_grouping_id \r\n" + 
				"						where d.level < 20\r\n" + 
				"					) SELECT \r\n" + 
				"					descendants.grouping_child_id, \r\n" + 
				"					descendants.asal, \r\n" + 
				"					descendants.task_child_id, \r\n" + 
				"					descendants.company_id, \r\n" + 
				"				CASE \r\n" + 
				"						 \r\n" + 
				"						WHEN descendants.task_child_id IS NOT NULL THEN \r\n" + 
				"						d.task_nama ELSE C.grouping_nama  \r\n" + 
				"					END AS nama, \r\n" + 
				"					descendants.m_grouping_id,  \r\n" + 
				"					descendants.level\r\n" + 
				"				FROM \r\n" + 
				"					descendants \r\n" + 
				"					LEFT JOIN GROUPING AS C ON C.grouping_id = descendants.grouping_child_id  \r\n" + 
				"					AND C.company_id = descendants.company_id \r\n" + 
				"					LEFT JOIN task AS d ON descendants.task_child_id = d.task_id  \r\n" + 
				"					AND d.company_id = descendants.company_id  \r\n" + 
				"				WHERE \r\n" + 
				"					descendants.company_id = :company  \r\n" + 
				"					\r\n" + 
				"				ORDER BY \r\n" + 
				"					m_grouping_id, \r\n" + 
				"					 \r\n" + 
				"					LEVEL, \r\n" + 
				"					grouping_child_id, \r\n" + 
				"					nourut \r\n" + 
				"					) as z \r\n" + 
				"					order by number desc")
				.setParameter("company", company)
				.setParameter("group", group)
				.getResultList();
	}
}
