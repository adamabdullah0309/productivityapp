package com.example.serviceimpl;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Timesheet;
import com.example.entity.TmPlannerAlone;
import com.example.service.TmPlannerAloneService;
import com.example.repository.TmPlannerAloneRepository;
import com.example.serializable.TimesheetSerializable;
import com.example.serializable.TmPlannerAloneSerializable;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class TmPlannerAloneServiceImpl implements TmPlannerAloneService {
	private static final Logger logger = LoggerFactory.getLogger(TmPlannerAloneServiceImpl.class);
	

	@Autowired
	private TmPlannerAloneRepository TmPlannerAloneRepository;
	
	@Autowired
	EntityManager em;

	@Override
	public List<Object[]> nextval() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('tm_planner_alone_sequence')").getResultList();
	}

	@Override
	public void save(TmPlannerAlone data) {
		// TODO Auto-generated method stub
		TmPlannerAloneRepository.save(data);
	}

	@Override
	public Optional<TmPlannerAlone> listDetail(int int1, int int2) {
		// TODO Auto-generated method stub
		return TmPlannerAloneRepository.findById(new TmPlannerAloneSerializable(int1,int2));
	}

	@Override
	public Optional<TmPlannerAlone> UpdatePlannerAlone(int id, int companyId) {
		// TODO Auto-generated method stub
		return TmPlannerAloneRepository.findById(new TmPlannerAloneSerializable(id,companyId));
	}

	

	
	
}
