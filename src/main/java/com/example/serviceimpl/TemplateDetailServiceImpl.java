package com.example.serviceimpl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.Grouping;
import com.example.entity.GroupingDetail;
import com.example.entity.ListDocument;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.entity.Template;
import com.example.entity.TemplateDetail;
import com.example.repository.TemplateDetailRepository;
import com.example.service.ListDocumentSharedService;
import com.example.service.TemplateDetailService;
import com.example.service.GroupingDetailService;
import com.example.serializable.ListDocumentSummarySubcategorySerializable;

@Service
public class TemplateDetailServiceImpl implements TemplateDetailService{
	private static final Logger logger = LoggerFactory.getLogger(TemplateDetailServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	private TemplateDetailRepository TemplateDetailRepository;

	@Override
	public void save(List<TemplateDetail> detail, Template lastId) {
		// TODO Auto-generated method stub
		
		for (TemplateDetail tasklistLooping : detail) {
//			em.persist(tasklistLooping);
			TemplateDetailRepository.save(tasklistLooping);
		}
	}

	@Override
	@Transactional
	public void delete(Integer groupingId, int companyId) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from template_detail where m_template_id = :id and company_id = :company")
		.setParameter("id", groupingId).setParameter("company", companyId).executeUpdate();
	}
//
//	@Override
//	public void save(ArrayList<TemplateDetail> detail, Grouping lastId) {
//		// TODO Auto-generated method stub
//		
//	}

}
