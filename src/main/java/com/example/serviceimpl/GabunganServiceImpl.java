package com.example.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.Grouping;
import com.example.entity.GroupingDetail;
import com.example.entity.ListDocument;
import com.example.entity.ListDocumentShared;
import com.example.entity.ListDocumentSummary;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.entity.Planner;
import com.example.entity.PlannerNote;
import com.example.entity.ProjectDetail;
import com.example.entity.ProjectDetailDependency;
import com.example.entity.ProjectDetailKaryawan;
import com.example.entity.Template;
import com.example.entity.TemplateDetail;
import com.example.entity.Timesheet;
import com.example.entity.TimesheetNote;
import com.example.entity.TmBoard;
import com.example.entity.TmBoardProjectDetail;
import com.example.entity.TmBoardProjectDetailAtt;
import com.example.entity.TmBoardStage;
import com.example.entity.TmPlannerAlone;
import com.example.entity.TmPlannerAloneNote;
import com.example.entity.TmProjectTimeframe;
import com.example.entity.TmProjectTimeframeBudgethour;
import com.example.service.GabunganService;
import com.example.service.GroupingService;
import com.example.service.ListDocumentService;
import com.example.service.ListDocumentSharedService;
import com.example.service.ListDocumentSummaryService;
import com.example.service.ListDocumentSummarySubcategoryService;
import com.example.service.TemplateDetailService;
import com.example.service.TemplateService;
import com.example.service.TimesheetNoteService;
import com.example.service.TmBoardProjectDetailAttService;
import com.example.service.TmBoardProjectDetailService;
import com.example.service.TmBoardService;
import com.example.service.TmBoardStageService;
import com.example.service.TmProjectTimeframeService;
import com.example.service.GroupingDetailService;
import com.example.service.ProjectDetailService;
import com.example.service.ProjectDetailKaryawanService;
import com.example.service.TmProjectTimeframeBudgethourService;
import com.example.service.ProjectDetailDependencyService;
import com.example.service.PlannerService;
import com.example.service.PlannerNoteService;
import com.example.service.TmPlannerAloneService;
import com.example.service.TmPlannerAloneNoteService;
import com.example.service.TimesheetService;

@Service
public class GabunganServiceImpl implements GabunganService {
	
	@Autowired
	private ListDocumentSharedService ListDocumentSharedService;
	
	@Autowired
	private ListDocumentService ListDocumentService;
	
	@Autowired 
	private ListDocumentSummaryService ListDocumentSummaryService;
	
	@Autowired
	private ListDocumentSummarySubcategoryService ListDocumentSummarySubcategoryService;
	
	@Autowired
	private GroupingService GroupingService;
	
	@Autowired
	private GroupingDetailService GroupingDetailService;
	
	@Autowired
	private TemplateDetailService TemplateDetailService;
	
	@Autowired
	private TemplateService TemplateService;
	
	@Autowired
	private ProjectDetailService ProjectDetailService;
	
	@Autowired
	private ProjectDetailKaryawanService ProjectDetailKaryawanService;
	
	@Autowired
	private TmProjectTimeframeService TmProjectTimeframeService;
	
	@Autowired
	private TmProjectTimeframeBudgethourService TmProjectTimeframeBudgethourService;
	
	@Autowired
	private TmBoardService TmBoardService;
	
	@Autowired
	private TmBoardStageService TmBoardStageService;
	
	@Autowired
	private TmBoardProjectDetailService TmBoardProjectDetailService;
	
	@Autowired
	private ProjectDetailDependencyService ProjectDetailDependencyService;
	
	@Autowired
	private TmBoardProjectDetailAttService TmBoardProjectDetailAttService;
	
	@Autowired
	private PlannerService PlannerService;
	
	@Autowired
	private PlannerNoteService PlannerNoteService;
	
	@Autowired
	private TmPlannerAloneService TmPlannerAloneService;
	
	@Autowired
	private TmPlannerAloneNoteService TmPlannerAloneNoteService;
	
	@Autowired
	private TimesheetService TimesheetService;
	
	@Autowired
	private TimesheetNoteService TimesheetNoteService;

	@Override
	@Transactional
	public void SaveShared(ListDocument data1, List<ListDocumentShared> data2) {
		// TODO Auto-generated method stub
		ListDocumentService.saveDoc(data1);
		ListDocumentSharedService.save(data2);
	}

	@Override
	@Transactional
	public void saveSummary(ListDocument lastId, List<ListDocumentSummary> dataCategory,
			List<ListDocumentSummarySubcategory> newData) {
		ListDocument LastId = ListDocumentService.saveDoc(lastId);
		ListDocumentSummaryService.save(dataCategory,LastId);
		ListDocumentSummarySubcategoryService.save(newData,LastId);
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public void updateSummary(ListDocument listDocument, ArrayList<ListDocumentSummary> dataCategory,
			ArrayList<ListDocumentSummarySubcategory> newData) {
		// TODO Auto-generated method stub
		ListDocument LastId = ListDocumentService.saveDoc(listDocument);
		ListDocumentSummaryService.delete(LastId.getListDocumentId(), LastId.getIdCompany());
		ListDocumentSummarySubcategoryService.delete(LastId.getListDocumentId(), LastId.getIdCompany());
		ListDocumentSummaryService.save(dataCategory,LastId);
		ListDocumentSummarySubcategoryService.save(newData,LastId);
	}

	@Override
	@Transactional
	public void saveGrouping(Grouping dataHeader, List<GroupingDetail> detail) {
		// TODO Auto-generated method stub
		Grouping lastId = GroupingService.save(dataHeader);
		GroupingDetailService.save(detail, lastId);
	}

	@Override
	@Transactional
	public void updateGrouping(Grouping grouping, List<GroupingDetail> detail) {
		// TODO Auto-generated method stub
		Grouping lastId = GroupingService.save(grouping);
		GroupingDetailService.delete(lastId.getGroupingId(), lastId.getCompanyId());
		GroupingDetailService.save(detail, lastId);
	}

	@Override
	@Transactional
	public void saveTemplate(Template dataHeader, List<TemplateDetail> detail) {
		// TODO Auto-generated method stub
		Template lastId = TemplateService.save(dataHeader);
		TemplateDetailService.save(detail, lastId);
	}

	@Override
	@Transactional
	public void updateTemplate(Template template, List<TemplateDetail> detail) {
		// TODO Auto-generated method stub
		Template lastId = TemplateService.save(template);
		TemplateDetailService.delete(lastId.getTemplateId(), lastId.getCompanyId());
		TemplateDetailService.save(detail, lastId);
	}

	@Override
	@Transactional
	public void saveProjectDetail(List<ProjectDetail> dataProject,int MprojectId, int company_id,int template,List<ProjectDetailKaryawan> dataKaryawan, ArrayList<ProjectDetailDependency> dependency) {
		// TODO Auto-generated method stub
		ProjectDetailService.deleteAll(MprojectId, company_id);
		ProjectDetailService.save(dataProject);
		ProjectDetailService.saveTemplate(template, company_id, MprojectId);
		ProjectDetailDependencyService.delete(MprojectId, company_id);
		ProjectDetailDependencyService.save(dependency);
		ProjectDetailKaryawanService.delete(MprojectId, company_id);
		ProjectDetailKaryawanService.save(dataKaryawan);
	}

	@Override
	@Transactional
	public void saveTimeframeDetail(ArrayList<TmProjectTimeframe> dataTimeframe,
			ArrayList<TmProjectTimeframeBudgethour> dataTimeframeBudgethour) {
		// TODO Auto-generated method stub
		TmProjectTimeframeService.delete(dataTimeframe.get(0).getTmProjectId(), dataTimeframe.get(0).getCompanyId());
		TmProjectTimeframeService.save(dataTimeframe);
		TmProjectTimeframeBudgethourService.save(dataTimeframeBudgethour);
	}

	@Override
	@Transactional
	public void saveBoardFirst(TmBoard data, List<TmBoardStage> dataStage,
			List<TmBoardProjectDetail> dataProjectDetail) {
		// TODO Auto-generated method stub
		TmBoardService.save(data);
		TmBoardStageService.save(dataStage);
		TmBoardProjectDetailService.save(dataProjectDetail);
	}

	@Override
	@Transactional
	public void saveKaryawan(ArrayList<ProjectDetailKaryawan> dataProjectDetailDependency) {
		// TODO Auto-generated method stub
//		ProjectDetailKaryawanService.delete(dataProjectDetailDependency.get(0).getmProjectId(), dataProjectDetailDependency.get(0).getProjectDetailId(), dataProjectDetailDependency.get(0).getCompanyId());
		ProjectDetailKaryawanService.save(dataProjectDetailDependency);
	}

	@Override
	@Transactional
	public void saveDependency(ArrayList<ProjectDetailDependency> dataProjectDetailDependency) {
		// TODO Auto-generated method stub
//		ProjectDetailDependencyService.delete(dataProjectDetailDependency.get(0).getmProjectId(), dataProjectDetailDependency.get(0).getProjectDetailId(), dataProjectDetailDependency.get(0).getCompanyId());
		ProjectDetailDependencyService.save(dataProjectDetailDependency);
	}

	@Override
	@Transactional
	public void saveBoardAttchement(ArrayList<TmBoardProjectDetailAtt> dataProjectDetail) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailAttService.delete(dataProjectDetail);
		TmBoardProjectDetailAttService.save(dataProjectDetail);
	}

	@Override
	@Transactional
	public void savePlanner(Planner isiPlanner, ArrayList<PlannerNote> isiPlannerNote) {
		// TODO Auto-generated method stub
		PlannerService.save(isiPlanner);
		if(isiPlannerNote.size()>0)
		{
			PlannerNoteService.delete(isiPlannerNote);
			PlannerNoteService.save(isiPlannerNote);
		}
		
	}

	@Override
	@Transactional
	public void savePlannerAlone(TmPlannerAlone data, ArrayList<TmPlannerAloneNote> isiPlannerNote) {
		// TODO Auto-generated method stub
		TmPlannerAloneService.save(data);
		if(isiPlannerNote.size()>0)
		{
			TmPlannerAloneNoteService.delete(isiPlannerNote);
			TmPlannerAloneNoteService.save(isiPlannerNote);
		}
	}

	@Override
	public void saveTimesheet(Timesheet data, ArrayList<TimesheetNote> isiPlannerNote) {
		// TODO Auto-generated method stub
		TimesheetService.save(data);
		if(isiPlannerNote.size()>0)
		{
			TimesheetNoteService.delete(isiPlannerNote);
			TimesheetNoteService.save(isiPlannerNote);
		}
	}

}
