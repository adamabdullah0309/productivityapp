package com.example.serviceimpl;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.ListDocument;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.repository.ListDocumentSummarySubcategoryRepository;
import com.example.service.ListDocumentSharedService;
import com.example.service.ListDocumentSummarySubcategoryService;
import com.example.serializable.ListDocumentSummarySubcategorySerializable;

@Service
public class ListDocumentSummarySubcategoryServiceImpl implements ListDocumentSummarySubcategoryService{
	private static final Logger logger = LoggerFactory.getLogger(ListDocumentSummarySubcategoryServiceImpl.class);
	
	@Autowired
	EntityManager em;

	@Autowired
	private ListDocumentSummarySubcategoryRepository ListDocumentSummarySubcategoryRepository;


	@Override
	public void save(List<ListDocumentSummarySubcategory> masterSharedDocument,ListDocument id) {
		// TODO Auto-generated method stub
		
//		em.createNativeQuery("delete from list_document_summary_subcategory where document_no_id = :id")
//		.setParameter("id", masterSharedDocument.getDocumentNoId()).executeUpdate();
//		
		for (ListDocumentSummarySubcategory tasklistLooping : masterSharedDocument) {
//			em.persist(tasklistLooping);
			tasklistLooping.setDocumentNoId(id.getListDocumentId());
			ListDocumentSummarySubcategoryRepository.save(tasklistLooping);
		}
	}


	@Override
	@Transactional
	public void delete(int doc, int company) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from list_document_summary_subcategory where document_no_id = :id and id_company = :company")
		.setParameter("id", doc).setParameter("company", company).executeUpdate();
	}


	@Override
	public List<Object[]> listDetailHierarchy(int company, int doc, int idcategory) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"	WHEN K\r\n" + 
				"		.asal = 0 THEN\r\n" + 
				"			NULL ELSE K.asal \r\n" + 
				"			END,\r\n" + 
				"		K.NUMBER,\r\n" + 
				"		K.ID,\r\n" + 
				"		K.parent_id,\r\n" + 
				"		K.document_no_id,\r\n" + 
				"		K.id_category,\r\n" + 
				"		K.nama_subcategory,\r\n" + 
				"		K.\r\n" + 
				"	VALUE\r\n" + 
				"		,\r\n" + 
				"		K.nama_subsubcategory,\r\n" + 
				"		K.id_company,\r\n" + 
				"		K.nourut ,\r\n" + 
				"		k.idsubsubcategory,\r\n" + 
				"		k.idsubcategory,\r\n" + 
				"		k.datatype_id,\r\n" + 
				"		k.datatype_name\r\n" + 
				"	FROM\r\n" + 
				"		(\r\n" + 
				"			WITH RECURSIVE nodes ( ID ) AS (\r\n" + 
				"			SELECT\r\n" + 
				"				s1.ID,\r\n" + 
				"				s1.parent_id,\r\n" + 
				"				0 AS asal,\r\n" + 
				"				s1.document_no_id,\r\n" + 
				"				s1.id_category,\r\n" + 
				"				s1.\r\n" + 
				"			VALUE\r\n" + 
				"				,\r\n" + 
				"				s1.id_subcategory,\r\n" + 
				"				s1.id_subsubcategory,\r\n" + 
				"				s1.id_company,\r\n" + 
				"				s1.nourut \r\n" + 
				"			FROM\r\n" + 
				"				list_document_summary_subcategory s1 \r\n" + 
				"			WHERE\r\n" + 
				"				s1.parent_id = 0 UNION\r\n" + 
				"			SELECT\r\n" + 
				"				s2.ID,\r\n" + 
				"				s2.parent_id,\r\n" + 
				"				s.ID AS asal,\r\n" + 
				"				s2.document_no_id,\r\n" + 
				"				s2.id_category,\r\n" + 
				"				s2.\r\n" + 
				"			VALUE\r\n" + 
				"				,\r\n" + 
				"				s2.id_subcategory,\r\n" + 
				"				s2.id_subsubcategory,\r\n" + 
				"				s2.id_company,\r\n" + 
				"				s2.nourut \r\n" + 
				"			FROM\r\n" + 
				"				list_document_summary_subcategory s2\r\n" + 
				"				INNER JOIN nodes s ON s2.parent_id = s.ID \r\n" + 
				"			) SELECT\r\n" + 
				"			nodes.asal,\r\n" + 
				"			ROW_NUMBER ( ) OVER ( ORDER BY 0 ) AS NUMBER,\r\n" + 
				"			nodes.ID,\r\n" + 
				"			nodes.parent_id,\r\n" + 
				"			nodes.document_no_id,\r\n" + 
				"			nodes.id_category,\r\n" + 
				"			nodes.\r\n" + 
				"		VALUE\r\n" + 
				"			,\r\n" + 
				"			b.caption AS nama_subcategory,\r\n" + 
				"			C.caption AS nama_subsubcategory,\r\n" + 
				"			nodes.id_company,\r\n" + 
				"			nodes.nourut,\r\n" + 
				"			b.id as idsubcategory,\r\n" + 
				"			c.id as idsubsubcategory,\r\n" + 
				"			b.datatype_id,\r\n" + 
				"			d.datatype_name\r\n" + 
				"		FROM\r\n" + 
				"			nodes\r\n" + 
				"			LEFT JOIN m_subcategory AS b ON nodes.id_subcategory = b.\r\n" + 
				"			ID LEFT JOIN m_subsubcategory AS C ON C.ID = nodes.id_subsubcategory \r\n" + 
				"			left join m_datatype as d on d.datatype_id = b.datatype_id\r\n" + 
				"		) AS K \r\n" + 
				"	WHERE\r\n" + 
				"		K.id_company = :company \r\n" + 
				"		AND K.document_no_id = :doc \r\n" + 
				"		AND K.id_category = :idcategory \r\n" + 
				"ORDER BY\r\n" + 
				"	K.NUMBER DESC").setParameter("company", company).setParameter("doc", doc).setParameter("idcategory", idcategory).getResultList();
	}


}
