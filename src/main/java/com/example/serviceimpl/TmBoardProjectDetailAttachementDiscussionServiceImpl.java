package com.example.serviceimpl;

import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.repository.TmBoardProjectDetailAttachementDiscussionRepository;
import com.example.entity.TmBoardProjectDetailAttchementDiscussion;
import com.example.service.TmBoardProjectDetailAttachementDiscussionService;

@Service
public class TmBoardProjectDetailAttachementDiscussionServiceImpl implements TmBoardProjectDetailAttachementDiscussionService {
	private static final Logger logger = LoggerFactory.getLogger(TmBoardProjectDetailAttachementDiscussionServiceImpl.class);
	
	@Autowired
	private TmBoardProjectDetailAttachementDiscussionRepository TmBoardProjectDetailAttachementDiscussionRepository;
	
	@Autowired
	EntityManager em;

	@Override
	public void save(TmBoardProjectDetailAttchementDiscussion data) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailAttachementDiscussionRepository.save(data);
	}

	@Override
	public List<Object[]> detailBoardAttachementDiscussion(int company, int board, 
			int boardProjectDetail, int boardProjectDetailAttId, UUID user_uuid) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"	* \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		* \r\n" + 
				"	FROM\r\n" + 
				"		(\r\n" + 
				"		SELECT A\r\n" + 
				"			.tm_project_id,\r\n" + 
				"			A.project_detail_id,\r\n" + 
				"			A.company_id,\r\n" + 
				"			'parent' AS parent_id,\r\n" + 
				"			A.tm_board_id,\r\n" + 
				"			A.tm_board_stage_id,\r\n" + 
				"			A.tm_board_project_detail_id,\r\n" + 
				"			h.tm_board_project_detail_att_id,\r\n" + 
				"		CASE\r\n" + 
				"				\r\n" + 
				"				WHEN o.grouping_id IS NULL THEN\r\n" + 
				"				n.task_nama ELSE o.grouping_nama \r\n" + 
				"			END AS nama_task,\r\n" + 
				"			h.datedocument,\r\n" + 
				"			h.doc_description,\r\n" + 
				"			h.doc_type,\r\n" + 
				"			r.doc_type_name,\r\n" + 
				"			h.file_path,\r\n" + 
				"			h1.tm_board_project_detail_att_disc_id,\r\n" + 
				"			CAST ( h1.contact_id AS VARCHAR ),\r\n" + 
				"			h1.comment_discussion,\r\n" + 
				"			h1.date_comment,\r\n" + 
				"			h1.tm_board_project_detail_att_disc_parent_id,\r\n" + 
				"			ROW_NUMBER ( ) OVER ( ORDER BY 0 ) AS NUMBER,"
				+ " case when h1.contact_id = '1bf3cba4-0fb5-4b1d-ab6b-17e84c6c249c' then 'owner' else 'guest' end as role  \r\n" + 
				"		FROM\r\n" + 
				"			tm_board_project_detail\r\n" + 
				"			AS A LEFT JOIN project_detail AS b ON A.tm_project_id = b.m_project_id \r\n" + 
				"			AND A.company_id = b.company_id \r\n" + 
				"			AND A.project_detail_id = b.project_detail_id\r\n" + 
				"			LEFT JOIN project_detail_karyawan AS C ON A.tm_project_id = C.project_id \r\n" + 
				"			AND C.project_detail_id = A.project_detail_id \r\n" + 
				"			AND C.company_id = A.company_id\r\n" + 
				"			LEFT JOIN GROUPING AS o ON o.grouping_id = b.m_grouping_id \r\n" + 
				"			AND o.company_id = b.company_id\r\n" + 
				"			LEFT JOIN task AS n ON b.m_task_id = n.task_id \r\n" + 
				"			AND b.company_id = n.company_id\r\n" + 
				"			RIGHT JOIN tm_board_project_detail_attachement AS h ON h.tm_board_id = A.tm_board_id \r\n" + 
				"			AND A.tm_board_project_detail_id = h.tm_board_project_detail_id \r\n" + 
				"			AND h.company_id = A.company_id\r\n" + 
				"			LEFT JOIN m_doc_type AS r ON r.company_id = h.company_id \r\n" + 
				"			AND h.doc_type = r.doc_type_id\r\n" + 
				"			RIGHT JOIN tm_board_project_detail_attachement_discussion AS h1 ON h.tm_board_id = h1.tm_board_id \r\n" +
				"			AND h1.tm_board_project_detail_id = h.tm_board_project_detail_id \r\n" + 
				"			AND h.company_id = h1.company_id \r\n" + 
				"			AND h.tm_board_project_detail_att_id = h1.tm_board_project_detail_att_id \r\n" + 
				"		WHERE\r\n" + 
				"			A.tm_board_id = :board \r\n" + 
				"			AND A.tm_board_project_detail_id = :boardProjectDetailId "
				+ " and h.tm_board_project_detail_att_id = :boardProjectDetailAttId \r\n" + 
				"			AND A.company_id = :company \r\n" + 
				"		) AS u \r\n" + 
				"	ORDER BY\r\n" + 
				"	CASE\r\n" + 
				"			\r\n" + 
				"			WHEN u.tm_board_project_detail_att_disc_parent_id = 0 THEN\r\n" + 
				"			u.tm_board_project_detail_att_disc_id ELSE u.tm_board_project_detail_att_disc_parent_id \r\n" + 
				"		END,\r\n" + 
				"		u.tm_board_project_detail_att_disc_parent_id <> 0,\r\n" + 
				"		u.tm_board_project_detail_att_disc_id \r\n" + 
				"	) AS q \r\n" + 
				"ORDER BY\r\n" + 
				"	q.NUMBER DESC")
				.setParameter("board", board)
				.setParameter("company", company)
				.setParameter("boardProjectDetailId", boardProjectDetail)
				.setParameter("boardProjectDetailAttId", boardProjectDetailAttId)
				.getResultList();
	}

	@Override
	public List<Object[]> nextval(int int1,int int3, int int4, int int5) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT tm_board_project_detail_att_disc_id + 1 as nextval FROM \"tm_board_project_detail_attachement_discussion\"\r\n" + 
				"where\r\n" + 
				"tm_board_id = :board \r\n" + 
				"and tm_board_project_detail_id = :boardProjectDetail \r\n" + 
				"and tm_board_project_detail_att_id = :boardProjectDetailAttId \r\n" + 
				"and company_id = :company \r\n" + 
				"order by tm_board_project_detail_att_disc_id desc\r\n" + 
				"limit 1")
				.setParameter("company", int5)
				.setParameter("board", int1)
				.setParameter("boardProjectDetail", int3)
				.setParameter("boardProjectDetailAttId", int4)
				.getResultList();
	}

	@Override
	@Transactional
	public void delete(int board, int boardProjectDetail, int boardProjectDetailAttId,
			int boardProjectDetailAttDiscId, int company) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailAttachementDiscussionRepository.deleteFunction(board, boardProjectDetail, boardProjectDetailAttId, boardProjectDetailAttDiscId, company);
	}

}
