package com.example.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import com.example.service.MasterDocTypeService;

@Service
public class MasterDocTypeServiceImpl implements MasterDocTypeService {
	private static final Logger logger = LoggerFactory.getLogger(MasterDocTypeServiceImpl.class);
	

	@Autowired
	EntityManager em;


	@Override
	public List<Object[]> listDataType(String paramAktif, int int1) {
		// TODO Auto-generated method stub
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and a.doc_type_status = 1 ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and a.doc_type_status = 0 ";
		}
		else
		{
			cari = "";
		}
		return em.createNativeQuery("select a.doc_type_id, a.doc_type_name, a.doc_type_status, a.company_id from m_doc_type as a where a.company_id = :company " + cari).setParameter("company", int1)
				.getResultList();
	}

}
