package com.example.serviceimpl;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import com.example.entity.Grouping;
import com.example.entity.GroupingDetail;
import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Planner;
import com.example.entity.ProjectDetail;
import com.example.service.PlannerService;
import com.example.service.ProjectDetailService;

import net.sf.json.JSONObject;

import com.example.repository.ProjectDetailRepository;
import com.example.serializable.PlannerSerializable;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
//@CacheConfig(cacheNames={"projectCaching"})
public class ProjectDetailServiceImpl implements ProjectDetailService {
	private static final Logger logger = LoggerFactory.getLogger(ProjectDetailServiceImpl.class);
	

	@Autowired
	private ProjectDetailRepository ProjectDetailRepository;
	
	@Autowired
	EntityManager em;
	
	@Override
//	@Cacheable("findProjectDetail")
	public List<Object[]> getProjectDetail(int project, int company) {
		// TODO Auto-generated method stub
		return ProjectDetailRepository.getListProjectDetail(project,company);
	}

	@Override
//	@CachePut(value = "findProjectDetail", key = "#ProjectDetail.projectDetailId + #ProjectDetail.mProjectId + #ProjectDetail.companyId")
	public void save(List<ProjectDetail> dataProject) {
		// TODO Auto-generated method stub
//		for (ProjectDetail tasklistLooping : dataProject) {
////			em.persist(tasklistLooping);
//			ProjectDetailRepository.save(tasklistLooping);
//		}
		ProjectDetailRepository.saveAll(dataProject);
	}

	@Override
	@Transactional
//	@Caching(
//			evict = 
//				{
//						@CacheEvict(value = "findProjectDetail", key = "#ProjectDetail.projectDetailId + #ProjectDetail.mProjectId + #ProjectDetail.companyId"),
//						@CacheEvict(value = "findProjectDetailKaryawan", key = "#ProjectDetailKaryawan.projectDetailId + #ProjectDetailKaryawan.mProjectId + #ProjectDetail.companyId + #ProjectDetailKaryawan.projectDetailKaryawanId")
//				}
//			)
	public void deleteAll(int mprojectId, int company_id) {
		// TODO Auto-generated method stub
		em.createNativeQuery("DELETE FROM project_detail WHERE m_project_id=:project and company_id = :company_id")
		.setParameter("project", mprojectId)
		.setParameter("company_id", company_id)
		.executeUpdate();
		
		em.createNativeQuery("DELETE FROM project_template_detail WHERE m_project_id=:project and company_id = :company_id")
		.setParameter("project", mprojectId)
		.setParameter("company_id", company_id)
		.executeUpdate();
	}

	@Override
	@Transactional
	public void saveTemplate(int template, int company_id, int mprojectId) {
		// TODO Auto-generated method stub
		em.createNativeQuery("insert into project_template_detail (m_template_id, \r\n" + 
				"m_project_id, \r\n" + 
				"company_id \r\n" + 
				") values(?,?,?)" )
		.setParameter(1, template)
		.setParameter(2, mprojectId)
		.setParameter(3, company_id)
		.executeUpdate();
	}

	@Override
	public List<Object[]> getProjectDetailAktif(int project, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"	a.* ,\r\n" + 
				"	c.task_nama,\r\n" + 
				"	b.grouping_nama,\r\n" + 
				"	(\r\n" + 
				"	SELECT COUNT\r\n" + 
				"		( a2.project_detail_karyawan_id ) \r\n" + 
				"	FROM\r\n" + 
				"		project_detail_karyawan AS a2 \r\n" + 
				"	WHERE\r\n" + 
				"		a2.company_id = A.company_id \r\n" + 
				"		AND a2.project_detail_id = A.project_detail_id \r\n" + 
				"		AND a2.project_id = A.m_project_id \r\n" + 
				"	) AS jmlh_karyawan \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"		WITH RECURSIVE nodes ( project_detail_id ) AS (\r\n" + 
				"		SELECT\r\n" + 
				"			s1.project_detail_id,\r\n" + 
				"			0 AS asal,\r\n" + 
				"			s1.company_id,\r\n" + 
				"			s1.parent_id,\r\n" + 
				"			s1.m_project_id,\r\n" + 
				"			s1.m_task_id,\r\n" + 
				"			s1.m_grouping_id,\r\n" + 
				"			s1.status,\r\n" + 
				"			s1.priority,\r\n" + 
				"			s1.no_dependency \r\n" + 
				"		FROM\r\n" + 
				"			project_detail s1 \r\n" + 
				"		WHERE\r\n" + 
				"			s1.parent_id = 0 UNION\r\n" + 
				"		SELECT\r\n" + 
				"			s2.project_detail_id,\r\n" + 
				"			s.project_detail_id AS asal,\r\n" + 
				"			s2.company_id,\r\n" + 
				"			s2.parent_id,\r\n" + 
				"			s2.m_project_id,\r\n" + 
				"			s2.m_task_id,\r\n" + 
				"			s2.m_grouping_id,\r\n" + 
				"			s2.status,\r\n" + 
				"			s2.priority,\r\n" + 
				"			s2.no_dependency \r\n" + 
				"		FROM\r\n" + 
				"			project_detail s2\r\n" + 
				"			INNER JOIN nodes s ON s2.parent_id = s.project_detail_id and s2.company_id = s.company_id and s.m_project_id = s2.m_project_id \r\n" + 
				"		) SELECT\r\n" + 
				"		nodes.*,\r\n" + 
				"		ROW_NUMBER ( ) OVER ( ORDER BY 0 ) AS NUMBER \r\n" + 
				"	FROM\r\n" + 
				"		nodes \r\n" + 
				"	ORDER BY\r\n" + 
				"	project_detail_id desc \r\n" + 
				"	)\r\n" + 
				"	AS A LEFT JOIN GROUPING AS b ON b.grouping_id = A.m_grouping_id \r\n" + 
				"	AND b.company_id = A.company_id\r\n" + 
				"	LEFT JOIN task AS C ON C.task_id = A.m_task_id \r\n" + 
				"	AND C.company_id = A.company_id\r\n" + 
				"	WHERE\r\n" + 
				"	A.m_project_id = :id \r\n" + 
				"	AND A.company_id = :company "
				+ " and a.status = 1 "
				+ " and not EXISTS ( SELECT 1 FROM project_detail AS b1 WHERE b1.parent_id = a.project_detail_id and b1.company_id = a.company_id and b1.m_project_id = a.m_project_id )   " + 
				"	")
				.setParameter("id", project)
				.setParameter("company", company)
				.getResultList();
	}

	@Override
	public void saveCoba(List<ProjectDetail> dataProject,int template) {
		// TODO Auto-generated method stub
		List<Object> contaa = new ArrayList<>();
		dataProject.stream().forEach(col->{
			
			JSONObject data = new JSONObject();
			data.put("mProjectId", col.getmProjectId());
			data.put("companyId", col.getCompanyId());
			data.put("description", col.getDescription());
			data.put("mGroupingId", col.getmGroupingId());
			data.put("mProjectId", col.getmProjectId());
			data.put("mTaskId", col.getmTaskId());
			data.put("parentId", col.getParentId());
			data.put("priority", col.getPriority());
			data.put("projectDetailId", col.getProjectDetailId());
			data.put("status", col.getStatus());
			contaa.add(data);
		});
		
		em.createNativeQuery("select * from coba_arrayv4('"+contaa+"', :project, :company)")
		.setParameter("project", dataProject.get(0).getmProjectId())
		.setParameter("company", dataProject.get(0).getCompanyId())
		.getResultList();
	}

	@Override
	public List<Object[]> listTask(int project, int companyId, String projectType, int projectDetail, String lowerCase) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and a.status = 1";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and a.status = 0";
		}
		else
		{
			status = "";
		}
		return em.createNativeQuery("WITH RECURSIVE coba AS (\r\n" + 
				"	SELECT A\r\n" + 
				"		.m_project_id,\r\n" + 
				"		A.project_detail_id,\r\n" + 
				"		A.parent_id,\r\n" + 
				"		A.company_id,\r\n" + 
				"		A.m_task_id,\r\n" + 
				"		A.m_grouping_id ,\r\n" + 
				"		a.status\r\n" + 
				"	FROM\r\n" + 
				"		project_detail AS A \r\n" + 
				"	WHERE\r\n" + 
				"		A.m_project_id = :project \r\n" + 
				"		AND A.company_id = :companyId \r\n" + 
				"		AND A.project_detail_id = :projectDetail UNION\r\n" + 
				"	SELECT A\r\n" + 
				"		.m_project_id,\r\n" + 
				"		A.project_detail_id,\r\n" + 
				"		A.parent_id,\r\n" + 
				"		A.company_id,\r\n" + 
				"		A.m_task_id,\r\n" + 
				"		A.m_grouping_id ,\r\n" + 
				"		a.status\r\n" + 
				"	FROM\r\n" + 
				"		project_detail\r\n" + 
				"		AS A INNER JOIN coba s ON s.project_detail_id = A.parent_id \r\n" + 
				"	WHERE\r\n" + 
				"		A.m_project_id = :project \r\n" + 
				"		AND A.company_id = :companyId \r\n" + 
				"	) SELECT A\r\n" + 
				"	.project_detail_id,\r\n" + 
				"	A.m_project_id,\r\n" + 
				"	d.project_name,\r\n" + 
				"	a.status,\r\n" + 
				"	A.m_task_id,\r\n" + 
				"	A.m_grouping_id,\r\n" + 
				"	case when o.grouping_nama is null then n.task_nama else o.grouping_nama end as task_nama,"
				+ "	a.company_id\r\n" + 
				"FROM\r\n" + 
				"	coba\r\n" + 
				"	AS A LEFT JOIN GROUPING AS o ON o.grouping_id = A.m_grouping_id \r\n" + 
				"	AND o.company_id = A.company_id\r\n" + 
				"	LEFT JOIN task AS n ON A.m_task_id = n.task_id \r\n" + 
				"	AND A.company_id = n.company_id \r\n" + 
				"	LEFT JOIN dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', 'SELECT project_name, project_status, project_step_productivitapp, status_timeframe, project_id, company_id, project_party, project_type from project ' ) AS d ( project_name VARCHAR, project_status INTEGER, project_step_productivitapp INTEGER, status_timeframe INTEGER, project_id INTEGER, company_id INTEGER, project_party INTEGER, project_type VARCHAR ) ON d.project_id = a.m_project_id\r\n" + 
				"	AND d.company_id = a.company_id\r\n" + 
				"WHERE\r\n" + 
				"	NOT EXISTS (\r\n" + 
				"	SELECT\r\n" + 
				"		1 \r\n" + 
				"	FROM\r\n" + 
				"		project_detail AS b1 \r\n" + 
				"	WHERE\r\n" + 
				"		A.project_detail_id = b1.parent_id \r\n" + 
				"		AND A.m_project_id = b1.m_project_id \r\n" + 
				"	AND A.company_id = b1.company_id \r\n" + 
				"	)\r\n" + 
				"and d.project_type = :projectType " + status)
				.setParameter("project", project)
				.setParameter("companyId", companyId)
				.setParameter("projectType", projectType.toLowerCase())
				.setParameter("projectDetail", projectDetail)
				.getResultList();
	}

	@Override
	public List<Object[]> listGroup(int projectId, int companyId, String projectType, String lowerCase) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and a.status = 1";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and a.status = 0";
		}
		else
		{
			status = "";
		}
		return em.createNativeQuery("SELECT a.m_project_id, d.project_name, a.m_task_id, a.m_grouping_id, a.company_id, a.status,\r\n" + 
				"	case when o.grouping_nama is null then n.task_nama else o.grouping_nama end as task_nama, a.project_detail_id  FROM \"project_detail\" as a\r\n" + 
				"LEFT JOIN dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', 'SELECT project_name, project_status, project_step_productivitapp, status_timeframe, project_id, company_id, project_party, project_type from project ' ) AS d ( project_name VARCHAR, project_status INTEGER, project_step_productivitapp INTEGER, status_timeframe INTEGER, project_id INTEGER, company_id INTEGER, project_party INTEGER, project_type VARCHAR ) ON d.project_id = a.m_project_id\r\n" + 
				"	AND d.company_id = a.company_id\r\n" + 
				"	LEFT JOIN GROUPING AS o ON o.grouping_id = A.m_grouping_id \r\n" + 
				"	AND o.company_id = A.company_id\r\n" + 
				"	LEFT JOIN task AS n ON A.m_task_id = n.task_id \r\n" + 
				"	AND A.company_id = n.company_id \r\n" + 
				"where\r\n" + 
				"a.m_project_id = :projectId\r\n" + 
				"and a.company_id = :companyId\r\n" + 
				"and EXISTS ( SELECT 1 FROM project_detail AS b1 WHERE a.project_detail_id = b1.parent_id and a.m_project_id = b1.m_project_id and a.company_id = b1.company_id ) \r\n" + 
				"and a.parent_id = 1\r\n" + 
				status + 
				"and d.project_type = :projectType \r\n" + 
				"order by a.project_detail_id")
				.setParameter("projectId", projectId)
				.setParameter("companyId", companyId)
				.setParameter("projectType", projectType)
				.getResultList();
	}



}
