package com.example.serviceimpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.TemplateDetail;
import com.example.entity.TmBoard;
import com.example.entity.TmBoardProjectDetail;
import com.example.entity.TmBoardStage;
import com.example.entity.TmProjectTimeframe;
import com.example.entity.TmProjectTimeframeBudgethour;
import com.example.repository.TmBoardProjectDetailRepository;
import com.example.serializable.TmBoardProjectDetailSerializable;
import com.example.service.TmBoardProjectDetailService;

@Service
public class TmBoardProjectDetailServiceImpl implements TmBoardProjectDetailService {
	private static final Logger logger = LoggerFactory.getLogger(TmBoardProjectDetailServiceImpl.class);
	
	@Autowired
	private TmBoardProjectDetailRepository TmBoardProjectDetailRepository;
	
	@Autowired
	EntityManager em;

	@Override
	public void save(List<TmBoardProjectDetail> dataProjectDetail) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailRepository.saveAll(dataProjectDetail);
	}

	@Override
	public void update(TmBoardProjectDetail data) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailRepository.save(data);
	}

	@Override
	public List<Object[]> detailTask(int boardId, int boardProjectDetail, int company_id, int projectId,UUID user_uuid) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.detailTask(company_id, projectId,user_uuid, boardId,  boardProjectDetail);
	}

	@Override
	public List<Object[]> detailGroupingAndTemplate(int boardId, int company_id, int projectId, int projectDetailId, UUID user_uuid) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.detailGroupingAndTemplate(company_id, projectId,user_uuid, boardId, projectDetailId);
	}

	@Override
	public List<Object[]> detailMember(int company_id, int projectId, int projectDetailId, UUID user_uuid, int boardId) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.detailMember(company_id, projectId, projectDetailId, user_uuid, boardId);
	}

	@Override
	public List<Object[]> detailMemberTemplate(int company_id, int project, UUID user_uuid, int boardId) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.detailMemberTemplate(company_id, project, user_uuid, boardId);
	}

	@Override
	public List<Object[]> detailMemberTask(int company, int board, UUID user_uuid, 
			int boardProjectDetailId) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT cast(C\r\n" + 
				"	.contact_id as varchar) as ca, 'guest' as gues\r\n" + 
				"FROM\r\n" + 
				"	tm_board_project_detail\r\n" + 
				"	AS A LEFT JOIN project_detail AS b ON A.tm_project_id = b.m_project_id \r\n" + 
				"	AND A.company_id = b.company_id \r\n" + 
				"	AND A.project_detail_id = b.project_detail_id\r\n" + 
				"	LEFT JOIN project_detail_karyawan AS C ON A.tm_project_id = C.project_id \r\n" + 
				"	AND C.project_detail_id = A.project_detail_id \r\n" + 
				"	AND C.company_id = A.company_id \r\n" + 
				"WHERE\r\n" + 
				"	A.tm_board_id = :board \r\n" + 
				"	AND A.tm_board_project_detail_id = :boardProjectDetailId \r\n" + 
				"	and a.company_id = :company ")
				.setParameter("board", board)
				.setParameter("company", company)
				.setParameter("boardProjectDetailId", boardProjectDetailId)
				.getResultList();
	}

	@Override
	public List<Object[]> detailAttachementTemplate(int company, int project, UUID user_uuid, int boardId) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.detailAttachementTemplate(company, project, user_uuid, boardId);
	}

	@Override
	public List<Object[]> detailBoardAttachementGrouping(int company_id, int projectId, int projectDetailId,
			UUID user_uuid, int boardId) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.detailBoardAttachementGrouping(company_id, projectId,user_uuid, boardId, projectDetailId);
	}

	@Override
	public List<Object[]> detailBoardAttachementTask(int company, int board, UUID user_uuid, 
			int boardProjectDetailId) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.tm_project_id,\r\n" + 
				"	A.project_detail_id,\r\n" + 
				"	A.company_id,\r\n" + 
				"	'parent' as parent,\r\n" + 
				"	CAST ( C.contact_id AS VARCHAR ) AS ca,\r\n" + 
				"	A.tm_board_id,\r\n" + 
				"	A.tm_board_stage_id,\r\n" + 
				"	A.tm_board_project_detail_id,\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"		WHEN o.grouping_id IS NULL THEN\r\n" + 
				"		n.task_nama ELSE o.grouping_nama \r\n" + 
				"	END AS nama_task ,\r\n" + 
				"	h.datedocument,\r\n" + 
				"	h.doc_description,\r\n" + 
				"	h.doc_type,\r\n" + 
				"	r.doc_type_name,\r\n" + 
				"	h.file_path,\r\n" + 
				"	h.tm_board_project_detail_att_id,"
				+ " h.nama_file\r\n" + 
				"FROM\r\n" + 
				"	tm_board_project_detail\r\n" + 
				"	AS A LEFT JOIN project_detail AS b ON A.tm_project_id = b.m_project_id \r\n" + 
				"	AND A.company_id = b.company_id \r\n" + 
				"	AND A.project_detail_id = b.project_detail_id\r\n" + 
				"	LEFT JOIN project_detail_karyawan AS C ON A.tm_project_id = C.project_id \r\n" + 
				"	AND C.project_detail_id = A.project_detail_id \r\n" + 
				"	AND C.company_id = A.company_id\r\n" + 
				"	LEFT JOIN GROUPING AS o ON o.grouping_id = b.m_grouping_id \r\n" + 
				"	AND o.company_id = b.company_id\r\n" + 
				"	LEFT JOIN task AS n ON b.m_task_id = n.task_id \r\n" + 
				"	AND b.company_id = n.company_id\r\n" + 
				"	RIGHT JOIN tm_board_project_detail_attachement AS h ON h.tm_board_id = a.tm_board_id \r\n" + 
				"	AND a.tm_board_project_detail_id = h.tm_board_project_detail_id \r\n" + 
				"	AND h.company_id = a.company_id \r\n" + 
				"	LEFT JOIN m_doc_type AS r ON r.company_id = h.company_id \r\n" + 
				"		AND h.doc_type = r.doc_type_id \r\n" + 
				"WHERE\r\n" + 
				"	A.tm_board_id = :board \r\n" + 
				"	AND A.tm_board_project_detail_id = :boardProjectDetailId \r\n" + 
				"	AND A.company_id = :company")
				.setParameter("board", board)
				.setParameter("company", company)
				.setParameter("boardProjectDetailId", boardProjectDetailId)
				.getResultList();
	}

	@Override
	public List<Object[]> detailBoardDiscussionTemplate(int company, int project, UUID user_uuid, int boardId) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.detailBoardDiscussionTemplate(company, project, user_uuid, boardId);
	}

	@Override
	public List<Object[]> detailBoardAttachementDiscussionTemplate(int company, int project, UUID user_uuid,
			int boardId) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.detailBoardAttachementDiscussionTemplate(company, project, user_uuid, boardId);
	}

	@Override
	public List<Object[]> detailBoardDiscussionGrouping(int company, int project, int projectDetailId, UUID user_uuid,
			int board) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.detailBoardDiscussionGrouping(company, project, projectDetailId, user_uuid, board);
	}

	@Override
	public List<Object[]> detailBoardAttachementDiscussionGrouping(int company, int project, int projectDetailId,
			UUID user_uuid, int board) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.detailBoardAttachementDiscussionGrouping(company, project, projectDetailId, user_uuid, board);
	}

	@Override
	public List<Object[]> detailBoardDiscussionTask(int company, int boardid, 
			int boardprojectdetailid, UUID user_uuid) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select\r\n" + 
				"*\r\n" + 
				"from\r\n" + 
				"(\r\n" + 
				"SELECT A\r\n" + 
				"					.tm_project_id,\r\n" + 
				"					A.project_detail_id,\r\n" + 
				"					A.company_id,\r\n" + 
				"					'parent' as parent_id,\r\n" + 
				"					CAST ( h.contact_id AS VARCHAR ) AS contact_id,\r\n" + 
				"					A.tm_board_id,\r\n" + 
				"					A.tm_board_stage_id,\r\n" + 
				"					A.tm_board_project_detail_id,\r\n" + 
				"					h.tm_board_project_detail_dis_id,\r\n" + 
				"					h.tm_board_project_detail_dis_parent_id,\r\n" + 
				"				CASE\r\n" + 
				"						\r\n" + 
				"						WHEN o.grouping_id IS NULL THEN\r\n" + 
				"						n.task_nama ELSE o.grouping_nama \r\n" + 
				"					END AS nama_task, \r\n" + 
				"					h.date_comment,\r\n" + 
				"					h.comment_discussion,\r\n" + 
				"					ROW_NUMBER ( ) OVER ( ORDER BY 0 ) AS NUMBER,"
				+ " case when h.contact_id = :user_uuid then 'owner' else 'guest' end as role \r\n" + 
				"					\r\n" + 
				"				FROM\r\n" + 
				"					tm_board_project_detail\r\n" + 
				"					AS A LEFT JOIN project_detail AS b ON A.tm_project_id = b.m_project_id \r\n" + 
				"					AND A.company_id = b.company_id \r\n" + 
				"					AND A.project_detail_id = b.project_detail_id\r\n" + 
				"					LEFT JOIN project_detail_karyawan AS C ON A.tm_project_id = C.project_id \r\n" + 
				"					AND C.project_detail_id = A.project_detail_id \r\n" + 
				"					AND C.company_id = A.company_id\r\n" + 
				"					LEFT JOIN GROUPING AS o ON o.grouping_id = b.m_grouping_id \r\n" + 
				"					AND o.company_id = b.company_id\r\n" + 
				"					LEFT JOIN task AS n ON b.m_task_id = n.task_id \r\n" + 
				"					AND b.company_id = n.company_id\r\n" + 
				"					RIGHT JOIN tm_board_project_detail_discussion AS h ON h.tm_board_id = a.tm_board_id \r\n" + 
				"					AND a.tm_board_project_detail_id = h.tm_board_project_detail_id \r\n" + 
				"					AND h.company_id = a.company_id \r\n" + 
				"					\r\n" + 
				"				WHERE\r\n" + 
				"					A.tm_board_id = :board \r\n" + 
				"					AND A.tm_board_project_detail_id = :boardProjectDetailId \r\n" + 
				"					AND A.company_id = :company \r\n" + 
				"					ORDER  BY CASE WHEN h.tm_board_project_detail_dis_parent_id = 0 THEN h.tm_board_project_detail_dis_id ELSE h.tm_board_project_detail_dis_parent_id END\r\n" + 
				"        , h.tm_board_project_detail_dis_parent_id <> 0\r\n" + 
				"        , h.tm_board_project_detail_dis_id\r\n" + 
				"				) as q\r\n" + 
				"				ORDER BY q.number desc")
				.setParameter("board", boardid)
				.setParameter("company", company)
				.setParameter("boardProjectDetailId", boardprojectdetailid)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
	}

	@Override
	public List<Object[]> detailBoardAttachementDiscussionTask(int company, int boardid,
			int boardprojectdetailid, UUID user_uuid) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"	* \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		* \r\n" + 
				"	FROM\r\n" + 
				"		(\r\n" + 
				"		SELECT A\r\n" + 
				"			.tm_project_id,\r\n" + 
				"			A.project_detail_id,\r\n" + 
				"			A.company_id,\r\n" + 
				"			'parent' AS parent_id,\r\n" + 
				"			A.tm_board_id,\r\n" + 
				"			A.tm_board_stage_id,\r\n" + 
				"			A.tm_board_project_detail_id,\r\n" + 
				"			h.tm_board_project_detail_att_id,\r\n" + 
				"		CASE\r\n" + 
				"				\r\n" + 
				"				WHEN o.grouping_id IS NULL THEN\r\n" + 
				"				n.task_nama ELSE o.grouping_nama \r\n" + 
				"			END AS nama_task,\r\n" + 
				"			h.datedocument,\r\n" + 
				"			h.doc_description,\r\n" + 
				"			h.doc_type,\r\n" + 
				"			r.doc_type_name,\r\n" + 
				"			h.file_path,\r\n" + 
				"			h1.tm_board_project_detail_att_disc_id,\r\n" + 
				"			CAST ( h1.contact_id AS VARCHAR ),\r\n" + 
				"			h1.comment_discussion,\r\n" + 
				"			h1.date_comment,\r\n" + 
				"			h1.tm_board_project_detail_att_disc_parent_id,\r\n" + 
				"			ROW_NUMBER ( ) OVER ( ORDER BY 0 ) AS NUMBER,"
				+ " case when h1.contact_id = :user_uuid then 'owner' else 'guest' end as role  \r\n" + 
				"		FROM\r\n" + 
				"			tm_board_project_detail\r\n" + 
				"			AS A LEFT JOIN project_detail AS b ON A.tm_project_id = b.m_project_id \r\n" + 
				"			AND A.company_id = b.company_id \r\n" + 
				"			AND A.project_detail_id = b.project_detail_id\r\n" + 
				"			LEFT JOIN project_detail_karyawan AS C ON A.tm_project_id = C.project_id \r\n" + 
				"			AND C.project_detail_id = A.project_detail_id \r\n" + 
				"			AND C.company_id = A.company_id\r\n" + 
				"			LEFT JOIN GROUPING AS o ON o.grouping_id = b.m_grouping_id \r\n" + 
				"			AND o.company_id = b.company_id\r\n" + 
				"			LEFT JOIN task AS n ON b.m_task_id = n.task_id \r\n" + 
				"			AND b.company_id = n.company_id\r\n" + 
				"			RIGHT JOIN tm_board_project_detail_attachement AS h ON h.tm_board_id = A.tm_board_id \r\n" + 
				"			AND A.tm_board_project_detail_id = h.tm_board_project_detail_id \r\n" + 
				"			AND h.company_id = A.company_id\r\n" + 
				"			LEFT JOIN m_doc_type AS r ON r.company_id = h.company_id \r\n" + 
				"			AND h.doc_type = r.doc_type_id\r\n" + 
				"			RIGHT JOIN tm_board_project_detail_attachement_discussion AS h1 ON h.tm_board_id = h1.tm_board_id \r\n" + 
				"			AND h1.tm_board_project_detail_id = h.tm_board_project_detail_id \r\n" + 
				"			AND h.company_id = h1.company_id \r\n" + 
				"			AND h.tm_board_project_detail_att_id = h1.tm_board_project_detail_att_id \r\n" + 
				"		WHERE\r\n" + 
				"			A.tm_board_id = :board \r\n" + 
				"			AND A.tm_board_project_detail_id = :boardProjectDetailId \r\n" + 
				"			AND A.company_id = :company \r\n" + 
				"		) AS u \r\n" + 
				"	ORDER BY\r\n" + 
				"	CASE\r\n" + 
				"			\r\n" + 
				"			WHEN u.tm_board_project_detail_att_disc_parent_id = 0 THEN\r\n" + 
				"			u.tm_board_project_detail_att_disc_id ELSE u.tm_board_project_detail_att_disc_parent_id \r\n" + 
				"		END,\r\n" + 
				"		u.tm_board_project_detail_att_disc_parent_id <> 0,\r\n" + 
				"		u.tm_board_project_detail_att_disc_id \r\n" + 
				"	) AS q \r\n" + 
				"ORDER BY\r\n" + 
				"	q.NUMBER DESC")
				.setParameter("board", boardid)
				.setParameter("company", company)
				.setParameter("boardProjectDetailId", boardprojectdetailid)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
	}

	@Override
	public void updateMuch(List<TmBoardProjectDetail> dataFinal) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailRepository.saveAll(dataFinal);
	}

	@Override
	public Optional<TmBoardProjectDetail> getDetail(int string, int int1, int int2) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.findById(new TmBoardProjectDetailSerializable(string, int1, int2));
	}

	@Override
	public void updateBoardStage(TmBoardProjectDetail tmBoardProjectDetail) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailRepository.save(tmBoardProjectDetail);
	}

	@Override
	public List<Object[]> listLog(int int1) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select project_id, timedate, cast (user_id as varchar), action, value from m_log where project_id = :project")
				.setParameter("project", int1)
				.getResultList();
	}

	@Override
	public List<Object[]> listMonitor(UUID user_uuid, int int1) {
		// TODO Auto-generated method stub
		return TmBoardProjectDetailRepository.listMonitor(user_uuid, int1);
	}



}
