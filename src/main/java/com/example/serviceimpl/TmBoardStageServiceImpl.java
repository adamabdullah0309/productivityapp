package com.example.serviceimpl;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.TemplateDetail;
import com.example.entity.TmBoard;
import com.example.entity.TmBoardStage;
import com.example.entity.TmProjectTimeframe;
import com.example.entity.TmProjectTimeframeBudgethour;
import com.example.repository.TmBoardStageRepository;
import com.example.repository.TmProjectTimeframeBudgethourRepository;
import com.example.service.TmBoardStageService;

@Service
public class TmBoardStageServiceImpl implements TmBoardStageService {
	private static final Logger logger = LoggerFactory.getLogger(TmBoardStageServiceImpl.class);
	
	@Autowired
	private TmBoardStageRepository TmBoardStageRepository;
	
	@Autowired
	EntityManager em;

	@Override
	public void save(List<TmBoardStage> dataStage) {
		// TODO Auto-generated method stub
		TmBoardStageRepository.saveAll(dataStage);
	}

}
