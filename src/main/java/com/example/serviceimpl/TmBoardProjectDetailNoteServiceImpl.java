package com.example.serviceimpl;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.TemplateDetail;
import com.example.entity.TmBoard;
import com.example.entity.TmBoardProjectDetailAtt;
import com.example.entity.TmBoardStage;
import com.example.entity.TmProjectTimeframe;
import com.example.entity.TmBoardProjectDetailDiscussion;
import com.example.entity.TmBoardProjectDetailNote;
import com.example.repository.TmBoardProjectDetailNoteRepository;
import com.example.serializable.TmBoardProjectDetailDiscussionSerializable;
import com.example.serializable.TmBoardProjectDetailNoteSerializable;
import com.example.service.TmBoardProjectDetailNoteService;

@Service
public class TmBoardProjectDetailNoteServiceImpl implements TmBoardProjectDetailNoteService {
	private static final Logger logger = LoggerFactory.getLogger(TmBoardProjectDetailNoteServiceImpl.class);
	
	@Autowired
	private TmBoardProjectDetailNoteRepository TmBoardProjectDetailNoteRepository;
	
	@Autowired
	EntityManager em;
//	public void save(TmBoardProjectDetailDiscussion data) {
//		// TODO Auto-generated method stub
//		TmBoardProjectDetailDiscussionRepository.save(data);
//	}
//
////	int TmBoardId, int TmBoardStageId,int TmBoardProjectDetailId, int TmBoardProjectDetailDisId, int companyId
//	@Override
//	public void delete(TmBoardProjectDetailDiscussion data) {	
//		// TODO Auto-generated method stub
//		TmBoardProjectDetailDiscussionRepository.deleteById(new TmBoardProjectDetailDiscussionSerializable(data.getTmBoardId(), data.getTmBoardStageId(), data.getTmBoardProjectDetailId(), data.getTmBoardProjectDetailDisId(), data.getCompanyId()));
//	}

	@Override
	public void save(TmBoardProjectDetailNote data) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailNoteRepository.save(data);
	}

//	int TmBoardId, int TmBoardStageId,int TmBoardProjectDetailId, int TmBoardProjectDetailNoteId, int companyId
	@Override
	public void delete(TmBoardProjectDetailNote data) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailNoteRepository.deleteById(new TmBoardProjectDetailNoteSerializable(data.getTmBoardId(), data.getTmBoardProjectDetailId(), data.getTmBoardProjectDetailNoteId(), data.getCompanyId()));
	}

	@Override
	public List<Object[]> nextval() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('note_sequence')").getResultList();
	}

	@Override
	public List<Object[]> listNote(int board,  int boardProjectDetailId, int company_id) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select * from tm_board_project_detail_note where tm_board_id = :board  and tm_board_project_detail_id = :boardProjectDetailId and company_id = :company_id ")
				.setParameter("board", board)
				.setParameter("boardProjectDetailId", boardProjectDetailId)
				.setParameter("company_id", company_id).getResultList();
	}

	@Override
	public List<Object[]> detailNote(int boardId, int boardProjectDetailId, int boardProjectDetailNoteId,
			int company_id) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select * from tm_board_project_detail_note where tm_board_id = :board and tm_board_project_detail_note_id = :boardProjectDetailNoteId and tm_board_project_detail_id = :boardProjectDetailId and company_id = :company_id ")
				.setParameter("board", boardId)
				.setParameter("boardProjectDetailNoteId", boardProjectDetailNoteId)
				.setParameter("boardProjectDetailId", boardProjectDetailId)
				.setParameter("company_id", company_id)
				.getResultList();
	}
	
	

}
