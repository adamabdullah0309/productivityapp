package com.example.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.TmProjectTimeframe;
import com.example.entity.TmProjectTimeframeBudgethour;
import com.example.repository.TmProjectTimeframeBudgethourRepository;
import com.example.service.TmProjectTimeframeBudgethourService;

@Service
public class TmProjectTimeframeBudgethourServiceImpl implements TmProjectTimeframeBudgethourService {
	private static final Logger logger = LoggerFactory.getLogger(TmProjectTimeframeBudgethourServiceImpl.class);
	

	@Autowired
	private TmProjectTimeframeBudgethourRepository TmProjectTimeframeBudgethourRepository;
	
	@Autowired
	EntityManager em;

	@Override
	public void save(ArrayList<TmProjectTimeframeBudgethour> dataTimeframeBudgethour) {
		// TODO Auto-generated method stub
		for (TmProjectTimeframeBudgethour tasklistLooping : dataTimeframeBudgethour) {
//			em.persist(tasklistLooping);
			TmProjectTimeframeBudgethourRepository.save(tasklistLooping);
		}
	}

	@Override
	public List<Object[]> getTimeframeBudgetDetail(int project, int detail, int timeframe, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"	* \r\n" + 
				"FROM\r\n" + 
				"	tm_project_timeframe_budgethour AS A \r\n" + 
				"WHERE\r\n" + 
				"	A.tm_project_id = :project \r\n" + 
				"	AND A.project_detail_id = :detail \r\n" + 
				"	AND A.timeframe_id = :timeframe \r\n" + 
				"	AND A.company_id = :company")
				.setParameter("project", project)
				.setParameter("detail", detail)
				.setParameter("timeframe", timeframe)
				.setParameter("company", company)
				.getResultList();
	}

}
