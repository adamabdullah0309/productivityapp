package com.example.serviceimpl;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.Grouping;
import com.example.entity.GroupingDetail;
import com.example.entity.ListDocument;
import com.example.entity.ListDocumentSummarySubcategory;
import com.example.repository.GroupingDetailRepository;
import com.example.service.ListDocumentSharedService;
import com.example.service.GroupingDetailService;
import com.example.serializable.ListDocumentSummarySubcategorySerializable;

@Service
@CacheConfig(cacheNames={"groupingCaching"})
public class GroupingDetailServiceImpl implements GroupingDetailService{
	private static final Logger logger = LoggerFactory.getLogger(GroupingDetailServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	private GroupingDetailRepository GroupingDetailRepository;

	@Override
//	@Caching(
//			evict = 
//				{
//						@CacheEvict(value = "findHeaderSubgroup", allEntries = true),
//						@CacheEvict(value = "findDetailSubgroup", allEntries = true),
//						@CacheEvict(value = "findDetailGroupDetail", allEntries = true)
//				}
//			)
	public void save(List<GroupingDetail> detail, Grouping lastId) {
		// TODO Auto-generated method stub
		
		for (GroupingDetail tasklistLooping : detail) {
//			em.persist(tasklistLooping);
			GroupingDetailRepository.save(tasklistLooping);
		}
	}

	@Override
	@Transactional
//	@Caching(
//			evict = 
//				{
//						@CacheEvict(value = "findHeaderSubgroup", allEntries = true),
//						@CacheEvict(value = "findDetailSubgroup", allEntries = true),
//						@CacheEvict(value = "findDetailGroupDetail", allEntries = true)
//				}
//			)
	public void delete(Integer groupingId, int companyId) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from subgroup_detail where m_grouping_id = :id and company_id = :company")
		.setParameter("id", groupingId).setParameter("company", companyId).executeUpdate();
	}
	
	@Override
//	@Cacheable("findDetailGroupDetail")
	public List<Object[]> detailGroupDetail(int group, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.*,\r\n" + 
				"	b.grouping_nama,\r\n" + 
				"	C.task_nama \r\n" + 
				"FROM\r\n" + 
				"	\"subgroup_detail\"\r\n" + 
				"	AS A LEFT JOIN GROUPING AS b ON A.grouping_child_id = b.grouping_id \r\n" + 
				"	AND A.company_id = b.company_id\r\n" + 
				"	LEFT JOIN task AS C ON C.task_id = A.task_child_id \r\n" + 
				"	AND C.company_id = A.company_id \r\n" + 
				"WHERE\r\n" + 
				"	A.company_id = :company \r\n" + 
				"	AND A.m_grouping_id = :group ")
				.setParameter("company", company)
				.setParameter("group", group)
				.getResultList();
				
	}

}
