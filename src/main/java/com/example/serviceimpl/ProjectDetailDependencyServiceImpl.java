package com.example.serviceimpl;import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Planner;
import com.example.entity.ProjectDetail;
import com.example.entity.ProjectDetailDependency;
import com.example.entity.ProjectDetailKaryawan;
import com.example.service.PlannerService;
import com.example.service.ProjectDetailDependencyService;
import com.example.repository.ProjectDetailDependencyRepository;
import com.example.repository.ProjectDetailKaryawanRepository;
import com.example.serializable.PlannerSerializable;
import com.example.serializable.ProjectDetailDependencySerializable;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProjectDetailDependencyServiceImpl implements ProjectDetailDependencyService{
	private static final Logger logger = LoggerFactory.getLogger(ProjectDetailDependencyService.class);
	
	@Autowired
	private ProjectDetailDependencyRepository ProjectDetailDependencyRepository;
	
	@Autowired
	EntityManager em;

	@Override
	public void save(ArrayList<ProjectDetailDependency> dataProjectDetailDependency) {
		// TODO Auto-generated method stub
		ProjectDetailDependencyRepository.saveAll(dataProjectDetailDependency);
	}
	

	@Override
	@Transactional
	public void delete(int project, int company) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from project_detail_dependency where m_project_id = :project and company_id = :company")
		.setParameter("project", project).setParameter("company", company).executeUpdate();
	}


	@Override
	public List<ProjectDetailDependency> findDetail(int project, int projectdetail, int company) {
		// TODO Auto-generated method stub
		List<ProjectDetailDependency> data2 =  new ArrayList<>();
		List<Object[]> dataMen = em.createNativeQuery("select * from project_detail_dependency where m_project_id = :project and project_detail_id = :projectDetail and company_id = :company")
		.setParameter("project", project).setParameter("projectDetail", projectdetail).setParameter("company", company).getResultList();
		
		if(dataMen.size()>0)
		{
			dataMen.stream().forEach(col->{
				ProjectDetailDependency data = new ProjectDetailDependency();
				data.setProjectDetailId(Integer.valueOf(col[0].toString()));
				data.setmProjectId(Integer.valueOf(col[1].toString()));
				data.setProjectDetailDependencyId(Integer.valueOf(col[2].toString()));
				data.setNoDependency(Integer.valueOf(col[3].toString()));
				data.setCompanyId(Integer.valueOf(col[4].toString()));
				data2.add(data);
			});
		}
		return data2;
	}
}
