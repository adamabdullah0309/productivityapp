package com.example.serviceimpl;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.TemplateDetail;
import com.example.entity.TmBoard;
import com.example.entity.TmBoardProjectDetailAtt;
import com.example.entity.TmBoardStage;
import com.example.entity.TmProjectTimeframe;
import com.example.entity.TmBoardProjectDetailDiscussion;
import com.example.repository.TmBoardProjectDetailDiscussionRepository;
import com.example.serializable.TmBoardProjectDetailDiscussionSerializable;
import com.example.service.TmBoardProjectDetailDiscussionService;

@Service
public class TmBoardProjectDetailDiscussionServiceImpl implements TmBoardProjectDetailDiscussionService {
	private static final Logger logger = LoggerFactory.getLogger(TmBoardProjectDetailDiscussionServiceImpl.class);
	
	@Autowired
	private TmBoardProjectDetailDiscussionRepository TmBoardProjectDetailDiscussionRepository;
	
	@Autowired
	EntityManager em;

	@Override
	public void save(TmBoardProjectDetailDiscussion data) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailDiscussionRepository.save(data);
	}

//	int TmBoardId, int TmBoardStageId,int TmBoardProjectDetailId, int TmBoardProjectDetailDisId, int companyId
	@Override
	public void delete(TmBoardProjectDetailDiscussion data) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailDiscussionRepository.deleteById(new TmBoardProjectDetailDiscussionSerializable(data.getTmBoardId(), data.getTmBoardProjectDetailId(), data.getTmBoardProjectDetailDisId(), data.getCompanyId()));
	}

	@Override
	public List<Object[]> nextval(int board, int detail, int copmany) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT tm_board_project_detail_dis_id FROM \"tm_board_project_detail_discussion\"\r\n" + 
				"where tm_board_id = :board and tm_board_project_detail_id = :detail and company_id = :copmany\r\n" + 
				"order by tm_board_project_detail_dis_id desc\r\n" + 
				"limit 1")
				.setParameter("board", board)
				.setParameter("detail", detail)
				.setParameter("copmany", copmany)
				.getResultList();
	}
	

//	@Override
//	@Transactional
//	public void delete(ArrayList<TmBoardProjectDetailDiscussion> dataProjectDetail) {
//		// TODO Auto-generated method stub
//		em.createNativeQuery("delete from tm_board_project_detail_attachement where tm_board_id = :boardId and tm_board_stage_id = :boardStageId and tm_board_project_detail_id=:boardProjectDetailId and company_id = :company")
//		.setParameter("boardId", dataProjectDetail.get(0).getTmBoardId()).setParameter("boardStageId", dataProjectDetail.get(0).getTmBoardStageId()).setParameter("boardProjectDetailId", dataProjectDetail.get(0).getTmBoardProjectDetailId()).setParameter("company", dataProjectDetail.get(0).getCompanyId()).executeUpdate();
//	}
//
//
//	@Override
//	public void save(ArrayList<TmBoardProjectDetailDiscussion> dataProjectDetail) {
//		// TODO Auto-generated method stub
//		TmBoardProjectDetailAttRepository.saveAll(dataProjectDetail);
//	}

}
