package com.example.serviceimpl;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.TemplateDetail;
import com.example.entity.TmBoard;
import com.example.entity.TmBoardProjectDetailAtt;
import com.example.entity.TmBoardStage;
import com.example.entity.TmProjectTimeframe;
import com.example.entity.TmProjectTimeframeBudgethour;
import com.example.repository.TmBoardProjectDetailAttRepository;
import com.example.service.TmBoardProjectDetailAttService;

@Service
public class TmBoardProjectDetailAttServiceImpl implements TmBoardProjectDetailAttService {
	private static final Logger logger = LoggerFactory.getLogger(TmBoardProjectDetailAttServiceImpl.class);
	
	@Autowired
	private TmBoardProjectDetailAttRepository TmBoardProjectDetailAttRepository;
	
	@Autowired
	EntityManager em;

	@Override
	@Transactional
	public void delete(ArrayList<TmBoardProjectDetailAtt> dataProjectDetail) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from tm_board_project_detail_attachement where tm_board_id = :boardId and tm_board_project_detail_id=:boardProjectDetailId and company_id = :company")
		.setParameter("boardId", dataProjectDetail.get(0).getTmBoardId()).setParameter("boardProjectDetailId", dataProjectDetail.get(0).getTmBoardProjectDetailId()).setParameter("company", dataProjectDetail.get(0).getCompanyId()).executeUpdate();
	}


	@Override
	public void save(ArrayList<TmBoardProjectDetailAtt> dataProjectDetail) {
		// TODO Auto-generated method stub
		TmBoardProjectDetailAttRepository.saveAll(dataProjectDetail);
	}


	@Override
	@Transactional
	public void deleteAtt(TmBoardProjectDetailAtt dataDetail) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from tm_board_project_detail_attachement where tm_board_id = :boardId and tm_board_project_detail_att_id = :attid  and tm_board_project_detail_id=:boardProjectDetailId and company_id = :company")
		.setParameter("boardId", dataDetail.getTmBoardId()).setParameter("attid", dataDetail.getTmBoardProjectDetailAttId()).setParameter("boardProjectDetailId", dataDetail.getTmBoardProjectDetailId()).setParameter("company", dataDetail.getCompanyId()).executeUpdate();
	}

}
