package com.example.serviceimpl;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.TimesheetNote;
import com.example.repository.TimesheetNoteRepository;
import com.example.serializable.TimesheetNoteSerializable;
import com.example.service.TimesheetNoteService;

@Service
public class TimesheetNoteServiceImpl implements TimesheetNoteService {
	private static final Logger logger = LoggerFactory.getLogger(TimesheetNoteServiceImpl.class);
	

	@Autowired
	private TimesheetNoteRepository TimesheetNoteRepository;
	
	@Autowired
	EntityManager em;

	@Override
	@Transactional
	public void delete(ArrayList<TimesheetNote> isiPlannerNote) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from timesheet_note where "
				+ "timesheet_id = :id "
				+ " and timesheet_company_id = :company_id "
				+ "  ")
		.setParameter("id", isiPlannerNote.get(0).getTimesheetId())
		.setParameter("company_id", isiPlannerNote.get(0).getTimesheetCompanyId())
		.executeUpdate();
	}

	@Override
	public void save(ArrayList<TimesheetNote> isiPlannerNote) {
		// TODO Auto-generated method stub
		TimesheetNoteRepository.saveAll(isiPlannerNote);
	}


}
