package com.example.serviceimpl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.service.ListDocumentService;
import com.example.repository.ListDocumentRepository;

@Service
public class ListDocumentServiceImpl implements ListDocumentService {
	private static final Logger logger = LoggerFactory.getLogger(ListDocumentServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	ListDocumentRepository ListDocumentRepository;

	@Override
	public List<Object[]> listDocument() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.list_document_id,\r\n" + 
				"	A.list_document_type,\r\n" + 
				"	A.list_document_status,\r\n" + 
				"	A.list_document_title \r\n" + 
				"FROM\r\n" + 
				"	\"list_document\" AS A\r\n" + 
				"	ORDER BY list_document_id desc").getResultList();
	}

	@Override
	public List<Object[]> listDocumentCategory(int id) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select \r\n" + 
				"string_agg(cat2223.category_name, ', ')\r\n" + 
				"from \r\n" + 
				"( SELECT\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"	WHEN\r\n" + 
				"		cat222.category_name IS NULL THEN\r\n" + 
				"			cat222.initial_name ELSE cat222.category_name \r\n" + 
				"		END as category_name\r\n" + 
				"		FROM\r\n" + 
				"			(\r\n" + 
				"			SELECT A\r\n" + 
				"				.list_document_summary_category,\r\n" + 
				"				b.m_hierarchy_subgroup_id,\r\n" + 
				"				b.m_hierarchy_subgroup_nourut,\r\n" + 
				"				(\r\n" + 
				"				SELECT\r\n" + 
				"					string_agg ( cat1.cat, ' ' ) \r\n" + 
				"				FROM\r\n" + 
				"					(\r\n" + 
				"					SELECT\r\n" + 
				"					CASE\r\n" + 
				"							\r\n" + 
				"						WHEN\r\n" + 
				"							a22.subgroup_id IS NOT NULL THEN\r\n" + 
				"								( SELECT a1.subgroup_name FROM m_subgroup AS a1 WHERE a22.subgroup_id = a1.subgroup_id ) ELSE ( SELECT a1.initial_name FROM m_initial AS a1 WHERE a22.initial_id = a1.initial_id ) \r\n" + 
				"								END AS cat \r\n" + 
				"						FROM\r\n" + 
				"							m_hierarchy_subgroup AS a22 \r\n" + 
				"						WHERE\r\n" + 
				"							a22.ID = b.m_hierarchy_subgroup_id \r\n" + 
				"							AND a22.nourut <= b.m_hierarchy_subgroup_nourut \r\n" + 
				"						) AS cat1 \r\n" + 
				"					) AS category_name,\r\n" + 
				"					b.initial_id,\r\n" + 
				"					d.initial_name \r\n" + 
				"				FROM\r\n" + 
				"					list_document_summary\r\n" + 
				"					AS A LEFT JOIN m_category AS b ON A.list_document_summary_category = b.category_id\r\n" + 
				"					LEFT JOIN m_hierarchy_subgroup AS C ON b.m_hierarchy_subgroup_id = C.ID \r\n" + 
				"					AND b.m_hierarchy_subgroup_nourut = C.nourut\r\n" + 
				"					LEFT JOIN m_initial AS d ON d.initial_id = b.initial_id \r\n" + 
				"				WHERE\r\n" + 
				"					A.list_document_id = :id \r\n" + 
				"				ORDER BY\r\n" + 
				"				A.list_document_summary_id ASC \r\n" + 
				"	) AS cat222 ) as cat2223").setParameter("id", id).getResultList();
	}

	@Override
	public List<Object[]> listDocumentDocumentType(int id) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"	string_agg ( cat2223.document_type_name, ', ' ) \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"	CASE\r\n" + 
				"			\r\n" + 
				"		WHEN\r\n" + 
				"			cat222.document_type_name IS NULL THEN\r\n" + 
				"				cat222.initial_name ELSE cat222.document_type_name \r\n" + 
				"				END AS document_type_name \r\n" + 
				"		FROM\r\n" + 
				"			(\r\n" + 
				"			SELECT\r\n" + 
				"				(\r\n" + 
				"				SELECT\r\n" + 
				"					string_agg ( cat1.cat, ' ' ) \r\n" + 
				"				FROM\r\n" + 
				"					(\r\n" + 
				"					SELECT\r\n" + 
				"					CASE\r\n" + 
				"							\r\n" + 
				"						WHEN\r\n" + 
				"							a22.subgroup_id IS NOT NULL THEN\r\n" + 
				"								( SELECT a1.subgroup_name FROM m_subgroup AS a1 WHERE a22.subgroup_id = a1.subgroup_id ) ELSE ( SELECT a1.initial_name FROM m_initial AS a1 WHERE a22.initial_id = a1.initial_id ) \r\n" + 
				"								END AS cat \r\n" + 
				"						FROM\r\n" + 
				"							m_hierarchy_subgroup AS a22 \r\n" + 
				"						WHERE\r\n" + 
				"							a22.ID = b.m_hierarchy_subgroup_id \r\n" + 
				"							AND a22.nourut <= b.m_hierarchy_subgroup_nourut \r\n" + 
				"						) AS cat1 \r\n" + 
				"					) AS document_type_name,\r\n" + 
				"					d.initial_name \r\n" + 
				"				FROM\r\n" + 
				"					list_document\r\n" + 
				"					AS A LEFT JOIN m_document_type AS b ON A.list_document_type = b.document_type_id\r\n" + 
				"					LEFT JOIN m_hierarchy_subgroup AS C ON b.m_hierarchy_subgroup_id = C.ID \r\n" + 
				"					AND b.m_hierarchy_subgroup_nourut = C.nourut\r\n" + 
				"					LEFT JOIN m_initial AS d ON d.initial_id = b.initial_id \r\n" + 
				"				WHERE\r\n" + 
				"					A.list_document_id = :id \r\n" + 
				"				) AS cat222 \r\n" + 
				"	) AS cat2223").setParameter("id", id).getResultList();
	}

	@Override 
	public List<Object[]> listDocumentDetail(int id,int user) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select a.list_document_summary_id, a.list_document_id,a.list_document_summary_document_no_value, a.list_document_summary_document_date_date, a.list_document_summary_value_value, a.note, a.list_document_parent_id from list_document_summary as a "
				+ " left join list_document as e on a.list_document_id = e.list_document_id "
				+ " where a.list_document_id = :id and e.user_id = :user order by a.list_document_summary_id asc").setParameter("id", id).setParameter("user", user).getResultList();
	}

	@Override
	public List<Object[]> listSummaryDocumentNo(int id, int summaryId,int user) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"	WHEN\r\n" + 
				"		cat222.document_type_name IS NULL THEN\r\n" + 
				"			cat222.initial_name ELSE cat222.document_type_name \r\n" + 
				"			END AS document_type_name \r\n" + 
				"	FROM\r\n" + 
				"		(\r\n" + 
				"		SELECT\r\n" + 
				"			(\r\n" + 
				"			SELECT\r\n" + 
				"				string_agg ( cat1.cat, ' ' ) \r\n" + 
				"			FROM\r\n" + 
				"				(\r\n" + 
				"				SELECT\r\n" + 
				"				CASE\r\n" + 
				"						\r\n" + 
				"					WHEN\r\n" + 
				"						a22.subgroup_id IS NOT NULL THEN\r\n" + 
				"							( SELECT a1.subgroup_name FROM m_subgroup AS a1 WHERE a22.subgroup_id = a1.subgroup_id ) ELSE ( SELECT a1.initial_name FROM m_initial AS a1 WHERE a22.initial_id = a1.initial_id ) \r\n" + 
				"							END AS cat \r\n" + 
				"					FROM\r\n" + 
				"						m_hierarchy_subgroup AS a22 \r\n" + 
				"					WHERE\r\n" + 
				"						a22.ID = b.m_hierarchy_subgroup_id \r\n" + 
				"						AND a22.nourut <= b.m_hierarchy_subgroup_nourut \r\n" + 
				"					) AS cat1 \r\n" + 
				"				) AS document_type_name,\r\n" + 
				"				d.initial_name \r\n" + 
				"			FROM\r\n" + 
				"				list_document_summary\r\n" + 
				"				AS A LEFT JOIN m_document_no AS b ON A.list_document_summary_document_no_type = b.document_no_id\r\n" + 
				"				LEFT JOIN m_hierarchy_subgroup AS C ON b.m_hierarchy_subgroup_id = C.ID \r\n" + 
				"				AND b.m_hierarchy_subgroup_nourut = C.nourut\r\n" + 
				"				LEFT JOIN m_initial AS d ON d.initial_id = b.initial_id"
				+ " left join list_document as e on a.list_document_id = e.list_document_id  \r\n" + 
				"			WHERE\r\n" + 
				"			A.list_document_id = :id and e.user_id = :user \r\n" + 
				"			and a.list_document_summary_id = :summaryId\r\n" + 
				"	) AS cat222").setParameter("id", id).setParameter("summaryId", summaryId).setParameter("user", user).getResultList();
	}

	@Override
	public List<Object[]> listSummaryDocumentDate(int parseInt, int parseInt2,int user) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"	WHEN\r\n" + 
				"		cat222.document_type_name IS NULL THEN\r\n" + 
				"			cat222.initial_name ELSE cat222.document_type_name \r\n" + 
				"			END AS document_type_name \r\n" + 
				"	FROM\r\n" + 
				"		(\r\n" + 
				"		SELECT\r\n" + 
				"			(\r\n" + 
				"			SELECT\r\n" + 
				"				string_agg ( cat1.cat, ' ' ) \r\n" + 
				"			FROM\r\n" + 
				"				(\r\n" + 
				"				SELECT\r\n" + 
				"				CASE\r\n" + 
				"						\r\n" + 
				"					WHEN\r\n" + 
				"						a22.subgroup_id IS NOT NULL THEN\r\n" + 
				"							( SELECT a1.subgroup_name FROM m_subgroup AS a1 WHERE a22.subgroup_id = a1.subgroup_id ) ELSE ( SELECT a1.initial_name FROM m_initial AS a1 WHERE a22.initial_id = a1.initial_id ) \r\n" + 
				"							END AS cat \r\n" + 
				"					FROM\r\n" + 
				"						m_hierarchy_subgroup AS a22 \r\n" + 
				"					WHERE\r\n" + 
				"						a22.ID = b.m_hierarchy_subgroup_id \r\n" + 
				"						AND a22.nourut <= b.m_hierarchy_subgroup_nourut \r\n" + 
				"					) AS cat1 \r\n" + 
				"				) AS document_type_name,\r\n" + 
				"				d.initial_name \r\n" + 
				"			FROM\r\n" + 
				"				list_document_summary\r\n" + 
				"				AS A LEFT JOIN m_document_date AS b ON A.list_document_summary_document_date_type = b.document_date_id\r\n" + 
				"				LEFT JOIN m_hierarchy_subgroup AS C ON b.m_hierarchy_subgroup_id = C.ID \r\n" + 
				"				AND b.m_hierarchy_subgroup_nourut = C.nourut\r\n" + 
				"				LEFT JOIN m_initial AS d ON d.initial_id = b.initial_id \r\n"
				+ " left join list_document as e on a.list_document_id = e.list_document_id " + 
				"			WHERE\r\n" + 
				"			A.list_document_id = :id and e.user_id = :user \r\n" + 
				"			and a.list_document_summary_id = :summaryId\r\n" + 
				"	) AS cat222").setParameter("id", parseInt).setParameter("summaryId", parseInt2).setParameter("user", user).getResultList();
	}

	@Override
	public List<Object[]> listSummaryValueType(int id, int summary,int user) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"	WHEN\r\n" + 
				"		cat222.document_type_name IS NULL THEN\r\n" + 
				"			cat222.initial_name ELSE cat222.document_type_name \r\n" + 
				"			END AS document_type_name \r\n" + 
				"	FROM\r\n" + 
				"		(\r\n" + 
				"		SELECT\r\n" + 
				"			(\r\n" + 
				"			SELECT\r\n" + 
				"				string_agg ( cat1.cat, ' ' ) \r\n" + 
				"			FROM\r\n" + 
				"				(\r\n" + 
				"				SELECT\r\n" + 
				"				CASE\r\n" + 
				"						\r\n" + 
				"					WHEN\r\n" + 
				"						a22.subgroup_id IS NOT NULL THEN\r\n" + 
				"							( SELECT a1.subgroup_name FROM m_subgroup AS a1 WHERE a22.subgroup_id = a1.subgroup_id ) ELSE ( SELECT a1.initial_name FROM m_initial AS a1 WHERE a22.initial_id = a1.initial_id ) \r\n" + 
				"							END AS cat \r\n" + 
				"					FROM\r\n" + 
				"						m_hierarchy_subgroup AS a22 \r\n" + 
				"					WHERE\r\n" + 
				"						a22.ID = b.m_hierarchy_subgroup_id \r\n" + 
				"						AND a22.nourut <= b.m_hierarchy_subgroup_nourut \r\n" + 
				"					) AS cat1 \r\n" + 
				"				) AS document_type_name,\r\n" + 
				"				d.initial_name \r\n" + 
				"			FROM\r\n" + 
				"				list_document_summary\r\n" + 
				"				AS A LEFT JOIN m_value_type AS b ON A.list_document_summary_value_type = b.value_type_id\r\n" + 
				"				LEFT JOIN m_hierarchy_subgroup AS C ON b.m_hierarchy_subgroup_id = C.ID \r\n" + 
				"				AND b.m_hierarchy_subgroup_nourut = C.nourut\r\n" + 
				"				LEFT JOIN m_initial AS d ON d.initial_id = b.initial_id "
				+ " left join list_document as e on a.list_document_id = e.list_document_id \r\n" + 
				"			WHERE\r\n" + 
				"			A.list_document_id = :id and e.user_id = :user \r\n" + 
				"			and a.list_document_summary_id = :summary\r\n" + 
				"	) AS cat222").setParameter("id", id).setParameter("summary", summary).setParameter("user", user) .getResultList();
	}

	@Override
	public List<Object[]> listBasedOnCategory(String param, int user_id) {
		// TODO Auto-generated method stub
		String cari = "";
//		if (param.equals("y"))
//		{
//			cari  = " where e.list_document_status = 1 ";
//		}
//		else if(param.equals("n"))
//		{
//			cari  = " where e.list_document_status = 0 ";
//		}
//		else
//		{
//			cari = "";
//		}
		return em.createNativeQuery("select \r\n" + 
				"-- string_agg(cat2223.category_name, ', ')\r\n" + 
				"cat2223.category_name,\r\n" + 
				"cat2223.list_document_id\r\n" + 
				"from \r\n" + 
				"( SELECT\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"	WHEN\r\n" + 
				"		cat222.category_name IS NULL THEN\r\n" + 
				"			cat222.initial_name ELSE cat222.category_name \r\n" + 
				"		END as category_name,\r\n" + 
				"		cat222.list_document_id,\r\n" + 
				"				 cat222.list_document_summary_id\r\n" + 
				"		FROM\r\n" + 
				"			(\r\n" + 
				"			SELECT A\r\n" + 
				"				.list_document_summary_category,\r\n" + 
				"				b.m_hierarchy_subgroup_id,\r\n" + 
				"				b.m_hierarchy_subgroup_nourut,\r\n" + 
				"				(\r\n" + 
				"				SELECT\r\n" + 
				"					string_agg ( cat1.cat, ' ' ) \r\n" + 
				"				FROM\r\n" + 
				"					(\r\n" + 
				"					SELECT\r\n" + 
				"					CASE\r\n" + 
				"							\r\n" + 
				"						WHEN\r\n" + 
				"							a22.subgroup_id IS NOT NULL THEN\r\n" + 
				"								( SELECT a1.subgroup_name FROM m_subgroup AS a1 WHERE a22.subgroup_id = a1.subgroup_id ) ELSE ( SELECT a1.initial_name FROM m_initial AS a1 WHERE a22.initial_id = a1.initial_id ) \r\n" + 
				"								END AS cat \r\n" + 
				"						FROM\r\n" + 
				"							m_hierarchy_subgroup AS a22 \r\n" + 
				"						WHERE\r\n" + 
				"							a22.ID = b.m_hierarchy_subgroup_id \r\n" + 
				"							AND a22.nourut <= b.m_hierarchy_subgroup_nourut \r\n" + 
				"						) AS cat1 \r\n" + 
				"					) AS category_name,\r\n" + 
				"					b.initial_id,\r\n" + 
				"					d.initial_name,\r\n" + 
				"				 a.list_document_id,\r\n" + 
				"				 a.list_document_summary_id\r\n" + 
				"				FROM\r\n" + 
				"					list_document_summary\r\n" + 
				"					AS A LEFT JOIN m_category AS b ON A.list_document_summary_category = b.category_id\r\n" + 
				"					LEFT JOIN m_hierarchy_subgroup AS C ON b.m_hierarchy_subgroup_id = C.ID \r\n" + 
				"					AND b.m_hierarchy_subgroup_nourut = C.nourut\r\n" + 
				"					LEFT JOIN m_initial AS d ON d.initial_id = b.initial_id \r\n"
				+ " left join list_document as e on a.list_document_id = e.list_document_id "
				+ " where e.user_id = :user_id " + 
				cari +
				"				ORDER BY\r\n" + 
				"				A.list_document_summary_id ASC \r\n" + 
				"	) AS cat222 ) as cat2223\r\n" + 
				"	where\r\n" + 
				"	cat2223.category_name is not null\r\n" + 
				"	ORDER BY category_name asc").setParameter("user_id", user_id).getResultList();
	}

	@Override
	public List<Object[]> listSearchedOnCategory(int id, String param) {
		// TODO Auto-generated method stub
		String cari = "";
		if (param.equals("y"))
		{
			cari  = " and a.list_document_status = 1 ";
		}
		else if(param.equals("n"))
		{
			cari  = " and a.list_document_status = 0 ";
		}
		else
		{
			cari = "";
		}
		return em.createNativeQuery("SELECT\r\n" + 
				"	string_agg ( cat2223.document_type_name, ', ' ) ,\r\n" + 
				"	cat2223.list_document_title\r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"	CASE\r\n" + 
				"			\r\n" + 
				"		WHEN\r\n" + 
				"			cat222.document_type_name IS NULL THEN\r\n" + 
				"				cat222.initial_name ELSE cat222.document_type_name \r\n" + 
				"				END AS document_type_name ,\r\n" + 
				"				cat222.list_document_title\r\n" + 
				"		FROM\r\n" + 
				"			(\r\n" + 
				"			SELECT\r\n" + 
				"				(\r\n" + 
				"				SELECT\r\n" + 
				"					string_agg ( cat1.cat, ' ' ) \r\n" + 
				"				FROM\r\n" + 
				"					(\r\n" + 
				"					SELECT\r\n" + 
				"					CASE\r\n" + 
				"							\r\n" + 
				"						WHEN\r\n" + 
				"							a22.subgroup_id IS NOT NULL THEN\r\n" + 
				"								( SELECT a1.subgroup_name FROM m_subgroup AS a1 WHERE a22.subgroup_id = a1.subgroup_id ) ELSE ( SELECT a1.initial_name FROM m_initial AS a1 WHERE a22.initial_id = a1.initial_id ) \r\n" + 
				"								END AS cat \r\n" + 
				"						FROM\r\n" + 
				"							m_hierarchy_subgroup AS a22 \r\n" + 
				"						WHERE\r\n" + 
				"							a22.ID = b.m_hierarchy_subgroup_id \r\n" + 
				"							AND a22.nourut <= b.m_hierarchy_subgroup_nourut \r\n" + 
				"						) AS cat1 \r\n" + 
				"					) AS document_type_name,\r\n" + 
				"					d.initial_name,\r\n" + 
				"				a.list_document_title	\r\n" + 
				"				FROM\r\n" + 
				"					list_document\r\n" + 
				"					AS A LEFT JOIN m_document_type AS b ON A.list_document_type = b.document_type_id\r\n" + 
				"					LEFT JOIN m_hierarchy_subgroup AS C ON b.m_hierarchy_subgroup_id = C.ID \r\n" + 
				"					AND b.m_hierarchy_subgroup_nourut = C.nourut\r\n" + 
				"					LEFT JOIN m_initial AS d ON d.initial_id = b.initial_id \r\n" + 
				"				WHERE\r\n" + 
				"					A.list_document_id = :id "
				+ cari + 
				"				) AS cat222 \r\n" + 
				"	) AS cat2223\r\n" + 
				"	GROUP BY cat2223.list_document_title").setParameter("id", id).getResultList();
	}

	@Override
	public List<Object[]> listSummaryValueUnit(int id, int summary, int user) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"	WHEN\r\n" + 
				"		cat222.document_type_name IS NULL THEN\r\n" + 
				"			cat222.initial_name ELSE cat222.document_type_name \r\n" + 
				"			END AS document_type_name \r\n" + 
				"	FROM\r\n" + 
				"		(\r\n" + 
				"		SELECT\r\n" + 
				"			(\r\n" + 
				"			SELECT\r\n" + 
				"				string_agg ( cat1.cat, ' ' ) \r\n" + 
				"			FROM\r\n" + 
				"				(\r\n" + 
				"				SELECT\r\n" + 
				"				CASE\r\n" + 
				"						\r\n" + 
				"					WHEN\r\n" + 
				"						a22.subgroup_id IS NOT NULL THEN\r\n" + 
				"							( SELECT a1.subgroup_name FROM m_subgroup AS a1 WHERE a22.subgroup_id = a1.subgroup_id ) ELSE ( SELECT a1.initial_name FROM m_initial AS a1 WHERE a22.initial_id = a1.initial_id ) \r\n" + 
				"							END AS cat \r\n" + 
				"					FROM\r\n" + 
				"						m_hierarchy_subgroup AS a22 \r\n" + 
				"					WHERE\r\n" + 
				"						a22.ID = b.m_hierarchy_subgroup_id \r\n" + 
				"						AND a22.nourut <= b.m_hierarchy_subgroup_nourut \r\n" + 
				"					) AS cat1 \r\n" + 
				"				) AS document_type_name,\r\n" + 
				"				d.initial_name \r\n" + 
				"			FROM\r\n" + 
				"				list_document_summary\r\n" + 
				"				AS A LEFT JOIN m_value_unit AS b ON A.list_document_summary_value_unit = b.value_unit_id\r\n" + 
				"				LEFT JOIN m_hierarchy_subgroup AS C ON b.m_hierarchy_subgroup_id = C.ID \r\n" + 
				"				AND b.m_hierarchy_subgroup_nourut = C.nourut\r\n" + 
				"				LEFT JOIN m_initial AS d ON d.initial_id = b.initial_id "
				+ " left join list_document as e on a.list_document_id = e.list_document_id \r\n" + 
				"			WHERE\r\n" + 
				"			A.list_document_id = :id and e.user_id = :user \r\n" + 
				"			and a.list_document_summary_id = :summary\r\n" + 
				"	) AS cat222").setParameter("id", id).setParameter("summary", summary).setParameter("user", user).getResultList();
	}

	@Override
	public List<Object[]> listIndex(int int1, String paramAktif, UUID user_uuid) {
		// TODO Auto-generated method stub
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and list_document_status = \'t\' ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and list_document_status = \'f\' ";
		}
		else
		{
			cari = "";
		}
		return em.createNativeQuery("select list_document_id, list_document_title,list_document_shared from list_document where id_company = "
				+ ":id " + cari + " and created_by = "
						+ ":user  order by list_document_title asc").setParameter("id", int1).setParameter("user", user_uuid).getResultList();
	}

	@Override
	public List<Object[]> listDocumentCategoryDetail(int id, int summary, int user) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"	WHEN\r\n" + 
				"		cat222.document_type_name IS NULL THEN\r\n" + 
				"			cat222.initial_name ELSE cat222.document_type_name \r\n" + 
				"			END AS document_type_name \r\n" + 
				"	FROM\r\n" + 
				"		(\r\n" + 
				"		SELECT\r\n" + 
				"			(\r\n" + 
				"			SELECT\r\n" + 
				"				string_agg ( cat1.cat, ' ' ) \r\n" + 
				"			FROM\r\n" + 
				"				(\r\n" + 
				"				SELECT\r\n" + 
				"				CASE\r\n" + 
				"						\r\n" + 
				"					WHEN\r\n" + 
				"						a22.subgroup_id IS NOT NULL THEN\r\n" + 
				"							( SELECT a1.subgroup_name FROM m_subgroup AS a1 WHERE a22.subgroup_id = a1.subgroup_id ) ELSE ( SELECT a1.initial_name FROM m_initial AS a1 WHERE a22.initial_id = a1.initial_id ) \r\n" + 
				"							END AS cat \r\n" + 
				"					FROM\r\n" + 
				"						m_hierarchy_subgroup AS a22 \r\n" + 
				"					WHERE\r\n" + 
				"						a22.ID = b.m_hierarchy_subgroup_id \r\n" + 
				"						AND a22.nourut <= b.m_hierarchy_subgroup_nourut \r\n" + 
				"					) AS cat1 \r\n" + 
				"				) AS document_type_name,\r\n" + 
				"				d.initial_name \r\n" + 
				"			FROM\r\n" + 
				"				list_document_summary\r\n" + 
				"				AS A LEFT JOIN m_category AS b ON A.list_document_summary_category = b.category_id\r\n" + 
				"				LEFT JOIN m_hierarchy_subgroup AS C ON b.m_hierarchy_subgroup_id = C.ID \r\n" + 
				"				AND b.m_hierarchy_subgroup_nourut = C.nourut\r\n" + 
				"				LEFT JOIN m_initial AS d ON d.initial_id = b.initial_id \r\n" + 
				"				left join list_document as e on a.list_document_id = e.list_document_id\r\n" + 
				"			WHERE\r\n" + 
				"			A.list_document_id = :id\r\n" + 
				"			and a.list_document_summary_id = :summary\r\n" + 
				"			and e.user_id = :user\r\n" + 
				"	) AS cat222").setParameter("id", id).setParameter("summary", summary).setParameter("user", user).getResultList();
	}

	@Override
	public List<Object[]> listContent(String paramAktif, int companyid,UUID createdby) {
		// TODO Auto-generated method stub
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and k.list_document_status = \'t\' ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and k.list_document_status = \'f\' ";
		}
		else
		{
			cari = "";
		}
		
		
		return em.createNativeQuery("select *\r\n" + 
				"from\r\n" + 
				"(\r\n" + 
				"SELECT \r\n" + 
				"	a.list_document_id,\r\n" + 
				"	a.list_document_title,\r\n" + 
				"	b.id_category,\r\n" + 
				"	C.caption,\r\n" + 
				"	a.list_document_type,\r\n" + 
				"	d.caption AS doc_type,\r\n" + 
				"	a.id_company ,\r\n" + 
				"	cast(a.created_by as varchar),\r\n" + 
				"	a.list_document_status\r\n" + 
				"FROM\r\n" + 
				"	list_document\r\n" + 
				"	AS A LEFT JOIN list_document_summary AS b ON A.list_document_id = b.document_no_id\r\n" + 
				"	LEFT JOIN m_category AS C ON C.ID = b.id_category\r\n" + 
				"	LEFT JOIN m_document_type AS d ON d.ID = A.list_document_type \r\n" + 
				"WHERE\r\n" + 
				"	b.document_no_id IS NULL\r\n" + 
				"	union\r\n" + 
				"SELECT A\r\n" + 
				"	.document_no_id,\r\n" + 
				"	b.list_document_title,\r\n" + 
				"	A.id_category,\r\n" + 
				"	C.caption,\r\n" + 
				"	b.list_document_type,\r\n" + 
				"	d.caption AS doc_type,\r\n" + 
				"	b.id_company ,\r\n" + 
				"	cast(b.created_by as varchar),\r\n" + 
				"	b.list_document_status\r\n" + 
				"FROM\r\n" + 
				"	list_document_summary\r\n" + 
				"	AS A LEFT JOIN list_document AS b ON b.list_document_id = A.document_no_id\r\n" + 
				"	LEFT JOIN m_category AS C ON C.ID = A.id_category\r\n" + 
				"	LEFT JOIN m_document_type AS d ON d.ID = b.list_document_type \r\n" + 
				"	) as k\r\n" + 
				"	where\r\n" + 
				"		k.id_company = :company and k.created_by = \'"+createdby+"\'" + 
				"		\r\n" + 
				cari +
				"	order by k.caption asc\r\n" + 
				"").setParameter("company", companyid).getResultList();
	}

	@Override
	public List<Object[]> listDetail(int company, int id) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT b.caption as subcategory,\r\n" + 
				"	A.VALUE,\r\n" + 
				"	C.caption as subsubcategory,\r\n" + 
				"	A.document_no_id as doc_id,\r\n" + 
				"	e.list_document_title as title,\r\n" + 
				"	A.id_category,\r\n" + 
				"	d.caption as category,\r\n" + 
				"	A.nourut,\r\n" + 
				"	A.parent_id,\r\n" + 
				"	A.ID " + 
				"FROM\r\n" + 
				"	\"list_document_summary_subcategory\"\r\n" + 
				"	AS A LEFT JOIN m_subcategory AS b ON A.id_subcategory = b.\r\n" + 
				"	ID LEFT JOIN m_subsubcategory AS C ON C.ID = A.id_subsubcategory\r\n" + 
				"	left join m_category as d on d.id = a.id_category\r\n" + 
				"	LEFT JOIN list_document AS e ON e.list_document_id = A.document_no_id\r\n" + 
				"	where \r\n" + 
				"	a.document_no_id = :id \r\n" + 
				"	and a.id_company = :company \r\n" + 
				"	order by a.document_no_id,a.id asc ").setParameter("id", id).setParameter("company", company).getResultList();
	}

	@Override
	public List<Object[]> listDetailHierarchy(int company, int id,int category) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"	WHEN K\r\n" + 
				"		.asal = 0 THEN\r\n" + 
				"			NULL ELSE K.asal \r\n" + 
				"			END,\r\n" + 
				"		K.NUMBER,\r\n" + 
				"		K.id,\r\n" + 
				"		k.parent_id,\r\n" + 
				"		k.document_no_id,\r\n" + 
				"		k.id_category,\r\n" + 
				"		k.nama_subcategory,\r\n" + 
				"		k.value,\r\n" + 
				"		k.nama_subsubcategory,\r\n" + 
				"		k.id_company,\r\n" + 
				"		k.nourut\r\n" + 
				"	FROM\r\n" + 
				"		(\r\n" + 
				"			WITH RECURSIVE nodes ( ID ) AS (\r\n" + 
				"			SELECT\r\n" + 
				"				s1.id,\r\n" + 
				"				s1.parent_id,\r\n" + 
				"				0 as asal,\r\n" + 
				"				s1.document_no_id,\r\n" + 
				"				s1.id_category,\r\n" + 
				"				s1.value,\r\n" + 
				"				s1.id_subcategory,\r\n" + 
				"				s1.id_subsubcategory,\r\n" + 
				"				s1.id_company,\r\n" + 
				"				s1.nourut\r\n" + 
				"			FROM\r\n" + 
				"				list_document_summary_subcategory s1 \r\n" + 
				"			WHERE\r\n" + 
				"				s1.parent_id = 0 UNION\r\n" + 
				"			SELECT\r\n" + 
				"				s2.id,\r\n" + 
				"				s2.parent_id ,\r\n" + 
				"				s.id AS asal,\r\n" + 
				"				s2.document_no_id,\r\n" + 
				"				s2.id_category,\r\n" + 
				"				s2.value,\r\n" + 
				"				s2.id_subcategory,\r\n" + 
				"				s2.id_subsubcategory,\r\n" + 
				"				s2.id_company,\r\n" + 
				"				s2.nourut\r\n" + 
				"			FROM\r\n" + 
				"				list_document_summary_subcategory s2\r\n" + 
				"				INNER JOIN nodes s ON s2.parent_id = s.id \r\n" + 
				"			) SELECT\r\n" + 
				"			nodes.asal,\r\n" + 
				"			ROW_NUMBER ( ) OVER ( ORDER BY 0 ) AS NUMBER,\r\n" + 
				"			nodes.id,\r\n" + 
				"			nodes.parent_id,\r\n" + 
				"			nodes.document_no_id,\r\n" + 
				"			nodes.id_category,\r\n" + 
				"			nodes.value,\r\n" + 
				"			b.caption as nama_subcategory,\r\n" + 
				"			c.caption as nama_subsubcategory,\r\n" + 
				"			nodes.id_company,\r\n" + 
				"			nodes.nourut\r\n" + 
				"		FROM\r\n" + 
				"			nodes\r\n" + 
				"			LEFT JOIN m_subcategory AS b ON nodes.id_subcategory = b.id\r\n" + 
				"			LEFT JOIN m_subsubcategory AS C ON C.ID = nodes.id_subsubcategory\r\n" + 
				"\r\n" + 
				"		) AS K \r\n" + 
				"		where k.id_company = :company and k.document_no_id = :id and k.id_category = :category \r\n" + 
				"ORDER BY\r\n" + 
				"	K.NUMBER DESC").setParameter("company", company).setParameter("id", id).setParameter("category", category).getResultList();
	}

	@Override
	public List<Object[]> listDetailCategory(int company, int id) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.id_category,\r\n" + 
				"	A.id_company,\r\n" + 
				"	A.document_no_id,\r\n" + 
				"	d.caption as category\r\n" + 
				"FROM\r\n" + 
				"	list_document_summary\r\n" + 
				"	AS A LEFT JOIN m_category AS d ON d.ID = A.id_category"
				+ " where "
				+ " a.document_no_id = :id and a.id_company = :company  ").setParameter("company", company).setParameter("id", id).getResultList();
	}

	@Override
	public ListDocument saveDoc(ListDocument listDoc) {
		// TODO Auto-generated method stub
		return ListDocumentRepository.save(listDoc);
	}

	@Override
	public List<Object[]> listMyDocument(int int1, String paramAktif, UUID user_uuid) {
		// TODO Auto-generated method stub
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and a.list_document_status = \'t\' ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and a.list_document_status = \'f\' ";
		}
		else
		{
			cari = "";
		}
		return em.createNativeQuery("SELECT A\r\n" + 
				"	.list_document_id,\r\n" + 
				"	A.list_document_title AS title,\r\n" + 
				"	b.caption AS doc_type,\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		string_agg ( b1.caption, ', ' ) \r\n" + 
				"	FROM\r\n" + 
				"		list_document_summary\r\n" + 
				"		AS A1 LEFT JOIN m_category AS b1 ON A1.id_category = b1.ID \r\n" + 
				"	WHERE\r\n" + 
				"		A1.document_no_id = a.list_document_id \r\n" + 
				"		AND A1.id_company = a.id_company\r\n" + 
				"	),"
				+ "a.list_document_shared \r\n" + 
				"FROM\r\n" + 
				"	list_document\r\n" + 
				"	AS A LEFT JOIN m_document_type AS b ON A.list_document_type = b.ID\r\n" + 
				"	where a.created_by = :user \r\n" + 
				cari +
				"	and a.id_company = :company ")
				.setParameter("user", user_uuid)
				.setParameter("company", int1).getResultList();
				
	}

	@Override
	public List<Object[]> listMyDocumentDetail(int company,  Integer doc_id) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"	b.caption \r\n" + 
				"FROM\r\n" + 
				"	list_document_summary\r\n" + 
				"	AS A LEFT JOIN m_category AS b ON A.id_category = b.ID\r\n" + 
				"	where\r\n" + 
				"	a.document_no_id = 1\r\n" + 
				"	and a.id_company = 1").getResultList();
	}

	@Override
	public Optional<ListDocument> findIdAndIdCompany(int id, int company) {
		// TODO Auto-generated method stub
		return ListDocumentRepository.findByListDocumentIdAndIdCompany(id, company);
	}
}
