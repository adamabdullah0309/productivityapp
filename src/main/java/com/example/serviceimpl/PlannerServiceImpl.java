package com.example.serviceimpl;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.example.entity.ListDocument;
import com.example.entity.MasterInitial;
import com.example.entity.Planner;
import com.example.entity.TmPlannerAlone;
import com.example.service.PlannerService;
import com.example.repository.PlannerRepository;
import com.example.serializable.PlannerSerializable;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class PlannerServiceImpl implements PlannerService {
	private static final Logger logger = LoggerFactory.getLogger(PlannerService.class);
	

	@Autowired
	private PlannerRepository PlannerRepository;
	
	@Autowired
	EntityManager em;


	@Override
	public void save(Planner data) {
		// TODO Auto-generated method stub
		PlannerRepository.save(data);
	}

	@Override
	public List<Object[]> nextval() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('planner_sequence')").getResultList();
	}

//	@Override
//	public Optional<Planner> findIdAndIdCompany(int int1, int int2) {
//		// TODO Auto-generated method stub
//		return PlannerRepository.findById(new PlannerSerializable(int1, int2));
//	}

	@Override
	public List<Object[]> listContent(String paramAktif, int int1, UUID user_uuid) {
		// TODO Auto-generated method stub
		String cari = "";
		if (paramAktif.equals("y"))
		{
			cari  = " and a.status = 1 ";
		}
		else if(paramAktif.equals("n"))
		{
			cari  = " and a.status = 0 ";
		}
		else
		{
			cari = "";
		}
		return em.createNativeQuery("select \r\n" + 
				"a.planner_id,\r\n" + 
				"a.from_planner,\r\n" + 
				"a.to_planner,\r\n" + 
				"a.project_name_planner,\r\n" + 
				"a.party,\r\n" + 
				"a.task,\r\n" + 
				"a.tag,\r\n" + 
				"a.note,\r\n" + 
				"a.location_planner,\r\n" + 
				"a.billable_planner,\r\n" + 
				"cast(a.created_by as varchar),\r\n" + 
				"a.created_at,\r\n" + 
				"a.company_id,\r\n" + 
				"a.status, a.date_planner\r\n" + 
				"from planner as a where\r\n" + 
				"a.created_by = :user \r\n" + 
				"and a.company_id = :company\r\n" + 
				cari + 
				" ")
				.setParameter("user", user_uuid)
				.setParameter("company", int1)
				.getResultList();
	}

	@Override
	public List<Object[]> listDetailPlanner(int plannerId, int company_id, int mProjectId, int projectDetailId, int timeframeId, UUID user_uuid) {
		// TODO Auto-generated method stub
		
		
		return em.createNativeQuery("SELECT \r\n" + 
				" d.planner_id,"
			+ " d.project_detail_id,"
			+ " d.project_id,"
			+ " d.date_planner,"
			+ " d.to_planner,"
			+ " d.planned_hour,"
			+ " d.overtime_hour,"
			+ " d.request_budget_hour,"
			+ " d.note_planner,"
			+ " d.building_planner,"
			+ " d.from_planner,"
			+ " cast(d.contact_id as varchar),"
			+ " d.timeframe_id, "
			+ " d.company_id,"
			+ " d.date_budgethour_timeframe, "
			+ " d.address_planner,"
			+ " d.status, " +
				"	e.tm_planner_note_id,\r\n" + 
				"	e.note,\r\n" + 
				"	e.date_note,f.m_task_id \r\n" + 
				"FROM\r\n" + 
				"	tm_planner AS d " + 
				" left join tm_planner_note AS e ON d.planner_id = e.planner_id \r\n" + 
				"					AND d.project_id = e.project_id\r\n" + 
				"					and d.project_detail_id = e.project_detail_id\r\n" + 
				"					and d.timeframe_id = e.timeframe_id\r\n" + 
				"					and d.company_id = e.company_id\r\n" + 
				"					and d.date_budgethour_timeframe = e.date_budgethour_timeframe\r\n"
				+ " left join project_detail as f on f.m_project_id = d.project_id and f.project_detail_id = d.project_detail_id and f.company_id = d.company_id " + 
				"WHERE\r\n" + 
				"	d.planner_id = :plannerId\r\n" + 
				"	and d.project_id = :mProjectId \r\n" + 
				"	and d.project_detail_id = :projectDetailId \r\n" + 
				"	and d.timeframe_id = :timeframeId \r\n" + 
				"	AND d.company_id = :company_id"
				+ " and d.contact_id = :user_uuid ")
				.setParameter("plannerId", plannerId)
				.setParameter("company_id", company_id)
				.setParameter("mProjectId", mProjectId)
				.setParameter("projectDetailId", projectDetailId)
				.setParameter("timeframeId", timeframeId)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
	}

	@Override
	public List<Object[]> listPlannerProject(int company, UUID user_uuid) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"k.*,k1.task_nama, k2.grouping_nama\r\n" + 
				"from\r\n" + 
				"(\r\n" + 
				"SELECT\r\n" + 
				"	'PROJECT' as name_col, data.*, b.project_detail_id, d.timeframe_id, cast(c.contact_id as varchar), b.m_task_id, b.m_grouping_id, d.budget_hour as timeframe, planner.planned_hour,\r\n" + 
				"	planner.overtime_hour, planner.request_budget_hour, planner.location_planner\r\n" + 
				"FROM\r\n" + 
				"	dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', 'SELECT project_id,company_id, project_name, project_party FROM project  ' ) AS DATA(projectId INTEGER, company_id INTEGER, project_name VARCHAR, project_party INTEGER)  \r\n" + 
				"	left join project_detail as b on data.projectId = b.m_project_id and b.company_id = data.company_id\r\n" + 
				"	left join project_detail_karyawan as c on b.m_project_id = c.project_id and b.project_detail_id = c.project_detail_id and b.company_id = c.company_id \r\n" + 
				"	left join tm_project_timeframe as d on d.tm_project_id = b.m_project_id and d.company_id = b.company_id and b.project_detail_id = d.project_detail_id\r\n" + 
				"	LEFT JOIN dblink ( 'host=localhost user=postgres password=proacc202010 dbname=productivityApp', 'SELECT planner_id,project_id,project_detail_id,timeframe_id,company_id, planned_hour, overtime_hour,request_budget_hour, location_planner, note_planner FROM tm_planner  ' ) AS planner(planner_id INTEGER, project_id INTEGER, project_detail_id INTEGER, timeframe_id INTEGER, company_id INTEGER, planned_hour FLOAT, overtime_hour FLOAT, request_budget_hour FLOAT, location_planner VARCHAR, note_planner date )  ON \r\n" + 
				"	d.tm_project_id = planner.project_id and d.company_id = planner.company_id and planner.project_detail_id = d.project_detail_id\r\n" + 
				"	and d.timeframe_id = planner.timeframe_id\r\n" + 
				"	\r\n" + 
				"	where\r\n" + 
				"	c.contact_id = :user_uuid and  c.company_id = :company\r\n" + 
				"	)\r\n" + 
				"	as k\r\n" + 
				"	left join task as k1 on k.m_task_id = k1.task_id and k.company_id = k1.company_id\r\n" + 
				"	left join grouping as k2 on k2.grouping_id = k.m_grouping_id and k.company_id = k2.company_id")
				.setParameter("company", company)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
				
	}

	@Override
	public List<Object[]> listPlannerProjectDetail(int company, UUID user_uuid) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"k.*,k1.task_nama, k2.grouping_nama\r\n" + 
				"from\r\n" + 
				"(\r\n" + 
				"SELECT\r\n" + 
				"	'PROJECT' as name_col, data.*, b.project_detail_id, d.timeframe_id, cast(c.contact_id as varchar), b.m_task_id, b.m_grouping_id, d.budget_hour as timeframe, planner.planned_hour,\r\n" + 
				"	planner.overtime_hour, planner.request_budget_hour, planner.location_planner\r\n" + 
				"FROM\r\n" + 
				"	dblink ( 'demodbrnd', 'SELECT project_id,company_id, project_name, project_party FROM project  ' ) AS DATA(projectId INTEGER, company_id INTEGER, project_name VARCHAR, project_party INTEGER)  \r\n" + 
				"	left join project_detail as b on data.projectId = b.m_project_id and b.company_id = data.company_id\r\n" + 
				"	left join project_detail_karyawan as c on b.m_project_id = c.project_id and b.project_detail_id = c.project_detail_id and b.company_id = c.company_id \r\n" + 
				"	left join tm_project_timeframe as d on d.tm_project_id = b.m_project_id and d.company_id = b.company_id and b.project_detail_id = d.project_detail_id\r\n" + 
				"	left join tm_project_timeframe_budgethour as e on d.tm_project_id = e.tm_project_id and d.company_id = e.company_id and e.project_detail_id = d.project_detail_id and e.timeframe_id = d.timeframe_id\r\n" + 
				"	LEFT JOIN dblink ( 'timemanagement_server4', 'SELECT planner_id,project_id,project_detail_id,timeframe_id,company_id, planned_hour, overtime_hour,request_budget_hour, location_planner, note_planner FROM tm_planner  ' ) AS planner(planner_id INTEGER, project_id INTEGER, project_detail_id INTEGER, timeframe_id INTEGER, company_id INTEGER, planned_hour FLOAT, overtime_hour FLOAT, request_budget_hour FLOAT, location_planner VARCHAR, note_planner date )  ON \r\n" + 
				"	d.tm_project_id = planner.project_id and d.company_id = planner.company_id and planner.project_detail_id = d.project_detail_id\r\n" + 
				"	and d.timeframe_id = planner.timeframe_id\r\n" + 
				"	\r\n" + 
				"	where\r\n" + 
				"	c.contact_id = :user_uuid and  c.company_id = :company\r\n" + 
				"	)\r\n" + 
				"	as k\r\n" + 
				"	left join task as k1 on k.m_task_id = k1.task_id and k.company_id = k1.company_id\r\n" + 
				"	left join grouping as k2 on k2.grouping_id = k.m_grouping_id and k.company_id = k2.company_id")
				.setParameter("company", company)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
	}

	@Override
	public List<Object[]> listPlanner(int int1, UUID user_uuid, int status) {
		// TODO Auto-generated method stub
		return PlannerRepository.listPlanner(user_uuid, int1, status);
	}

	@Override
	public List<Object[]> listPlannerAlone(int int1, UUID user_uuid, String status) {
		// TODO Auto-generated method stub
		String statusCoba = "";
		if(status.equals("y"))
		{
			statusCoba = " and d.status = 1";
		}
		else if(status.equals("n"))
		{
			statusCoba = " and d.status = 0";
		}
		else
		{
			statusCoba = "";
		}
		return em.createNativeQuery("SELECT\r\n" + 
				"	d.planner_alone_id,"
				+ " d.party_id,"
				+ " d.task_id,"
				+ " d.date_planner_internal,"
				+ " d.start_hour,"
				+ " d.end_hour,"
				+ " d.note,"
				+ " d.location,"
				+ " d.planned_hour,"
				+ " d.overtime_hour,"
				+ " d.request_budget_hour,"
				+ " cast(d.contact_id as varchar),"
				+ " d.company_id,"
				+ " d.building, "
				+ " d.address,"
				+ " d.status, \r\n" + 
				"	K.task_nama \r\n" + 
				"FROM\r\n" + 
				"	tm_planner_alone as d " + 
				"	LEFT JOIN task AS K ON K.task_id = d.task_id \r\n" + 
				"	AND K.company_id = d.company_id \r\n" + 
				"WHERE\r\n" + 
				"	d.contact_id = :user_uuid \r\n" + 
				"	AND d.company_id = :int1"
				+ statusCoba)
				.setParameter("int1", int1)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
	}

	@Override
	public List<Object[]> detailPlannerAlone(int plannerAloneId, int companyId, UUID user_uuid) {
		// TODO Auto-generated method stub
		
		return em.createNativeQuery("SELECT\r\n" + 
				"	d.planner_alone_id,"
				+ "  d.party_id,"
				+ " d.task_id,"
				+ " d.date_planner_internal,"
				+ " d.start_hour,"
				+ " d.end_hour,"
				+ " d.location,"
				+ " d.planned_hour,"
				+ " d.overtime_hour,"
				+ " cast (d.contact_id as varchar),"
				+ " d.company_id,"
				+ " d.building,"
				+ "   \r\n" + 
				"	e.planner_alone_note_id,\r\n" + 
				"	e.note,\r\n" + 
				"	e.date_note,"
				+ " d.address,"
				+ " d.status  \r\n" + 
				"FROM\r\n" + 
				"	tm_planner_alone as d" + 
				"	LEFT JOIN tm_planner_alone_note AS e " + 
				"		ON d.planner_alone_id = e.planner_alone_id \r\n" + 
				"					and d.company_id = e.company_id\r\n" + 
				"WHERE\r\n" + 
				"	d.planner_alone_id = :plannerAloneId\r\n" + 
				"	AND d.company_id = :companyId\r\n" + 
				"	and d.contact_id = :user_uuid")
				.setParameter("plannerAloneId", plannerAloneId)
				.setParameter("companyId", companyId)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
	}

	@Override
	public List<Object[]> detailTimeframe(int company_id, int mProjectId, int projectDetailId, int timeframeId,
			Date date) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select a.*, d.project_id, b.m_task_id  from tm_project_timeframe_budgethour as a\r\n" + 
				"left join project_detail as b on a.tm_project_id = b.m_project_id and a.company_id = b.company_id and a.project_detail_id = b.project_detail_id\r\n" + 
				"LEFT JOIN dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', 'SELECT project_name, project_status, project_step_productivitapp, status_timeframe, project_id, company_id, project_party from project ' ) AS d ( project_name VARCHAR, project_status INTEGER, project_step_productivitapp INTEGER, status_timeframe INTEGER, project_id INTEGER, company_id INTEGER, project_party integer ) ON d.project_id = a.tm_project_id \r\n" + 
				"					AND d.company_id = a.company_id\r\n" + 
				"where\r\n" + 
				"a.project_detail_id = :projectDetailId\r\n" + 
				"and a.tm_project_id = :mProjectId\r\n" + 
				"and a.timeframe_id = :timeframeId\r\n" + 
				"and a.company_id = :company_id\r\n" + 
				"and a.date_budgethour = :date_budgethour_timeframe_re")
				.setParameter("projectDetailId", projectDetailId)
				.setParameter("mProjectId", mProjectId)
				.setParameter("timeframeId", timeframeId)
				.setParameter("company_id", company_id)
				.setParameter("date_budgethour_timeframe_re",date )
				.getResultList();
	}

	@Override
	public List<Object[]> listTaskProject(int projectId, int company_id, String paramAktif,UUID user_uuid) {
		// TODO Auto-generated method stub
		String cari = "";
		if(paramAktif.toLowerCase() == "y")
		{
			cari = " and n.task_status = 1";
		}
		else if(paramAktif.toLowerCase() == "n")
		{
			cari = " and n.task_status = 0";
		}
		else
		{
			cari = "";
		}
		return em.createNativeQuery("SELECT\r\n" + 
				"	n.task_id, n.task_nama, n.company_id, n.task_status, z.project_detail_id, z.m_project_id,\r\n" + 
				"	q.timeframe_id\r\n" + 
				"FROM\r\n" + 
				"	project_detail AS z\r\n" + 
				"	LEFT JOIN GROUPING AS o ON o.grouping_id = z.m_grouping_id \r\n" + 
				"	AND o.company_id = z.company_id\r\n" + 
				"	LEFT JOIN task AS n ON z.m_task_id = n.task_id \r\n" + 
				"	AND z.company_id = n.company_id "
				+ " LEFT JOIN project_detail_karyawan AS C ON C.project_id = z.m_project_id \r\n" + 
				"	AND c.company_id = z.company_id \r\n" + 
				"	AND c.project_detail_id = z.project_detail_id "
				+ " 	LEFT JOIN tm_project_timeframe AS q ON z.company_id = q.company_id \r\n" + 
				"	AND z.m_project_id = q.tm_project_id \r\n" + 
				"	AND q.project_detail_id = z.project_detail_id"
				+ " 	 \r\n" + 
				"WHERE\r\n" + 
				"	NOT EXISTS ( SELECT 1 FROM project_detail AS b1 WHERE z.project_detail_id = b1.parent_id AND z.m_project_id = b1.m_project_id ) \r\n" + 
				"	AND z.m_project_id = :projectId \r\n" + 
				"	AND z.company_id = :company_id "
				+ " and c.contact_id = :user_uuid \r\n" + 
				"	" + cari)
				.setParameter("projectId", projectId)
				.setParameter("company_id", company_id)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
	}

	@Override
	public Optional<Planner> UpdatePlanner(int plannerId, int projectId, int projectDetailId, int timeframeId, int company_id) {
		// TODO Auto-generated method stub
		return PlannerRepository.findById(new PlannerSerializable(plannerId, projectId, projectDetailId, timeframeId, company_id));
	}

	@Override
	public List<Object[]> listProjectCombobox(UUID user_uuid, int int1, int party) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT \r\n" + 
				"a.projectId, a.project_name, a.project_party, cast(c.contact_id as varchar) \r\n" + 
				"FROM\r\n" + 
				"	dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', 'SELECT project_id,company_id, project_name, project_party,project_status, project_step_productivitapp, status_timeframe FROM project  ' ) AS a ( projectId INTEGER, company_id INTEGER, project_name VARCHAR, project_party INTEGER,project_status INTEGER, project_step_productivitapp INTEGER, status_timeframe INTEGER )\r\n" + 
				"	LEFT JOIN project_detail AS b   "
				+ "ON b.company_id = a.company_id\r\n" + 
				"	AND a.projectId = b.m_project_id\r\n" + 
				"	LEFT JOIN project_detail_karyawan AS c  " 
				+ "ON c.company_id = b.company_id AND c.project_id = b.m_project_id and c.project_detail_id = b.project_detail_id" + 
				"	where\r\n" + 
				"	c.contact_id = :user_uuid \r\n" + 
				"	and a.company_id = :companyn \r\n" + 
				"	and a.project_party = :party "
				+ " AND a.project_status = 1 \r\n" + 
				"					AND a.project_step_productivitapp = 2 \r\n" + 
				"					AND a.status_timeframe = 3  \r\n" + 
				"	GROUP BY \r\n" + 
				"	a.projectId, a.project_name, c.contact_id,a.project_party")
				.setParameter("user_uuid", user_uuid)
				.setParameter("companyn", int1)
				.setParameter("party", party)
				.getResultList();
	}
	
	@Override
	public List<Object[]> listParty(UUID user_uuid, int int1) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT \r\n" + 
				"a.project_party, cast (c.contact_id as varchar) \r\n" + 
				"FROM\r\n" + 
				"	dblink ( 'host=localhost user=postgres password=proacc202010 dbname=projectManagement', 'SELECT project_id,company_id, project_name, project_party FROM project  ' ) AS a ( projectId INTEGER, company_id INTEGER, project_name VARCHAR, project_party INTEGER )\r\n" + 
				"	LEFT JOIN project_detail AS b ON b.company_id = a.company_id\r\n" + 
				"	AND a.projectId = b.m_project_id\r\n" + 
				"	LEFT JOIN project_detail_karyawan AS c  "
				+ "ON c.company_id = b.company_id AND c.project_id = b.m_project_id and c.project_detail_id = b.project_detail_id\r\n" + 
				"	AND a.projectId = b.m_project_id\r\n" + 
				"	where\r\n" + 
				"	c.contact_id = :user_uuid\r\n" + 
				"	and a.company_id = :companyn \r\n" + 
				"	GROUP BY \r\n" + 
				"	a.project_party, c.contact_id")
				.setParameter("user_uuid", user_uuid)
				.setParameter("companyn", int1)
				.getResultList();
	}

	@Override
	public List<Object[]> listTimeframeFromPlanner(int projectId, int projectDetailId,int timeframe, int company) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"	planned_hour,\r\n" + 
				"	overtime_hour,\r\n" + 
				"	request_budget_hour\r\n" + 
				"FROM\r\n" + 
				"	\"tm_planner\" \r\n" + 
				"WHERE\r\n" + 
				"	project_detail_id = :projectDetailId\r\n" + 
				"	and project_id = :projectId\r\n" + 
				"	and timeframe_id = :timeframe\r\n" + 
				"	and company_id = :company ")
				.setParameter("projectDetailId", projectDetailId)
				.setParameter("projectId", projectId)
				.setParameter("company", company)
				.setParameter("timeframe", timeframe)
				.getResultList();
	}

	@Override
	public List<Object[]> listTimeframeFromPlannerId(int projectId, int projectDetailId, int timeframe, int company, int plannerId) {
		// TODO Auto-generated method stub
		return em.createNativeQuery("SELECT\r\n" + 
				"	planned_hour,\r\n" + 
				"	overtime_hour,\r\n" + 
				"	request_budget_hour\r\n" + 
				"FROM\r\n" + 
				"	\"tm_planner\" \r\n" + 
				"WHERE\r\n" + 
				"	project_detail_id = :projectDetailId\r\n" + 
				"	and project_id = :projectId\r\n" + 
				"	and timeframe_id = :timeframe\r\n" + 
				"	and company_id = :company\r\n" + 
				"	and planner_id != :plannerId")
				.setParameter("plannerId", plannerId)
				.setParameter("company", company)
				.setParameter("timeframe", timeframe)
				.setParameter("projectDetailId", projectDetailId)
				.setParameter("projectId", projectId)
				.getResultList();
	}


}
