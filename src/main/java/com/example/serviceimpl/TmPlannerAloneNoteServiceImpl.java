package com.example.serviceimpl;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.PlannerNote;
import com.example.entity.TmPlannerAloneNote;
import com.example.repository.TmPlannerAloneNoteRepository;
import com.example.service.TmPlannerAloneNoteService;
import com.example.service.PlannerService;

@Service
public class TmPlannerAloneNoteServiceImpl implements TmPlannerAloneNoteService {
	private static final Logger logger = LoggerFactory.getLogger(PlannerService.class);
	

	@Autowired
	private TmPlannerAloneNoteRepository TmPlannerAloneNoteRepository;
	
	@Autowired
	EntityManager em;

	@Override
	@Transactional
	public void delete(ArrayList<TmPlannerAloneNote> isiPlannerNote) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from tm_planner_alone_note where "
				+ "planner_alone_id = :planner_alone_id "
				+ "and planner_alone_note_id = :planner_alone_note_id "
				+ "and company_id = :company_id"
				+ "  ")
		.setParameter("planner_alone_id", isiPlannerNote.get(0).getPlannerAloneId())
		.setParameter("planner_alone_note_id", isiPlannerNote.get(0).getPlannerAloneNoteId())
		.setParameter("company_id", isiPlannerNote.get(0).getCompanyId())
		.executeUpdate();
	}

	@Override
	public void save(ArrayList<TmPlannerAloneNote> isiPlannerNote) {
		// TODO Auto-generated method stub
		TmPlannerAloneNoteRepository.saveAll(isiPlannerNote);
	}

}
