package com.example.serviceimpl;

import org.springframework.stereotype.Service;

//import com.example.controllers.MTaskListController;
import com.example.entity.TaskList;
//import com.example.entity.TmConfig;
//import com.example.entity.TmTmpNumberTrx;
import com.example.repository.MTaskListRepository;
import com.example.serializable.MTaskListSerializable;
import com.example.service.MTaskListService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames={"taskCaching"})
public class MTaskListServiceImpl implements MTaskListService {
	private static final Logger logger = LoggerFactory.getLogger(MTaskListServiceImpl.class);
	@Autowired
	private MTaskListRepository MTaskListRepository;
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	EntityManager em;

	@Override
//	@Cacheable("findAll")
	public List<TaskList> findByStatusAndCompanyId(int status, int companyId, UUID user_uuid) {
		// TODO Auto-generated method stub
		return MTaskListRepository.findByStatusAndCompanyIdAndCreatedBy(status,companyId, user_uuid);
	}
//	@Autowired

	@Override
//	@Cacheable("findAllCompanyId")
	public List<TaskList> findByCompanyId(int companyId,UUID user_uuid) {
		// TODO Auto-generated method stub
		return MTaskListRepository.findByCompanyIdAndCreatedBy(companyId, user_uuid);
	}

	@Override
//	@Cacheable("detail")
	public Optional<TaskList> FindByTaskListIdAndCompanyId(Integer tasklistId, int companyId) {
		// TODO Auto-generated method stub
		return MTaskListRepository.findById(new MTaskListSerializable(tasklistId, companyId));
	}

	@Override
	@Transactional
//	@Caching(
//			evict = 
//				{
//						@CacheEvict(value = "findAll", allEntries = true),
//						@CacheEvict(value = "findAllCompanyId", allEntries = true),
//						@CacheEvict(value = "detail", allEntries = true)
//				}
//			)
	public void save(List<TaskList> paramList) {
		// TODO Auto-generated method stub
		 for (TaskList tasklistLooping : paramList) {
			  LocalDateTime localDateTime = LocalDateTime.now();
		      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
		      String formatDateTime = localDateTime.format(formatter);
		      LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
		      
		      if(tasklistLooping.getTasklistId() == 00)
			  {  
		    	  List<Object[]> dataNext = generateId();
			    	int id = 0;
			    	if(dataNext.size()>0)
			    	{
			    		id = Integer.parseInt(String.valueOf(dataNext.get(0)));
			    	}
		    	  em.createNativeQuery("insert into "
					 		+ "task(task_id, task_nama, created_by, created_at, company_id,task_status,task_description) "
					 		+ "values (?, ?, ?,?,?,?,?)")
					 .setParameter(1,id)
					 .setParameter(2, tasklistLooping.getTasklistNama())
					 .setParameter(3, tasklistLooping.getCreatedBy())
					 .setParameter(4, tasklistLooping.getCreatedDate())
					 .setParameter(5, tasklistLooping.getCompanyId())
					 .setParameter(6, tasklistLooping.getStatus())
					 .setParameter(7, tasklistLooping.getDescription())
					 .executeUpdate();
			  }
		      else
		      {
		    	  tasklistLooping.setUpdatedBy(tasklistLooping.getCreatedBy());
		    	  tasklistLooping.setUpdatedDate(localDateTime2);
		    	  MTaskListRepository.save(tasklistLooping);
		      }
			  
		      
//			 if(tasklistLooping.getTasklistId() == "" || tasklistLooping.getTasklistId() == null || tasklistLooping.getTasklistId().isEmpty())
//			 {
//				 logger.info("tasklistLooping.getTasklistId() = " + tasklistLooping.getTasklistId());
//				 em.createNativeQuery("insert into "
//				 		+ "m_task_list(tasklist_id, tasklist_nama, created_by, created_date, company_id,status,description) "
//				 		+ "values (?, ?, ?,?,?,?,?)")
//				 .setParameter(1, tasklistLooping.getTasklistId())
//				 .setParameter(2, tasklistLooping.getTasklistNama())
//				 .setParameter(3, tasklistLooping.getCreatedBy())
//				 .setParameter(4, tasklistLooping.getCreatedDate())
//				 .setParameter(5, tasklistLooping.getCompanyId())
//				 .setParameter(6, tasklistLooping.getStatus())
//				 .setParameter(7, tasklistLooping.getDescription())
//				 .executeUpdate();
//				 
//				  
//			 }
	            
	        }
		
//		MTaskListRepository.save(paramList);
	}

	@Override
	@Transactional
//	@Caching(
//			evict = 
//				{
//						@CacheEvict(value = "findAll", allEntries = true),
//						@CacheEvict(value = "findAllCompanyId", allEntries = true),
//						@CacheEvict(value = "detail", allEntries = true)
//				}
//			)
	public void updateSave(TaskList param) {
		// TODO Auto-generated method stub
		MTaskListRepository.save(param);
	}
	
	
	public List<Object[]> generateId()
	{
		return em.createNativeQuery("select nextval('task_sequence')").getResultList();
	}

	
	
}
